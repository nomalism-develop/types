import * as shared from './shared/interface';

import * as viewErrors from './shared/errors/viewErrors';
import * as stockErrors from './shared/errors/stockErrors';
import * as userErrors from './shared/errors/userErrors';
import * as documentManagementErrors from './shared/errors/documentManagementErrors';
import * as integrationErrors from './shared/errors/integrationErrors';

import * as BankData from './modules/user/bankData/interface';
import * as BankDataRoutes from './modules/user/bankData/route.schema';

import * as Client from './modules/user/clients/interface';
import * as ClientRoutes from './modules/user/clients/route.schema';

import * as ClientType from './modules/user/clientType/interface';
import * as ClientTypeRoutes from './modules/user/clientType/route.schema';

import * as ZipCode from './modules/user/zipCode/interfaces';
import * as ZipCodeRoutes from './modules/user/zipCode/route.schema';

import * as Chat from './modules/stock/chat/interfaces';
import * as ChatRoutes from './modules/stock/chat/route.schema';

import * as Commissioner from './modules/user/commissioner/interface';
import * as CommissionerRoutes from './modules/user/commissioner/route.schema';

import * as Counter from './modules/supply/counter/interfaces';
import * as CounterRoutes from './modules/supply/counter/route.schema';

import * as Country from './modules/user/country/interface';
import * as CountryRoutes from './modules/user/country/route.schema';

import * as DeliveryMethods from './modules/user/deliveryMethods/interfaces';
import * as DeliveryMethodsRoutes from './modules/user/deliveryMethods/route.schema';

import * as DocumentHeader from './modules/supply/documentHeader/interfaces';
import * as DocumentHeaderRoutes from './modules/supply/documentHeader/route.schema';

import * as BillOfLading from './modules/document/billOfLading/interfaces';
import * as BillOfLadingRoutes from './modules/document/billOfLading/route.schema';

import * as ProductionOrder from './modules/document/productionOrder/interfaces';
import * as ProductionOrderRoutes from './modules/document/productionOrder/route.schema';

import * as Proforma from './modules/document/proforma/interfaces';
import * as ProformaRoutes from './modules/document/proforma/route.schema';

import * as PropostaFornecedor from './modules/document/propostaFornecedor/interfaces';
import * as PropostaFornecedorRoutes from './modules/document/propostaFornecedor/route.schema';

import * as ProviderCreditNoteFromReturn from './modules/document/providerCreditNoteFromReturn/interfaces';
import * as ProviderCreditNoteFromReturnRoutes from './modules/document/providerCreditNoteFromReturn/route.schema';

import * as ProviderFinancialCreditNote from './modules/document/providerFinancialCreditNote/interfaces';
import * as ProviderFinancialCreditNoteRoutes from './modules/document/providerFinancialCreditNote/route.schema';

import * as ProviderServiceInvoice from './modules/document/providerServiceInvoice/interfaces';
import * as ProviderServiceInvoiceRoutes from './modules/document/providerServiceInvoice/route.schema';

import * as DocumentLine from './modules/supply/documentLine/interfaces';
import * as DocumentLineRoutes from './modules/supply/documentLine/route.schema';

import * as DocumentLineAssoc from './modules/supply/documentLineAssoc/interfaces';
import * as DocumentLineAssocRoutes from './modules/supply/documentLineAssoc/route.schema';

import * as DocumentSet from './modules/supply/documentSet/interface';
import * as DocumentSetRoutes from './modules/supply/documentSet/route.schema';

import * as DocumentType from './modules/supply/documentType/interfaces';
import * as DocumentTypeRoutes from './modules/supply/documentType/route.schema';

import * as ExternalDocumentType from './modules/supply/externalDocumentType/interface';
// import * as ExternalDocumentTypeRoutes from './modules/stock/externalDocumentType/route.schema';

import * as ExternalDocumentHeader from './modules/supply/externalDocumentHeader/interface';
import * as ExternalDocumentHeaderRoutes from './modules/supply/externalDocumentHeader/route.schema';

import * as Favorites from './modules/user/favorites/interfaces';
import * as FavoritesRoutes from './modules/user/favorites/route.schema';

import * as File from './modules/stock/file/interface';
import * as FileRoutes from './modules/stock/file/route.schema';

import * as GroupPermissions from './modules/user/groupPermissions/interface';
import * as GroupPermissionsRoutes from './modules/user/groupPermissions/route.schema';

import * as GoogleSheets from './modules/integration/googleSheets/interfaces';
import * as GoogleSheetsRoutes from './modules/integration/googleSheets/route.schema';

import * as Language from './modules/user/language/interface';
import * as LanguageRoutes from './modules/user/language/route.schema';

import * as Location from './modules/stock/location/interface';
import * as LocationRoutes from './modules/stock/location/route.schema';

import * as MaturityDates from './modules/user/maturityDates/interfaces';
import * as MaturityDatesRoutes from './modules/user/maturityDates/route.schema';
// import * as MaturityDatesResponses from './modules/user/maturityDates/tests';

import * as Multimedia from './modules/documentManagement/multimedia/interfaces';
import * as MultimediaRoutes from './modules/documentManagement/multimedia/route.schema';

import * as OrderManagement from './modules/supply/orderManagement/interface';
// import * as OrderManagementRoutes from './modules/stock/orderManagement/route.schema';

import * as Observation from './modules/integration/observation/interfaces';
import * as ObservationRoutes from './modules/integration/observation/route.schema';

import * as ObservationType from './modules/integration/observationType/interfaces';
import * as ObservationTypeRoutes from './modules/integration/observationType/route.schema';

import * as Password from './modules/user/password/interfaces';
import * as PasswordRoutes from './modules/user/password/route.schema';

import * as Payment from './modules/supply/payment/interface';
import * as PaymentRoutes from './modules/supply/payment/route.schema';

import * as PaymentMethods from './modules/user/paymentMethods/interfaces';
import * as PaymentMethodsRoutes from './modules/user/paymentMethods/route.schema';
// import * as PaymentMethodsResponses from './modules/user/paymentMethods/tests';

import * as Permissions from './modules/user/permissions/interface';
import * as PermissionsRoutes from './modules/user/permissions/route.schema';

import * as ProductImage from './modules/stock/productImage/interface';
import * as ProductImageRoutes from './modules/stock/productImage/route.schema';

import * as Promotion from './modules/stock/promotion/interface';
import * as PromotionRoutes from './modules/stock/promotion/route.schema';

import * as PromotionAssoc from './modules/stock/promotionAssoc/interface';
import * as PromotionAssocRoutes from './modules/stock/promotionAssoc/route.schema';

import * as Providers from './modules/user/providers/interface';
import * as ProvidersRoutes from './modules/user/providers/route.schema';

import * as ProviderType from './modules/user/providerType/interface';
import * as ProviderTypeRoutes from './modules/user/providerType/route.schema';

import * as PurchaseConditions from './modules/user/purchaseConditions/interface';
import * as PurchaseConditionsRoutes from './modules/user/purchaseConditions/route.schema';

import * as ReasonForExemption from './modules/user/reasonForExemption/interface';
import * as ReasonForExemptionRoutes from './modules/user/reasonForExemption/route.schema';

import * as RefreshToken from './modules/user/refreshToken/interface';
import * as RefreshTokenRoutes from './modules/user/refreshToken/route.schema';

import * as SegmentsArea from './modules/user/segmentsArea/interface';
import * as SegmentsAreaRoutes from './modules/user/segmentsArea/route.schema';

import * as Sessions from './modules/user/sessions/interface';
import * as SessionsRoutes from './modules/user/sessions/route.schema';

import * as Shippings from './modules/user/shippings/interface';
import * as ShippingsRoutes from './modules/user/shippings/route.schema';

import * as StoreOperator from './modules/user/storeOperator/interface';
import * as StoreOperatorRoutes from './modules/user/storeOperator/route.schema';

import * as Swift from './modules/user/swift/interface';
import * as SwiftRoutes from './modules/user/swift/route.schema';

import * as TypeOfLocation from './modules/stock/typeOfLocation/interface';
import * as TypeOfLocationRoutes from './modules/stock/typeOfLocation/route.schema';

import * as UnitOfMeasure from './modules/stock/unitOfMeasure/interface';
import * as UnitOfMeasureRoutes from './modules/stock/unitOfMeasure/route.schema';

import * as UserPermissions from './modules/user/userPermissions/interface';
import * as UserPermissionsRoutes from './modules/user/userPermissions/route.schema';

import * as UserPositions from './modules/user/userPositions/interface';
import * as UserPositionsRoutes from './modules/user/userPositions/route.schema';

import * as Users from './modules/user/users/interface';
import * as UsersRoutes from './modules/user/users/route.schema';

import * as UserTokens from './modules/user/userTokens/interface';
import * as UserTokensRoutes from './modules/user/userTokens/route.schema';

import * as VatTax from './modules/stock/vatTax/interface';
import * as VatTaxRoutes from './modules/stock/vatTax/route.schema';

import * as VatValidation from './modules/stock/vatValidation/interfaces';
import * as VatValidationRoutes from './modules/stock/vatValidation/route.schema';

import * as VatTaxZone from './modules/stock/vatTaxZone/interface';
import * as VatTaxZoneRoutes from './modules/stock/vatTaxZone/route.schema';

import * as Vehicles from './modules/user/vehicles/interfaces';
import * as VehiclesRoutes from './modules/user/vehicles/route.schema';
// import * as VehiclesResponses from './modules/vehicles/tests';

import * as Workflow from './modules/stock/workflow/interfaces';
import * as WorkflowRoutes from './modules/stock/workflow/route.schema';

import * as ClientCurrentAccount from './modules/supply/clientCurrentAccount/interfaces';
import * as ClientCurrentAccountRoutes from './modules/supply/clientCurrentAccount/route.schema';
// import * as ClientCurrentAccount from "./modules/stock/clientCurrentAccount/oas.schema";
// import * as ClientCurrentAccount from './modules/stock/clientCurrentAccount/tests';

import * as ProviderCurrentAccount from './modules/supply/providerCurrentAccount/interfaces';
import * as ProviderCurrentAccountRoutes from './modules/supply/providerCurrentAccount/route.schema';
// import * as ProviderCurrentAccount from "./modules/stock/providerCurrentAccount/oas.schema";
// import * as ProviderCurrentAccount from './modules/stock/providerCurrentAccount/tests';

import * as StockMovement from './modules/stock/stockMovement/interface';
import * as StockMovementRoutes from './modules/stock/stockMovement/route.schema';

import * as Tenant from './modules/user/tenant/interface';
import * as TenantRoutes from './modules/user/tenant/route.schema';

import * as PreSale from './modules/supply/preSale/interface';
import * as PreSaleRoutes from './modules/supply/preSale/route.schema';

import * as PreSaleProduct from './modules/supply/preSaleProduct/interface';
import * as PreSaleProductRoutes from './modules/supply/preSaleProduct/route.schema';

import * as Npc from './modules/print/npc/interfaces';
import * as NpcRoutes from './modules/print/npc/route.schema';

import * as Printer from './modules/print/printer/interfaces';
import * as PrinterRoutes from './modules/print/printer/route.schema';

import * as SchedulePrintJob from './modules/print/schedulePrintJob/interfaces';
import * as SchedulePrintJobRoutes from './modules/print/schedulePrintJob/route.schema';

import * as QueryList from './modules/stock/queryList/interfaces';
import * as QueryListRoutes from './modules/stock/queryList/route.schema';

import * as QueryParameter from './modules/stock/queryParameter/interfaces';
import * as QueryParameterRoutes from './modules/stock/queryParameter/route.schema';

import * as ReturnReason from './modules/stock/returnReason/interfaces';
import * as ReturnReasonRoutes from './modules/stock/returnReason/route.schema';

import * as PropostaSheets from './modules/document/propostaSheets/interface';
import * as PropostaSheetsRoutes from './modules/document/propostaSheets/route.schema';

import * as Schedule from './modules/supply/schedule/interface';
import * as ScheduleRoutes from './modules/supply/schedule/route.schema';

import * as GoogleFilePermission from './modules/integration/googleFilePermission/interfaces';
import * as GoogleFilePermissionRoutes from './modules/integration/googleFilePermission/route.schema';

import * as Settings from './modules/integration/settings/interfaces';
import * as SettingsRoutes from './modules/integration/settings/route.schema';

import * as Tickets from './modules/tickets/tickets/interfaces';
import * as TicketsRoutes from './modules/tickets/tickets/route.schema';

import * as Channel from './modules/tickets/channel/interfaces';
import * as ChannelRoutes from './modules/tickets/channel/route.schema';

import * as TicketsLanguage from './modules/tickets/language/interfaces';
import * as TicketsLanguageRoutes from './modules/tickets/language/route.schema';

import * as CLT from './modules/tickets/clt/interfaces';
import * as CLTRoutes from './modules/tickets/clt/route.schema';

import * as StartDocumentHeaderLastUpdate from './modules/supply/startDocumentHeaderLastUpdate/interface';
// import * as StartDocumentHeaderLastUpdateRoutes from './modules/stock/startDocumentHeaderLastUpdate/route.schema';

import * as Persona from './modules/user/persona/interface';
import * as PersonaRoutes from './modules/user/persona/route.schema';

import * as ProjectInfo from './modules/integration/projectInfo/interfaces';
import * as ProjectInfoRoutes from './modules/integration/projectInfo/route.schema';

import * as EmailVerification from './modules/integration/emailVerification/interfaces';
import * as EmailVerificationRoutes from './modules/integration/emailVerification/route.schema';

import * as EmailLog from './modules/integration/emailLog/interfaces';
import * as EmailLogRoutes from './modules/integration/emailLog/route.schema';

import * as Purchase from './modules/document/purchase/interfaces';
import * as PurchaseRoutes from './modules/document/purchase/route.schema';

import * as Order from './modules/document/order/interfaces';
import * as OrderRoutes from './modules/document/order/route.schema';

import * as MaterialEntrance from './modules/document/materialEntrance/interfaces';
import * as MaterialEntranceRoutes from './modules/document/materialEntrance/route.schema';

import * as Transformado from './modules/document/transformado/interfaces';
import * as TransformadoRoutes from './modules/document/transformado/route.schema';

import * as Quebra from './modules/document/quebra/interfaces';
import * as QuebraRoutes from './modules/document/quebra/route.schema';

import * as UpfrontReturn from './modules/document/upfrontReturn/interfaces';
import * as UpfrontReturnRoutes from './modules/document/upfrontReturn/route.schema';

import * as SavedEmPicking from './modules/stock/savedEmPicking/interfaces';
import * as SavedEmPickingRoutes from './modules/stock/savedEmPicking/route.schema';

import * as EmailTemplate from './modules/integration/emailTemplate/interfaces';
import * as EmailTemplateRoutes from './modules/integration/emailTemplate/route.schema';

import * as EmailTemplateAttachment from './modules/integration/emailTemplateAttachment/interfaces';
import * as EmailTemplateAttachmentRoutes from './modules/integration/emailTemplateAttachment/route.schema';

import * as Inventario from './modules/document/inventario/interface';
import * as InventarioRoutes from './modules/document/inventario/route.schema';

import * as ReturnToProvider from './modules/document/returnToProvider/interface';
import * as ReturnToProviderRoutes from './modules/document/returnToProvider/route.schema';

import * as Prison from './modules/stock/prison/interface';
import * as PrisonRoutes from './modules/stock/prison/route.schema';

import * as DocumentLineNote from './modules/supply/documentLineNote/interfaces';
import * as DocumentLineNoteRoutes from './modules/supply/documentLineNote/route.schema';

import * as SavedProviderProposal from './modules/stock/savedProviderProposal/interfaces';
import * as SavedProviderProposalRoutes from './modules/stock/savedProviderProposal/route.schema';

import * as ProductGoogleSheets from './modules/stock/productGoogleSheets/interface';
import * as ProductGoogleSheetsRoutes from './modules/stock/productGoogleSheets/route.schema';

import * as Task from './modules/stock/task/interface';
import * as TaskRoutes from './modules/stock/task/route.schema';

import * as TaskMessage from './modules/stock/taskMessage/interface';
import * as TaskMessageRoutes from './modules/stock/taskMessage/route.schema';

import * as RecurrentTasks from './modules/stock/recurrentTasks/interface';

import * as TaskRead from './modules/stock/taskRead/interface';
import * as TaskReadRoutes from './modules/stock/taskRead/route.schema';

import * as Theme from './modules/user/theme/interfaces';
import * as ThemeRoutes from './modules/user/theme/route.schema';

import * as Dashboard from './modules/stock/dashboard/interface';
import * as DashboardRoutes from './modules/stock/dashboard/route.schema';

import * as ChatRapidMessage from './modules/stock/chatRapidMessage/interface';
import * as ChatRapidMessageRoutes from './modules/stock/chatRapidMessage/route.schema';

import * as SideMenu from './modules/stock/sideMenu/interface';
import * as SideMenuRoutes from './modules/stock/sideMenu/route.schema';

import * as AdminPanel from './modules/view/adminPanel/interfaces';

import * as ErrorLog from './modules/view/errorLog/interfaces';
import * as ErrorLogRoutes from './modules/view/errorLog/route.schema';

export {
  shared,
  // Errors,
  viewErrors,
  stockErrors,
  userErrors,
  documentManagementErrors,
  integrationErrors,
  // ZipCode
  ZipCode,
  ZipCodeRoutes,
  // BankData,
  BankData,
  BankDataRoutes,
  // Client,
  Client,
  ClientRoutes,
  // ClientType,
  ClientType,
  ClientTypeRoutes,
  // ClientCurrentAccount,
  ClientCurrentAccount,
  ClientCurrentAccountRoutes,
  // Chat,
  Chat,
  ChatRoutes,
  // Commissioner,
  Commissioner,
  CommissionerRoutes,
  // Counter,
  Counter,
  CounterRoutes,
  // Country,
  Country,
  CountryRoutes,
  //  DeliveryMethods,
  DeliveryMethods,
  DeliveryMethodsRoutes,
  // DeliveryMethodsResponses,
  // DocumentHeader,
  DocumentHeader,
  DocumentHeaderRoutes,
  // BillOfLading,
  BillOfLading,
  BillOfLadingRoutes,
  // ProductionOrder,
  ProductionOrder,
  ProductionOrderRoutes,
  // Proforma,
  Proforma,
  ProformaRoutes,
  // PropostaFornecedor,
  PropostaFornecedor,
  PropostaFornecedorRoutes,
  // ProviderCreditNoteFromReturn,
  ProviderCreditNoteFromReturn,
  ProviderCreditNoteFromReturnRoutes,
  // ProviderFinancialCreditNote,
  ProviderFinancialCreditNote,
  ProviderFinancialCreditNoteRoutes,
  // ProviderServiceInvoice,
  ProviderServiceInvoice,
  ProviderServiceInvoiceRoutes,
  // DocumentLine,
  DocumentLine,
  DocumentLineRoutes,
  // DocumentLineAssoc,
  DocumentLineAssoc,
  DocumentLineAssocRoutes,
  // DocumentSet,
  DocumentSet,
  DocumentSetRoutes,
  // DocumentType,
  DocumentType,
  DocumentTypeRoutes,
  // ExternalDocumentType,
  ExternalDocumentType,
  // ExternalDocumentTypeRoutes,
  // ExternalDocumentHeader,
  ExternalDocumentHeader,
  ExternalDocumentHeaderRoutes,
  // Favorites,
  Favorites,
  FavoritesRoutes,
  // File,
  File,
  FileRoutes,
  // GroupPermissions,
  GroupPermissions,
  GroupPermissionsRoutes,
  // GoogleSheets
  GoogleSheets,
  GoogleSheetsRoutes,
  // Language,
  Language,
  LanguageRoutes,
  // Location,
  Location,
  LocationRoutes,
  // MaturityDates,
  MaturityDates,
  MaturityDatesRoutes,
  // MaturityDatesResponses,
  // Multimedia,
  Multimedia,
  MultimediaRoutes,
  // OrderManagement,
  OrderManagement,
  // OrderManagementRoutes,
  // Observation,
  Observation,
  ObservationRoutes,
  // ObservationType,
  ObservationType,
  ObservationTypeRoutes,
  // Password,
  Password,
  PasswordRoutes,
  // Payment,
  Payment,
  PaymentRoutes,
  // PaymentMethods,
  PaymentMethods,
  PaymentMethodsRoutes,
  // PaymentMethodsResponses,
  // Permissions,
  Permissions,
  PermissionsRoutes,
  // ProductImage,
  ProductImage,
  ProductImageRoutes,
  // Promotion,
  Promotion,
  PromotionRoutes,
  // PromotionAssoc,
  PromotionAssoc,
  PromotionAssocRoutes,
  // Providers,
  Providers,
  ProvidersRoutes,
  // ProviderType,
  ProviderType,
  ProviderTypeRoutes,
  ProviderCurrentAccount,
  ProviderCurrentAccountRoutes,
  // PurchaseConditions,
  PurchaseConditions,
  PurchaseConditionsRoutes,
  // ReasonForExemption,
  ReasonForExemption,
  ReasonForExemptionRoutes,
  // RefreshToken,
  RefreshToken,
  RefreshTokenRoutes,
  // SegmentsArea,
  SegmentsArea,
  SegmentsAreaRoutes,
  // Sessions,
  Sessions,
  SessionsRoutes,
  // Shippings,
  Shippings,
  ShippingsRoutes,
  // StoreOperator,
  StoreOperator,
  StoreOperatorRoutes,
  // Swift,
  Swift,
  SwiftRoutes,
  // TypeOfLocation,
  TypeOfLocation,
  TypeOfLocationRoutes,
  // UnitOfMeasure,
  UnitOfMeasure,
  UnitOfMeasureRoutes,
  // UserPermissions,
  UserPermissions,
  UserPermissionsRoutes,
  // UserPositions,
  UserPositions,
  UserPositionsRoutes,
  // Users,
  Users,
  UsersRoutes,
  // UserTokens,
  UserTokens,
  UserTokensRoutes,
  // VatTax,
  VatValidation,
  VatValidationRoutes,
  VatTax,
  VatTaxRoutes,
  // VatTaxZone,
  VatTaxZone,
  VatTaxZoneRoutes,
  // Vehicles,
  Vehicles,
  VehiclesRoutes,
  // VehiclesResponses,
  Workflow,
  WorkflowRoutes,
  StockMovement,
  StockMovementRoutes,
  Tenant,
  TenantRoutes,
  PreSale,
  PreSaleRoutes,
  PreSaleProduct,
  PreSaleProductRoutes,
  Npc,
  NpcRoutes,
  Printer,
  PrinterRoutes,
  SchedulePrintJob,
  SchedulePrintJobRoutes,
  QueryList,
  QueryListRoutes,
  QueryParameter,
  QueryParameterRoutes,
  ReturnReason,
  ReturnReasonRoutes,
  PropostaSheets,
  PropostaSheetsRoutes,
  Schedule,
  ScheduleRoutes,
  GoogleFilePermission,
  GoogleFilePermissionRoutes,
  Settings,
  SettingsRoutes,
  Tickets,
  TicketsRoutes,
  Channel,
  ChannelRoutes,
  TicketsLanguage,
  TicketsLanguageRoutes,
  CLT,
  CLTRoutes,
  StartDocumentHeaderLastUpdate,
  // StartDocumentHeaderLastUpdateRoutes,
  Persona,
  PersonaRoutes,
  ProjectInfo,
  ProjectInfoRoutes,
  EmailVerification,
  EmailVerificationRoutes,
  EmailLog,
  EmailLogRoutes,
  Purchase,
  PurchaseRoutes,
  Order,
  OrderRoutes,
  MaterialEntrance,
  MaterialEntranceRoutes,
  Transformado,
  TransformadoRoutes,
  Quebra,
  QuebraRoutes,
  UpfrontReturn,
  UpfrontReturnRoutes,
  SavedEmPicking,
  SavedEmPickingRoutes,
  EmailTemplate,
  EmailTemplateRoutes,
  EmailTemplateAttachment,
  EmailTemplateAttachmentRoutes,
  // Inventario,
  Inventario,
  InventarioRoutes,
  // ReturnToProvider
  ReturnToProvider,
  ReturnToProviderRoutes,
  // Prison
  Prison,
  PrisonRoutes,
  // DocumentLineNote
  DocumentLineNote,
  DocumentLineNoteRoutes,
  // SavedProviderProposal
  SavedProviderProposal,
  SavedProviderProposalRoutes,
  // ProductGoogleSheetsRoutes
  ProductGoogleSheets,
  ProductGoogleSheetsRoutes,
  // Task
  Task,
  TaskRoutes,
  // TaskMessage
  TaskMessage,
  TaskMessageRoutes,
  // RecurrentTasks
  RecurrentTasks,
  // TaskRead,
  TaskRead,
  TaskReadRoutes,
  // Theme
  Theme,
  ThemeRoutes,
  // Dashboard
  Dashboard,
  DashboardRoutes,
  // ChatRapidMessage
  ChatRapidMessage,
  ChatRapidMessageRoutes,
  // SideMenu
  SideMenu,
  SideMenuRoutes,
  // AdminPanel
  AdminPanel,
  // ErrorLog
  ErrorLog,
  ErrorLogRoutes,
};
