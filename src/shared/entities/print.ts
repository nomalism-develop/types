export type ScheduledPrintJob = {
  id: string;
  printer_id: string;
  external_url: string | null;
  moloni_json: unknown;
  sent: Date | null;
  done: Date | null;
  created_at: Date;
  created_by: string | null;
};

/**
 * Model Printer
 *
 */
export type Printer = {
  id: string;
  npc_id: string;
  printer_id: string;
  name: string | null;
  connected: boolean;
  enable: boolean;
  paper: boolean;
  receipt: boolean;
  sticker: boolean;
  normalWidth: number;
  condensedWidth: number;
  dotWidth: number;
  hasDrawer: number;
  hasCutter: number;
  lowDensity: number;
  imagePrintMode: number;
  created_at: Date;
  updated_at: Date;
};

/**
 * Model Npc
 *
 */
export type Npc = {
  id: string;
  name: string | null;
  mac_address: string;
  secret: string | null;
};
