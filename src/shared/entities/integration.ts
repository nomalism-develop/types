import { IProjectInfoType } from '../../modules/integration/projectInfo/interfaces';

/**
 * Model Observation
 *
 */
export type Observation = {
  id: string;
  owner_id: string;
  observation_type_id: string | null;
  description: string;
  internal: boolean;
  created_by: string;
  updated_by: string;
  created_at: Date;
  updated_at: Date;
};

/**
 * Model ObservationType
 *
 */
export type ObservationType = {
  id: string;
  name: string;
  created_by: string;
  updated_by: string;
  created_at: Date;
  updated_at: Date;
};

/**
 * Model GoogleFilePermission
 *
 */
export type GoogleFilePermission = {
  id: string;
  file_id: string;
  email: string;
  permission_id: string;
  created_at: Date;
  updated_at: Date;
};

/**
 * Model Settings
 *
 */
export type Settings = {
  id: number;
  key: string;
  value: string;
  created_at: Date;
  updated_at: Date;
};

/**
 * Model ProjectInfo
 *
 */
export type ProjectInfo = {
  id: number;
  type: IProjectInfoType;
  name: string;
  email: string;
  telephone: string;
  description: string;
  document_header_id: string | null;
  created_at: Date;
  updated_at: Date;
};

/**
 * Model EmailLog
 *
 */
export type EmailLog = {
  id: string;
  template: string;
  owner_id: string | null;
  document_header_id: string | null;
  chat_id: string | null;
  variables: unknown;
  mail_options: unknown;
  smtp_response: unknown;
  error: string | null;
  retried: boolean;
  created_at: Date;
  updated_at: Date;
};

/**
 * Model EmailTemplate
 *
 */
export type EmailTemplate = {
  id: string;
  name: string;
  to: string;
  subject: string;
  body: string;
  created_by: string;
  updated_by: string;
  created_at: Date;
  updated_at: Date;
};

/**
 * Model EmailTemplateAttachments
 *
 */
export type EmailTemplateAttachment = {
  id: string;
  email_template_id: string;
  name: string;
  multimedia_id: string;
  created_by: string;
  updated_by: string;
  created_at: Date;
  updated_at: Date;
};

/**
 * Model EmailVerification
 *
 */
export type EmailVerification = {
  id: string;
  email: string;
  token: string;
  verified: boolean;
  created_by: string;
  created_at: Date;
  updated_by: string;
  updated_at: Date;
};
