/**
 * Model Tickets
 *
 */
export type Tickets = {
  id: string;
  number: number;
  channel_id: number;
  who_attended: string | null;
  attended_at: Date | null;
  language_id: number;
  created_by: string;
  updated_by: string | null;
  created_at: Date;
  updated_at: Date;
};

/**
 * Model Channel
 *
 */
export type Channel = {
  id: number;
  key: string;
  order: number;
  color: string;
  text_color: string;
  disabled_color: string;
  created_by: string;
  updated_by: string | null;
  created_at: Date;
  updated_at: Date;
};

/**
 * Model Language
 *
 */
export type Language = {
  id: number;
  name: string;
  alpha2: string;
  lang: string;
  voice_name: string;
  ssml: string;
  call_text: string;
  created_by: string;
  updated_by: string | null;
  order: number;
  created_at: Date;
  updated_at: Date;
};

/**
 * Model Translation
 *
 */
export type Translation = {
  id: number;
  key: string;
  created_by: string;
  updated_by: string | null;
  created_at: Date;
  updated_at: Date;
};

/**
 * Model CLT
 *
 */
export type CLT = {
  id: number;
  channel_id: number;
  language_id: number;
  translation_id: number;
  name: string;
  text: string;
  created_by: string;
  updated_by: string | null;
  created_at: Date;
  updated_at: Date;
};
