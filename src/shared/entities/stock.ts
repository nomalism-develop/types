import { IConflict } from '../../modules/document/propostaSheets/interface';
import {
  IDocumentTypeUserType,
  IDocumentTypeCodeType,
} from '../../modules/supply/documentType/interfaces';
import { IBatchType, IPaymentOrigin } from '../../modules/supply/payment/interface';
import { IProductType } from '../../modules/stock/productGoogleSheets/interface';
import { IImageType } from '../../modules/stock/productImage/interface';
import { IQueryParameterDatatype } from '../../modules/stock/queryParameter/interfaces';
import {
  IData,
  IDataDocumentoPago,
  IDataPagamento,
  IPromotions,
} from '../../modules/supply/documentLine/interfaces';
import { IPhcDocumentType } from '../../modules/supply/clientCurrentAccount/interfaces';
import { ISavedEmPickingData } from '../../modules/stock/savedEmPicking/interfaces';
import { IDocumentLineNoteType } from '../../modules/supply/documentLineNote/interfaces';
import { IProductSheetState } from '../../modules/stock/productGoogleSheets/interface';
import type { IFindProposals } from '../../modules/supply/orderManagement/interface';
import { ITaskStatusDataType } from '../../modules/stock/task/interface';

/**
 * Model Chat
 *
 */
export type Chat = {
  id: string;
  owner_id: string;
  document_header_id: string;
  message: string;
  email_processed: boolean;
  email_delivered: boolean;
  email_opened: boolean;
  email_clicked: boolean;
  whatsapp_sent: boolean;
  read: boolean;
  is_customer: boolean;
  deleted: boolean;
  created_by: string;
  updated_by: string;
  created_at: Date;
  updated_at: Date;
};

/**
 * Model Location
 *
 */
export type Location = {
  id: string;
  type_of_location_id: string;
  country_id: string;
  country_name: string;
  name: string;
  street: string;
  locality: string;
  postal_code: string;
  geolocation: unknown | null;
  default_billing: boolean;
  default_delivery: boolean;
  origin_id: string | null;
  created_at: Date;
  updated_at: Date;
  created_by: string;
  updated_by: string;
};

/**
 * Model PreSale
 *
 */
export type PreSale = {
  id: string;
  counter: number;
  barcode: string | null;
  document_header_id: string | null;
  created_at: Date;
  updated_at: Date;
  created_by: string;
  updated_by: string;
};

/**
 * Model PreSaleProduct
 *
 */
export type PreSaleProduct = {
  id: string;
  pre_sale_id: string;
  product_id: string;
  quantity: number;
  created_at: Date;
  updated_at: Date;
  created_by: string;
  updated_by: string;
};

/**
 * Model ProductPrices
 *
 */
export type ProductPrices = {
  id: string;
  price_cost: number;
  price_sale: number;
  avg_unit_cost: number | null;
  product_id: string;
  created_at: Date;
  updated_at: Date;
  origin_id: string | null;
  created_by: string;
  updated_by: string;
};

/**
 * Model ProductImage
 *
 */
export type ProductImage = {
  id: string;
  product_id: string;
  multimedia_id: string;
  type: IImageType;
  created_at: Date;
  updated_at: Date;
  origin_id: string | null;
  created_by: string;
  updated_by: string;
};

/**
 * Model UnitOfMeasure
 *
 */
export type UnitOfMeasure = {
  id: string;
  name: string;
  notation: string;
  type: string;
  default_quantity: boolean;
  integer_only: boolean;
  divisible: boolean;
  external_id: string | null;
  origin_id: string | null;
  created_at: Date;
  updated_at: Date;
  created_by: string;
  updated_by: string;
};

/**
 * Model Promotion
 *
 */
export type Promotion = {
  id: string;
  name: string;
  initial_date: Date;
  final_date: Date;
  active_for_purchases_above: number;
  free_shipping_for_purchases_above: number | null;
  discount: number;
  origin_id: string | null;
  created_at: Date;
  updated_at: Date;
  created_by: string;
  updated_by: string;
};

/**
 * Model PromotionAssoc
 *
 */
export type PromotionAssoc = {
  id: string;
  client_id: string | null;
  location_id: string | null;
  product_id: string | null;
  promotion_id: string;
  origin_id: string | null;
  created_at: Date;
  updated_at: Date;
  created_by: string;
  updated_by: string;
};

/**
 * Model TypeOfLocation
 *
 */
export type TypeOfLocation = {
  id: string;
  name: string;
  origin_id: string | null;
  created_at: Date;
  updated_at: Date;
  created_by: string;
  updated_by: string;
};

/**
 * Model VatTax
 *
 */
export type VatTax = {
  id: string;
  name: string;
  created_at: Date;
  updated_at: Date;
  origin_id: string | null;
  created_by: string;
  updated_by: string;
  external_id: string | null;
};

/**
 * Model VatTaxZone
 *
 */
export type VatTaxZone = {
  id: string;
  name: string;
  value: number;
  vat_tax_id: string;
  origin_id: string | null;
  created_at: Date;
  updated_at: Date;
  created_by: string;
  updated_by: string;
  country_id: string;
  external_id: string | null;
};

/**
 * Model Counter
 *
 */
export type Counter = {
  document_type: number;
  year: number;
  name: string | null;
  count: number;
  created_at: Date;
  updated_at: Date;
  created_by: string;
  updated_by: string;
};

/**
 * Model DocumentType
 *
 */
export type DocumentType = {
  id: number;
  external_document_type_id: string | null;
  code: IDocumentTypeCodeType;
  description: string;
  group: string;
  is_searchable: boolean;
  quantity_in: boolean;
  quantity_out: boolean;
  credit: boolean;
  debit: boolean;
  user_type: IDocumentTypeUserType;
  start: boolean;
  invoice: boolean;
  return: boolean;
  pay: boolean;
  allow_create_from_header_without_lines: boolean;
  allow_commissioner: boolean;
  final_consumer: boolean;
  ensure_same_provider: boolean;
  duplicate_group: string | null;
  allow_duplicates: boolean;
  requires_picking: boolean;
  allow_document_line_assoc_plugins: boolean;
  created_at: Date;
  updated_at: Date;
  created_by: string;
  updated_by: string;
};

/**
 * Model DocumentHeader
 *
 */
export type DocumentHeader = {
  id: string;
  from_document_header_id: string | null;
  document_type_id: number;
  document_number: number;
  document_set_id: string;
  external_id: string | null;
  pdf_link: string | null;
  google_sheet_id: string | null;
  emission_date: Date;
  external_pdf_link: string | null;
  external_data: unknown;
  owner_id: string;
  owner_number: number;
  owner_name: string | null;
  billing_persona_id: string | null;
  billing_persona_nif: string | null;
  billing_persona_name: string | null;
  billing_persona_email: string | null;
  billing_persona_telephone: string | null;
  billing_persona_address_street: string | null;
  billing_persona_address_postal_code: string | null;
  billing_persona_address_locality: string | null;
  billing_persona_address_country_id: string | null;
  billing_persona_address_country_name: string | null;
  delivery_persona_id: string | null;
  delivery_persona_name: string | null;
  delivery_persona_email: string | null;
  delivery_persona_telephone: string | null;
  delivery_persona_address_street: string | null;
  delivery_persona_address_postal_code: string | null;
  delivery_persona_address_locality: string | null;
  delivery_persona_address_country_id: string | null;
  delivery_persona_address_country_name: string | null;
  reason_for_exemption_id: string | null;
  reason_for_exemption: string | null;
  delivery_departure_street: string | null;
  delivery_departure_locality: string | null;
  delivery_departure_postal_code: string | null;
  delivery_method_id: string | null;
  delivery_method: string | null;
  vehicle_id: string | null;
  vehicle: string | null;
  payment_method_id: string | null;
  payment_method: string | null;
  maturity_date_id: string | null;
  maturity_date: string | null;
  maturity_date_days: number | null;
  commissioner_id: string | null;
  commission_percentage: number;
  commission_paid: boolean;
  return_reason: string | null;
  is_archived: boolean;
  is_void: boolean;
  origin_id: string | null;
  who_handles: string | null;
  tags: string | null;
  warning: string | null;
  created_at: Date;
  updated_at: Date;
  created_by: string;
  updated_by: string;
};

/**
 * Model DocumentLine
 *
 */
export type DocumentLine = {
  id: string;
  product_id: string | null;
  product_promotions: IPromotions[];
  product_thumbnail: string | null;
  product_reference: string | null;
  product_designation: string;
  product_measure: string | null;
  avg_unit_cost: number | null;
  prison: boolean;
  cativado: boolean;
  cativado_date: Date | null;
  consignacao: boolean;
  document_line_group_id: string;
  index: number;
  data: IData;
  quantity: number;
  preco_custo: number;
  provider_discount: number;
  provider_purchase_discount: number;
  provider_tax: number;
  preco_venda: number;
  client_tax: number;
  client_discount: number;
  provider_estimated_delivery_date: Date | null;
  sent_to_client: boolean;
  sent_to_provider: boolean;
  created_at: Date;
  updated_at: Date;
  created_by: string;
  updated_by: string;
};

/**
 * Model DocumentLineAssoc
 *
 */
export type DocumentLineAssoc = {
  id: string;
  document_header_id: string;
  document_line_id: string;
  external_id: string | null;
  origin_id: string | null;
  created_at: Date;
  updated_at: Date;
  created_by: string;
  updated_by: string;
};

/**
 * Model Workflow
 *
 */
export type Workflow = {
  id: number;
  from_document_type_id: number | null;
  to_document_type_id: number;
  transition_name: string;
  is_active: boolean;
  created_at: Date;
  updated_at: Date;
  created_by: string;
  updated_by: string;
};

/**
 * Model DocumentSet
 *
 */
export type DocumentSet = {
  id: string;
  name: string;
  document_set_id: number | null;
  cash_vat_scheme_indicator: number;
  active_by_default: boolean;
  template_id: number;
  img_gr_1: string;
  created_at: Date;
  updated_at: Date;
  created_by: string;
  updated_by: string;
};

/**
 * Model ExternalDocumentType
 *
 */
export type ExternalDocumentType = {
  id: string;
  name: string;
  document_type_id: number;
  saft_code: string;
  created_at: Date;
  updated_at: Date;
  created_by: string;
  updated_by: string;
};

/**
 * Model ExternalDocumentHeader
 *
 */
export type ExternalDocumentHeader = {
  id: string;
  document_header_id: string;
  external_id: string;
  external_pdf_link: string;
  pdf_link: string;
  created_at: Date;
  updated_at: Date;
  created_by: string;
  updated_by: string;
};

/**
 * Model Payment
 *
 */
export type Payment = {
  id: string;
  document_header_id: string;
  owner_id: string;
  date: Date;
  debit: number;
  credit: number;
  payment_method_id: string | null;
  payment_method: string | null;
  notes: string | null;
  iban: string | null;
  payment_batch_id: string | null;
  created_at: Date;
  updated_at: Date;
  created_by: string;
  updated_by: string;
};

/**
 * Model BankPaymentsBatch
 *
 */
export type BankPaymentsBatch = {
  id: string;
  value: number;
  type: IBatchType | null;
  times_downloaded: number;
  multimedia_id: string | null;
  created_by: string;
  updated_by: string;
  created_at: Date;
  updated_at: Date;
};

/**
 * Model ClientCurrentAccount
 *
 */
export type ClientCurrentAccount = {
  id: string;
  owner_id: string;
  origin_id: string;
  numero_cliente: number;
  data_documento: Date;
  data_vencimento: Date;
  nome_documento: string;
  numero_documento: string;
  debito: number;
  credito: number;
  observacoes: string;
  saldo: number;
  nao_regularizado: number;
  ordem: number;
  created_at: Date;
  updated_at: Date;
  created_by: string;
  updated_by: string;
};

/**
 * Model ProviderCurrentAccount
 *
 */
export type ProviderCurrentAccount = {
  id: string;
  owner_id: string;
  origin_id: string;
  numero_fornecedor: number;
  data_compra: Date;
  data_documento: Date;
  data_vencimento: Date;
  nome_documento: string;
  numero_documento: string;
  debito: number;
  credito: number;
  observacoes: string;
  ordem: number;
  saldo: number;
  nao_regularizado: number;
  created_at: Date;
  updated_at: Date;
  created_by: string;
  updated_by: string;
};

/**
 * Model VatValidation
 *
 */
export type VatValidation = {
  id: string;
  vat_number: string;
  valid: boolean;
  status: string;
  payload: unknown;
  created_at: Date;
  updated_at: Date;
};

/**
 * Model QueryList
 *
 */
export type QueryList = {
  id: string;
  label: string;
  code: string;
  group: string | null;
  created_at: Date;
  updated_at: Date;
  created_by: string;
  updated_by: string;
};

/**
 * Model QueryParameter
 *
 */
export type QueryParameter = {
  id: string;
  label: string;
  accessor: string;
  datatype: IQueryParameterDatatype;
  suffix: string | null;
  preffix: string | null;
  input: boolean;
  output: boolean;
  align: string;
  width: number | null;
  query_id: string;
  order: number | null;
  created_at: Date;
  updated_at: Date;
  created_by: string;
  updated_by: string;
};

/**
 * Model ReturnReason
 *
 */
export type ReturnReason = {
  id: string;
  reason: string;
  created_at: Date;
  updated_at: Date;
  created_by: string;
  updated_by: string;
};

/**
 * Model DocumentLinePendingConflict
 *
 */
export type DocumentLinePendingConflict = {
  id: string;
  document_line_id: string;
  designation: string;
  quantity: number;
  price_sale: number;
  price_cost: number;
  updated_product_price_sale: number | null;
  discount: number;
  conflict: IConflict;
  created_at: Date;
  updated_at: Date;
  created_by: string;
  updated_by: string;
};

/**
 * Model PhcHeaders
 *
 */
export type PhcHeaders = {
  id: string;
  origin_id: string;
  owner_id: string;
  document_number: number;
  client_nif: string;
  client_number: number;
  client_name: string;
  persona_id: string;
  email: string | null;
  telephone: string | null;
  already_imported: boolean;
  sm_external_id: string | null;
  phc_document_type: IPhcDocumentType;
  created_at: Date;
  updated_at: Date;
  created_by: string;
  updated_by: string;
};

/**
 * Model PhcClientOrderLines
 *
 */
export type PhcClientOrderLines = {
  id: string;
  header_id: string;
  origin_id: string;
  ref: string;
  design: string;
  qtt: number;
  pcusto: number;
  pvp: number;
  taxa_iva: number;
  desconto: number;
  unidade: string;
  cativo: boolean;
  levantado: boolean;
  fechada: boolean;
  estado: string;
  created_at: Date;
  updated_at: Date;
  created_by: string;
  updated_by: string;
};

/**
 * Model PhcClientAdvanceLines
 *
 */
export type PhcClientAdvanceLines = {
  id: string;
  header_id: string;
  origin_id: string;
  enc_cli_bostamp: string;
  total: number;
  iva: number;
  base: number;
  por_reg: number;
  created_at: Date;
  updated_at: Date;
  created_by: string;
  updated_by: string;
};

/**
 * Model PhcInvoiceLines
 *
 */
export type PhcInvoiceLines = {
  id: string;
  header_id: string;
  origin_id: string;
  ref: string;
  design: string;
  qtt: number;
  pcusto: number;
  pvp: number;
  taxa_iva: number;
  desconto: number;
  unidade: string;
  already_returned: boolean;
  created_at: Date;
  updated_at: Date;
  created_by: string;
  updated_by: string;
};

/**
 * Model ProductPromotionVirtuals
 *
 */
export type ProductPromotionVirtuals = {
  product_id: string;
  promotion_assoc_id: string;
  name: string;
  initial_date: Date;
  final_date: Date;
  active_for_purchases_above: number;
  free_shipping_for_purchases_above: number | null;
  discount: number;
  promotion_price: number;
  exclusive_to_client_id: string | null;
  exclusive_to_location_id: string | null;
};

/**
 * Model ProductBaseVirtuals
 *
 */
export type ProductBaseVirtuals = {
  product_id: string;
  price_cost: number;
  price_sale: number;
  last_avg_unit_cost: number;
  margin: number | null;
};

/**
 * Model ProductVirtuals
 *
 */
export type ProductVirtuals = {
  product_id: string;
  price_cost: number;
  price_sale: number;
  margin: number | null;
  last_avg_unit_cost: number;
  reserved: number;
  prison: number;
  total: number;
  available: number;
  ordered_quantity: number;
  inativa: boolean;
};

/**
 * Model DocumentLineBaseVirtuals
 *
 */
export type DocumentLineBaseVirtuals = {
  document_line_id: string;
  // client unit
  client_unit_total_without_tax: number;
  client_unit_total_without_discount: number;
  client_unit_total: number;
  client_unit_total_discount: number;
  client_unit_total_tax: number;
  // client total
  client_total_without_tax: number;
  client_total_without_discount: number;
  client_total: number;
  client_total_discount: number;
  client_total_tax: number;
  // provider unit
  provider_unit_total_without_tax: number;
  provider_unit_total_without_discount: number;
  provider_unit_total: number;
  provider_unit_total_discount: number;
  provider_unit_total_tax: number;
  // provider total
  provider_total_without_tax: number;
  provider_total_without_discount: number;
  provider_total: number;
  provider_total_discount: number;
  provider_total_tax: number;
  // commissions
  commission_percentage: number;
  commission_total: number;
  // flags
  cancelled: boolean;
  phc_closed: boolean;
  provider_invoice: boolean;
  purchased_from_provider: boolean;
  pending_return_to_provider: boolean;
  available_for_client_order: boolean;
  already_client_order: boolean;
  already_material_entrance: boolean;
  to_order: boolean;
  rejected_return_to_provider: boolean;
  inventory_loss: boolean;
  return_to_provider_without_credit_note: boolean;
  returned_to_provider: boolean;
  awaiting_production: boolean;
  ordered: boolean;
  group_header: boolean;
  waiting_for_bulk_order: boolean;
  waiting_for_order: boolean;
  closed: boolean;
  provider_closed: boolean;
  // documents
  stock_in: boolean;
  stock_out: boolean;
  client_start_id: string | null;
  provider_start_id: string | null;
  // shortcuts
  invoice_id: string | null;
  invoice_document_header_external_id: string | null;
  invoice_external_id: string | null;
  credit_note_id: string | null;
  // data values
  df_rejeitada: boolean;
  levantar_df: boolean;
  df_enviada: boolean;
  df_fechada: boolean;
  conferir_encomenda: string | null;
  documento_pago: IDataDocumentoPago | null;
  pagamento: IDataPagamento | null;
};

/**
 * Model DocumentLineVirtuals
 *
 */
export type DocumentLineVirtuals = {
  document_line_id: string;
  // client unit
  client_unit_total_without_tax: number;
  client_unit_total_without_discount: number;
  client_unit_total: number;
  client_unit_total_discount: number;
  client_unit_total_tax: number;
  // client total
  client_total_without_tax: number;
  client_total_without_discount: number;
  client_total: number;
  client_total_discount: number;
  client_total_tax: number;
  // provider unit
  provider_unit_total_without_tax: number;
  provider_unit_total_without_discount: number;
  provider_unit_total: number;
  provider_unit_total_discount: number;
  provider_unit_total_tax: number;
  // provider total
  provider_total_without_tax: number;
  provider_total_without_discount: number;
  provider_total: number;
  provider_total_discount: number;
  provider_total_tax: number;
  // commissions
  commission_percentage: number;
  commission_total: number;
  // state flags
  cancelled: boolean;
  phc_closed: boolean;
  closed: boolean;
  provider_closed: boolean;
  group_header: boolean;
  waiting_for_bulk_order: boolean;
  waiting_for_order: boolean;
  stock_in: boolean;
  stock_out: boolean;
  conferir_encomenda: string | null;
  original_product_id: string | null;
  imported_from_sheets: boolean;
  // document flags
  purchased_from_provider: boolean;
  pending_return_to_provider: boolean;
  available_for_client_order: boolean;
  already_client_order: boolean;
  to_order: boolean;
  already_material_entrance: boolean;
  rejected_return_to_provider: boolean;
  inventory_loss: boolean;
  return_to_provider_without_credit_note: boolean;
  returned_to_provider: boolean;
  awaiting_production: boolean;
  ordered: boolean;
  provider_invoice: boolean;
  // shortcuts
  invoice_external_id: string | null;
  invoice_document_header_external_id: string | null;
  invoice_id: string | null;
  credit_note_id: string | null;
  client_start_id: string | null;
  provider_start_id: string | null;
  // processed
  state: string;
  provider_state: string;
  phc_state: string | null;
  awaits_client_notification: boolean;
  upfront_total: number;
  // data values
  levantar_df: boolean;
  df_enviada: boolean;
};

/**
 * Model StartDocumentHeaderLastUpdateVirtuals
 *
 */
export type StartDocumentHeaderLastUpdateVirtuals = {
  p_id: string;
  updated_at: Date;
  updated_by: string;
};

/**
 * Model StartDocumentHeaderVirtuals
 *
 */
export type StartDocumentHeaderVirtuals = {
  parent_document_header_id: string;
  name: string;
  external_name: string | null;
  paid: number;
  total_without_tax: number;
  total: number;
  total_discount: number;
  total_tax: number;
  upfront_document_header_id: string | null;
  upfront_name: string | null;
  upfront_external_name: string | null;
  upfront_total: number;
  closed: boolean;
  state: string;
};

/**
 * Model DocumentHeaderClientPaymentVirtuals
 *
 */
export type DocumentHeaderClientPaymentVirtuals = {
  id: string;
  client_paid: number;
  client_unpaid: number;
};

/**
 * Model DocumentHeaderBaseVirtuals
 *
 */
export type DocumentHeaderBaseVirtuals = {
  id: string;
  p_id: string;
  name: string;
  alias: string;
  external_name: string | null;
  document_number: string;
};

/**
 * Model DocumentHeaderVirtuals
 *
 */
export type DocumentHeaderVirtuals = {
  id: string;
  p_id: string;
  name: string;
  alias: string;
  external_name: string | null;
  document_number: string;
  client_total_without_tax: number;
  client_total: number;
  client_total_discount: number;
  client_total_tax: number;
  provider_total_discount: number;
  provider_total_tax: number;
  provider_total_without_tax: number;
  provider_total: number;
};

/**
 * Model DocumentHeaderClientVirtuals
 *
 */
export type DocumentHeaderClientVirtuals = {
  id: string;
  p_id: string;
  name: string;
  alias: string;
  external_name: string | null;
  document_number: string;
  client_total_without_tax: number;
  client_total: number;
  client_total_discount: number;
  client_total_tax: number;
};

/**
 * Model DocumentHeaderProviderVirtuals
 *
 */
export type DocumentHeaderProviderVirtuals = {
  id: string;
  p_id: string;
  name: string;
  alias: string;
  external_name: string | null;
  document_number: string;
  provider_total_discount: number;
  provider_total_tax: number;
  provider_total_without_tax: number;
  provider_total: number;
};

/**
 * Model StockMovement
 *
 */
export type StockMovement = {
  document_line_id: string;
  product_id: string;
  emission_date: string;
  name: string;
  pdf_link: string | null;
  parent_document_header_id: string | null;
  created_by: string;
  quantity: number;
  stock_to_date: number;
};

/**
 * Model SettledMaterialEntrance
 *
 */
export type SettledMaterialEntrance = {
  id: string;
  document_description: string;
  document_name: string;
  total: number;
  saldo: number;
  data_vencimento: Date;
  emission_date: Date;
  pdf_link: string | null;
  type: IPaymentOrigin;
  owner_id: string;
};

/**
 * Model ProviderProductView
 *
 */
export type ProviderProductView = {
  product_id: string;
  type: IProductType;
  id_provider: number;
  obs: boolean;
  multimedia_id: string | null;
  ref: string;
  des: string;
  provider_ref: string | null;
  price_cost: number;
  price_sale: number;
  available: number;
  last_ef_date: Date | null;
  ordered_quantity: number;
  last_em_date: Date | null;
  notation: string;
  pfs: IFindProposals[];
  pcs: IFindProposals[];
  client_quantity: number;
  pre_ordered_quantity: number;
  last6m_consumption: number;
};

export type DocumentLineGroup = {
  id: string;
  created_at: Date;
  updated_at: Date;
  created_by: string;
  updated_by: string;
};

export type SavedEmPicking = {
  id: string;
  provider_id: string;
  document_header_id: string;
  document_line_group_id: string;
  data: ISavedEmPickingData;
  created_at: Date;
  updated_at: Date;
  created_by: string;
  updated_by: string;
};

export type GoogleSheetImports = {
  id: string;
  google_sheet_id: string;
  done: boolean;
  data: unknown | null;
  error: unknown | null;
  created_at: Date;
  created_by: string;
  updated_at: Date;
};

export type DocumentLineNote = {
  id: string;
  note: string;
  type: IDocumentLineNoteType;
  document_line_id: string;
  created_by: string;
  created_at: Date;
  updated_by: string;
  updated_at: Date;
};

export type SavedProviderProposal = {
  id: string;
  provider_id: string;
  product_id: string;
  quantity: number;
  created_at: Date;
  updated_at: Date;
  created_by: string;
  updated_by: string;
};

export type ProductSheet = {
  id: string;
  reference: string;
  type: IProductType;
  state: IProductSheetState;
  values: unknown;
  imported_from_sheets: boolean;
  vat_tax_id: string;
  unit_of_measure_quantity_id: string;
  cabide_product_color: string | null;
  weight: number | null;
  width: number | null;
  height: number | null;
  length: number | null;
  diameter: number | null;
  provider_ref: string | null;
  id_provider: number;
  name: string | null;
  designation: string;
  origin_id: string | null;
  type_of_inventory: string;
  external_id: string | null;
  max_discount: number;
  maintenances: string;
  compositions: string;
  attributes: string;
  location: string | null;
  created_at: Date;
  updated_at: Date;
  created_by: string;
  updated_by: string;
};

export interface Task {
  id: string;
  from: string;
  to: string | null;
  task: string;
  finished: boolean;
  archived: boolean;
  history: unknown | null;
  status: ITaskStatusDataType;
  future_date: Date | null;
  number: number;
  created_at: Date;
  updated_at: Date;
  created_by: string;
  updated_by: string;
}

export interface TaskMessage {
  id: string;
  message: string;
  task_id: string;
  created_at: Date;
  updated_at: Date;
  created_by: string;
  updated_by: string;
}

export interface TaskRead {
  id: string;
  task_id: string;
  user_id: string;
  created_at: Date;
}

/**
 * Model ChatRapidMessage
 *
 */
export type ChatRapidMessage = {
  id: string;
  title: string;
  message: string;
  created_at: Date;
  updated_at: Date;
  created_by: string;
  updated_by: string;
};

/**
 * Model File
 *
 */
export type File = {
  id: string;
  title: string;
  owner_id: string;
  document_header_id: string | null;
  multimedia_id: string;
  filename: string;
  is_customer: boolean;
  is_private: boolean;
  tags: string;
  created_by: string;
  updated_by: string;
  created_at: Date;
  updated_at: Date;
};
