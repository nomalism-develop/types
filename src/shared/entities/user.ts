import type { IChatType } from '../../modules/stock/chat/interfaces';

/**
 * Model Users
 *
 */
export type Users = {
  id: string;
  account: string | null;
  password: string | null;
  created_at: Date;
  updated_at: Date;
  created_by: string;
  updated_by: string;
  origin_id: string | null;
};

/**
 * Model UserTokens
 *
 */
export type UserTokens = {
  id: string;
  token: string;
  user_id: string;
  consumed: boolean;
  created_at: Date;
  updated_at: Date;
  created_by: string;
  updated_by: string;
};

/**
 * Model Favorites
 *
 */
export type Favorites = {
  id: string;
  user_id: string;
  url: string;
  created_at: Date;
  updated_at: Date;
};

/**
 * Model Permissions
 *
 */
export type Permissions = {
  id: number;
  code: string;
  description: string;
  enable: boolean;
  created_at: Date;
  updated_at: Date;
  created_by: string;
  updated_by: string;
};

/**
 * Model GroupPermissions
 *
 */
export type GroupPermissions = {
  id: number;
  code: string;
  description: string;
  enable: boolean;
  created_at: Date;
  updated_at: Date;
  created_by: string;
  updated_by: string;
};

/**
 * Model UserPermissions
 *
 */
export type UserPermissions = {
  id: number;
  user_id: string;
  group_id: number;
  permission_id: number;
  created_at: Date;
  updated_at: Date;
  created_by: string;
  updated_by: string;
};

/**
 * Model Providers
 *
 */
export type Providers = {
  id: string;
  chat_type: IChatType;
  user_id: string;
  main_persona_id: string;
  contact_persona_id: string;
  number: number;
  country_id: string | null;
  language_id: string | null;
  support_language_id: string | null;
  provider_type_id: string | null;
  inactive: boolean;
  manufacturer: boolean;
  default_payment_method_id: string | null;
  default_maturity_date_id: string | null;
  default_delivery_method_id: string | null;
  default_vehicle_id: string | null;
  default_discount: number;
  created_at: Date;
  updated_at: Date;
  created_by: string;
  updated_by: string;
  origin_id: string | null;
};

/**
 * Model ProviderType
 *
 */
export type ProviderType = {
  id: string;
  name: string;
  created_at: Date;
  updated_at: Date;
  created_by: string;
  updated_by: string;
  origin_id: string | null;
};

/**
 * Model Country
 *
 */
export type Country = {
  id: string;
  name: string;
  alpha2: string;
  created_at: Date;
  updated_at: Date;
  created_by: string;
  updated_by: string;
  origin_id: string | null;
  external_id: string;
};

/**
 * Model Language
 *
 */
export type Language = {
  id: string;
  name: string;
  created_at: Date;
  updated_at: Date;
  created_by: string;
  updated_by: string;
  origin_id: string | null;
  external_id: string | null;
};

/**
 * Model Clients
 *
 */
export type Clients = {
  id: string;
  user_id: string;
  chat_type: IChatType;
  main_persona_id: string;
  contact_persona_id: string;
  number: number;
  country_id: string | null;
  language_id: string | null;
  client_type_id: string | null;
  newsletter: boolean;
  inactive: boolean;
  segments_area_id: string | null;
  default_discount: number;
  default_payment_method_id: string | null;
  default_maturity_date_id: string | null;
  default_delivery_method_id: string | null;
  default_vehicle_id: string | null;
  deferred_payment: boolean;
  origin_id: string | null;
  created_at: Date;
  updated_at: Date;
  created_by: string;
  updated_by: string;
};

/**
 * Model ClientType
 *
 */
export type ClientType = {
  id: string;
  name: string;
  created_at: Date;
  updated_at: Date;
  created_by: string;
  updated_by: string;
  origin_id: string | null;
};

/**
 * Model SegmentsArea
 *
 */
export type SegmentsArea = {
  id: string;
  type: string;
  name: string;
  created_at: Date;
  updated_at: Date;
  created_by: string;
  updated_by: string;
  origin_id: string | null;
};

/**
 * Model PurchaseConditions
 *
 */
export type PurchaseConditions = {
  id: string;
  provider_id: string;
  discount: string;
  deadline: string;
  minimum_quantity: string;
  created_at: Date;
  updated_at: Date;
  created_by: string;
  updated_by: string;
  origin_id: string | null;
};

/**
 * Model Shippings
 *
 */
export type Shippings = {
  id: string;
  provider_id: string;
  type: string;
  quantity: string;
  value: string;
  unit: string;
  created_at: Date;
  updated_at: Date;
  created_by: string;
  updated_by: string;
  origin_id: string | null;
};

/**
 * Model Commissioner
 *
 */
export type Commissioner = {
  id: string;
  user_id: string;
  chat_type: IChatType;
  main_persona_id: string;
  contact_persona_id: string;
  number: number;
  country_id: string | null;
  language_id: string | null;
  default_percentage: number;
  inactive: boolean;
  default_payment_method_id: string | null;
  default_maturity_date_id: string | null;
  default_delivery_method_id: string | null;
  default_vehicle_id: string | null;
  created_at: Date;
  updated_at: Date;
  created_by: string;
  updated_by: string;
  origin_id: string | null;
};

/**
 * Model BankData
 *
 */
export type BankData = {
  id: string;
  provider_id: string | null;
  client_id: string | null;
  store_operator_id: string | null;
  commissioner_id: string | null;
  iban: string;
  swift_id: string | null;
  country_id: string;
  created_at: Date;
  updated_at: Date;
  created_by: string;
  updated_by: string;
  origin_id: string | null;
  notes: string | null;
};

/**
 * Model Swift
 *
 */
export type Swift = {
  id: string;
  bank: string;
  city: string;
  branch: string | null;
  code: string;
  country_id: string;
  created_at: Date;
  updated_at: Date;
  created_by: string;
  updated_by: string;
  origin_id: string | null;
};

/**
 * Model ReasonForExemption
 *
 */
export type ReasonForExemption = {
  id: string;
  name: string;
  description: string;
  saft_code: string;
  created_at: Date;
  updated_at: Date;
  created_by: string;
  updated_by: string;
  origin_id: string | null;
  external_id: string;
};

/**
 * Model StoreOperator
 *
 */
export type StoreOperator = {
  id: string;
  user_id: string;
  chat_type: IChatType;
  user_position_id: string | null;
  number: number;
  internal_email: string | null;
  support_language_id: string | null;
  main_persona_id: string;
  contact_persona_id: string;
  pin: string;
  location_id: string;
  inactive: boolean;
  created_at: Date;
  updated_at: Date;
  created_by: string;
  updated_by: string;
  origin_id: string | null;
};

/**
 * Model UserPositions
 *
 */
export type UserPositions = {
  id: string;
  name: string;
  created_at: Date;
  updated_at: Date;
  created_by: string;
  updated_by: string;
  origin_id: string | null;
};

/**
 * Model PaymentMethods
 *
 */
export type PaymentMethods = {
  id: string;
  payment_method_id: number | null;
  name: string;
  hidden: boolean | null;
  is_numerary: boolean;
  is_mb: boolean;
  is_credit: boolean;
  created_at: Date;
  updated_at: Date;
  created_by: string;
  updated_by: string;
};

/**
 * Model DeliveryMethods
 *
 */
export type DeliveryMethods = {
  id: string;
  delivery_method_id: number | null;
  name: string;
  created_at: Date;
  updated_at: Date;
  created_by: string;
  updated_by: string;
};

/**
 * Model MaturityDates
 *
 */
export type MaturityDates = {
  id: string;
  maturity_date_id: number | null;
  name: string;
  days: number;
  created_at: Date;
  updated_at: Date;
  created_by: string;
  updated_by: string;
};

/**
 * Model Vehicles
 *
 */
export type Vehicles = {
  id: string;
  vehicle_id: number | null;
  description: string;
  number_plate: string;
  created_at: Date;
  updated_at: Date;
  created_by: string;
  updated_by: string;
};

/**
 * Model ZipCode
 *
 */
export type ZipCode = {
  id: string;
  postal_code: string;
  address: string;
  locality: string;
  parish: string;
  county: string;
  county_code: number;
  district: string;
  latitude: string;
  longitude: string;
};

/**
 * Model Tenant
 *
 */
export type Tenant = {
  id: string;
  customer_id: string;
  name: string;
  taxpayer_number: string;
  commercial_registration_number: number | null;
  share_capital: number | null;
  conservatory: string | null;
  logo: string | null;
  email: string;
  address: string;
  postal_code: string;
  locality: string;
  country: string;
  Timezone: string | null;
  telephone: string | null;
  fax: string | null;
  website: string | null;
  created_at: Date;
  updated_at: Date;
  created_by: string;
  updated_by: string;
};

/**
 * Model Persona
 *
 */
export type Persona = {
  id: string;
  name: string | null;
  nif: string | null;
  telephone: string | null;
  email: string | null;
  observations: string | null;
  street: string | null;
  postal_code: string | null;
  locality: string | null;
  country_id: string | null;
  reason_for_exemption_id: string | null;
  provider_id: string | null;
  client_id: string | null;
  commissioner_id: string | null;
  store_operator_id: string | null;
  created_at: Date;
  updated_at: Date;
  created_by: string;
  updated_by: string;
};

/**
 * Model Theme
 *
 */
export type Theme = {
  id: string;
  colors: unknown;
  store_operator_id: string;
  created_at: Date;
  updated_at: Date;
  created_by: string;
  updated_by: string;
};
