/**
 * Model Multimedia
 */

export type Multimedia = {
  id: string;
  url: string;
  filepath: string;
  size: number;
  hash: string;
  content_type: string;
  original_filename: string;
  origin_id: string | null;
  created_at: Date;
  updated_at: Date;
};
