export default {
  moduleUser: {
    tokenExpired: 'Sessão expirada',
    jwtNotFound: 'Sessão expirada',
    jwtInvalid: 'Sessão expirada',
  },
  moduleMultimedia: {
    notFound: 'Recurso multimedia não encontrado em registo',
    noUpload: 'Nenhum upload carregado',
    notDeleted: 'Não foi possível apagar recurso multimedia',
    fileNotFound: 'Recurso multimedia não encontrado em disco',
  },
};
