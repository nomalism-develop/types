export default {
  moduleUser: {
    notMatch: 'Incorrect email/password combination',
    informOldPassword: 'You need to inform old password to set a new password',
    oldPasswordNotMatch: 'Old password does not match.',
    preferencesExceeded: 'You can only have one active preference',
    alreadyExistsTypeBusinessContacts: 'Already exists this type business contacts with same name',
    emailAlreadyExits: 'Email address already exists',
    typeBusinessContactsDontExists: 'This type business contacts not found',
    addressNotFound: 'This address is not found',
    userNotFound: `This user is not found`,
    userNotValid: `This user is not valid`,
    bankDataNotFound: 'This bank data not found',
    businessContactsNotFound: 'Business contacts not found',
    contactsNotFound: 'This contacts not found',
    socialMediaNotFound: 'This social media not found',
    userTokenNotFound: 'User token does not exists',
    tokenExpired: 'Token Expired',
    dontHavePermissionDelete: 'You dont have permission to delete this item',
    dontHavePermissionUpdate: 'You dont have permission to update this item',
    cantDeleteAssociation: 'You cant delete this item because it is still associated',
    errorToDelete: 'Occurred an error to delete this item',
    errorToCreate: 'Occurred an error to create this item',
    refreshTokenNotExists: 'Refresh Token does not exists',
    jwtNotExists: 'JWT invalid',
    groupPermissionWithSameCode: 'Already exists this group with same code',
    groupPermissionNotFound: 'This group permission not found',
    permissionWithSameCode: 'Already exists this permission with same code',
    permissionNotFound: 'This permission not found',
    entityListNotFound: 'This entity list not found',
    entityListNotCreated: 'Entity list not created',
    entityListItemNotCreated: 'Entity list item not created',
    entityListItemNotUpdated: 'Entity list item not updated',
    entityNameExist: 'Entity name already exists',
    entityNotFound: 'Entity not found',
  },
  moduleProvider: {
    purchaseConditionsNotFound: 'This purchase conditions not found',
    providerNotFound: 'This provider not found',
    paymentConditionsNotFound: 'This payment conditions not found',
    shippingsNotFound: 'This shipping not found',
  },
  moduleClient: {
    segmentsAreaNotFound: 'SegmentsArea dont found',
    alreadySegmentExistWithSameName: 'Already exists segment with same name',
    typeNotAvailable: 'This type is not available',
    clientNotFound: 'This client not found',
    writeTypeIntended: 'You have to write intended type',
    oneFieldFilled: 'You must fill only one field',
    atLeast4: 'Must be at least 4 characters',
    clientNotCreated: 'Unable to create Client',
  },
  moduleCommissioner: {
    segmentsAreaNotFound: 'SegmentsArea dont found',
    alreadySegmentExistWithSameName: 'Already exists segment with same name',
    typeNotAvailable: 'This type is not available',
    CommissionerNotFound: 'This Commissioner not found',
    writeTypeIntended: 'You have to write intended type',
    oneFieldFilled: 'You must fill only one field',
    atLeast4: 'Must be at least 4 characters',
  },

  moduleStoreOperator: {
    storeOperadorNotFound: 'Store Operator not found',
    invalidPin: 'Pin not match in user',
    deniedOperation: 'Users cannot be the same',

    informOldPin: 'You need to inform old pin to set a new pin',
    oldPinNotMatch: 'Old pin does not match',
    notMatch: 'Incorrect username/pin combination',
  },
  moduleFavorite: {
    notFound: 'Favorite not found',
  },
  moduleAddress: {
    shouldHaveExactlyOneUser:
      'Should have exactly one user, either store operator, customer, supplier or comissioner',
  },
  moduleSocialMedia: {
    shouldHaveExactlyOneUser:
      'Should have exactly one user, either store operator, customer, supplier or comissioner',
  },
  moduleBank: {
    shouldHaveExactlyOneUser:
      'Should have exactly one user, either store operator, customer, supplier or comissioner',
  },
  moduleClientType: {
    NotCreateUpdate: 'Could not create or update Client Type record',
    NotDelete: 'Could not delete Client Type record',
  },
  moduleCountry: {
    NotCreateUpdate: 'Could not create or update Country record',
    NotDelete: 'Could not delete Country record',
    NotFound: 'Could not find Country',
  },
  moduleLanguage: {
    NotCreateUpdate: 'Could not create or update Language record',
    NotDelete: 'Could not delete Language record',
  },
  moduleProviderType: {
    NotCreateUpdate: 'Could not create or update Provider Type record',
    NotDelete: 'Could not delete Provider Type record',
  },
  moduleSwift: {
    NotCreateUpdate: 'Could not create or update Swift record',
    NotDelete: 'Could not delete Swift Type record',
  },
};
