export default {
  moduleUser: {
    jwtNotExists: 'Token does not exist',
    tokenExpired: 'Token expired',
  },
};
