export default {
  moduleUser: {
    informAvatar: 'You need to inform avatar file',
    userAvatarNotFound: 'This user avatar not found',
    tokenExpired: 'Token Expired',
    jwtNotExists: 'JWT invalid',
  },
  moduleField: {
    NotFound: 'Field Group not found',
    NotCreateUpdate: 'Unable to Create/Update Field Group',
    NotCreateSameName: 'Unable to Create with the same name',
    NotDelete: 'Unable to Delete Field Group',
  },
  moduleTasks: {
    AlreadyExists: 'This task group already exists',
    ErrorOnDelete: 'Error on Delete',
    NotFound: 'Task not found',
    ParentNotFound: 'Parent Task not found',
  },
  moduleTasksGroup: {
    AlreadyExists: 'This task group already exists',
    ErrorOnDelete: 'Error on Task Group Delete',
    NotFound: 'Task Group not found',
  },
  moduleTaskNote: {
    NotFound: 'Task Note not found',
    NotCreateUpdate: 'Unable to Create/Update Task Note',
  },
  moduleObservation: {
    dontExist: 'this observation dont exists',
    notFound: 'observation not found',
    alreadyExists: 'this observation already exists',
    notDeleted: 'observation not deleted',
    notUpdated: 'observation not updated',
  },
  moduleObservationType: {
    dontExist: 'this type of observation dont exists',
    notFound: 'type of observation not found',
    alreadyExists: 'this type of observation already exists',
    childExists: 'type of observation has observations',
    notDeleted: 'type of observation not deleted',
    notUpdated: 'type of observation not updated',
  },
};
