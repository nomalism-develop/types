import joi from 'joi';
import { messages } from './messages';
import type {
  ILastTicketCalledTodayByChannel,
  IRemainingTodayTickets,
} from '../modules/tickets/tickets/interfaces';
import type { IGetUserData } from '../modules/integration/googleSheets/interfaces';
import type { IDocumentTypeUserType } from '../modules/supply/documentType/interfaces';
import type { IScheduleType } from '../modules/supply/schedule/interface';
import type { IWarningType } from '../modules/supply/documentHeader/interfaces';
import type { IProjectInfoType } from '../modules/integration/projectInfo/interfaces';

export { messages };

// timestamps
type CreatedAt = 'created_at';

type UpdatedAt = 'updated_at';

export interface WithCreatedAt {
  created_at: Date;
}

export interface WithUpdatedAt {
  updated_at: Date;
}

export interface WithOriginId {
  origin_id: string;
}

export type WithTimestamps = WithCreatedAt & WithUpdatedAt;

export type WithTimestampsOriginID = (WithCreatedAt & WithUpdatedAt) | WithOriginId;

export type WithoutTimestamps<T> = Omit<T, CreatedAt | UpdatedAt>;

// userstamps
type CreatedBy = 'created_by';

type UpdatedBy = 'updated_by';

export interface WithCreatedBy {
  created_by?: string | null;
}

export interface WithUpdatedBy {
  updated_by?: string | null;
}

export type WithUserstamps = WithCreatedBy & WithUpdatedBy;

export type WithoutUserstamps<T> = Omit<T, CreatedBy | UpdatedBy>;

export interface IUserToken {
  id: string;
  location_id: string;
}

export type IEntityWithUserToken<T> = {
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  [K in keyof T]: T[K] extends (...params: infer U) => any
    ? (...params: [...U, IUserToken]) => ReturnType<T[K]>
    : T[K];
};

// all stamps
export type WithStamps = WithTimestamps & WithUserstamps;

export type WithStampsResponse<T> = T & WithStamps;

export type WithoutStamps<T> = Omit<T, CreatedAt | UpdatedAt | CreatedBy | UpdatedBy>;

// pagination
export interface IPaginationRequest {
  per_page?: number;
  current_page?: number;
}

export interface IPaginationResponse<T> {
  per_page: number;
  current_page: number;
  total: number;
  data: T[];
}

// find by id
export interface IFindByIdRequest extends Record<string, string> {
  id: string;
}

// find by id number

export interface IFindByIdNumberRequest {
  id: number;
}

// find minified
export interface IFindMinifiedRequest {
  owner_id?: string;
  search_value?: string | null;
}

export interface IFindMinifiedResponse {
  id: string;
  name: string;
  group?: string;
  multimedia_id?: string;
}

// find by owner id
export interface IFindByOwnerIdRequest {
  owner_id: string;
}

export interface IFindOptionalByOwnerIdRequest {
  owner_id?: string;
}

// update many response
export interface IUpdateManyResponse {
  count: number;
}

export const IDataTypeEnum: {
  date: 'date';
  datetime: 'datetime';
  text: 'text';
  decimal: 'decimal';
  integer: 'integer';
  select: 'select';
  select_multi: 'select_multi';
  select_custom: 'select_custom';
  boolean: 'boolean';
  url: 'url';
  email: 'email';
  tel: 'tel';
  file: 'file';
  password: 'password';
  multiline: 'multiline';
  json: 'json';
  userstamp: 'userstamp';
} = {
  date: 'date',
  datetime: 'datetime',
  text: 'text',
  decimal: 'decimal',
  integer: 'integer',
  select: 'select',
  select_multi: 'select_multi',
  select_custom: 'select_custom',
  boolean: 'boolean',
  url: 'url',
  email: 'email',
  tel: 'tel',
  file: 'file',
  password: 'password',
  multiline: 'multiline',
  json: 'json',
  userstamp: 'userstamp',
};

export type IDatatype = (typeof IDataTypeEnum)[keyof typeof IDataTypeEnum];

export const dataTypes = Object.keys(IDataTypeEnum);

// route validation
export type IRouteRequest<T> = Record<keyof WithoutStamps<T>, joi.AnySchema>;
export type IRouteRequestWithStamps<T> = Record<keyof T, joi.AnySchema>;

export type IRouteResponseWithStamp<T> = Record<keyof WithStampsResponse<T>, joi.AnySchema>;

export type IRouteResponseWithoutStamps<T> = Record<keyof WithoutStamps<T>, joi.AnySchema>;

export type IRouteResponsePagination<T> = Record<keyof IPaginationResponse<T>, joi.AnySchema>;

// find minified query validation
const findMinifiedQueryKeys: IRouteRequest<IFindMinifiedRequest> = {
  search_value: joi.string().allow('', null).optional(),
  owner_id: joi.string().uuid().optional(),
};
export const findMinifiedQuery = joi.object().keys(findMinifiedQueryKeys).messages(messages);

// find optional by owner id query validation
const findOptionalByOwnerIdQueryKeys: IRouteRequest<IFindOptionalByOwnerIdRequest> = {
  owner_id: joi.string().uuid().optional(),
};
export const findByOptionalOwnerIdQuery = joi
  .object()
  .keys(findOptionalByOwnerIdQueryKeys)
  .messages(messages);

// find by owner id query validation
export const findByOwnerIdQueryKeys: IRouteRequest<IFindByOwnerIdRequest> = {
  owner_id: joi.string().uuid().required(),
};
export const findByOwnerIdQuery = joi.object().keys(findByOwnerIdQueryKeys).messages(messages);

// find by id param validation
const findByIdParamKeys: IRouteRequest<IFindByIdRequest> = {
  id: joi.string().uuid().required(),
};
export const idParam = joi.object().keys(findByIdParamKeys).messages(messages);

const findByIdNumberParamKeys: IRouteRequest<IFindByIdNumberRequest> = {
  id: joi.number().positive().required(),
};
export const idNumberParam = joi.object().keys(findByIdNumberParamKeys).messages(messages);

// find minified response validation
const findMinifiedReponseKeys: IRouteResponseWithoutStamps<IFindMinifiedResponse> = {
  id: joi.string().uuid().required(),
  name: joi.string().required(),
  group: joi.string(),
  multimedia_id: joi.string(),
};

export const findMinifiedReponse = joi.array().items(findMinifiedReponseKeys);

// all stamps response validation
export const joiValidateStamps: IRouteResponseWithoutStamps<WithTimestampsOriginID> = {
  created_at: joi.date().required(),
  updated_at: joi.date().required(),
  created_by: joi.string().allow(null),
  updated_by: joi.string().allow(null),
  origin_id: joi.string().allow(null),
};

export interface ITaskClusterLoading {
  task_id: string;
  description: string;
}
export interface ITaskCluster {
  owner_id: string;
  loading: ITaskClusterLoading[];
}

export const IUserSendEmailDocumentTemplateEnum: {
  generic: 'generic';
  provider_payment_order: 'provider_payment_order';
} = {
  generic: 'generic',
  provider_payment_order: 'provider_payment_order',
};

export type IUserSendEmailDocumentTemplate =
  (typeof IUserSendEmailDocumentTemplateEnum)[keyof typeof IUserSendEmailDocumentTemplateEnum];

export const IUserSendEmailDocumentTemplateTypes = Object.keys(IUserSendEmailDocumentTemplateEnum);

// broker messages
export enum IBrokerTopic {
  // events
  create = 'create',
  update = 'update',
  delete = 'delete',
  // moloni
  moloni_create_document = 'moloni_create_document',
  moloni_backup_pdf = 'moloni_backup_pdf',
  // tickets
  tickets_change = 'tickets_change',
  // auth
  user_send_email_password = 'user_send_email_password',
  user_send_email_document = 'user_send_email_document',
  // google access
  grant_access = 'grant_access',
  remove_access = 'remove_access',
  google_sheet_set_permissions = 'google_sheet_set_permissions',
  google_sheet_update = 'google_sheet_update',
  google_sheet_import = 'google_sheet_import',
  import_master_artigos = 'import_master_artigos',
  // emails
  provider_order_schedule_email = 'provider_order_schedule_email',
  client_notice_schedule_email = 'client_notice_schedule_email',
  retry_errored_emails = 'retry_errored_emails',
  send_errored_emails_summary = 'send_errored_emails_summary',
  warning_email = 'warning_email',
  verification_email = 'verification_email',
  dispatch_chat_message = 'dispatch_chat_message',
  // async task
  perform_task = 'perform_task',
  // project info
  create_site_proposal = 'create_site_proposal',
  // socket
  task_done = 'task_done',
  push_notification = 'push_notification',
  // logs
  error_log = 'error_log',
  kafka_log = 'kafka_log',
  express_log = 'express_log',
  cron_log = 'cron_log',
  moloni_log = 'moloni_log',
}

export type IBrokerTopicPayload = {
  [IBrokerTopic.create]: Record<string, unknown>;
  [IBrokerTopic.update]: Record<string, unknown>;
  [IBrokerTopic.delete]: Record<string, unknown>;
  [IBrokerTopic.task_done]: {
    owner_id: string;
    task_id: string;
    description: string;
  };
  [IBrokerTopic.google_sheet_import]: {
    google_sheet_import_id: string;
    google_sheet_id: string;
    original_document_header_id?: string;
    user_id: string;
    location_id: string;
  };
  [IBrokerTopic.moloni_create_document]: {
    document_header_id: string;
    user_id: string;
    location_id: string;
  };
  [IBrokerTopic.moloni_backup_pdf]: {
    document_header_id: string;
    user_id: string;
    location_id: string;
  };
  [IBrokerTopic.user_send_email_password]: {
    token: string;
    user: {
      name: string | null;
      account: string;
      email: string;
    };
  };
  [IBrokerTopic.user_send_email_document]: {
    template: IUserSendEmailDocumentTemplate;
    user_type: IDocumentTypeUserType;
    user: {
      name: string;
      email: string;
    };
    owner_id: string;
    document_header_id: string;
    start_document_name: string;
    documents: {
      document_header_id: string;
      document_name: string;
      document_fullname: string;
      multimedia_id: string;
    }[];
  };
  [IBrokerTopic.grant_access]: {
    email: string;
  };
  [IBrokerTopic.remove_access]: {
    email: string;
  };
  [IBrokerTopic.tickets_change]: {
    last_tickets_called_today_by_channel: ILastTicketCalledTodayByChannel[];
    remaining_today_tickets_to_call: IRemainingTodayTickets[];
  };
  [IBrokerTopic.google_sheet_set_permissions]: {
    file_id: string;
    email: string;
  };
  [IBrokerTopic.google_sheet_update]: {
    spreadsheetId: string;
    document_name: string;
    client_data?: IGetUserData;
  };
  [IBrokerTopic.provider_order_schedule_email]: {
    provider_id: string;
    ef_dh_id: string; // EF
    ef_pdf_link: string;
    pf_number: string;
    provider_name: string;
    provider_email: string;
  };
  [IBrokerTopic.client_notice_schedule_email]: {
    client_id: string;
    pc_dh_id: string; // PC
    chat_id: string;
    pc_number: string;
    client_name: string;
    client_email: string;
  };
  [IBrokerTopic.perform_task]: {
    task_name: IScheduleType;
    user_token: IUserToken;
  };
  [IBrokerTopic.warning_email]: {
    email: string;
    name: string;
    warning: IWarningType;
    document_header_id: string; // PC
    document_number: number;
    document_lines: {
      designation: string;
      quantity: string;
      state: string;
    }[];
  };
  [IBrokerTopic.retry_errored_emails]: Record<string, never>;
  [IBrokerTopic.send_errored_emails_summary]: { warningErrors: string[] };
  [IBrokerTopic.dispatch_chat_message]: {
    email: string;
    name: string;
    phone_number: string | null;
    reply_url: string;
    owner_id: string;
    document_header_id: string | null;
    document_name: string;
    chat_id: string;
    email_subject?: string;
    created_by: string;
  };
  [IBrokerTopic.create_site_proposal]: {
    document_header_id: string;
    type: IProjectInfoType;
    name: string;
    email: string;
    telephone: string;
    description: string;
  };
  [IBrokerTopic.verification_email]: {
    email: string;
    token: string;
  };
  [IBrokerTopic.push_notification]: {
    counts: Record<string, number>;
    timestamp: string;
  };
  [IBrokerTopic.import_master_artigos]: {};
  [IBrokerTopic.kafka_log]: {
    id: string;
    topic: string;
    payload: unknown;
    error?: unknown;
    output?: unknown;
    done: boolean;
  };
  [IBrokerTopic.error_log]: {
    service: string;
    type: 'prisma' | 'unhandled' | 'axios' | 'webapp' | 'moloni';
    error: { name: string; message: string; stack?: string };
    request?: { body?: unknown; query?: unknown; params?: unknown };
  };
  [IBrokerTopic.express_log]: {
    id: string;
    service: string;
    method: string;
    url: string;
    user: unknown;
    headers: unknown;
    params: unknown;
    query: unknown;
    body: unknown;
    statusCode?: number;
    error?: unknown;
    responseSize?: number;
    responseTime?: number;
  };
  [IBrokerTopic.cron_log]: {
    id: string;
    task: string;
    error?: unknown;
    output?: unknown;
    done: boolean;
  };
  [IBrokerTopic.moloni_log]: {
    request_method: string;
    request_url: string;
    request_headers: unknown;
    request_params: unknown;
    request_body: unknown;
    response_body: unknown;
  };
};

export interface IBrokerMessage<T extends IBrokerTopic = IBrokerTopic> {
  topic: T;
  payload: IBrokerTopicPayload[T];
}

// typescript utils
export type RequireOnlyOne<T, Keys extends keyof T = keyof T> = Pick<T, Exclude<keyof T, Keys>> &
  {
    [K in Keys]-?: Required<Pick<T, K>> & Partial<Record<Exclude<Keys, K>, undefined>>;
  }[Keys];

export type RequireAtLeastOne<T, Keys extends keyof T = keyof T> = Pick<T, Exclude<keyof T, Keys>> &
  {
    [K in Keys]-?: Required<Pick<T, K>> & Partial<Pick<T, Exclude<Keys, K>>>;
  }[Keys];

// eslint-disable-next-line @typescript-eslint/no-explicit-any
type Impossible<K extends keyof any> = {
  [P in K]: never;
};

export type NoExtraProperties<T, U extends T = T> = U & Impossible<Exclude<keyof U, keyof T>>;

export type ExactPromise<T> = Promise<NoExtraProperties<T>>;
