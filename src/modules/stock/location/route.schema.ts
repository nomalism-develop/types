import joi from 'joi';
import { messages } from '../../../shared/messages';
import * as IShared from '../../../shared/interface';
import { IFindRequest, IFindPaginatedRequest, ICreateRequest, IUpdateRequest } from './interface';

// create body validation
const createBodyKeys: IShared.IRouteRequest<ICreateRequest> = {
  name: joi.string().required(),
  type_of_location_id: joi.string().uuid().required(),
  country_id: joi.string().uuid().required(),
  country_name: joi.string().required(),
  geolocation: joi
    .object({
      lat: joi.number().required(),
      lon: joi.number().required(),
    })
    .optional(),
  street: joi.string().required(),
  locality: joi.string().required(),
  postal_code: joi.string().trim(true).lowercase().required(),
  default_billing: joi.boolean(),
  default_delivery: joi.boolean(),
};
export const createBody = joi.object().keys(createBodyKeys).messages(messages);

// update body validation
const updateBodyKeys: IShared.IRouteRequest<IUpdateRequest> = {
  name: joi.string().optional(),
  type_of_location_id: joi.string().uuid().optional(),
  country_id: joi.string().uuid().optional(),
  country_name: joi.string().optional(),
  geolocation: joi
    .object({
      lat: joi.number().required(),
      lon: joi.number().required(),
    })
    .optional(),
  street: joi.string().optional(),
  locality: joi.string().optional(),
  postal_code: joi.string().trim(true).lowercase().optional(),
  default_billing: joi.boolean(),
  default_delivery: joi.boolean(),
};
export const updateBody = joi.object().keys(updateBodyKeys).messages(messages);

const findQueryKeys: IShared.IRouteRequest<IFindRequest> = {};
export const findQuery = joi.object().keys(findQueryKeys).messages(messages);

// find with pagination query validation
const findWithPaginationQueryKeys: IShared.IRouteRequest<IFindPaginatedRequest> = {
  per_page: joi.number().integer().positive().default(10).optional(),
  current_page: joi.number().integer().positive().default(1).optional(),
};
export const findWithPaginationQuery = joi
  .object()
  .keys(findWithPaginationQueryKeys)
  .messages(messages);
