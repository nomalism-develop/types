import * as IShared from '../../../shared/interface';
import { Location, TypeOfLocation } from '../../../shared/entities';

export type Entity = Location;
export const Route = 'location';
export const UpperName = 'Location';
export const LowerName = UpperName[0].toLowerCase() + UpperName.substring(1);

interface IFindDetailedResponse extends Entity {
  type_of_location: TypeOfLocation;
}

// omit any unnecessary fields
export type IFindByIdResponse = Omit<IFindDetailedResponse, 'type_of_location'>;

// omit any unnecessary fields
export type IFindByOwnerIdResponse = Omit<IFindDetailedResponse, 'type_of_location'>;

export type IFindRequest = Record<string, unknown>;

// omit any unnecessary fields
export type IFindResponse = Omit<IFindDetailedResponse, ''>;

export interface IFindPaginatedRequest extends IFindRequest, IShared.IPaginationRequest {}

export type IFindWithPaginationResponse = IShared.IPaginationResponse<
  Omit<IFindDetailedResponse, 'type_of_location'>
>;

export interface ICreateRequest {
  // create fields
  name: string;
  street: string;
  country_id: string;
  country_name: string;
  locality: string;
  postal_code: string;
  type_of_location_id: string;
  geolocation?: {
    lat: number;
    lon: number;
  };
  default_billing?: boolean;
  default_delivery?: boolean;
}

export interface IUpdateRequest {
  // updatable fields
  name?: string;
  street?: string;
  locality?: string;
  country_id?: string;
  country_name?: string;
  postal_code?: string;
  type_of_location_id?: string;
  geolocation?: {
    lat?: number;
    lon?: number;
  };
  default_billing?: boolean;
  default_delivery?: boolean;
}

export interface IRepository {
  findById(selector: IShared.IFindByIdRequest): Promise<IFindByIdResponse | null>;

  findByOwnerId(params: IShared.IFindByOwnerIdRequest): Promise<IFindByOwnerIdResponse[]>;
  findMinified(params?: IShared.IFindMinifiedRequest): Promise<IShared.IFindMinifiedResponse[]>;
  find(selector: IFindRequest): Promise<IFindResponse[]>;
  findPaginated(selector: IFindPaginatedRequest): Promise<IFindWithPaginationResponse>;

  create(data: ICreateRequest): Promise<IFindResponse>;
  update(selector: IShared.IFindByIdRequest, data: IUpdateRequest): Promise<IFindResponse | null>;
  deleteOne(selector: IShared.IFindByIdRequest): Promise<IFindResponse | null>;
}

export type IController = IShared.IEntityWithUserToken<IRepository>;
