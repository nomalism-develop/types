import { VatValidation } from '../../../shared/entities';

export type Entity = VatValidation;
export const Route = 'vat_validation';
export const UpperName = 'VatValidation';
export const LowerName = UpperName[0].toLowerCase() + UpperName.substring(1);

export interface IRepository {
  findByLatest(nif: string): Promise<Entity | null>;
}
