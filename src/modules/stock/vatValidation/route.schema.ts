import joi from 'joi';
import { messages } from '../../../shared/messages';

const findByLatestBodyKeys = {
  nif: joi.string().required(),
};

export const findByLatestBody = joi.object().keys(findByLatestBodyKeys).messages(messages);
