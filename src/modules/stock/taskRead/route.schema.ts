import joi from 'joi';
import { messages } from '../../../shared/messages';
import * as IShared from '../../../shared/interface';
import { ICreateRequest, IDeleteRequest } from './interface';

// create body validation
const createBodyKeys: IShared.IRouteRequest<ICreateRequest> = {
  task_id: joi.string().uuid().required(),
  user_id: joi.string().uuid().required(),
};
export const createBody = joi.object().keys(createBodyKeys).messages(messages);

// delete params validation
const deleteParamsKeys: IShared.IRouteRequest<IDeleteRequest> = {
  task_id: joi.string().uuid().required(),
  user_id: joi.string().uuid().required(),
};
export const deleteBody = joi.object().keys(deleteParamsKeys).messages(messages);
