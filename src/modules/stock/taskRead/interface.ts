import * as IShared from '../../../shared/interface';
import { TaskRead } from '../../../shared/entities/stock';

export type Entity = TaskRead;
export const Route = 'task_read';
export const UpperName = 'TaskRead';
export const LowerName = UpperName[0].toLowerCase() + UpperName.substring(1);

export type ICreateRequest = Pick<Entity, 'user_id' | 'task_id'>;
export type IDeleteRequest = Pick<Entity, 'user_id' | 'task_id'>;

export interface IRepository {
  create(data: ICreateRequest): Promise<void>;
  delete(selector: IDeleteRequest): Promise<void>;
}

export type IController = IShared.IEntityWithUserToken<IRepository>;
