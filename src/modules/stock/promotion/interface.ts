import * as IShared from '../../../shared/interface';
import { Promotion } from '../../../shared/entities';

export type Entity = Promotion;
export const Route = 'promotion';
export const UpperName = 'Promotion';
export const LowerName = UpperName[0].toLowerCase() + UpperName.substring(1);

type IFindDetailedResponse = Entity;

// omit any unnecessary fields
export type IFindByIdResponse = Omit<IFindDetailedResponse, ''>;

// omit any unnecessary fields
export type IFindByOwnerIdResponse = Omit<IFindDetailedResponse, ''>;

export type IFindRequest = Record<string, unknown>;

// omit any unnecessary fields
export type IFindResponse = Omit<IFindDetailedResponse, ''>;

export interface IFindPaginatedRequest extends IFindRequest, IShared.IPaginationRequest {}

export type IFindWithPaginationResponse = IShared.IPaginationResponse<
  Omit<IFindDetailedResponse, ''>
>;

export interface ICreateRequest {
  // create fields
  name: string;
  initial_date: Date;
  final_date: Date;
  active_for_purchases_above?: number;
  free_shipping_for_purchases_above?: number | null;
  discount: number;
}

export interface IUpdateRequest {
  // updatable fields
  name?: string;
  initial_date?: Date;
  final_date?: Date;
  active_for_purchases_above?: number;
  free_shipping_for_purchases_above?: number | null;
  discount?: number;
}

export interface IRepository {
  findById(selector: IShared.IFindByIdRequest): Promise<IFindByIdResponse | null>;

  findByOwnerId(params: IShared.IFindByOwnerIdRequest): Promise<IFindByOwnerIdResponse[]>;
  findMinified(params?: IShared.IFindMinifiedRequest): Promise<IShared.IFindMinifiedResponse[]>;
  find(selector: IFindRequest): Promise<IFindResponse[]>;
  findPaginated(selector: IFindPaginatedRequest): Promise<IFindWithPaginationResponse>;

  create(data: ICreateRequest): Promise<Entity>;
  update(selector: IShared.IFindByIdRequest, data: IUpdateRequest): Promise<Entity | null>;
  deleteOne(selector: IShared.IFindByIdRequest): Promise<Entity | null>;
}

export type IController = IShared.IEntityWithUserToken<IRepository>;
