import joi from 'joi';
import { messages } from '../../../shared/messages';
import * as IShared from '../../../shared/interface';
import { IFindRequest, IFindPaginatedRequest, ICreateRequest, IUpdateRequest } from './interface';

// create body validation
const createBodyKeys: IShared.IRouteRequest<ICreateRequest> = {
  name: joi.string().required(),
  initial_date: joi.date().required(),
  final_date: joi.date().required(),
  active_for_purchases_above: joi.number().integer().positive().allow(0).optional(),
  free_shipping_for_purchases_above: joi.number().integer().positive().allow(0, null).optional(),
  discount: joi.number().positive().max(100).required(),
};
export const createBody = joi.object().keys(createBodyKeys).messages(messages);

// update body validation
const updateBodyKeys: IShared.IRouteRequest<IUpdateRequest> = {
  name: joi.string().optional(),
  initial_date: joi.date().optional(),
  final_date: joi.date().optional(),
  active_for_purchases_above: joi.number().integer().positive().allow(0).optional(),
  free_shipping_for_purchases_above: joi.number().integer().positive().allow(0, null).optional(),
  discount: joi.number().positive().max(100).optional(),
};
export const updateBody = joi.object().keys(updateBodyKeys).messages(messages);

const findQueryKeys: IShared.IRouteRequest<IFindRequest> = {};
export const findQuery = joi.object().keys(findQueryKeys).messages(messages);

// find with pagination query validation
const findWithPaginationQueryKeys: IShared.IRouteRequest<IFindPaginatedRequest> = {
  per_page: joi.number().integer().positive().default(10).optional(),
  current_page: joi.number().integer().positive().default(1).optional(),
};
export const findWithPaginationQuery = joi
  .object()
  .keys(findWithPaginationQueryKeys)
  .messages(messages);
