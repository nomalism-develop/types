export const Route = 'chat_rapid_message';
export const UpperName = 'ChatRapidMessage';
export const LowerName = UpperName[0].toLowerCase() + UpperName.substring(1);
import { ChatRapidMessage } from '../../../shared/entities';
import * as IShared from '../../../shared/interface';

export type Entity = ChatRapidMessage;

export interface ICreateRequest {
  title: string;
  message: string;
}

export interface IUpdateRequest {
  title?: string;
  message?: string;
}

export interface IFindMinifiedResponse {
  id: string;
  title: string;
}
export interface IRepository {
  create(data: ICreateRequest): Promise<Entity>;
  findMinified(): Promise<IFindMinifiedResponse[]>;
  findById(selector: IShared.IFindByIdRequest): Promise<Entity | null>;
  update(selector: IShared.IFindByIdRequest, data: IUpdateRequest): Promise<void>;
  delete(selector: IShared.IFindByIdRequest): Promise<void>;
}

export type IController = IShared.IEntityWithUserToken<IRepository>;
