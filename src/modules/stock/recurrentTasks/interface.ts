import * as IShared from '../../../shared/interface';

export const Route = 'recurrent_tasks';
export const UpperName = 'RecurrentTasks';
export const LowerName = UpperName[0].toLowerCase() + UpperName.substring(1);

export interface IRepository {
  from_master(): Promise<string>;
  from_master_checked(): Promise<string>;
}

export type IController = IShared.IEntityWithUserToken<IRepository>;
