import * as IShared from '../../../shared/interface';
import { SavedProviderProposal } from '../../../shared/entities';

export type Entity = SavedProviderProposal;
export const Route = 'saved_provider_proposal';
export const UpperName = 'SavedProviderProposal';
export const LowerName = UpperName[0].toLowerCase() + UpperName.substring(1);

export interface ICreateRequest {
  provider_id: string;
  product_id: string;
  quantity: number;
}

export interface IFindManyResponse {
  product_id: string;
  quantity: number;
}

export type IUpdateRequest = Omit<ICreateRequest, 'provider_id' | 'product_id'>;

export interface IRepository {
  findByProviderId(selector: IShared.IFindByIdRequest): Promise<IFindManyResponse[]>;

  create(data: ICreateRequest): Promise<void>;

  update(selector: IShared.IFindByIdRequest, data: IUpdateRequest): Promise<void>;

  deleteOne(selector: IShared.IFindByIdRequest): Promise<void>;
}

export type IController = IShared.IEntityWithUserToken<IRepository>;
