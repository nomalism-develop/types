import joi from 'joi';
import { messages } from '../../../shared/messages';
import * as IShared from '../../../shared/interface';
import { ICreateRequest, IUpdateRequest } from './interfaces';

const createBodyKeys: IShared.IRouteRequest<ICreateRequest> = {
  provider_id: joi.string().uuid().required(),
  product_id: joi.string().uuid().required(),
  quantity: joi.number().positive().required(),
};
export const createBody = joi.object().keys(createBodyKeys).messages(messages);

const updateBodyKeys: IShared.IRouteRequest<IUpdateRequest> = {
  quantity: joi.number().positive().required(),
};
export const updateBody = joi.object().keys(updateBodyKeys).messages(messages);
