import joi from 'joi';
import { messages } from '../../../shared/messages';
import * as IShared from '../../../shared/interface';
import {
  IBasicSearchRequest,
  IFindByProductTypeRequest,
  IFindByReferenceRequest,
  IValidateByReferenceRequest,
  productTypes,
  productState,
  IFindForMobileRequest,
  type IUpdateRequest,
  ICheckStockByIdsRequest,
  ICreateRequest,
  IPrintLabelQueryRequest,
  IFindProductSheetByReference,
  IProductLocation,
} from './interface';

const printLabelQueryKeys: IShared.IRouteRequest<IPrintLabelQueryRequest> = {
  token: joi.string().required(),
};

export const printLabelQuery = joi.object().keys(printLabelQueryKeys).messages(messages);

export const checkStockByIdsBody = joi
  .object<ICheckStockByIdsRequest>()
  .keys({ ids: joi.array().items(joi.string().uuid().required()).required() })
  .messages(messages);

const basicSearchQueryKeys: IShared.IRouteRequest<IBasicSearchRequest> = {
  per_page: joi.number().integer().positive().default(10).optional(),
  current_page: joi.number().integer().positive().default(1).optional(),
  active: joi.boolean().default(false).optional(),
  inactive: joi.boolean().default(false).optional(),
  search_value: joi.string().allow('', null).optional(),
  dnt_come_anymore: joi.boolean().default(false).optional(),
  types: joi.string().optional(),
};
export const basicSearchQuery = joi.object().keys(basicSearchQueryKeys).messages(messages);

const findByRefKeys: IShared.IRouteRequest<IFindByReferenceRequest> = {
  reference: joi.string().required(),
  id_provider: joi.number().integer().positive().optional(),
  available: joi.boolean().optional(),
  allowProviderWildcard: joi.boolean().optional(),
  type: joi
    .string()
    .valid(...productTypes)
    .optional(),
};
export const findByRef = joi.object().keys(findByRefKeys).messages(messages);

const validateByRefKeys: IShared.IRouteRequest<IValidateByReferenceRequest> = {
  ...findByRefKeys,
  billing_country_id: joi.string().uuid().optional(),
};
export const validateByRef = joi.object().keys(validateByRefKeys).messages(messages);

const findByProductTypeKeys: IShared.IRouteRequest<IFindByProductTypeRequest> = {
  id_provider: joi.number().positive().required(),
  type: joi
    .string()
    .valid(...productTypes)
    .required(),
};
export const findByProductTypeParams = joi.object().keys(findByProductTypeKeys).messages(messages);

// create body validation
const createBodyKeys: IShared.IRouteRequest<ICreateRequest> = {
  id: joi.string().uuid().optional(),
  origin_id: joi.string().empty('').optional(),
  id_provider: joi.number().integer().positive().allow(0).optional(),
  provider_ref: joi.string().empty('').optional(),
  name: joi.string().empty('').optional(),
  designation: joi.string().empty('').optional(),
  type: joi
    .string()
    .valid(...productTypes)
    .optional(),
  external_id: joi.string().empty('').optional(),
  vat_tax_id: joi.string().uuid().empty('').optional(),
  weight: joi.number().positive().allow(0).allow(null, '').optional(),
  width: joi.number().positive().allow(0).allow(null, '').optional(),
  height: joi.number().positive().allow(0).allow(null, '').optional(),
  length: joi.number().positive().allow(0).allow(null, '').optional(),
  diameter: joi.number().positive().allow(0).allow(null, '').optional(),
  price_cost: joi.number().positive().allow(0).optional(),
  price_sale: joi.number().positive().allow(0).optional(),
  unit_of_measure_quantity_id: joi.string().uuid().empty('').optional(),
  cabide_product_color: joi.string().allow(null, '').optional(),
  imported_from_sheets: joi.boolean().optional(),
  reference: joi.string().optional(),
  state: joi
    .string()
    .valid(...productState)
    .optional(),
  type_of_inventory: joi.string().optional(),
  max_discount: joi.number().positive().allow(0).min(0).max(100).optional(),
  values: joi.object().optional(),
  maintenances: joi.string().optional(),
  compositions: joi.string().optional(),
  attributes: joi.string().optional(),
};
export const createBody = joi.object().keys(createBodyKeys).messages(messages);

const updateBodyKeys: IShared.IRouteRequest<IUpdateRequest> = {
  designation: joi.string().empty('').optional(),
  provider_ref: joi.string().empty('').optional(),
  type: joi
    .string()
    .valid(...productTypes)
    .optional(),
  price_cost: joi.number().positive().allow(0).optional(),
  price_sale: joi.number().positive().allow(0).optional(),
  unit_of_measure_quantity_notation: joi.string().empty('').optional(),
  state: joi
    .string()
    .valid(...productState)
    .optional(),
};
export const updateBody = joi.object().keys(updateBodyKeys).messages(messages);

export const findForMobileQuery = joi
  .object<IFindForMobileRequest>()
  .keys({
    id: joi.string().uuid().optional(),
    reference: joi.string().optional(),
  })
  .xor('id', 'reference')
  .messages(messages);

export const findProductSheetByReferenceQuery = joi
  .object<IFindProductSheetByReference>()
  .keys({
    selector: joi.string().required(),
  })
  .messages(messages);

export const productLocationBody = joi
  .object<IProductLocation>()
  .keys({
    location_number: joi.number().required(),
    product_id: joi.array().items(joi.string().uuid()).required(),
  })
  .messages(messages);
