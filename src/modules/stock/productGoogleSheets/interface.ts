import {
  type ProductImage,
  type ProductVirtuals,
  type UnitOfMeasure,
  type VatTax,
  type VatTaxZone,
  type ProductPromotionVirtuals,
  type ProductBaseVirtuals,
  type ProductSheet,
} from '../../../shared/entities';
import * as IShared from '../../../shared/interface';
import * as IDocumentLine from '../../supply/documentLine/interfaces';

export const Route = 'product_sheets';

export const UpperName = 'ProductsSheets';

export const LowerName = UpperName[0].toLowerCase() + UpperName.substring(1);

export type IProductPromotionVirtuals = ProductPromotionVirtuals;

export type IProductVirtuals = ProductVirtuals;

export type IProductBaseVirtuals = ProductBaseVirtuals;

export type Entity = ProductSheet;

// eslint-disable-next-line @typescript-eslint/no-explicit-any
export type IProductsForSheetsResponse = Record<string, any>;

// eslint-disable-next-line @typescript-eslint/no-explicit-any
export type IProductFromSheetsValues = Record<string, any>;

export type IProductSheetState = 'Ativa' | 'NaoVemMais' | 'VemTemporariamente';

export const ProductStateEnum: {
  Ativa: 'Ativa';
  NaoVemMais: 'NaoVemMais';
  VemTemporariamente: 'VemTemporariamente';
} = {
  Ativa: 'Ativa',
  NaoVemMais: 'NaoVemMais',
  VemTemporariamente: 'VemTemporariamente',
};

export const ProductStateLabels: Record<keyof typeof ProductStateEnum, string> = {
  Ativa: 'Ativa',
  NaoVemMais: 'Nao Vem Mais',
  VemTemporariamente: 'Vem Temporariamente',
};

export type IProductState = (typeof ProductStateEnum)[keyof typeof ProductStateEnum];

export const productState = Object.keys(ProductStateEnum);

export const ProductTypeEnum: {
  Encomenda: 'Encomenda';
  Stock: 'Stock';
  Servico: 'Servico';
  Producao: 'Producao';
  Portes: 'Portes';
} = {
  Encomenda: 'Encomenda',
  Stock: 'Stock',
  Servico: 'Servico',
  Producao: 'Producao',
  Portes: 'Portes',
};

export type IProductType = (typeof ProductTypeEnum)[keyof typeof ProductTypeEnum];

export const productTypes = Object.keys(ProductTypeEnum);

export interface ICreateRequest {
  // create fields
  id?: string;
  reference?: string;
  origin_id?: string | null;
  id_provider?: number;
  provider_ref?: string | null;
  name?: string | null;
  designation: string | null;
  state?: IProductSheetState; // defaults to Ativa
  type?: IProductType; // defaults to stock
  type_of_inventory?: string; // defaults to 'M'
  external_id?: string;
  vat_tax_id?: string; // defaults to 'Normal'
  weight?: number | null;
  width?: number | null;
  height?: number | null;
  length?: number | null;
  diameter?: number | null;
  price_cost?: number; // can be undefined if price_sale & margin are numbers
  price_sale?: number; // can be undefined if price_cost & margin are numbers
  max_discount?: number; // replaces no_discount
  unit_of_measure_quantity_id?: string; // defaults to configured quantity unit of measure
  cabide_product_color?: string | null;
  imported_from_sheets?: boolean;
  values?: IProductsForSheetsResponse;
  maintenances?: string;
  compositions?: string;
  attributes?: string;
  created_by?: string;
  updated_by?: string;
}

export interface IUpdateRequest {
  designation?: string;
  provider_ref?: string;
  type?: IProductType;
  unit_of_measure_quantity_notation?: string;
  state?: IProductSheetState;
  price_cost?: number;
  price_sale?: number;
}

export interface IFindProductSheetByReference {
  selector: string;
}

interface IProductSheetEntityBase extends Omit<Entity, 'values'> {
  unit_of_measure_quantity: UnitOfMeasure;
  vat_tax: VatTax & {
    vat_tax_zones: VatTaxZone[];
  };
  promotions: ProductPromotionVirtuals[];
  product_images: ProductImage[];
}

export interface IProductSheetEntity extends IProductSheetEntityBase {
  base_virtual: ProductBaseVirtuals | null;
}

export interface IProductCabideColors {
  id: string;
  reference: string;
  cabide_product_color: string;
  thumbnail: string | null;
}

export interface IProductSheetCabideEntity {
  reference: string;
  designation: string;
  provider: number;
  hanger: string;
  pattern: string;
  type: string;
  price_cost: number;
  shipping: number;
  margin: number;
  tax_value: number;
  price_sale_less_3m: number;
  price_sale_greater_3m: number;
  price_sale_piece: number;
  width: number;
  unit_of_measure_quantity_id: string;
  unit_of_measure_quantity_notation: string;
  composition: string;
  maintenance: string;
  attributes: string;
  estimated_delivery: string;
  existing_colors: IProductCabideColors[];
}

export interface IBasicSearchRequest extends IShared.IPaginationRequest {
  search_value: string | null;
  active?: boolean;
  inactive?: boolean;
  dnt_come_anymore?: boolean;
  types?: string;
}

export interface IBasicSearchResponse {
  id: string;
  reference: string;
  designation: string;
  price: number;
  promotion_price: number | null;
  thumbnail?: string | null;
  stock: number;
  updated_at: Date;
  state: IProductSheetState;
  inactive: boolean;
}

export interface IEntityMinified {
  id: string;
  id_provider: number;
  reference: string;
  designation: string;
  price_sale: number;
  price_cost: number;
  tax: number;
  multimedia_id: string | null;
  integer_only: boolean;
}

export interface IPrintLabelQueryRequest {
  token: string;
}

export interface IFindByReferenceRequest {
  reference: string; // partial search term (virtual reference)
  allowProviderWildcard?: boolean; // allow searching for products with undefined provider "0-" (default false)
  id_provider?: number; // limit search to this provider number
  available?: boolean; // limit search to products with available stock
  type?: IProductType; // limit search to product of this type
}

export interface IValidateByReferenceRequest extends IFindByReferenceRequest {
  billing_country_id?: string;
}

export interface IFindSimilarProductsByNameRequest {
  name: string;
}

export interface IFindSimilarProductsByNameResponse {
  id: string;
  reference: string;
  thumbnail: string | null;
}

export interface IFindByProductTypeRequest {
  id_provider: number;
  type: IProductType;
}

export interface IFindByProductTypeResponse {
  id: string;
  reference: string;
  price: number;
}

export interface ICheckStockByIdsRequest {
  ids: string[];
}

export interface ICheckStockByIdsResponse {
  id: string;
  reference: string;
  available: number;
}

export interface IFindReferenceAndIdForMobileResponse {
  id: string;
  reference: string;
}

export interface IFindDataForMobileResponse {
  id: string;
  reference: string;
  designation: string;
  type: IProductType;
  state: IProductSheetState;
  thumbnail: string | null;
  attributes: string;
  compositions: string;
  unit_of_measure_quantity: Pick<UnitOfMeasure, 'notation' | 'integer_only'>;
  promotions: IDocumentLine.IPromotions[];
}

export interface IFindStockForMobileResponse {
  virtual: Pick<
    IProductVirtuals,
    'price_sale' | 'available' | 'ordered_quantity' | 'prison' | 'reserved' | 'total' | 'inativa'
  > | null;
}

export type IFindForMobileRequest =
  | {
      reference: string;
      id?: never;
    }
  | {
      reference?: never;
      id: string;
    };

export interface IProductLocation {
  location_number: number;
  product_id: string[];
}

export interface IRepository {
  basicSearch(
    params: IBasicSearchRequest,
  ): Promise<IShared.IPaginationResponse<IBasicSearchResponse>>;

  findReferenceAndIdForMobile(
    params: IFindForMobileRequest,
  ): Promise<IFindReferenceAndIdForMobileResponse | null>;

  findDataForMobile(params: IShared.IFindByIdRequest): Promise<IFindDataForMobileResponse | null>;

  findStockForMobile(params: IShared.IFindByIdRequest): Promise<IFindStockForMobileResponse | null>;

  validateByRef(selector: IValidateByReferenceRequest): Promise<IEntityMinified | null>;

  findSimilarProductsByName(
    params: IFindSimilarProductsByNameRequest,
  ): Promise<IFindSimilarProductsByNameResponse[]>;

  findByProductType(params: IFindByProductTypeRequest): Promise<IFindByProductTypeResponse[]>;

  findById(selector: IShared.IFindByIdRequest): Promise<IProductSheetEntity>;

  findPreArtigo(selector: IFindProductSheetByReference): Promise<IProductSheetCabideEntity | null>;

  updatePricesFromPreArtigo(): Promise<void>;

  findByIdWithStock(selector: IShared.IFindByIdRequest): Promise<ProductVirtuals>;

  googleSheetsFieldsByRef(selector: IFindByReferenceRequest): Promise<string>;

  findRowPositionOnSheetsWithColumns(params: IFindProductSheetByReference): Promise<string | null>;

  checkStockByIds(data: ICheckStockByIdsRequest): Promise<ICheckStockByIdsResponse[]>;

  create(data: ICreateRequest): Promise<string>;

  update(params: IShared.IFindByIdRequest, data: IUpdateRequest): Promise<void>;

  updateGoogleSheetsLinesProduct(data: IShared.IFindByIdRequest[]): Promise<void>;

  deleteOne(params: IShared.IFindByIdRequest): Promise<void>;

  updateDbFromSheets(): Promise<void>;

  partialLocation(data: IProductLocation): Promise<void>;

  fullLocation(data: IProductLocation): Promise<void>;
}

export type IController = IShared.IEntityWithUserToken<IRepository>;
