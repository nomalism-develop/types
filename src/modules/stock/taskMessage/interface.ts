import * as IShared from '../../../shared/interface';
import { TaskMessage } from '../../../shared/entities';

export type Entity = TaskMessage;
export const Route = 'task_message';
export const UpperName = 'TaskMessage';
export const LowerName = UpperName[0].toLowerCase() + UpperName.substring(1);

export type ICreateRequest = Pick<Entity, 'message' | 'task_id'>;
export type IUpdateRequest = Pick<Entity, 'message' | 'task_id'>;

export interface IRepository {
  create(data: ICreateRequest): Promise<Entity>;
  update(selector: IShared.IFindByIdRequest, data: IUpdateRequest): Promise<void>;
  delete(selector: IShared.IFindByIdRequest): Promise<void>;
}

export type IController = IShared.IEntityWithUserToken<IRepository>;
