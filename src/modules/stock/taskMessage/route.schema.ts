import joi from 'joi';
import { messages } from '../../../shared/messages';
import * as IShared from '../../../shared/interface';
import { ICreateRequest, IUpdateRequest } from './interface';

// create body validation
const createBodyKeys: IShared.IRouteRequest<ICreateRequest> = {
  task_id: joi.string().uuid().required(),
  message: joi.string().required(),
};
export const createBody = joi.object().keys(createBodyKeys).messages(messages);

// update body validation
const updateBodyKeys: IShared.IRouteRequest<IUpdateRequest> = {
  task_id: joi.string().uuid().required(),
  message: joi.string().required(),
};

export const updateBody = joi.object().keys(updateBodyKeys).messages(messages);
