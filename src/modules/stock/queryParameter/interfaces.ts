import * as IShared from '../../../shared/interface';
import { QueryParameter } from '../../../shared/entities';

export type Entity = QueryParameter;
export const Route = 'query_parameter';
export const UpperName = 'QueryParameter';
export const LowerName = UpperName[0].toLowerCase() + UpperName.substring(1);

export const QueryParameterDatatypeEnum: {
  boolean: 'boolean';
  decimal: 'decimal';
  integer: 'integer';
  text: 'text';
  date: 'date';
  month: 'month';
  idnr: 'idnr';
  link: 'link';
  user: 'user';
  hidden: 'hidden';
} = {
  boolean: 'boolean',
  decimal: 'decimal',
  integer: 'integer',
  text: 'text',
  date: 'date',
  month: 'month',
  idnr: 'idnr',
  link: 'link',
  user: 'user',
  hidden: 'hidden',
};

export type IQueryParameterDatatype =
  (typeof QueryParameterDatatypeEnum)[keyof typeof QueryParameterDatatypeEnum];

export const queryParametersDatatypes = Object.keys(QueryParameterDatatypeEnum);

export interface ICreateRequest {
  label: string;
  accessor: string;
  datatype: IQueryParameterDatatype;
  suffix: string | null;
  preffix: string | null;
  input: boolean;
  width: number | null;
  output: boolean;
  query_id: string;
}

export interface IUpdateRequest {
  label?: string;
  accessor?: string;
  width?: number;
  datatype?: IQueryParameterDatatype;
  suffix?: string | null;
  preffix?: string | null;
  input?: boolean;
  output?: boolean;
}

export type IFindByOwnerIdResponse = Entity;

export interface IRepository {
  find(): Promise<Entity[]>;
  findById(selector: IShared.IFindByIdRequest): Promise<Entity | null>;
  create(data: ICreateRequest): Promise<Entity>;
  update(selector: IShared.IFindByIdRequest, data: IUpdateRequest): Promise<Entity | null>;
  deleteOne(selector: IShared.IFindByIdRequest): Promise<void>;
  findByOwnerId(selector: IShared.IFindByOwnerIdRequest): Promise<IFindByOwnerIdResponse[]>;
}

export type IController = IShared.IEntityWithUserToken<IRepository>;
