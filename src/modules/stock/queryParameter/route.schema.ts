import joi from 'joi';
import { messages } from '../../../shared/messages';
import * as IShared from '../../../shared/interface';
import { ICreateRequest, IUpdateRequest } from './interfaces';

// create body validation
const createBodyKeys: IShared.IRouteRequest<ICreateRequest> = {
  label: joi.string().required(),
  accessor: joi.string().required(),
  datatype: joi
    .string()
    .valid(...Object.values(IShared.IDataTypeEnum))
    .required(),
  input: joi.boolean().default(false),
  output: joi.boolean().default(false),
  preffix: joi.string().allow(null).required(),
  suffix: joi.string().allow(null).required(),
  query_id: joi.string().uuid().required(),
  width: joi.number().default(55),
};

export const createBody = joi.object().keys(createBodyKeys).messages(messages);

// update body validation
const updateBodyKeys: IShared.IRouteRequest<IUpdateRequest> = {
  label: joi.string().optional(),
  accessor: joi.string().optional(),
  datatype: joi.string().valid(...Object.values(IShared.IDataTypeEnum)),
  input: joi.boolean().optional(),
  output: joi.boolean().optional(),
  preffix: joi.string().allow(null).optional(),
  suffix: joi.string().allow(null).optional(),
  width: joi.number().optional(),
};

export const updateBody = joi.object().keys(updateBodyKeys).messages(messages);
