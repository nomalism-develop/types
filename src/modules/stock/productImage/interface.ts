import * as IShared from '../../../shared/interface';
import { ProductImage } from '../../../shared/entities';

export type Entity = ProductImage;
export const Route = 'product_image';
export const UpperName = 'ProductImage';
export const LowerName = UpperName[0].toLowerCase() + UpperName.substring(1);

type IFindDetailedResponse = Entity;

// omit any unnecessary fields
export type IFindByIdResponse = Omit<IFindDetailedResponse, ''>;

// omit any unnecessary fields
export type IFindByOwnerIdResponse = Omit<IFindDetailedResponse, ''>;

export type IFindRequest = Record<string, unknown>;

// omit any unnecessary fields
export type IFindResponse = Omit<IFindDetailedResponse, ''>;

export const ImageTypeEnum: {
  thumbnail: 'thumbnail';
  site: 'site';
} = {
  thumbnail: 'thumbnail',
  site: 'site',
};

export type IImageType = (typeof ImageTypeEnum)[keyof typeof ImageTypeEnum];

export const imageTypes = Object.keys(ImageTypeEnum);

export interface IFindPaginatedRequest extends IFindRequest, IShared.IPaginationRequest {}

export type IFindWithPaginationResponse = IShared.IPaginationResponse<
  Omit<IFindDetailedResponse, ''>
>;

export interface ICreateRequest {
  // create fields
  product_id: string;
  product_sheet_id?: string;
  multimedia_id: string;
  type: IImageType;
  created_by?: string | null;
  updated_by?: string | null;
}

export interface IUpdateRequest {
  // updatable fields
  product_id?: string;
  multimedia_id?: string;
  type?: IImageType;
}

export interface IRepository {
  findById(selector: IShared.IFindByIdRequest): Promise<IFindByIdResponse | null>;

  findByOwnerId(params: IShared.IFindByOwnerIdRequest): Promise<IFindByOwnerIdResponse[]>;
  find(selector: IFindRequest): Promise<IFindResponse[]>;
  findPaginated(selector: IFindPaginatedRequest): Promise<IFindWithPaginationResponse>;

  create(data: ICreateRequest): Promise<Entity>;
  update(selector: IShared.IFindByIdRequest, data: IUpdateRequest): Promise<Entity | null>;
  deleteOne(selector: IShared.IFindByIdRequest): Promise<Entity | null>;
}

export type IController = IShared.IEntityWithUserToken<IRepository>;
