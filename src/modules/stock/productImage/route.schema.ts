import joi from 'joi';
import { messages } from '../../../shared/messages';
import * as IShared from '../../../shared/interface';
import {
  IFindRequest,
  IFindPaginatedRequest,
  ICreateRequest,
  IUpdateRequest,
  imageTypes,
} from './interface';

// create body validation
const createBodyKeys: IShared.IRouteRequestWithStamps<ICreateRequest> = {
  type: joi
    .string()
    .valid(...imageTypes)
    .required(),
  product_id: joi.string().uuid().required(),
  product_sheet_id: joi.string().uuid().optional(),
  multimedia_id: joi.string().uuid().required(),
  created_by: joi.string().uuid().optional(),
  updated_by: joi.string().uuid().optional(),
};
export const createBody = joi
  .object()
  .keys(createBodyKeys)
  .xor('product_id', 'product_sheet_id')
  .messages(messages);

// update body validation
const updateBodyKeys: IShared.IRouteRequest<IUpdateRequest> = {
  type: joi
    .string()
    .valid(...imageTypes)
    .optional(),
  product_id: joi.string().uuid().optional(),
  multimedia_id: joi.string().uuid().optional(),
};
export const updateBody = joi.object().keys(updateBodyKeys).messages(messages);

const findQueryKeys: IShared.IRouteRequest<IFindRequest> = {};
export const findQuery = joi.object().keys(findQueryKeys).messages(messages);

// find with pagination query validation
const findWithPaginationQueryKeys: IShared.IRouteRequest<IFindPaginatedRequest> = {
  per_page: joi.number().integer().positive().default(10).optional(),
  current_page: joi.number().integer().positive().default(1).optional(),
};
export const findWithPaginationQuery = joi
  .object()
  .keys(findWithPaginationQueryKeys)
  .messages(messages);
