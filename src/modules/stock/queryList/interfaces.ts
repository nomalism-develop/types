import * as IShared from '../../../shared/interface';
import { QueryList, QueryParameter } from '../../../shared/entities';

export type Entity = QueryList;
export const Route = 'query';
export const UpperName = 'QueryList';
export const LowerName = UpperName[0].toLowerCase() + UpperName.substring(1);

export interface ICreateRequest {
  label: string;
  code: string;
  group: string | null;
}

export interface IUpdateRequest {
  label?: string;
  code?: string;
  group?: string | null;
}

export interface IFindResponse extends QueryList {
  parameters: QueryParameter[];
}

export const executeOutput = ['csv', 'pdf', 'xlsx', 'json'] as const;

export type IExecuteOutput = (typeof executeOutput)[number];

type IParameterDataType = string | Date | number | boolean | null | undefined;

export type IExecuteInputParameters = Record<string, IParameterDataType>;

export interface IExecuteRequest extends IExecuteInputParameters {
  query_id: string;
  output: IExecuteOutput;
  token?: string;
}

export interface IExecuteResponse extends IFindResponse {
  outputParameters: QueryParameter[];
  data: IParameterDataType[][];
}

export interface IRepository {
  find(): Promise<IFindResponse[]>;
  findById(selector: IShared.IFindByIdRequest): Promise<IFindResponse | null>;
  create(data: ICreateRequest): Promise<IFindResponse>;
  update(selector: IShared.IFindByIdRequest, data: IUpdateRequest): Promise<IFindResponse | null>;
  deleteOne(selector: IShared.IFindByIdRequest): Promise<void>;
}

export type IApi = Omit<IRepository, 'execute'> & {
  execute(data: IExecuteRequest): Promise<IExecuteResponse>;
};

export type IController = IShared.IEntityWithUserToken<IRepository>;
