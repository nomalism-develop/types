import joi from 'joi';
import { messages } from '../../../shared/messages';
import * as IShared from '../../../shared/interface';
import { ICreateRequest, IUpdateRequest, IExecuteRequest, executeOutput } from './interfaces';

// create body validation
const createBodyKeys: IShared.IRouteRequest<ICreateRequest> = {
  code: joi.string().required(),
  label: joi.string().required(),
  group: joi.string().allow(null).required(),
};

export const createBody = joi.object().keys(createBodyKeys).messages(messages);

// update body validation
const updateBodyKeys: IShared.IRouteRequest<IUpdateRequest> = {
  code: joi.string().optional(),
  label: joi.string().optional(),
  group: joi.string().allow(null).optional(),
};
export const updateBody = joi.object().keys(updateBodyKeys).messages(messages);

export const executeBodyKeys: IShared.IRouteRequest<IExecuteRequest> = {
  output: joi
    .string()
    .valid(...executeOutput)
    .required(),
  query_id: joi.string().uuid().required(),
  token: joi.string().optional(),
};

export const executeBody = joi
  .object()
  .keys(executeBodyKeys)
  .pattern(joi.string(), joi.any())
  .optional()
  .messages(messages);
