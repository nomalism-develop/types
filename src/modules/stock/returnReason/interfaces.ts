import * as IShared from '../../../shared/interface';
import { ReturnReason } from '../../../shared/entities';

export type Entity = ReturnReason;
export const Route = 'return_reason';
export const UpperName = 'ReturnReason';
export const LowerName = UpperName[0].toLowerCase() + UpperName.substring(1);

export interface ICreateRequest {
  reason: string;
}

export interface IUpdateRequest {
  reason: string;
}
export interface IRepository {
  create(data: ICreateRequest): Promise<Entity>;
  update(selector: IShared.IFindByIdRequest, data: IUpdateRequest): Promise<Entity | null>;
  find(): Promise<Entity[]>;
  deleteOne(selector: IShared.IFindByIdRequest): Promise<Entity | null>;
}

export type IController = IShared.IEntityWithUserToken<IRepository>;
