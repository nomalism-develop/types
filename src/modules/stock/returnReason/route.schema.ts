import joi from 'joi';
import { messages } from '../../../shared/messages';
import * as IShared from '../../../shared/interface';
import { ICreateRequest, IUpdateRequest } from './interfaces';

const createBodyKeys: IShared.IRouteRequest<ICreateRequest> = {
  reason: joi.string().required(),
};
export const createBody = joi.object().keys(createBodyKeys).messages(messages);

const updateBodyKeys: IShared.IRouteRequest<IUpdateRequest> = {
  reason: joi.string(),
};
export const updateBody = joi.object().keys(updateBodyKeys).messages(messages);
