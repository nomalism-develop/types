import joi from 'joi';
import { messages } from '../../../shared/messages';
import * as IShared from '../../../shared/interface';
import {
  ICheckLinesInPrisonLinesRequest,
  ICheckLinesInPrisonRequest,
  ICreateLinesInPrisonProduct,
  ICreateLinesInPrisonRequest,
  checkLineInPrisonOptions,
} from './interface';

// check lines in prison
const checkLinesInPrisonLinesBodyKeys: IShared.IRouteRequest<ICheckLinesInPrisonLinesRequest> = {
  document_line_id: joi.string().uuid().required(),
};
const checkLinesInPrisonBodyKeys: IShared.IRouteRequest<ICheckLinesInPrisonRequest> = {
  option: joi
    .string()
    .valid(...checkLineInPrisonOptions)
    .required(),
  lines: joi
    .array()
    .items(joi.object().keys(checkLinesInPrisonLinesBodyKeys).required())
    .required(),
};

export const checkLinesInPrisonBody = joi
  .object()
  .keys(checkLinesInPrisonBodyKeys)
  .messages(messages);

export const createLinesInPrisonBody = joi
  .object<ICreateLinesInPrisonRequest>()
  .keys({
    createProviderReturn: joi.boolean().required(),
    products: joi
      .array()
      .items(
        joi
          .object<ICreateLinesInPrisonProduct>()
          .keys({
            id: joi.string().uuid().required(),
            quantity: joi.number().positive().required(),
            note: joi.string().optional(),
          })
          .required(),
      )
      .required(),
  })
  .required();
