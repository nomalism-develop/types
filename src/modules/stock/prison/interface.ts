import { DocumentLineNote } from '../../../shared/entities';
import * as IShared from '../../../shared/interface';

export const Route = 'prison';
export const UpperName = 'Prison';
export const LowerName = UpperName[0].toLowerCase() + UpperName.substring(1);

export interface IFindLinesInPrisonResponse {
  id: string;
  quantity: number;
  preco_custo: number;
  id_provider: number;
  product: {
    id: string;
    thumbnail_multimedia_id: string | null;
    virtual: {
      designation: string;
      reference: string;
    };
  };
  notes: DocumentLineNote[];
  updated_by: string;
  updated_at: Date;
}

export const checkLineInPrisonOptions = ['Desprender', 'Quebrar', 'Devolver a fornecedor'] as const;

export type ICheckLineInPrisonOptions = (typeof checkLineInPrisonOptions)[number];

export interface ICheckLinesInPrisonLinesRequest {
  document_line_id: string;
}

export interface ICheckLinesInPrisonRequest {
  option: ICheckLineInPrisonOptions;
  lines: ICheckLinesInPrisonLinesRequest[];
}

export interface ICreateLinesInPrisonProduct {
  id: string;
  quantity: number;
  note?: string;
}

export interface ICreateLinesInPrisonRequest {
  products: ICreateLinesInPrisonProduct[];
  createProviderReturn: boolean;
}

export interface IRepository {
  findLinesInPrison(): Promise<IFindLinesInPrisonResponse[]>;

  findLinesInPrisonProviders(): Promise<IShared.IFindMinifiedResponse[]>;

  putLineInPrison(params: IShared.IFindByIdRequest): Promise<void>;

  removeLineFromPrison(params: IShared.IFindByIdRequest): Promise<void>;

  checkLinesInPrison(
    params: ICheckLinesInPrisonRequest,
  ): Promise<IShared.IBrokerMessage<IShared.IBrokerTopic>[]>;

  createLinesInPrison(params: ICreateLinesInPrisonRequest): Promise<string[]>;
}

export type IApi = Omit<IRepository, 'checkLinesInPrison'> & {
  checkLinesInPrison(params: ICheckLinesInPrisonRequest): Promise<void>;
};

export type IController = IShared.IEntityWithUserToken<IRepository>;
