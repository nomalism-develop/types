import joi from 'joi';
import { messages } from '../../../shared/messages';
import * as IShared from '../../../shared/interface';
import { IFindRequest, IFindPaginatedRequest, ICreateRequest, IUpdateRequest } from './interface';

// create body validation
const createBodyKeys: IShared.IRouteRequest<ICreateRequest> = {
  name: joi.string().required(),
  notation: joi.string().required(),
  type: joi.string().valid('length', 'area', 'mass', 'volume').required(),
  external_id: joi.string().allow(null).optional(),
  default_quantity: joi.boolean().optional(),
  divisible: joi.boolean().optional(),
  integer_only: joi.boolean().optional(),
};
export const createBody = joi.object().keys(createBodyKeys).messages(messages);

// update body validation
const updateBodyKeys: IShared.IRouteRequest<IUpdateRequest> = {
  name: joi.string().optional(),
  notation: joi.string().optional(),
  type: joi.string().valid('length', 'area', 'mass', 'volume').optional(),
  external_id: joi.string().allow(null).optional(),
  default_quantity: joi.boolean().optional(),
  divisible: joi.boolean().optional(),
  integer_only: joi.boolean().optional(),
};
export const updateBody = joi.object().keys(updateBodyKeys).messages(messages);

const findQueryKeys: IShared.IRouteRequest<IFindRequest> = {};
export const findQuery = joi.object().keys(findQueryKeys).messages(messages);

// find with pagination query validation
const findWithPaginationQueryKeys: IShared.IRouteRequest<IFindPaginatedRequest> = {
  per_page: joi.number().integer().positive().default(10).optional(),
  current_page: joi.number().integer().positive().default(1).optional(),
};
export const findWithPaginationQuery = joi
  .object()
  .keys(findWithPaginationQueryKeys)
  .messages(messages);
