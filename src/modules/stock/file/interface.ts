import * as IShared from '../../../shared/interface';
import { File } from '../../../shared/entities';

export type Entity = File;
export const Route = 'file';
export const UpperName = 'File';
export const LowerName = UpperName[0].toLowerCase() + UpperName.substring(1);

export interface IFindByOwnerRequest {
  owner_id: string;
  tags?: string;
  shared_only: boolean;
}

export interface ICreateRequest {
  title: string;
  owner_id: string;
  document_header_id: string | null;
  multimedia_id: string;
  filename: string;
  is_customer?: boolean;
  is_private: boolean;
  tags: string;
  username: string;
}

export interface IUpdateRequest {
  document_header_id: string;
}

export interface IRepository {
  findByOwnerId(params: IFindByOwnerRequest): Promise<Entity[]>;
  create(data: ICreateRequest): Promise<Entity>;
  update(selector: IShared.IFindByIdRequest, data: IUpdateRequest): Promise<void>;
  deleteOne(selector: IShared.IFindByIdRequest): Promise<Entity | null>;
}
