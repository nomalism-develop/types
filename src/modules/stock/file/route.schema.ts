import joi from 'joi';
import { messages } from '../../../shared/messages';
import * as IShared from '../../../shared/interface';
import { ICreateRequest, IFindByOwnerRequest, type IUpdateRequest } from './interface';

const createBodyKeys: IShared.IRouteRequest<ICreateRequest> = {
  title: joi.string().required(),
  owner_id: joi.string().uuid().required(),
  document_header_id: joi.string().uuid().allow(null).required(),
  multimedia_id: joi.string().uuid().required(),
  filename: joi.string().required(),
  is_customer: joi.boolean().default(false).optional(),
  is_private: joi.boolean().required(),
  tags: joi.string().allow('').empty('').required(),
  username: joi.string().required(),
};
export const createBody = joi.object().keys(createBodyKeys).messages(messages);

const updateBodyKeys: IShared.IRouteRequest<IUpdateRequest> = {
  document_header_id: joi.string().uuid().required(),
};
export const updateBody = joi.object().keys(updateBodyKeys).messages(messages);

export const findByOwnerIdQuery = joi
  .object<IFindByOwnerRequest>()
  .keys({
    owner_id: joi.string().uuid().required(),
    tags: joi.string().optional(),
    shared_only: joi.boolean().required(),
  })
  .messages(messages);
