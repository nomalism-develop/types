import * as IShared from '../../../shared/interface';
import { VatTaxZone, VatTax } from '../../../shared/entities';

export type Entity = VatTaxZone;
export const Route = 'vat_tax_zone';
export const UpperName = 'VatTaxZone';
export const LowerName = UpperName[0].toLowerCase() + UpperName.substring(1);

interface IFindDetailedResponse extends Entity {
  vat_tax: VatTax;
}

// omit any unnecessary fields
export type IFindByIdResponse = Omit<IFindDetailedResponse, 'vat_tax'>;

// omit any unnecessary fields
export type IFindByOwnerIdResponse = Omit<IFindDetailedResponse, 'vat_tax'>;

export type IFindRequest = Record<string, unknown>;

// omit any unnecessary fields
export type IFindResponse = Omit<IFindDetailedResponse, ''>;

export interface IFindPaginatedRequest extends IFindRequest, IShared.IPaginationRequest {}

export type IFindWithPaginationResponse = IShared.IPaginationResponse<
  Omit<IFindDetailedResponse, 'vat_tax'>
>;

export interface ICreateRequest {
  // create fields
  name: string;
  value: number;
  vat_tax_id: string;
  country_id: string;
  external_id: string | null;
}

export interface IUpdateRequest {
  // updatable fields
  name?: string;
  value?: number;
  vat_tax_id?: string;
  country_id?: string;
  external_id?: string | null;
}

export interface IRepository {
  findById(selector: IShared.IFindByIdRequest): Promise<IFindByIdResponse | null>;

  findByOwnerId(params: IShared.IFindByOwnerIdRequest): Promise<IFindByOwnerIdResponse[]>;
  findMinified(selector: IShared.IFindMinifiedRequest): Promise<IShared.IFindMinifiedResponse[]>;
  find(selector: IFindRequest): Promise<IFindResponse[]>;
  findPaginated(selector: IFindPaginatedRequest): Promise<IFindWithPaginationResponse>;

  create(data: ICreateRequest): Promise<IFindResponse>;
  update(selector: IShared.IFindByIdRequest, data: IUpdateRequest): Promise<IFindResponse | null>;
  deleteOne(selector: IShared.IFindByIdRequest): Promise<IFindResponse | null>;
}

export type IController = IShared.IEntityWithUserToken<IRepository>;
