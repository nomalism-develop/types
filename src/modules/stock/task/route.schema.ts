import joi from 'joi';
import { messages } from '../../../shared/messages';
import * as IShared from '../../../shared/interface';
import { ICreateRequest, IUpdateRequest, IFindTasksRequest, taskStatusDataType } from './interface';

// create body validation
const createBodyKeys: IShared.IRouteRequest<ICreateRequest> = {
  task: joi.string().required(),
};
export const createBody = joi.object().keys(createBodyKeys).messages(messages);

// update body validation
const updateBodyKeys: IShared.IRouteRequest<IUpdateRequest> = {
  from: joi.string().uuid().optional(),
  to: joi.string().uuid().allow(null).optional(),
  task: joi.string().optional(),
  read: joi.boolean().optional(),
  finished: joi.boolean().optional(),
  archived: joi.boolean().optional(),
  future_date: joi.date().allow(null, '').optional(),
  status: joi
    .string()
    .valid(...taskStatusDataType)
    .optional(),
};
export const updateBody = joi.object().keys(updateBodyKeys).messages(messages);

const findTaskParamsKeys: IShared.IRouteRequest<IFindTasksRequest> = {
  archived: joi.boolean().optional(),
};
export const findTaskParams = joi.object().keys(findTaskParamsKeys).messages(messages);
