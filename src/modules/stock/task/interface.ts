import * as IShared from '../../../shared/interface';
import { Task, TaskMessage, DocumentHeader, File } from '../../../shared/entities';

export type Entity = Task;
export const Route = 'task';
export const UpperName = 'Task';
export const LowerName = UpperName[0].toLowerCase() + UpperName.substring(1);

export const ITaskStatusDataTypeEnum: {
  Normal: 'Normal';
  Urgent: 'Urgent';
  Problem: 'Problem';
} = {
  Normal: 'Normal',
  Urgent: 'Urgent',
  Problem: 'Problem',
};

export type ITaskStatusDataType =
  (typeof ITaskStatusDataTypeEnum)[keyof typeof ITaskStatusDataTypeEnum];

export const taskStatusDataType = Object.keys(ITaskStatusDataTypeEnum);

export type ICreateRequest = {
  task: string;
};
export type IUpdateRequest = {
  from?: string;
  to?: string | null;
  task?: string;
  read?: boolean;
  finished?: boolean;
  archived?: boolean;
  status?: ITaskStatusDataType;
  future_date?: Date | null;
};

export const IHistoryStatusDataTypeEnum: {
  created_task: 'created_task';
  updated_task: 'updated_task';
  created_message: 'created_message';
  updated_message: 'updated_message';
  deleted_message: 'deleted_message';
} = {
  created_task: 'created_task',
  updated_task: 'updated_task',
  created_message: 'created_message',
  updated_message: 'updated_message',
  deleted_message: 'deleted_message',
};

export type IHistoryStatusDataType =
  (typeof IHistoryStatusDataTypeEnum)[keyof typeof IHistoryStatusDataTypeEnum];

export const historyStatusDataType = Object.keys(IHistoryStatusDataTypeEnum);

export interface ITaskHistoryData {
  status: IHistoryStatusDataType | null;
  data: string;
  changedAt: Date;
  changedBy: string;
}
export interface ITaskHistoryResponse {
  status: IHistoryStatusDataType | null;
  data: string;
  changedAt: Date;
  changedBy: string;
}

export type IFindResponse = Omit<Entity, 'history'> & {
  task_read: boolean;
};

export type ITaskFileType = Pick<
  File,
  'multimedia_id' | 'filename' | 'created_at' | 'created_by'
> & {
  id?: string;
};

export interface IFindWithMessagesResponse extends Entity {
  messages: TaskMessage[];
  document_headers: Pick<DocumentHeader, 'id' | 'document_number'>[];
  task_read: boolean;
  task_files: ITaskFileType[];
}
export type IFindTasksRequest = {
  archived?: boolean;
};

export type IFindByOwnerResponse = {
  task_id: string;
  from: string;
  to: string | null;
  task: string;
  created_at: Date;
  task_read: boolean;
  number: number;
  status: ITaskStatusDataType;
  future_date: Date | null;
};

export interface IRepository {
  find(selector: IFindTasksRequest): Promise<IFindResponse[]>;

  findWithMessages(id: IShared.IFindByIdRequest): Promise<IFindWithMessagesResponse>;

  create(data: ICreateRequest): Promise<Entity>;

  update(selector: IShared.IFindByIdRequest, data: IUpdateRequest): Promise<void>;

  delete(selector: IShared.IFindByIdRequest): Promise<void>;

  findByOwnerId: (selector: IShared.IFindByOwnerIdRequest) => Promise<IFindByOwnerResponse[]>;
}

export type IController = IShared.IEntityWithUserToken<IRepository>;
