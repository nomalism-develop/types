import joi from 'joi';
import { messages } from '../../../shared/messages';
import * as IShared from '../../../shared/interface';
import {
  IExportRequest,
  exportVersions,
  exportFormats,
  IFindStockMovementPaginatedRequest,
} from './interface';

const exportCsvQueryKeys: IShared.IRouteRequest<IExportRequest> = {
  date: joi.date().required(),
  format: joi
    .string()
    .valid(...exportFormats)
    .required(),
  version: joi
    .string()
    .valid(...exportVersions)
    .required(),
  token: joi.string().required(),
};
export const exportCsvQuery = joi.object().keys(exportCsvQueryKeys).messages(messages);

// find with pagination query validation
const findWithPaginationQueryKeys: IShared.IRouteRequest<IFindStockMovementPaginatedRequest> = {
  owner_id: joi.string().uuid().required(),
  per_page: joi.number().integer().positive().default(10).optional(),
  current_page: joi.number().integer().positive().default(1).optional(),
};
export const findStockMovementWithPaginationQuery = joi
  .object()
  .keys(findWithPaginationQueryKeys)
  .messages(messages);
