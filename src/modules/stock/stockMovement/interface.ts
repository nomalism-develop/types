import * as IShared from '../../../shared/interface';
import { StockMovement } from '../../../shared/entities';

export type Entity = StockMovement;
export const Route = 'stock_movement';
export const UpperName = 'StockMovement';
export const LowerName = UpperName[0].toLowerCase() + UpperName.substring(1);

export interface IFindByProviderResponse
  extends Omit<StockMovement, 'product_id' | 'stock_to_date'> {
  reference: string;
}

export type IFindByOwnerIdResponse = Omit<StockMovement, 'document_line_id' | 'product_id'>;

export const ExportFormatEnum: {
  csv: 'csv';
  xml: 'xml';
} = {
  csv: 'csv',
  xml: 'xml',
};

export type IExportFormat = (typeof ExportFormatEnum)[keyof typeof ExportFormatEnum];

export const exportFormats = Object.keys(ExportFormatEnum);

export const ExportVersionEnum: {
  v1_02: 'v1_02';
  v2_01: 'v2_01';
} = {
  v1_02: 'v1_02',
  v2_01: 'v2_01',
};

export type IExportVersion = (typeof ExportVersionEnum)[keyof typeof ExportVersionEnum];

export const exportVersions = Object.keys(ExportVersionEnum);

export interface IExportRequest {
  date: Date;
  format: IExportFormat;
  version: IExportVersion;
  token: string;
}

// https://info.portaldasfinancas.gov.pt/pt/apoio_contribuinte/Comunicacao_inventarios_exist%C3%AAncias/Documents/comunicacaoinventarios.pdf
export interface IExportCsvResponse {
  productCategory: string;
  productCode: string;
  productDescription: string;
  productNumberCode: string;
  closingStockQuantity: number;
  unitOfMeasure: string;
  closingStockValue: number;
}

export interface IFindStockMovementPaginatedRequest
  extends IShared.IFindByOwnerIdRequest,
    IShared.IPaginationRequest {}

export interface IRepository {
  findByProvider(selector: IShared.IFindByIdNumberRequest): Promise<IFindByProviderResponse[]>;

  findByOwnerId(selector: IShared.IFindByOwnerIdRequest): Promise<IFindByOwnerIdResponse[]>;

  exportCsv(selector: IExportRequest): Promise<IExportCsvResponse[]>;

  findStockMovementPaginated(
    selector: IFindStockMovementPaginatedRequest,
  ): Promise<IShared.IPaginationResponse<IFindByOwnerIdResponse>>;
}

export interface IController extends IRepository {
  findByOwnerId(selector: IShared.IFindByOwnerIdRequest): Promise<IFindByOwnerIdResponse[]>;

  exportCsv(selector: IExportRequest): Promise<IExportCsvResponse[]>;
}
