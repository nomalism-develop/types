import joi from 'joi';
import { messages } from '../../../shared/messages';
import * as IShared from '../../../shared/interface';
import { IFindRequest, IFindPaginatedRequest, ICreateRequest, IUpdateRequest } from './interface';

// create body validation
const createBodyKeys: IShared.IRouteRequest<ICreateRequest> = {
  promotion_id: joi.string().uuid().required(),
  product_id: joi.string().uuid().optional().allow(null),
  client_id: joi.string().uuid().optional().allow(null),
  location_id: joi.string().uuid().optional().allow(null),
};
export const createBody = joi.object().keys(createBodyKeys).messages(messages);

// update body validation
const updateBodyKeys: IShared.IRouteRequest<IUpdateRequest> = {
  promotion_id: joi.string().uuid().optional(),
  client_id: joi.string().uuid().optional().allow(null),
  location_id: joi.string().uuid().optional().allow(null),
  product_id: joi.string().uuid().optional().allow(null),
};
export const updateBody = joi.object().keys(updateBodyKeys).messages(messages);

const findQueryKeys: IShared.IRouteRequest<IFindRequest> = {};
export const findQuery = joi.object().keys(findQueryKeys).messages(messages);

// find with pagination query validation
const findWithPaginationQueryKeys: IShared.IRouteRequest<IFindPaginatedRequest> = {
  per_page: joi.number().integer().positive().default(10).optional(),
  current_page: joi.number().integer().positive().default(1).optional(),
};
export const findWithPaginationQuery = joi
  .object()
  .keys(findWithPaginationQueryKeys)
  .messages(messages);
