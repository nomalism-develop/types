import * as IShared from '../../../shared/interface';
import { PromotionAssoc, Promotion, Location } from '../../../shared/entities';

export type Entity = PromotionAssoc;
export const Route = 'promotion_assoc';
export const UpperName = 'PromotionAssoc';
export const LowerName = UpperName[0].toLowerCase() + UpperName.substring(1);

interface IFindDetailedResponse extends Entity {
  location: Location | null;
  promotion: Promotion;
}

// omit any unnecessary fields
export type IFindByIdResponse = Omit<IFindDetailedResponse, 'location' | 'promotion'>;

// omit any unnecessary fields
export type IFindByOwnerIdResponse = Omit<IFindDetailedResponse, ''>;

export type IFindRequest = Record<string, unknown>;

// omit any unnecessary fields
export type IFindResponse = Omit<IFindDetailedResponse, 'location' | 'promotion'>;

export interface IFindPaginatedRequest extends IFindRequest, IShared.IPaginationRequest {}

export type IFindWithPaginationResponse = IShared.IPaginationResponse<
  Omit<IFindDetailedResponse, 'location' | 'promotion'>
>;

export interface ICreateRequest {
  // create fields
  client_id: string;
  location_id: string;
  product_id: string;
  promotion_id: string;
}

export interface IUpdateRequest {
  // updatable fields
  client_id?: string;
  location_id?: string;
  product_id?: string;
  promotion_id?: string;
}

export interface IRepository {
  findById(selector: IShared.IFindByIdRequest): Promise<IFindByIdResponse | null>;

  findByOwnerId(params: IShared.IFindByOwnerIdRequest): Promise<IFindByOwnerIdResponse[]>;
  find(selector: IFindRequest): Promise<IFindResponse[]>;
  findPaginated(selector: IFindPaginatedRequest): Promise<IFindWithPaginationResponse>;

  create(data: ICreateRequest): Promise<IFindByOwnerIdResponse>;
  update(
    selector: IShared.IFindByIdRequest,
    data: IUpdateRequest,
  ): Promise<IFindByOwnerIdResponse | null>;
  deleteOne(selector: IShared.IFindByIdRequest): Promise<IFindByOwnerIdResponse | null>;
}

export type IController = IShared.IEntityWithUserToken<IRepository>;
