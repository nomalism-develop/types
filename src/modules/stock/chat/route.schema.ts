import joi from 'joi';
import { messages } from '../../../shared/messages';
import * as IShared from '../../../shared/interface';
import { ICreateRequest, IUpdateRequest } from './interfaces';

// create body validation
const createBodyKeys: IShared.IRouteRequest<ICreateRequest> = {
  owner_id: joi.string().uuid().required(),
  document_header_id: joi.string().uuid().allow(null).required(),
  message: joi.string().required(),
  username: joi.string().required(),
  is_customer: joi.boolean().default(false).optional(),
};
export const createBody = joi.object().keys(createBodyKeys).messages(messages);

// update body validation
const updateBodyKeys: IShared.IRouteRequest<IUpdateRequest> = {
  document_header_id: joi.string().uuid().optional(),
  message: joi.string().optional(),
  email_processed: joi.boolean().optional(),
  email_delivered: joi.boolean().optional(),
  email_opened: joi.boolean().optional(),
  email_clicked: joi.boolean().optional(),
  whatsapp_sent: joi.boolean().optional(),
  read: joi.boolean().optional(),
  username: joi.string().optional(),
};
export const updateBody = joi.object().keys(updateBodyKeys).messages(messages);
