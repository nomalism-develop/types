import type * as IShared from '../../../shared/interface';
import type * as Persona from '../../user/persona/interface';
import { Chat, File } from '../../../shared/entities';

export type Entity = Chat;
export const Route = 'chat';
export const UpperName = 'Chat';
export const LowerName = UpperName[0].toLowerCase() + UpperName.substring(1);

export const ChatTypeEnum: {
  por_proposta: 'por_proposta';
  global: 'global';
} = {
  por_proposta: 'por_proposta',
  global: 'global',
};

export type IChatType = (typeof ChatTypeEnum)[keyof typeof ChatTypeEnum];

export const chatTypes = Object.keys(ChatTypeEnum);

export interface IChatBalloon {
  chat?: Pick<Entity, 'message'> & {
    id?: string;
    read?: boolean;
  };
  file?: Pick<File, 'multimedia_id' | 'filename'> & {
    id?: string;
    read?: boolean;
  };
  created_at: Date;
  created_by: string;
  is_customer: boolean;
}

export type IReport = Pick<
  Entity,
  'email_processed' | 'email_delivered' | 'email_opened' | 'email_clicked'
>;

export interface IPublicFindActiveByOwnerIdResponse {
  persona?: Persona.IFindContactPersonaByOwnerId;
  last_group_report?: IReport;
  owner_id?: string;
  chat: IChatBalloon[];
}

export interface ICreateRequest {
  owner_id: string;
  document_header_id: string;
  message: string;
  username: string;
  is_customer: boolean;
}

export interface IUpdateRequest {
  document_header_id?: string;
  message?: string;
  email_processed?: boolean;
  email_delivered?: boolean;
  email_opened?: boolean;
  email_clicked?: boolean;
  whatsapp_sent?: boolean;
  read?: boolean;
  username?: string;
}

export interface IRepository {
  findActiveByOwnerId(
    params: IShared.IFindByOwnerIdRequest,
  ): Promise<IPublicFindActiveByOwnerIdResponse>;

  resendLast(params: IShared.IFindByOwnerIdRequest): Promise<void>;
  create(data: ICreateRequest): Promise<Chat>;
  update(selector: IShared.IFindByIdRequest, data: IUpdateRequest): Promise<void>;
  deleteOne(selector: IShared.IFindByIdRequest): Promise<void>;

  markAllAsRead(params: IShared.IFindByOwnerIdRequest): Promise<void>;
  markAllAsUnread(params: IShared.IFindByOwnerIdRequest): Promise<void>;
}

export type IController = IShared.IEntityWithUserToken<IRepository>;
