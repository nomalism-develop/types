import joi from 'joi';
import { messages } from '../../../shared/messages';
import * as IShared from '../../../shared/interface';
import { ICreateRequest, IUpdateRequest, IFindByOwnerIdRequest } from './interfaces';
import { documentTypeUserTypes } from '../../supply/documentType/interfaces';

const postBodyValidatorKeys: IShared.IRouteRequest<ICreateRequest> = {
  from_document_type_id: joi.number().integer(),
  to_document_type_id: joi.number().integer(),
  transition_name: joi.string().required(),
  is_active: joi.boolean().default(false).optional(),
};
export const postBodyValidator = joi.object().keys(postBodyValidatorKeys).messages(messages);

const putBodyValidatorKeys: IShared.IRouteRequest<IUpdateRequest> = {
  from_document_type_id: joi.number().integer(),
  to_document_type_id: joi.number().integer(),
  transition_name: joi.string(),
  is_active: joi.boolean().optional(),
};
export const putBodyValidator = joi.object().keys(putBodyValidatorKeys).messages(messages);

const getByOwnerQueryKeys: IShared.IRouteRequest<IFindByOwnerIdRequest> = {
  from: joi.number().integer().allow(null),
  to: joi.number().integer().allow(null),
  user_type: joi.string().valid(...documentTypeUserTypes),
};
export const getByOwnerQuery = joi.object().keys(getByOwnerQueryKeys).messages(messages);
