import * as IShared from '../../../shared/interface';
import { Workflow } from '../../../shared/entities';
import * as DocumentType from '../../supply/documentType/interfaces';
import { IDocumentTypeUserType } from '../../supply/documentType/interfaces';

export type Entity = Workflow;
export const Route = 'workflow';
export const UpperName = 'Workflow';
export const LowerName = UpperName[0].toLowerCase() + UpperName.substring(1);

export type IFindRequest = Record<string, unknown>;

export interface ICreateRequest {
  from_document_type_id: number;
  to_document_type_id: number;

  transition_name: string;

  is_active: boolean;
}

export interface IUpdateRequest {
  from_document_type_id?: number;
  to_document_type_id?: number;

  transition_name?: string;

  is_active?: boolean;
}

export interface IFindByQueryRequest extends IShared.IPaginationRequest {
  id?: number;
  from?: number;
}

export interface IFindByOwnerIdRequest {
  from?: number;
  to?: number;
  user_type?: IDocumentTypeUserType;
}

export interface IFindByOwnerIdResponse extends Entity {
  from_document_type: DocumentType.Entity | null;
  to_document_type: DocumentType.Entity | null;
}

export interface IRepository {
  findById(id: number): Promise<Workflow | null>;
  findByQuery(data: IFindByQueryRequest): Promise<IShared.IPaginationResponse<Workflow>>;

  findByOwnerId(data: IFindByOwnerIdRequest): Promise<IFindByOwnerIdResponse[]>;
  find(data: IFindRequest): Promise<IFindByOwnerIdResponse[]>;

  create(data: ICreateRequest): Promise<IFindByOwnerIdResponse>;
  update(
    selector: IShared.IFindByIdNumberRequest,
    data: IUpdateRequest,
  ): Promise<IFindByOwnerIdResponse | null>;
  deleteOne(selector: IShared.IFindByIdNumberRequest): Promise<IFindByOwnerIdResponse | null>;
}

export type IController = IShared.IEntityWithUserToken<IRepository>;

export type IApi = Omit<IRepository, 'findById' | 'findByQuery'>;
