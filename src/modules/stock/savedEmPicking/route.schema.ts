import joi from 'joi';
import { messages } from '../../../shared/messages';
import * as IShared from '../../../shared/interface';
import {
  ICreateManyRequest,
  ICreateRequest,
  ISavedEmPickingData,
  type IDeleteManyRequest,
} from './interfaces';

import {
  IEntradaDeMaterialClientOrder,
  IEntradaDeMaterialNC,
  IEntradaDeMaterialStock,
  createMaterialEntranceClientOrderOptions,
  createMaterialEntranceStockOptions,
  createMaterialEntranceStockWithoutOrderOptions,
} from '../../document/materialEntrance/interfaces';

const entradaDeMaterialClientOrderBodyKeys: IShared.IRouteRequest<IEntradaDeMaterialClientOrder> = {
  ef_dla_id: joi.string().uuid().required(),
  ordered_quantity: joi.number().positive().required(),
  pc_virtual_name: joi.string().required(),
  pc_dh_id: joi.string().uuid().required(),
  pc_dh_number: joi.number().integer().positive().required(),
  pc_dla_id: joi.string().uuid().required(),
  pc_sheet: joi.string().allow(null),

  document_line_id: joi.string().uuid().required(),
  quantity: joi.number().positive().allow(0).required(),
  option: joi
    .string()
    .valid(...createMaterialEntranceClientOrderOptions)
    .allow(null)
    .required(),
  note: joi.string().allow(null),
  printed: joi.string().allow(null),
};

const entradaDeMaterialClientOrderBody = joi
  .object()
  .keys(entradaDeMaterialClientOrderBodyKeys)
  .messages(messages);

const entradaDeMaterialStockBodyKeys: IShared.IRouteRequest<IEntradaDeMaterialStock> = {
  ef_dla_id: joi.string().uuid().allow(null).required(),
  ordered_quantity: joi.number().positive().allow(0).required(),

  document_line_id: joi.string().uuid().required(),
  quantity: joi.number().positive().allow(0).required(),
  option: joi
    .string()
    .valid(...createMaterialEntranceStockOptions, ...createMaterialEntranceStockWithoutOrderOptions)
    .allow(null)
    .required(),
  note: joi.string().allow(null),
  printed: joi.string().allow(null),
};

const entradaDeMaterialStockBody = joi
  .object()
  .keys(entradaDeMaterialStockBodyKeys)
  .required()

  .messages(messages);

const entradaDeMaterialNCBodyKeys: IShared.IRouteRequest<IEntradaDeMaterialNC> = {
  ef_dla_id: joi.valid(null).required(),
  ordered_quantity: joi.number().valid(0).required(),

  document_line_id: joi.string().uuid().required(),
  quantity: joi.number().positive().allow(0).required(),
  option: joi.valid(null).required(),
  note: joi.string().allow(null),
  printed: joi.string().allow(null),
};

const entradaDeMaterialNCBody = joi.object().keys(entradaDeMaterialNCBodyKeys).messages(messages);

const entradaDeMaterialDocumentHeaderBodyKeys: IShared.IRouteRequest<ISavedEmPickingData> = {
  clientOrders: joi.array().items(entradaDeMaterialClientOrderBody),
  stock: entradaDeMaterialStockBody,
  not_ok: entradaDeMaterialNCBody,
};

const entradaDeMaterialDocumentHeaderBody = joi
  .object()
  .keys(entradaDeMaterialDocumentHeaderBodyKeys)
  .messages(messages);

// create body validation
const createBodyKeys: IShared.IRouteRequest<ICreateRequest> = {
  provider_id: joi.string().uuid().required(),
  document_header_id: joi.string().uuid().required(),
  document_line_group_id: joi.string().uuid().required(),
  data: entradaDeMaterialDocumentHeaderBody,
};
export const createBody = joi.object().keys(createBodyKeys).messages(messages);

export const createManyBody = joi
  .object<ICreateManyRequest>()
  .keys({
    items: joi.array().items(createBody).required(),
  })
  .messages(messages);

export const deleteManyBody = joi
  .object<IDeleteManyRequest>()
  .keys({
    ids: joi.array().items(joi.string().uuid().required()).required(),
  })
  .messages(messages);
