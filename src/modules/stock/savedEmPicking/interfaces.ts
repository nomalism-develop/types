import * as IShared from '../../../shared/interface';
import { SavedEmPicking } from '../../../shared/entities';
import { IUnfinishedPickingLineGroupExtended } from '../../document/materialEntrance/interfaces';

export type Entity = SavedEmPicking;
export const Route = 'saved_em_picking';
export const UpperName = 'SavedEmPicking';
export const LowerName = UpperName[0].toLowerCase() + UpperName.substring(1);

export type ISavedEmPickingData = Pick<
  IUnfinishedPickingLineGroupExtended,
  'clientOrders' | 'stock' | 'not_ok'
>;

export interface ICreateRequest {
  provider_id: string;
  document_header_id: string;
  document_line_group_id: string;
  data: ISavedEmPickingData;
}

export interface ICreateManyRequest {
  items: ICreateRequest[];
}

export interface IDeleteManyRequest {
  ids: string[];
}

export interface IRepository {
  create(data: ICreateRequest): Promise<string>;
  createMany(data: ICreateManyRequest): Promise<string[]>;
  deleteOne(selector: IShared.IFindByIdRequest): Promise<void>;
  deleteMany(data: IDeleteManyRequest): Promise<void>;
}

export type IController = IShared.IEntityWithUserToken<IRepository>;
