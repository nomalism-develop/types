import joi from 'joi';
import { messages } from '../../../shared/messages';
import * as IShared from '../../../shared/interface';
import { IFindByDocumentLineStateRequest, IFindByStateRequest } from './interface';

const findByDocumentLineStateQueryKeys: IShared.IRouteRequest<IFindByDocumentLineStateRequest> = {
  document_type_id: joi.number().valid(100, 116).required(),
  document_line_state: joi.string().required(),
};

const findByStateQueryKeys: IShared.IRouteRequest<IFindByStateRequest> = {
  document_type_id: joi.number().integer().positive().required(),
  search_value: joi.string().allow(null).empty('').optional(),
};
export const findByStateQuery = joi.object().keys(findByStateQueryKeys).messages(messages);

export const findByDocumentLineStateQuery = joi
  .object()
  .keys(findByDocumentLineStateQueryKeys)
  .messages(messages);
