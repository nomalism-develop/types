import * as IShared from '../../../shared/interface';

export interface IFindByDocumentLineStateRequest {
  document_type_id: 100 | 116;
  document_line_state: string;
}

export interface IFindByDocumentLineStateResponse {
  document_header_id: string;
  document_number: string;
  identifier: string | null;
  created_at: Date;
  created_by: string;
  who_handles: string | null;
  tags: string | null;
  upfront_total: number;
  unread: boolean;
}

export interface IFindByStateRequest {
  document_type_id: number;
  search_value?: string | null;
}

export interface IFindByStateResponse {
  name: string;
  count: number;
  unread: boolean;
}

export interface IFindSideMenuCountersResponse {
  counts: Record<string, number>;
  timestamp: string;
}
interface IRepository {
  findSideMenuStoreOperators(): Promise<string[]>;

  findByState(data: IFindByStateRequest): Promise<IFindByStateResponse[]>;

  findByDocumentLineState(
    params: IFindByDocumentLineStateRequest,
  ): Promise<IFindByDocumentLineStateResponse[]>;

  findSideMenuCounters(): Promise<IFindSideMenuCountersResponse>;
}

export type IController = IShared.IEntityWithUserToken<IRepository>;
export {};
