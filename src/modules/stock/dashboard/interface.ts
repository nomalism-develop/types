export const Route = 'dashboard';
export const UpperName = 'Dashboard';
export const LowerName = UpperName[0].toLowerCase() + UpperName.substring(1);

export type IDashboardNames = 'dia' | 'mes' | 'ano';

export interface IData {
  chave: number;
  faturaRecibo: number;
  fatura: number;
  adiantamento: number;
}

export interface IDashboardResponse {
  id: number;
  name: string;
  data: IData[];
}
export interface IFindDashboardByDate {
  date: Date;
  name: IDashboardNames;
}
export interface IRepository {
  findByDate(selector: IFindDashboardByDate): Promise<IDashboardResponse>;
}
