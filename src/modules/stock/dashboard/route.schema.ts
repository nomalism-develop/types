import joi from 'joi';
import { messages } from '../../../shared/messages';
import * as IShared from '../../../shared/interface';
import { IFindDashboardByDate } from './interface';

// create body validation
const findByDateKeys: IShared.IRouteRequest<IFindDashboardByDate> = {
  date: joi.date().required(),
  name: joi.string().valid('dia', 'mes', 'ano').required(),
};
export const findByDateParams = joi.object().keys(findByDateKeys).messages(messages);
