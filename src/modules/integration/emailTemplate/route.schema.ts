import joi from 'joi';
import { messages } from '../../../shared/messages';
import { ICreateRequest, IUpdateRequest, ISendRequest, ICreateAttachments } from './interfaces';
import * as IShared from '../../../shared/interface';
import { IEmailAddress, IEmailAttachment } from '../emailLog/interfaces';

const createBodyKeys: IShared.IRouteRequest<ICreateRequest> = {
  name: joi.string().required(),
  to: joi.string().required(),
  subject: joi.string().required(),
  body: joi.string().required(),
  attachments: joi
    .array()
    .items(
      joi.object<ICreateAttachments>().keys({
        multimedia_id: joi.string().uuid().required(),
        name: joi.string().required(),
      }),
    )
    .required(),
};

export const createBody = joi.object().keys(createBodyKeys).messages(messages);

const updateBodyKeys: IShared.IRouteRequest<IUpdateRequest> = {
  name: joi.string().optional(),
  to: joi.string().optional(),
  subject: joi.string().optional(),
  body: joi.string().optional(),
};

export const updateBody = joi.object().keys(updateBodyKeys).messages(messages);

const sendBodyKeys: IShared.IRouteRequest<ISendRequest> = {
  owner_id: joi.string().uuid().allow(null).required(),
  from: joi
    .object<IEmailAddress>()
    .keys({
      name: joi.string().allow('', null).default('').required(),
      address: joi
        .string()
        .email({ tlds: { allow: false } })
        .required(),
    })
    .required(),
  to: joi
    .array()
    .items(
      joi.object<IEmailAddress>().keys({
        name: joi.string().allow('', null).default('').required(),
        address: joi
          .string()
          .email({ tlds: { allow: false } })
          .required(),
      }),
    )
    .required(),
  cc: joi
    .array()
    .items(
      joi.object<IEmailAddress>().keys({
        name: joi.string().allow('', null).default('').required(),
        address: joi
          .string()
          .email({ tlds: { allow: false } })
          .required(),
      }),
    )
    .required(),
  bcc: joi
    .array()
    .items(
      joi.object<IEmailAddress>().keys({
        name: joi.string().allow('', null).default('').required(),
        address: joi
          .string()
          .email({ tlds: { allow: false } })
          .required(),
      }),
    )
    .required(),
  subject: joi.string().required(),
  html: joi.string().required(),
  attachments: joi
    .array()
    .items(
      joi.object<IEmailAttachment>().keys({
        filename: joi.string().required(),
        path: joi.string().required(),
      }),
    )
    .required(),
};

export const sendBody = joi.object().keys(sendBodyKeys).messages(messages);
