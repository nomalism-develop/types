import * as IShared from '../../../shared/interface';
import { EmailTemplate, EmailTemplateAttachment } from '../../../shared/entities';
import { IEmailAddress, IEmailAttachment } from '../emailLog/interfaces';

export type Entity = EmailTemplate;
export const Route = 'email_template';
export const UpperName = 'EmailTemplate';
export const LowerName = UpperName[0].toLowerCase() + UpperName.substring(1);

export interface IEntityExtended extends Entity {
  attachments: EmailTemplateAttachment[];
}

export interface ICreateAttachments {
  multimedia_id: string;
  name: string;
}

export interface ICreateRequest {
  name: string;
  to: string;
  subject: string;
  body: string;
  attachments: ICreateAttachments[];
}

export interface IUpdateRequest {
  name?: string;
  to?: string;
  subject?: string;
  body?: string;
}

export interface ISendRequest {
  owner_id: string | null;
  from: IEmailAddress;
  to: IEmailAddress[];
  cc: IEmailAddress[];
  bcc: IEmailAddress[];
  subject: string;
  html: string;
  attachments: IEmailAttachment[];
}

export interface IRepository {
  find(): Promise<IEntityExtended[]>;
  create(data: ICreateRequest): Promise<string>;
  update(selector: IShared.IFindByIdRequest, data: IUpdateRequest): Promise<void>;
  deleteOne(selector: IShared.IFindByIdRequest): Promise<void>;
  send(data: ISendRequest): Promise<void>;
}

export type IController = IShared.IEntityWithUserToken<IRepository>;

export type IApi = IRepository;
