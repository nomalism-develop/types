import joi from 'joi';
import { messages } from '../../../shared/messages';

import { ICreateRequest, IUpdateRequest } from './interfaces';
import * as IShared from '../../../shared/interface';

export const createBodyKeys: IShared.IRouteRequest<ICreateRequest> = {
  name: joi.string().required(),
};

export const createBody = joi.object().keys(createBodyKeys).messages(messages);

export const updateBodyKeys: IShared.IRouteRequest<IUpdateRequest> = {
  name: joi.string().required(),
};

export const updateBody = joi.object().keys(updateBodyKeys).messages(messages);
