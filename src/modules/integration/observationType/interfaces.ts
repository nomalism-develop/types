import * as IShared from '../../../shared/interface';
import { ObservationType } from '../../../shared/entities';

export type Entity = ObservationType;
export const Route = 'observationType';
export const UpperName = 'ObservationType';
export const LowerName = UpperName[0].toLowerCase() + UpperName.substring(1);

export interface IFindByQueryRequest {
  id?: string;
  name?: string;
}

export interface ICreateRequest {
  name: string;
}

export interface IUpdateRequest {
  name: string;
}

export interface IRepository {
  // test
  findById(selector: IShared.IFindByIdRequest): Promise<Entity | null>;
  findByQuery(data: IFindByQueryRequest): Promise<Entity[]>;

  // rest
  findMinified(params?: IShared.IFindMinifiedRequest): Promise<IShared.IFindMinifiedResponse[]>;
  find(): Promise<ObservationType[]>;
  create(data: ICreateRequest): Promise<Entity>;
  update(selector: IShared.IFindByIdRequest, data: IUpdateRequest): Promise<Entity | null>;
  deleteOne(selector: IShared.IFindByIdRequest): Promise<Entity | null>;
}

export type IController = IShared.IEntityWithUserToken<IRepository>;

export type IApi = Omit<IRepository, 'findById' | 'findByQuery'>;
