import { ProjectInfo } from '../../../shared/entities';

export type Entity = ProjectInfo;
export const Route = 'project_info';
export const UpperName = 'ProjectInfo';
export const LowerName = UpperName[0].toLowerCase() + UpperName.substring(1);

export const IProjectInfoTypeEnum: {
  particular: 'particular';
  professional: 'professional';
  company: 'company';
} = {
  particular: 'particular',
  professional: 'professional',
  company: 'company',
};

export type IProjectInfoType = (typeof IProjectInfoTypeEnum)[keyof typeof IProjectInfoTypeEnum];

export const projectInfoTypes = Object.keys(IProjectInfoTypeEnum);

export type ISendEmail = Omit<Entity, 'created_at' | 'updated_at' | 'document_header_id' | 'id'>;

export interface IController {
  sendEmail(data: ISendEmail): Promise<void>;
}
