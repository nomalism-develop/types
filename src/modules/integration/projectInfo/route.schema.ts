import joi from 'joi';
import { messages } from '../../../shared/messages';
import { ISendEmail } from './interfaces';
import * as IShared from '../../../shared/interface';

const senEmailBodyKeys: IShared.IRouteRequest<ISendEmail> = {
  name: joi.string().required(),
  description: joi.string().required(),
  telephone: joi.string().required(),
  type: joi.string().valid('particular', 'professional', 'company'),
  email: joi
    .string()
    .trim(true)
    .lowercase()
    .email({ tlds: { allow: false } }),
};

export const sendEmailBody = joi.object().keys(senEmailBodyKeys).messages(messages);
