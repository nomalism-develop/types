import * as IShared from '../../../shared/interface';
import { GoogleFilePermission } from '../../../shared/entities';

export type Entity = GoogleFilePermission;
export const Route = 'google_file_permission';
export const UpperName = 'GoogleFilePermission';
export const LowerName = UpperName[0].toLowerCase() + UpperName.substring(1);

export type IFindRequest = Record<string, unknown>;

export interface ICreateRequest {
  file_id: string;
  email: string;
}

export interface ICreateRequestRepository extends ICreateRequest {
  permission_id: string;
}

export interface IController {
  create(data: ICreateRequest): Promise<Entity>;
  findById(shared: IShared.IFindByIdRequest): Promise<Entity | null>;
  find(): Promise<Entity[]>;
  deleteOne(selector: IShared.IFindByIdRequest): Promise<Entity | null>;
}

export interface IRepository {
  create(data: ICreateRequestRepository): Promise<Entity>;
  findById(shared: IShared.IFindByIdRequest): Promise<Entity | null>;
  find(): Promise<Entity[]>;
  deleteOne(selector: IShared.IFindByIdRequest): Promise<Entity | null>;
}
