import joi from 'joi';
import { messages } from '../../../shared/messages';
import { ICreateRequest } from './interfaces';

import * as IShared from '../../../shared/interface';

export const createBodyKeys: IShared.IRouteRequest<ICreateRequest> = {
  email: joi
    .string()
    .email({ tlds: { allow: false } })
    .required(),
  file_id: joi.string().required(),
};

export const createBody = joi.object().keys(createBodyKeys).messages(messages);
