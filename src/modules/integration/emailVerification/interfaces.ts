import { EmailVerification } from '../../../shared/entities';
import * as IShared from '../../../shared/interface';

export type Entity = EmailVerification;
export const Route = 'email_verification';
export const UpperName = 'EmailVerification';
export const LowerName = UpperName[0].toLowerCase() + UpperName.substring(1);

export interface IIsVerifiedRequest {
  email: string;
}

export interface ISendVerificationRequest {
  email: string;
}

export interface IMarkAsVerifiedRequest {
  email: string;
  token: string;
}

export interface IRepository {
  isVerified(selector: IIsVerifiedRequest): Promise<boolean>;
  sendVerificationEmail(data: ISendVerificationRequest): Promise<void>;
  markAsVerified(data: IMarkAsVerifiedRequest): Promise<void>;
}

export type IController = IShared.IEntityWithUserToken<IRepository>;

export type IApi = IRepository;
