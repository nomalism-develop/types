import joi from 'joi';
import { messages } from '../../../shared/messages';
import { IIsVerifiedRequest, ISendVerificationRequest, IMarkAsVerifiedRequest } from './interfaces';

export const isVerifiedQuery = joi
  .object<IIsVerifiedRequest>()
  .keys({
    email: joi
      .string()
      .email({ tlds: { allow: false } })
      .required(),
  })
  .messages(messages);

export const sendVerificationBody = joi
  .object<ISendVerificationRequest>()
  .keys({
    email: joi
      .string()
      .email({ tlds: { allow: false } })
      .required(),
  })
  .messages(messages);

export const markAsVerifiedBody = joi
  .object<IMarkAsVerifiedRequest>()
  .keys({
    email: joi
      .string()
      .email({ tlds: { allow: false } })
      .required(),
    token: joi.string().uuid().required(),
  })
  .messages(messages);
