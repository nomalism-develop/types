import * as IShared from '../../../shared/interface';
import { EmailTemplateAttachment } from '../../../shared/entities';

export type Entity = EmailTemplateAttachment;
export const Route = 'email_template_attachment';
export const UpperName = 'EmailTemplateAttachment';
export const LowerName = UpperName[0].toLowerCase() + UpperName.substring(1);

export interface ICreateRequest {
  email_template_id: string;
  multimedia_id: string;
  name: string;
}

export interface IRepository {
  create(data: ICreateRequest): Promise<string>;
  deleteOne(selector: IShared.IFindByIdRequest): Promise<void>;
}

export type IController = IShared.IEntityWithUserToken<IRepository>;

export type IApi = IRepository;
