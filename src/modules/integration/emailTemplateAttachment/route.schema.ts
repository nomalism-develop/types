import joi from 'joi';
import { messages } from '../../../shared/messages';
import { ICreateRequest } from './interfaces';
import * as IShared from '../../../shared/interface';

const createBodyKeys: IShared.IRouteRequest<ICreateRequest> = {
  email_template_id: joi.string().uuid().required(),
  multimedia_id: joi.string().uuid().required(),
  name: joi.string().required(),
};

export const createBody = joi.object().keys(createBodyKeys).messages(messages);
