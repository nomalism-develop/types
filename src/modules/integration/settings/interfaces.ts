import * as IShared from '../../../shared/interface';
import { Settings } from '../../../shared/entities';

export type Entity = Settings;
export const Route = 'settings';
export const UpperName = 'Settings';
export const LowerName = UpperName[0].toLowerCase() + UpperName.substring(1);

export interface IFindByKeyRequest {
  key: string;
}

export interface ICreateRequest {
  key: string;
  value: string;
}

export interface IUpdateRequest {
  key: string;
  value: string;
}

export interface IRepository {
  find(): Promise<Entity[]>;
  findByKey(selector: IFindByKeyRequest): Promise<Entity | null>;
  create(data: ICreateRequest): Promise<Entity>;
  update(data: IUpdateRequest): Promise<Entity | null>;
  deleteOne(selector: IShared.IFindByIdNumberRequest): Promise<Entity | null>;
}

export type IController = IShared.IEntityWithUserToken<IRepository>;
