import joi from 'joi';
import { messages } from '../../../shared/messages';
import { ICreateRequest, IUpdateRequest, IFindByKeyRequest } from './interfaces';
import * as IShared from '../../../shared/interface';

const createBodyKeys: IShared.IRouteRequest<ICreateRequest> = {
  key: joi.string().required(),
  value: joi.string().required(),
};

export const createBody = joi.object().keys(createBodyKeys).messages(messages);

const updateBodyKeys: IShared.IRouteRequest<IUpdateRequest> = {
  key: joi.string().required(),
  value: joi.string().required(),
};

export const updateBody = joi.object().keys(updateBodyKeys).messages(messages);

const findQueryKeys: IShared.IRouteRequest<IFindByKeyRequest> = {
  key: joi.string().required(),
};

export const findQuery = joi.object().keys(findQueryKeys).messages(messages);
