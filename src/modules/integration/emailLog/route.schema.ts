import joi from 'joi';
import { messages } from '../../../shared/messages';
import { IFindRequest, ICheckSent } from './interfaces';

export const findQuery = joi
  .object<IFindRequest>()
  .keys({
    date_start: joi.date().required(),
    date_end: joi.date().required(),
  })
  .messages(messages);

export const checkSentBody = joi
  .object<ICheckSent>()
  .keys({
    owner_ids: joi.array().items(joi.string().uuid().required()).required(),
  })
  .messages(messages);
