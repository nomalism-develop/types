import * as IShared from '../../../shared/interface';
import { EmailLog } from '../../../shared/entities';

export type Entity = EmailLog;
export const Route = 'email_log';
export const UpperName = 'EmailLog';
export const LowerName = UpperName[0].toLowerCase() + UpperName.substring(1);

export interface IFindRequest {
  date_start: Date;
  date_end: Date;
}

export interface IFindResponse {
  id: string;
  template: string;
  created_at: Date;
  to: string;
  subject: string;
  sent: boolean;
  retried: boolean;
  error: string | null;
}

export interface IEmailAddress {
  name: string;
  address: string;
}

export interface IEmailAttachment {
  path: string;
  filename: string;
}

export const SendGridCallbackEventEnum: {
  open: 'open';
  delivered: 'delivered';
  click: 'click';
  processed: 'processed';
  dropped: 'dropped';
  deferred: 'deferred';
  bounce: 'bounce';
} = {
  open: 'open',
  delivered: 'delivered',
  click: 'click',
  processed: 'processed',
  dropped: 'dropped',
  deferred: 'deferred',
  bounce: 'bounce',
};

export type ISendGridCallbackEvent =
  (typeof SendGridCallbackEventEnum)[keyof typeof SendGridCallbackEventEnum];

export const sendGridCallbackEvents = Object.keys(SendGridCallbackEventEnum);

export interface ISendGridCallbacks {
  email: string;
  event: ISendGridCallbackEvent;
  event_time: Date;
  ip: string | null;
  useragent: string | null;
  // event dependent
  reason: string | null;
  type: string | null;
  status: string | null;
  bounce_classification: string | null;
  response: string | null;
  url: string | null;
  attempt: number | null;
}

export interface IFindByIdResponse {
  id: string;
  owner_id: string | null;
  chat_id: string | null;
  template: string;
  created_at: Date;

  sent: boolean;
  retried: boolean;
  accepted: string[];
  rejected: string[];

  from: IEmailAddress;
  to: IEmailAddress[];
  cc: IEmailAddress[];
  bcc: IEmailAddress[];
  subject: string;
  html: string;
  attachments: IEmailAttachment[];

  error: string | null;

  callbacks: ISendGridCallbacks[];
}

export interface ICheckSent {
  owner_ids: string[];
}

export interface IRepository {
  find(params: IFindRequest): Promise<IFindResponse[]>;
  findById(selector: IShared.IFindByIdRequest): Promise<IFindByIdResponse>;
  checkSent(data: ICheckSent): Promise<boolean[]>;
}

export type IApi = IRepository;
