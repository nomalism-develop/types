import joi from 'joi';
import { messages } from '../../../shared/messages';
import {
  ICreateRequest,
  IUpdateRequest,
  IFindByOwnersRequest,
  IFindByOwnerRequest,
} from './interfaces';
import * as IShared from '../../../shared/interface';

const createBodyKeys: IShared.IRouteRequest<ICreateRequest> = {
  owner_id: joi.string().uuid().required(),
  observation_type_id: joi.string().uuid().allow(null).optional(),
  description: joi.string().required(),
  internal: joi.boolean().default(false).optional(),
};

export const createBody = joi.object().keys(createBodyKeys).messages(messages);

const updateBodyKeys: IShared.IRouteRequest<IUpdateRequest> = {
  observation_type_id: joi.string().uuid().allow(null).optional(),
  description: joi.string().optional(),
  internal: joi.boolean().optional(),
};

export const updateBody = joi.object().keys(updateBodyKeys).messages(messages);

const findByOwnersBodyKeys: IShared.IRouteRequest<IFindByOwnersRequest> = {
  owner_ids: joi.array().items(joi.string().uuid().required()).required(),
  includeInternal: joi.boolean().optional(),
};

export const findByOwnersBody = joi.object().keys(findByOwnersBodyKeys).messages(messages);

const findByOwnerQueryKeys: IShared.IRouteRequest<IFindByOwnerRequest> = {
  owner_id: joi.string().uuid().required(),
  includeInternal: joi.boolean().optional(),
};

export const findByOwnerQuery = joi.object().keys(findByOwnerQueryKeys).messages(messages);
