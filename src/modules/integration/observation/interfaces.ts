import * as IShared from '../../../shared/interface';
import { Observation, ObservationType } from '../../../shared/entities';

export type Entity = Observation;
export const Route = 'observation';
export const UpperName = 'Observation';
export const LowerName = UpperName[0].toLowerCase() + UpperName.substring(1);

export interface ICreateRequest {
  owner_id: string;
  observation_type_id?: string | null;
  description: string;
  internal: boolean;
}

export interface IUpdateRequest {
  observation_type_id?: string | null;
  description?: string;
  internal?: boolean;
}

export interface IFindByQueryRequest {
  owner_id?: string;
  observation_type_id?: string;
  description?: string;
  internal?: boolean;
}

export interface IFindByOwnerRequest {
  owner_id: string;
  includeInternal?: boolean;
}

export interface IFindByOwnersRequest {
  owner_ids: string[];
  includeInternal?: boolean;
}

export interface IFindByOwnerResponse extends Entity {
  observation_type: ObservationType | null;
}

export interface IRepository {
  // test
  findById(selector: IShared.IFindByIdRequest): Promise<Entity | null>;
  findByQuery(data: IFindByQueryRequest): Promise<Entity[]>;

  // rest
  findByOwnerId(params: IFindByOwnerRequest): Promise<IFindByOwnerResponse[]>;
  findByOwnerIds(params: IFindByOwnersRequest): Promise<IFindByOwnerResponse[]>;
  create(data: ICreateRequest): Promise<IFindByOwnerResponse>;
  update(
    selector: IShared.IFindByIdRequest,
    data: IUpdateRequest,
  ): Promise<IFindByOwnerResponse | null>;
  deleteOne(selector: IShared.IFindByIdRequest): Promise<Entity | null>;
}

export type IController = IShared.IEntityWithUserToken<IRepository>;

export type IApi = Omit<IRepository, 'findById' | 'findByQuery'>;
