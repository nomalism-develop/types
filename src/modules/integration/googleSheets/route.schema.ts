import joi from 'joi';
import { messages } from '../../../shared/messages';
import { ICloneRequest, IUpdateRequest, IGetUserData, IExportRequest } from './interfaces';
import * as IShared from '../../../shared/interface';

const cloneQueryKeys: IShared.IRouteRequest<ICloneRequest> = {
  document_name: joi.string().allow(null).optional(),
};

export const cloneQuery = joi.object().keys(cloneQueryKeys).messages(messages);

const exportQueryKeys: IShared.IRouteRequest<IExportRequest> = {
  document_header_id: joi.string().uuid().required(),
  spreadsheetId: joi.string().required(),
};

export const exportQuery = joi.object().keys(exportQueryKeys).messages(messages);

const userData: IShared.IRouteRequestWithStamps<IGetUserData> = {
  document_number: joi.number().optional(),
  emission_date: joi.date().optional(),
  owner_number: joi.number().optional(),
  contact_persona_name: joi.string().optional(),
  contact_persona_email: joi.string().trim(true).lowercase().optional(),
  contact_persona_telephone: joi.string().optional(),
  billing_persona_nif: joi.string().optional(),
  billing_persona_name: joi.string().optional(),
  billing_persona_email: joi.string().trim(true).lowercase().optional(),
  billing_persona_telephone: joi.string().optional(),
  billing_persona_address_street: joi.string().optional(),
  billing_persona_address_locality: joi.string().optional(),
  billing_persona_address_postal_code: joi.string().trim(true).lowercase().optional(),
  billing_persona_address_country_name: joi.string().optional(),
  delivery_persona_name: joi.string().optional(),
  delivery_persona_email: joi.string().trim(true).lowercase().optional(),
  delivery_persona_telephone: joi.string().optional(),
  delivery_persona_address_street: joi.string().optional(),
  delivery_persona_address_locality: joi.string().optional(),
  delivery_persona_address_postal_code: joi.string().trim(true).lowercase().optional(),
  delivery_persona_address_country_name: joi.string().optional(),
  reason_for_exemption_name: joi.string().required().allow(null),
  store_operator_id: joi.string().required().allow(null),
  store_operator_name: joi.string().required().allow(null),
  store_operator_number: joi.number().required().allow(null),
};

const updateQueryKeys: IShared.IRouteRequest<IUpdateRequest> = {
  document_name: joi.string().required(),
  client_data: joi.object().keys(userData).optional(),
};

export const updateQuery = joi.object().keys(updateQueryKeys).messages(messages);
