import { DocumentHeader, Persona } from '../../../shared/entities';
import * as IShared from '../../../shared/interface';

export const Route = 'google';
export const UpperName = 'Google';
export const LowerName = UpperName[0].toLowerCase() + UpperName.substring(1);

export interface IGetUserData
  extends Pick<
    DocumentHeader,
    | 'document_number'
    | 'owner_number'
    | 'billing_persona_nif'
    | 'billing_persona_name'
    | 'billing_persona_email'
    | 'billing_persona_telephone'
    | 'billing_persona_address_street'
    | 'billing_persona_address_locality'
    | 'billing_persona_address_postal_code'
    | 'billing_persona_address_country_name'
    | 'delivery_persona_name'
    | 'delivery_persona_email'
    | 'delivery_persona_telephone'
    | 'delivery_persona_address_street'
    | 'delivery_persona_address_locality'
    | 'delivery_persona_address_postal_code'
    | 'delivery_persona_address_country_name'
  > {
  emission_date: string;
  store_operator_id: string | null;
  store_operator_name: string | null;
  store_operator_number: number | null;
  reason_for_exemption_name: string | null;
  contact_persona_name: Persona['name'];
  contact_persona_email: Persona['email'];
  contact_persona_telephone: Persona['telephone'];
}

export interface IUpdateRequest {
  document_name: string;
  client_data?: IGetUserData;
}
export interface ICloneRequest {
  document_name: string;
}

export interface ICloneResponse {
  spreadsheetId: string;
}

export type IGetProductGroup =
  | 'cortinado'
  | 'tapete'
  | 'estore'
  | 'estofo'
  | 'almofada'
  | 'outros'
  | 'calhas&varões'
  | 'papel'
  | 'varoes'
  | 'terceiros';

export type IGetProductType =
  | 'confecção'
  | 'calha'
  | 'tecido 1'
  | 'tecido 2'
  | 'varoes'
  | 'livre'
  | 'papel';

export type IReferenceType = 'noma' | 'sheets' | undefined;

export interface IGetProduct {
  index: number; // line index in sheet's config tab
  group: IGetProductGroup;
  type: IGetProductType;
  quantity: number;
  reference: string;
  designation: string;
  discount: number;
  price_sale: number;
  price_cost: number;
  reference_type?: IReferenceType;
}
export interface IGetDataResponse {
  products: IGetProduct[];
}

export interface IExportRequest {
  spreadsheetId: string;
  document_header_id: string;
}

export interface IController {
  clone(params: IShared.IFindByIdRequest, data?: ICloneRequest): Promise<ICloneResponse>;
  export(params: IExportRequest): Promise<string>;
  getData(params: IShared.IFindByIdRequest): Promise<IGetDataResponse>;
  update(params: IShared.IFindByIdRequest, data: IUpdateRequest): Promise<void>;
}
