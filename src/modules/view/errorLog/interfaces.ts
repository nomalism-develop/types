export const Route = 'error_handler';
export const UpperName = 'ErrorHandler';
export const LowerName = UpperName[0].toLowerCase() + UpperName.substring(1);

export interface ICreateRequest {
  service: string;
  type: 'prisma' | 'unhandled' | 'axios' | 'webapp' | 'moloni' | 'kafka';
  error: { name: string; message: string; stack?: string };
  request?: { body?: unknown; query?: unknown; params?: unknown };
}

export interface IRepository {
  create(data: ICreateRequest): Promise<void>;
}
