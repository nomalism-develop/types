import joi from 'joi';
import { messages } from '../../../shared/messages';
import * as IShared from '../../../shared/interface';
import { ICreateRequest } from './interfaces';

const createBodyKeys: IShared.IRouteRequest<ICreateRequest> = {
  error: joi.object({
    name: joi.string().required(),
    message: joi.string().required(),
    stack: joi.string().optional(),
  }),
  request: joi.object({
    body: joi.any().optional(),
    query: joi.any().optional(),
    params: joi.any().optional(),
  }),

  service: joi.string().required(),
  type: joi.string().valid('prisma', 'unhandled', 'axios', 'webapp'),
};

export const createBody = joi.object().keys(createBodyKeys).messages(messages);
