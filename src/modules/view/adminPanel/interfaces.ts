export const Route = 'admin_panel';
export const UpperName = 'AdminPanel';
export const LowerName = UpperName[0].toLowerCase() + UpperName.substring(1);

// UTILS
interface ICalendar {
  year: number;
  month: number;
  day: number;
}

// SERVER
interface IServiceStatus {
  tier: 'high' | 'mid' | 'low';
  service: string;
  color: 'green' | 'red';
  description: string;
}

interface IServiceLogs {
  name: string;
  description: string;
}

interface IPostgresBackup extends ICalendar {
  done: boolean;
  service: string;
  size: number;
}

// GOOGLE SHEETS
interface IPropostaImport {
  done: boolean;
  error: string | null;
  lines: number;
  groups: string;
  duration: number;
  created_at: Date;
}

interface IProposta {
  label: string;
  url: string;
  last_import: string;
  imports: IPropostaImport[];
}

interface ITask extends ICalendar {
  count: number;
  auto: boolean;
}

interface IStock {
  created_at: Date;
  inserts: number;
  updates: number;
  deletes: number;
  error: string | null;
}

interface IEncomenda extends ICalendar {
  finds: number;
  misses: number;
  price_updates: number;
  new_products: number;
  pre_artigo_count: number;
  error_count: number;
}

// OTHER SERVICES
interface IPrint extends ICalendar {
  label: string;
  completed: number;
  undone: number;
  unsent: number;
}

interface IEventstore extends ICalendar {
  label: string;
  count: number;
}

interface IMoloni extends ICalendar {
  label: string;
  count: number;
  error: number;
}

interface IMoloniError {
  label: string;
  url: string;
  stage: string;
}

interface IProjectInfo extends ICalendar {
  label: string;
  email_sent: boolean;
  document_header_created: boolean;
}

interface IMultimedia extends ICalendar {
  uploads: number;
  size: number;
}

interface IMultimediaSummary {
  total: number;
  obsolete: number;
}

interface ICron extends ICalendar {
  label: string;
  auto: boolean;
  avg_time_to_complete: number;
  count: number;
  error_count: number;
}

interface IEmail extends ICalendar {
  label: string;
  count: number;
  errors: number;
  events: number;
}

interface IError extends ICalendar {
  label: string;
  count: number;
  description: string;
}

interface IChat extends ICalendar {
  count: number;
  processed: number;
  delivered: number;
}

// GLOBAL
export interface IGetDataResponse {
  server: {
    services: IServiceStatus[];
    logs: IServiceLogs[];
    certificates: string;
    filesystem: string;
    backups: IPostgresBackup[];
  };
  googleSheets: {
    propostas: IProposta[];
    tasks: ITask[];
    stock: IStock[];
    encomenda: IEncomenda[];
  };
  services: {
    prints: IPrint[];
    eventstores: IEventstore[];
    moloniRequests: IMoloni[];
    moloniErrors: IMoloniError[];
    projectInfos: IProjectInfo[];
    multimedias: IMultimedia[];
    multimediaSummary: IMultimediaSummary;
    crons: ICron[];
    emails: IEmail[];
    errors: IError[];
    chats: IChat[];
  };
}

export interface IRepository {
  getData(): Promise<IGetDataResponse>;
}
