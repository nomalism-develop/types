import joi from 'joi';
import { IFindByIdRequest } from './interfaces';
import * as IShared from '../../../shared/interface';
import { messages } from '../../../shared/messages';

const findByIdQueryKeys: IShared.IRouteRequest<IFindByIdRequest> = {
  download: joi.boolean().optional(),
};
export const findByIdQuery = joi.object().keys(findByIdQueryKeys).messages(messages);
