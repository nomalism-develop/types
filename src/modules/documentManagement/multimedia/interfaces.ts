import { Multimedia } from '../../../shared/entities';

export type Entity = Multimedia;
export const Route = 'multimedia';
export const UpperName = 'Multimedia';
export const LowerName = UpperName[0].toLowerCase() + UpperName.substring(1);

export interface IFindByIdRequest {
  download?: boolean;
}

export interface IRepository {
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  create(multipartFormData: any, headers?: any): Promise<Entity>;
  findById(multimedia_id: string): Promise<Entity | null>;
}

export interface IController extends Omit<IRepository, 'findById'> {
  findById(multimedia_id: string, params: IFindByIdRequest): Promise<void>;
}

export type IApi = Omit<IRepository, 'findById'>;
