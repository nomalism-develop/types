import * as IShared from '../../../shared/interface';
import { StartDocumentHeaderLastUpdateVirtuals } from '../../../shared/entities';

export type Entity = StartDocumentHeaderLastUpdateVirtuals;
export const Route = 'start_document_header_last_update';
export const UpperName = 'StartDocumentHeaderLastUpdate';
export const LowerName = UpperName[0].toLowerCase() + UpperName.substring(1);

export interface IRepository {
  findById(
    selector: IShared.IFindByIdRequest,
  ): Promise<StartDocumentHeaderLastUpdateVirtuals | null>;
}

export type IController = IRepository;
