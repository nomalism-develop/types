import joi from 'joi';
import { messages } from '../../../shared/messages';
import * as IShared from '../../../shared/interface';
import {
  ICreateRequest,
  IPrintLabelParamsRequest,
  IPrintLabelQueryRequest,
  IUpdateDataRequest,
  IUpdateManyRequest,
  IUpdateRequest,
} from './interfaces';

const createBodyKeys: IShared.IRouteRequest<ICreateRequest> = {
  product_id: joi.string().uuid().allow(null).optional(),
  // user to merge lines
  document_line_id: joi.string().uuid().optional(),
  // assoc specific
  document_header_id: joi.string().uuid().required(),
  // index: joi.number().integer().default(0).optional(),
  quantity: joi.number().positive().allow(0).default(0).required(),
  preco_venda: joi.number().positive().allow(0).default(0).required(),
  client_tax: joi.number().positive().allow(0).required(),
  client_discount: joi.number().positive().allow(0).default(0).max(100).required(),
  preco_custo: joi.number().positive().allow(0).default(0).required(),
  provider_tax: joi.number().positive().allow(0).required(),
  provider_discount: joi.number().positive().allow(0).default(0).max(100).required(),
  document_line_group_id: joi.string().optional(),
  data: joi
    .object()
    .pattern(
      joi.string(),
      joi.object().keys({
        value: joi.any().required(),
      }),
    )
    .allow({}),
};

export const createBody = joi.array().items(joi.object().keys(createBodyKeys)).messages(messages);

const updateBodyKeys: IShared.IRouteRequest<IUpdateRequest> = {
  product_id: joi.string().uuid().allow(null).optional(),

  quantity: joi.number().positive().allow(0).optional(),
  preco_venda: joi.number().positive().allow(0).optional(),
  client_tax: joi.number().positive().allow(0).optional(),
  client_discount: joi.number().positive().allow(0).max(100).optional(),
  preco_custo: joi.number().positive().allow(0).optional(),
  provider_tax: joi.number().positive().allow(0).optional(),
  provider_discount: joi.number().positive().allow(0).max(100).optional(),
  provider_purchase_discount: joi.number().positive().allow(0).max(100).optional(),
  document_line_group_id: joi.string().optional(),
  sent_to_client: joi.boolean().optional(),
  sent_to_provider: joi.boolean().optional(),
};

export const updateBody = joi.object().keys(updateBodyKeys).messages(messages);

const updateDataBodyKeys: IShared.IRouteRequest<IUpdateDataRequest> = {
  trigger_states: joi.boolean().optional(),
  document_header_id: joi.string().uuid().required(),
  document_line_ids: joi.array().items(joi.string().uuid().required()).required(),
  datafield: joi.string().required(),
  payload: joi
    .object()
    .keys({
      value: joi.any().required(),
      updated_by: joi.string().uuid().optional(),
      updated_at: joi.date().optional(),
      location_id: joi.string().uuid().optional(),
    })
    .required(),
};

export const updateDataBody = joi.object().keys(updateDataBodyKeys).messages(messages);

const updateManyKeys: IShared.IRouteRequest<IUpdateManyRequest> = {
  ids: joi.array().items(joi.string().uuid().required()).required(),
  client_discount: joi.number().positive().allow(0).max(100).optional(),
  price_cost: joi.number().positive().allow(0).optional(),
  provider_discount: joi.number().positive().allow(0).max(100).optional(),
  provider_estimated_delivery_date: joi.date().allow(null, '').optional(),
  provider_purchase_discount: joi.number().positive().allow(0).max(100).optional(),
};

export const updateManyBody = joi.object().keys(updateManyKeys).messages(messages);

export const updateIndexesBody = joi
  .array()
  .items(joi.string().uuid().required())
  .required()
  .messages(messages);

export const deleteBody = joi.array().items(joi.string().uuid().required()).messages(messages);

const printLabelParamsKeys: IShared.IRouteRequest<IPrintLabelParamsRequest> = {
  document_line_ids: joi.string().required(),
};

export const printLabelParams = joi.object().keys(printLabelParamsKeys).messages(messages);

const printLabelQueryKeys: IShared.IRouteRequest<IPrintLabelQueryRequest> = {
  token: joi.string().required(),
};

export const printLabelQuery = joi.object().keys(printLabelQueryKeys).messages(messages);
