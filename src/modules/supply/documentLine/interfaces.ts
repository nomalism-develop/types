import * as IShared from '../../../shared/interface';
import {
  DocumentLine,
  DocumentLineVirtuals,
  DocumentLinePendingConflict,
  DocumentLineBaseVirtuals,
  ProductPromotionVirtuals,
  DocumentLineNote,
} from '../../../shared/entities';
import * as IDocumentLineAssoc from '../documentLineAssoc/interfaces';
import { IDocumentTypeCodeType, IDocumentTypeUserType } from '../documentType/interfaces';
import { IProductType } from '../../stock/productGoogleSheets/interface';

export type Entity = DocumentLine;
export const Route = 'documentLine';
export const UpperName = 'DocumentLine';
export const LowerName = UpperName[0].toLowerCase() + UpperName.substring(1);

export type IDocumentLineVirtuals = DocumentLineVirtuals;
export type IDocumentLineBaseVirtuals = DocumentLineBaseVirtuals;

export type IPromotions = Pick<
  ProductPromotionVirtuals,
  'exclusive_to_location_id' | 'exclusive_to_client_id' | 'discount'
>;

export type IDataKeys =
  // aviso cliente
  | 'AvisoCliente'
  // entrada de material
  | 'Quantidade Encomendada Originalmente' // Usado na entrada de material quando a quantidade de entrada é diferente da encomendada e o utilizador marca como conforme. Nesse caso a quantidade da encomenda é alterada para coincidir com a quantidade de entrada e a quantidade original encomendada é guardada nesta chave para histórico
  | 'Linha criada na EM' // Linha que foi criada na entrada de material resultante da desdobragem ou chegada inesperada
  | 'Entrada de Material' // Parcela deu entrada com alterações na quantidade inicialmente encomendada (várias peças ou quantidade diferença)
  | 'Opcao EM' // Usado para todas as seleções de entrada de material
  // devoluções a fornecedor
  | 'Levantar DF' // Parcela presa, marcada para devolução, dada como levantada
  | 'Enviar Email NCF' // DESCONTINUAR: Parcela presa, marcada para devolução, enviado email ao fornecedor a pedir emissão de nota de crédito
  | 'DF Rejeitada' // Parcela presa, marcada para devolução, devolvida a stock
  | 'Fechar DF' // DESCONTINUAR: Parcela presa, marcada para devolução, fechada
  // regularizações
  | 'Documento pago' // Usado nas linhas da NPF que regularizam documentos não-legados
  // notas de crédito de cliente
  | 'Devolucao Cliente' // Parcela re-entrou por motivos de devolução de cliente, a nova linha tem esta chave para guardar informação da fatura de origem
  // recibo
  | 'Recibo do documento' // Usado por linhas de recibo para indicar a fatura relacionada
  // estado de linha
  | 'Cancelar' // Linha cancelada
  // wildcard provider
  | 'OriginalProductId' // Usado quando é associado um fornecedor a um artigo de fornecedor indefinido e caso seja necessário voltar atrás
  // sheets
  | 'imported_from_sheets' // Usado para indicar que linha foi importada do sheets
  | 'sheets_line_position' // Usado para indicar a posição da linha no sheets
  // moloni retry
  | 'Emitir' // Foi solicitada a emissão de documento externo manualmente
  | 'Backup PDF' // Foi solicitado o backup do PDF externo manualmente
  | 'Conferir' // Conferir adjudicação para efetuar adiantamento antes de encomenda de fornecedor
  | 'Conferir Encomenda' // Informações durante a conferência de encomenda de fornecedor: Espera Info Cliente ou interna, Espera Nomalism, etc
  | 'Continuar' // Conferência de encomenda de fornecedor
  // PHC
  | 'Saldos Migrados' // Usado para indicar que linha foi importada de PHC
  | 'Estado PHC' // Usado para importar o estado do PHC à data da importação
  | 'Pagamento' // Usado nas linhas da NPF que regularizam documentos PHC
  | 'PHC origin_id' // Usado para o id da linha do PHC
  | 'PHC ef_stamp'; // Usado para o id da linha de encomenda de fornecedor do PHC

export interface IDataPayload {
  value: unknown;
  updated_at: Date;
  updated_by: string;
  location_id: string;
  history?: IDataPayload[];
}

export type IData = { [K in IDataKeys]?: IDataPayload };

export interface IDataDocumentoPago {
  name: string;
  dh_id: string;
  value: number;
  discount_value: number;
}

export interface IDataPagamento {
  name: string;
  cca_id?: string;
  pca_id?: string;
  value: number;
  discount_value: number;
}

export interface IDocumentLineDocuments {
  code: IDocumentTypeCodeType;
  user_type: IDocumentTypeUserType;
  start_document_header_id?: string;
  pdf_link?: string;
  external_url?: string;
  label?: string;
  edh_id?: string;
}

export interface IHistory {
  what: string;
  when: string;
  who: string;
}

export interface IEntityExtended extends Omit<DocumentLine, 'data'> {
  group: string;
  data: IHistory[];
  document_headers: IDocumentLineDocuments[];
  pending_conflict: DocumentLinePendingConflict | null;
  document_line_notes: DocumentLineNote[];
  product_type: IProductType | null;
  product_id_provider: number | null;
  product_provider_ref: string | null;
  product_dnt_come_anymore: boolean;
  product_no_discount: boolean;
  product_measure: string | null;
  product_integer_only: boolean;
}

export interface ICreateRequest
  extends Omit<IDocumentLineAssoc.ICreateRequest, 'document_line_id'> {
  document_line_id?: string;

  product_id?: string;
  quantity: number;
  preco_venda: number;
  client_tax: number;
  client_discount: number;
  preco_custo: number;
  provider_tax: number;
  provider_discount: number;
  document_line_group_id?: string;
  data: IData;
}

export interface IUpdateRequest {
  product_id?: string;
  quantity?: number;
  preco_venda?: number;
  client_tax?: number;
  client_discount?: number;
  preco_custo?: number;
  provider_tax?: number;
  provider_discount?: number;
  provider_purchase_discount?: number;
  document_line_group_id?: string;
  sent_to_client?: boolean;
  sent_to_provider?: boolean;
}

export interface IUpdateDataRequest {
  trigger_states?: boolean;

  document_header_id: string;
  document_line_ids: string[];
  datafield: IDataKeys;
  payload: IDataPayload;
}

export interface IUpdateManyRequest {
  ids: string[];
  client_discount?: number;
  price_cost?: number;
  provider_discount?: number;
  provider_estimated_delivery_date?: Date | null;
  provider_purchase_discount?: number;
}

export interface IUpdateManyByDocumentHeaderRequest {
  sent_to_provider?: boolean;
}

export interface IUpdateDataRequestWithoutStamps extends Omit<IUpdateDataRequest, 'payload'> {
  payload: Pick<IDataPayload, 'value'> & {
    updated_by?: string;
  };
}

export interface IUpdateLineStatesTriggerResponse {
  document_header_id: string;
  trigger_event: boolean;
  broker_topic?: IShared.IBrokerTopic;
}

export interface IPrintLabelParamsRequest {
  document_line_ids: string;
}

export interface IPrintLabelQueryRequest {
  token: string;
}

export interface IRepository {
  create(data: ICreateRequest[]): Promise<IDocumentLineAssoc.IEntityExtended[]>;

  update(selector: IShared.IFindByIdRequest, data: IUpdateRequest): Promise<void>;

  updateData(data: IUpdateDataRequest): Promise<IDocumentLineAssoc.IEntityWithDocumentLine[]>;

  updateMany(data: IUpdateManyRequest): Promise<void>;

  updateManyByDocumentHeader(
    selector: IShared.IFindByIdRequest,
    data: IUpdateManyByDocumentHeaderRequest,
  ): Promise<void>;

  updateIndexes(document_line_ids: string[]): Promise<void>;

  deleteMany(document_line_ids: string[]): Promise<void>;
}

export type IController = IShared.IEntityWithUserToken<IRepository>;
