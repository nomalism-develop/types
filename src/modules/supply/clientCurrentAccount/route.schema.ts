import joi from 'joi';
import { messages } from '../../../shared/messages';
import * as IShared from '../../../shared/interface';
import {
  IFindRequest,
  IDevolverRequest,
  IPagarFaturaRequest,
  IBasicSearchRequest,
  IMultipleRegularizations,
  IMultipleRegularizationDocuments,
  IMultipleRegularizationPayments,
} from './interfaces';

const findByOwnerIdBodyKeys: IShared.IRouteRequest<IFindRequest> = {
  per_page: joi.number().integer().positive().default(10).optional(),
  current_page: joi.number().integer().positive().default(1).optional(),
  owner_id: joi.string().uuid().required(),
};
export const findByOwnerIdBodyKeysBody = joi
  .object()
  .keys(findByOwnerIdBodyKeys)
  .messages(messages);

// pagarFatura
const pagarFaturaBodyKeys: IShared.IRouteRequest<IPagarFaturaRequest> = {
  id: joi.string().uuid().required(),
  valor: joi.number().positive().required(),
};
export const pagarFaturaBody = joi.object().keys(pagarFaturaBodyKeys).messages(messages);

// devolver
const devolverBodyKeys: IShared.IRouteRequest<IDevolverRequest> = {
  ids: joi.array().items(joi.string().uuid().required()).required(),
};
export const devolverBody = joi.object().keys(devolverBodyKeys).messages(messages);

// basic search
const basicSearchQueryKeys: IShared.IRouteRequest<IBasicSearchRequest> = {
  search_value: joi.string().allow('', null).optional(),
  per_page: joi.number().positive().integer().default(10).allow(null).optional(),
  inactive: joi.boolean().default(false).optional(),
  current_page: joi.number().positive().integer().allow(null).optional(),
};
export const basicSearchQuery = joi.object().keys(basicSearchQueryKeys).messages(messages);

// regularizar multiplos documentos
const multipleDocumentsBodyKeys: IShared.IRouteRequest<IMultipleRegularizationDocuments> = {
  owner_id: joi.string().uuid().required(),
  document_header_id: joi.string().uuid().required(),
  type: joi.string().valid('credit', 'debit').required(),
  value: joi.number().positive().required(),
};

const multiplePaymentsBodyKeys: IShared.IRouteRequest<IMultipleRegularizationPayments> = {
  value: joi.number().positive().required(),
  payment_method_id: joi.string().uuid().required(),
  payment_method: joi.string().required(),
};
const regularizarMultiplosBodyKeys: IShared.IRouteRequest<IMultipleRegularizations> = {
  documents: joi.array().items(multipleDocumentsBodyKeys).required(),
  payments: joi.array().items(multiplePaymentsBodyKeys).optional(),
};
export const regularizarMultiplosBody = joi
  .object()
  .keys(regularizarMultiplosBodyKeys)
  .messages(messages);
