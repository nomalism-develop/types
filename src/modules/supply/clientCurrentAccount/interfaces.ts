import * as IShared from '../../../shared/interface';
import { ClientCurrentAccount } from '../../../shared/entities';

export type Entity = ClientCurrentAccount;
export const Route = 'client_current_account';
export const UpperName = 'ClientCurrentAccount';
export const LowerName = UpperName[0].toLowerCase() + UpperName.substring(1);

// phc document type enum
export const IPhcDocumentTypeEnum: {
  encomenda_de_cliente: '1|Encomenda de Cliente';
  fatura_ad: '1|Fatura AD';
  factura: '1|Factura';
  factura_re: '9|Fatura RE';
  factura_re_app: '12|Fatura RE APP';
} = {
  encomenda_de_cliente: '1|Encomenda de Cliente',
  fatura_ad: '1|Fatura AD',
  factura: '1|Factura',
  factura_re: '9|Fatura RE',
  factura_re_app: '12|Fatura RE APP',
};
export type IPhcDocumentType = (typeof IPhcDocumentTypeEnum)[keyof typeof IPhcDocumentTypeEnum];
export const IPhcDocumentType = Object.keys(IPhcDocumentTypeEnum);

export const IBasicSearchDocumentTypeEnum: {
  faturaPorDevolver: 'faturaPorDevolver';
  faturaDevolvida: 'faturaDevolvida';
  faturaPorPagar: 'faturaPorPagar';
  faturaPaga: 'faturaPaga';
  adiantamento: 'adiantamento';
  adiantamentoUsado: 'adiantamentoUsado';
  encomendaAberta: 'encomendaAberta';
  encomendaFechada: 'encomendaFechada';
} = {
  faturaPorDevolver: 'faturaPorDevolver',
  faturaDevolvida: 'faturaDevolvida',
  faturaPorPagar: 'faturaPorPagar',
  faturaPaga: 'faturaPaga',
  adiantamento: 'adiantamento',
  adiantamentoUsado: 'adiantamentoUsado',
  encomendaAberta: 'encomendaAberta',
  encomendaFechada: 'encomendaFechada',
};

export type IBasicSearchDocumentType =
  (typeof IBasicSearchDocumentTypeEnum)[keyof typeof IBasicSearchDocumentTypeEnum];
export const IBasicSearchDocumentType = Object.keys(IBasicSearchDocumentTypeEnum);

// find
export type IFindRequest = IShared.IPaginationRequest & IShared.IFindByOwnerIdRequest;

// faturasPorPagar
export interface IFindFaturasPorPagarResponse {
  id: string;
  owner_id: string;
  label: string;
  valor_max_regularizavel: number;
}
export interface IPagarFaturaRequest {
  id: string;
  valor: number;
}

// devoluções
export interface ILinhasFaturasParaDevolucao {
  id: string; // (phc_documents.id)
  reference: string;
  designation: string;
  quantity: number;
  preco_venda: number;
  total: number;
  devolvido: boolean; // (se a linha já foi devolvida ou não)
  desconto: number;
  taxa_iva: number;
  unidade: string;
}
export interface IFindFaturasParaDevolucaoResponse {
  id: string;
  owner_id: string;
  owner_number: number;
  label: string;
  linhas: ILinhasFaturasParaDevolucao[];
}
export interface IDevolverRequest {
  ids: string[];
}

export interface IBasicSearchRequest extends IShared.IPaginationRequest {
  search_value: string | null;
  inactive?: boolean;
}

export interface IBasicSearchResponse {
  id: string;
  owner_id: string;
  document_header_id?: string;
  type: IBasicSearchDocumentType;
  nome: string;
  nif: string;
  email: string;
  telephone: string;
  numero_cliente: string;
  numero_documento: string;
  valor: number;
}

export interface IMultipleRegularizationPayments {
  value: number;
  payment_method_id: string;
  payment_method: string;
}

export type IMultipleRegularizationPaymentsType = 'credit' | 'debit';

export interface IMultipleRegularizationDocuments {
  owner_id: string;
  document_header_id: string;
  type: IMultipleRegularizationPaymentsType;
  value: number;
}

export interface IMultipleRegularizations {
  documents: IMultipleRegularizationDocuments[];
  payments?: IMultipleRegularizationPayments[];
}

export interface IRepository {
  find(params: IShared.IFindByOwnerIdRequest): Promise<Entity[]>;

  findByOwnerId(params: IFindRequest): Promise<IShared.IPaginationResponse<Entity>>;

  findFaturaPorPagar(params: IShared.IFindByIdRequest): Promise<IFindFaturasPorPagarResponse>;

  pagarFatura(params: IPagarFaturaRequest): Promise<void>;

  findFaturaParaDevolver(
    params: IShared.IFindByIdRequest,
  ): Promise<IFindFaturasParaDevolucaoResponse>;

  devolver(params: IDevolverRequest): Promise<void>;

  findByBasicSearch(
    data: IBasicSearchRequest,
  ): Promise<IShared.IPaginationResponse<IBasicSearchResponse>>;

  regularizarMultiplosDocumentos(data: IMultipleRegularizations): Promise<void>;
}

export type IController = IShared.IEntityWithUserToken<IRepository>;

export type IApi = Omit<IRepository, 'find'>;
