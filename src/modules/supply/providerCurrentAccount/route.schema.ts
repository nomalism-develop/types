import joi from 'joi';
import { messages } from '../../../shared/messages';
import * as IShared from '../../../shared/interface';
import { IFindRequest } from './interfaces';

const findByOwnerIdBodyKeys: IShared.IRouteRequest<IFindRequest> = {
  per_page: joi.number().integer().positive().default(10).optional(),
  current_page: joi.number().integer().positive().default(1).optional(),
  owner_id: joi.string().uuid().required(),
};
export const findByOwnerIdBodyKeysBody = joi
  .object()
  .keys(findByOwnerIdBodyKeys)
  .messages(messages);
