import * as IShared from '../../../shared/interface';
import { ProviderCurrentAccount } from '../../../shared/entities';

export type Entity = ProviderCurrentAccount;
export const Route = 'provider_current_account';
export const UpperName = 'ProviderCurrentAccount';
export const LowerName = UpperName[0].toLowerCase() + UpperName.substring(1);

export type IFindRequest = IShared.IPaginationRequest & IShared.IFindByOwnerIdRequest;

export interface IRepository {
  findByOwnerId(params: IFindRequest): Promise<IShared.IPaginationResponse<Entity>>;

  find(params: IShared.IFindByOwnerIdRequest): Promise<Entity[]>;
}

export type IController = IShared.IEntityWithUserToken<IRepository>;

export type IApi = Omit<IRepository, 'find'>;
