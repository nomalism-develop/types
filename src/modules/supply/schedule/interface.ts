import * as IShared from '../../../shared/interface';

export const Route = 'schedule';
export const UpperName = 'Schedule';
export const LowerName = UpperName[0].toLowerCase() + UpperName.substring(1);

export const ScheduleTypeEnum: {
  executeEncomendasStock: 'executeEncomendasStock';
  executeEncomendasVA: 'executeEncomendasVA';
  executeAvisoTotal: 'executeAvisoTotal';
  executeAvisoParcial: 'executeAvisoParcial';
  executeReAvisoTotal: 'executeReAvisoTotal';
  sendErroredEmailsSummary: 'sendErroredEmailsSummary';
  retryErroredEmails: 'retryErroredEmails';
} = {
  executeEncomendasStock: 'executeEncomendasStock',
  executeEncomendasVA: 'executeEncomendasVA',
  executeAvisoTotal: 'executeAvisoTotal',
  executeAvisoParcial: 'executeAvisoParcial',
  executeReAvisoTotal: 'executeReAvisoTotal',
  sendErroredEmailsSummary: 'sendErroredEmailsSummary',
  retryErroredEmails: 'retryErroredEmails',
};

export type IScheduleType = (typeof ScheduleTypeEnum)[keyof typeof ScheduleTypeEnum];

export const scheduleTypes = Object.keys(ScheduleTypeEnum);

export const ScheduleTypeLabel: Record<IScheduleType, string> = {
  executeEncomendasStock: 'Processar encomendas de stock',
  executeEncomendasVA: 'Processar encomendas VA',
  executeAvisoTotal: 'Processar avisos totais',
  executeAvisoParcial: 'Processar avisos parciais',
  executeReAvisoTotal: 'Processar re-avisos totais',
  sendErroredEmailsSummary: 'Enviar sumário de emails c/ erro',
  retryErroredEmails: 'Re-enviar emails c/ erro',
};

export interface IRunImmediatelyRequest {
  scheduleType: IScheduleType;
}

export interface IRepository {
  runImmediately(params: IRunImmediatelyRequest): Promise<void>;
}

export type IController = IShared.IEntityWithUserToken<IRepository>;
