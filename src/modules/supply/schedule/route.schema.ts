import joi from 'joi';

import * as IShared from '../../../shared/interface';
import { messages } from '../../../shared/messages';
import { IRunImmediatelyRequest, scheduleTypes } from './interface';

// find by google sheet id validation
const runImmediatelyKeys: IShared.IRouteRequestWithStamps<IRunImmediatelyRequest> = {
  scheduleType: joi
    .string()
    .valid(...scheduleTypes)
    .required(),
};
export const runImmediately = joi.object().keys(runImmediatelyKeys).messages(messages);
