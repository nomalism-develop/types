import * as IShared from '../../../shared/interface';
import {
  DocumentHeader,
  DocumentHeaderClientVirtuals,
  DocumentHeaderProviderVirtuals,
  DocumentHeaderVirtuals,
  DocumentHeaderBaseVirtuals,
  DocumentType,
  StartDocumentHeaderVirtuals,
  DocumentHeaderClientPaymentVirtuals,
  DocumentLineNote,
} from '../../../shared/entities';

import * as IDocumentType from '../documentType/interfaces';
import * as IPayment from '../payment/interface';

export type Entity = Omit<DocumentHeader, 'external_data'>;
export const Route = 'documentHeader';
export const UpperName = 'DocumentHeader';
export const LowerName = UpperName[0].toLowerCase() + UpperName.substring(1);

export type IDocumentHeaderBaseVirtuals = DocumentHeaderBaseVirtuals;
export type IDocumentHeaderVirtuals = DocumentHeaderVirtuals;
export type IDocumentHeaderClientVirtuals = DocumentHeaderClientVirtuals;
export type IDocumentHeaderProviderVirtuals = DocumentHeaderProviderVirtuals;
export type IStartDocumentHeaderVirtuals = StartDocumentHeaderVirtuals;

export type IDocumentHeaderClientPaymentVirtuals = DocumentHeaderClientPaymentVirtuals;

export interface IFindOneBase extends Omit<Entity, 'external_data'> {
  document_type: IDocumentType.IEntityExtended;
}

export interface IFindOneResponse extends IFindOneBase {
  base_virtual: IDocumentHeaderBaseVirtuals | null;
}

export interface IBasicSearchRequest extends IShared.IPaginationRequest {
  search_value: string | null;
  inactive?: boolean;
}

export interface IBasicSearchResponse {
  document_type_name: string;
  number: number;
  name: string | null;
  user_number: number;
  document_line_count: number;
  document_header_id: string | null;
  start_document_header_id: string | null;
  pdf_link: string | null;
  updated_at: Date;
}

export interface IFindRequest {
  owner_id: string;
  closed?: boolean;
  document_type_id: number;
}
export interface ICreateFromHeaderMaturityDate {
  id: string;
  name: string;
  days: number;
}

export interface ICreateRequest {
  document_type_id: number;

  external_id?: string | null;
  document_set_id: string;
  pdf_link?: string | null;
  external_pdf_link?: string | null;
  google_sheet_id?: string | null;
  external_data?: unknown;
  sent?: boolean;

  owner_id: string;
  owner_number: number;
  owner_name: string;

  reason_for_exemption_id?: string | null;
  reason_for_exemption?: string | null;

  billing_persona_id?: string | null;
  billing_persona_nif?: string | null;
  billing_persona_name?: string | null;
  billing_persona_email?: string | null;
  billing_persona_telephone?: string | null;
  billing_persona_address_street?: string | null;
  billing_persona_address_postal_code?: string | null;
  billing_persona_address_locality?: string | null;
  billing_persona_address_country_id?: string | null;
  billing_persona_address_country_name?: string | null;

  delivery_persona_id?: string | null;
  delivery_persona_name?: string | null;
  delivery_persona_email?: string | null;
  delivery_persona_telephone?: string | null;
  delivery_persona_address_street?: string | null;
  delivery_persona_address_postal_code?: string | null;
  delivery_persona_address_locality?: string | null;
  delivery_persona_address_country_id?: string | null;
  delivery_persona_address_country_name?: string | null;

  payment_method_id?: string | null;
  payment_method?: string | null;

  maturity_date?: ICreateFromHeaderMaturityDate;

  delivery_method_id?: string | null;
  delivery_method?: string | null;

  delivery_departure_street?: string | null;
  delivery_departure_locality?: string | null;
  delivery_departure_postal_code?: string | null;

  vehicle_id?: string | null;
  vehicle?: string | null;

  commissioner_id?: string | null;
  commission_percentage?: number;
  return_reason?: string | null;

  created_by?: string | null;
  updated_by?: string | null;
}

export interface ICreateResponse {
  id: string;
  owner_id: string;
  document_number: number;
  google_sheet_id: string | null;
}

export interface IDeliveryAdresses {
  delivery_departure_street?: string | null;
  delivery_departure_locality?: string | null;
  delivery_departure_postal_code?: string | null;
  delivery_street?: string | null;
  delivery_locality?: string | null;
  delivery_postal_code?: string | null;
}

export interface ICreateFromHeaderRequest {
  document_type_id: number;
  emission_date?: Date;
  from_header: string[];
  from_lines: string[];
  payments: (IPayment.ICreateRequest | IPayment.ICreateForThisDocumentHeaderRequest)[];
  delivery_adresses?: IDeliveryAdresses;
  delivery_method_id?: string | null;
  email?: string;
  maturity_date?: ICreateFromHeaderMaturityDate;
  return_reason?: string;
  provider_ref?: string;
  global_discount?: number;
  current_account?: boolean;
}

export interface IUpdateRequest {
  external_data?: unknown;
  external_id?: string | null;
  external_pdf_link?: string | null;
  google_sheet_id?: string | null;
  pdf_link?: string | null;
  sent?: boolean;

  owner_id?: string;
  owner_number?: number;
  owner_name?: string;

  reason_for_exemption_id?: string | null;
  reason_for_exemption?: string | null;

  billing_persona_id?: string | null;
  billing_persona_nif?: string | null;
  billing_persona_name?: string | null;
  billing_persona_email?: string | null;
  billing_persona_telephone?: string | null;
  billing_persona_address_street?: string | null;
  billing_persona_address_postal_code?: string | null;
  billing_persona_address_locality?: string | null;
  billing_persona_address_country_id?: string | null;
  billing_persona_address_country_name?: string | null;

  delivery_persona_id?: string | null;
  delivery_persona_name?: string | null;
  delivery_persona_email?: string | null;
  delivery_persona_telephone?: string | null;
  delivery_persona_address_street?: string | null;
  delivery_persona_address_postal_code?: string | null;
  delivery_persona_address_locality?: string | null;
  delivery_persona_address_country_id?: string | null;
  delivery_persona_address_country_name?: string | null;

  payment_method_id?: string | null;
  payment_method?: string | null;

  maturity_date_id?: string | null;
  maturity_date?: string | null;
  maturity_date_days?: number | null;

  delivery_method_id?: string | null;
  delivery_method?: string | null;

  delivery_departure_street?: string | null;
  delivery_departure_locality?: string | null;
  delivery_departure_postal_code?: string | null;

  vehicle_id?: string | null;
  vehicle?: string | null;

  commissioner_id?: string | null;
  commission_percentage?: number;
  commission_paid?: boolean;

  is_archived?: boolean;
  is_void?: boolean;
  who_handles?: string | null;
  tags?: string | null;
  warning?: IWarningType;
  created_by?: string;
}

export type IGetActionsDocumentType = Pick<
  DocumentType,
  | 'user_type'
  | 'allow_create_from_header_without_lines'
  | 'pay'
  | 'credit'
  | 'debit'
  | 'invoice'
  | 'code'
  | 'return'
  | 'id'
  | 'description'
>;

export interface IGetActionsResponse {
  document_type: IGetActionsDocumentType;
  transition_name: string; // descrição para os botoes do FrontEnd
  document_line_ids: string[];
  from_document_headers: {
    id: string;
    document_number: string;
  }[];
}

export interface IUnpaidDocumentResponse {
  id: string;
  name: string;
  group: string;
  total: number;
  params: ICreateFromHeaderRequest;
}

export interface ITransferOwnershipRequest {
  document_header_id: string;
  client_id: string;
}

export interface IFindResponse {
  p_id: string;
  document_header_id: string;
  document_number: number;
  legacy_id: string | null;
  total: number;
  emission_date: string;
  line_count: number;
  tags: string;
  is_void: boolean;
  is_archived: boolean;
  adjudicado: boolean;
}

export type IUpdateManyWithPersona = {
  id: string;
  name: string | null;
  telephone: string | null;
  email: string | null;
  street: string | null;
  postal_code: string | null;
  locality: string | null;
  country_id: string | null;
  country_name: string | null;
};

export interface ISendClientNotificationRequest {
  name: string;
  email: string;
  dontSendEmail?: boolean;
}

export interface IDocumentPdfRequest {
  token?: string;
}

export interface IFindByTypeRequest {
  documentTypeCodes: IDocumentType.IDocumentTypeCodeType[];
}

export interface IFindByTypeLine {
  document_line_id: string;
  quantity: number;
  notes: DocumentLineNote[];
  total_without_tax: number;
  total: number;

  product_id: string | null;
  designation: string | null;
  reference: string | null;
  measure: string | null;
  thumbnail: string | null;
}

export interface IFindByTypeResponse {
  document_header_id: string;
  document_name: string;
  pdf_link: string | null;

  owner_number: number;
  billing_persona_name: string | null;
  emission_date: string;
  created_by: string;

  lines: IFindByTypeLine[];
}

export interface IDocumentListRequest {
  closed: boolean;
}

export interface IDocumentListResponse {
  user_type: IDocumentType.IDocumentTypeUserType;
  emission_date: Date;
  virtual_name: string;
  id: string;
  billing_persona_name: string | null;
  billing_persona_nif: string | null;
  owner_number: number;
  owner_id: string;
  quantity: number;
  stock_in: boolean;
  stock_out: boolean;
  cativado: boolean;
  client_upfront: boolean;
}

// warning type enum
export const IWarningEnum: {
  AvisoTotal: 'AvisoTotal';
  AvisoParcial: 'AvisoParcial';
  SemAviso: 'SemAviso';
} = {
  AvisoTotal: 'AvisoTotal',
  AvisoParcial: 'AvisoParcial',
  SemAviso: 'SemAviso',
};
export type IWarningType = (typeof IWarningEnum)[keyof typeof IWarningEnum];
export const IWarningType = Object.keys(IWarningEnum);

export interface IFindStartDocumentHeaderSiblingsResponse {
  next: string | null;
  previous: string | null;
}

export interface IRepository {
  findOne(selector: IShared.IFindByIdRequest): Promise<IFindOneResponse | null>;

  findStart(selector: IShared.IFindByIdRequest): Promise<IFindOneBase | null>;

  findStartVirtual(
    selector: IShared.IFindByIdRequest,
  ): Promise<IStartDocumentHeaderVirtuals | null>;

  findByType(params: IFindByTypeRequest): Promise<IFindByTypeResponse[]>;

  findByBasicSearch(
    data: IBasicSearchRequest,
  ): Promise<IShared.IPaginationResponse<IBasicSearchResponse>>;

  find(params: IFindRequest): Promise<IFindResponse[]>;

  findUnpaidCommissions(params: IShared.IFindByOwnerIdRequest): Promise<IUnpaidDocumentResponse[]>;

  findClientUnpaidCredit(params: IShared.IFindByOwnerIdRequest): Promise<IUnpaidDocumentResponse[]>;

  findClientUnpaidDebit(params: IShared.IFindByOwnerIdRequest): Promise<IUnpaidDocumentResponse[]>;

  getActions(selector: IShared.IFindByIdRequest): Promise<IGetActionsResponse[]>;

  create(data: ICreateRequest): Promise<ICreateResponse>;

  createFromHeader(data: ICreateFromHeaderRequest): Promise<IShared.ITaskCluster[]>;

  update(selector: IShared.IFindByIdRequest, data: IUpdateRequest): Promise<void>;

  updateManyWithPersona(params: IUpdateManyWithPersona): Promise<void>;

  transferClientOwnership(params: ITransferOwnershipRequest): Promise<void>;

  deleteOne(selector: IShared.IFindByIdRequest): Promise<void>;

  sendClientNotification(
    selector: IShared.IFindByIdRequest,
    data: ISendClientNotificationRequest,
  ): Promise<void>;

  markUnsentClientNotification(selector: IShared.IFindByIdRequest): Promise<void>;

  documentPdf(selector: IShared.IFindByIdRequest, data: IDocumentPdfRequest): Promise<string>;

  documentList(
    selector: IShared.IFindByIdRequest,
    params: IDocumentListRequest,
  ): Promise<IDocumentListResponse[]>;

  getAllRelatedDocumentHeaderIds(params: IShared.IFindByIdRequest): Promise<string[]>;

  findStartDocumentHeaderSiblings(
    params: IShared.IFindByIdRequest,
  ): Promise<IFindStartDocumentHeaderSiblingsResponse>;
}

export type IController = IShared.IEntityWithUserToken<IRepository>;
