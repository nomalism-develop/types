import joi from 'joi';
import { messages } from '../../../shared/messages';
import * as IShared from '../../../shared/interface';
import {
  ICreateRequest,
  IUpdateRequest,
  ICreateFromHeaderRequest,
  IBasicSearchRequest,
  IFindRequest,
  ICreateFromHeaderMaturityDate,
  ITransferOwnershipRequest,
  IUpdateManyWithPersona,
  ISendClientNotificationRequest,
  IDocumentPdfRequest,
  IFindByTypeRequest,
  IDocumentListRequest,
  IWarningType,
} from './interfaces';
import { documentTypeCodeTypes } from '../documentType/interfaces';

const createFromHeaderMaturityDate: IShared.IRouteRequest<ICreateFromHeaderMaturityDate> = {
  id: joi.string().uuid().required(),
  name: joi.string().required(),
  days: joi.number().integer().positive().allow(0).required(),
};

const createBodyKeys: IShared.IRouteRequestWithStamps<ICreateRequest> = {
  document_type_id: joi.number().required(),

  external_id: joi.string().allow(null).optional(),
  document_set_id: joi.string().uuid().required(),
  pdf_link: joi.string().allow(null).optional(),
  google_sheet_id: joi.string().allow(null).optional(),
  external_pdf_link: joi.string().allow(null).optional(),
  external_data: joi.any().optional(),
  sent: joi.boolean().optional(),

  owner_id: joi.string().uuid().required(),
  owner_number: joi.number().required(),
  owner_name: joi.string().required(),

  reason_for_exemption_id: joi.string().uuid().allow(null).optional(),
  reason_for_exemption: joi.string().allow(null),

  billing_persona_id: joi.string().uuid().allow(null).optional(),
  billing_persona_nif: joi.string().allow(null).optional(),
  billing_persona_name: joi.string().allow(null).optional(),
  billing_persona_email: joi
    .string()
    .email({ tlds: { allow: false } })
    .allow(null)
    .optional(),
  billing_persona_telephone: joi.string().allow(null).optional(),
  billing_persona_address_street: joi.string().allow(null).optional(),
  billing_persona_address_postal_code: joi.string().trim(true).lowercase().allow(null).optional(),
  billing_persona_address_locality: joi.string().allow(null).optional(),
  billing_persona_address_country_id: joi.string().allow(null).optional(),
  billing_persona_address_country_name: joi.string().allow(null).optional(),

  delivery_persona_id: joi.string().uuid().allow(null).optional(),
  delivery_persona_name: joi.string().allow(null).optional(),
  delivery_persona_email: joi
    .string()
    .email({ tlds: { allow: false } })
    .allow(null)
    .optional(),
  delivery_persona_telephone: joi.string().allow(null).optional(),
  delivery_persona_address_street: joi.string().allow(null).optional(),
  delivery_persona_address_postal_code: joi.string().trim(true).lowercase().allow(null).optional(),
  delivery_persona_address_locality: joi.string().allow(null).optional(),
  delivery_persona_address_country_id: joi.string().allow(null).optional(),
  delivery_persona_address_country_name: joi.string().allow(null).optional(),

  payment_method_id: joi.string().uuid().allow(null).optional(),
  payment_method: joi.string().allow(null).optional(),

  maturity_date: joi.object().keys(createFromHeaderMaturityDate).optional(),

  delivery_method_id: joi.string().uuid().allow(null).optional(),
  delivery_method: joi.string().allow(null).optional(),

  delivery_departure_street: joi.string().allow(null).optional(),
  delivery_departure_postal_code: joi.string().trim(true).lowercase().allow(null).optional(),
  delivery_departure_locality: joi.string().allow(null).optional(),

  vehicle_id: joi.string().uuid().allow(null).optional(),
  vehicle: joi.string().allow(null).optional(),

  commissioner_id: joi.string().uuid().allow(null).optional(),
  commission_percentage: joi.number().positive().allow(0).default(0).optional(),
  return_reason: joi.string().allow(null).optional(),

  updated_by: joi.string().uuid().allow(null).optional(),
  created_by: joi.string().uuid().allow(null).optional(),
};
export const createBody = joi
  .object()
  .keys(createBodyKeys)
  .and('reason_for_exemption_id', 'reason_for_exemption')
  // .and('persona_id', 'contact_name', 'email', 'phone_number')
  // .and(
  //   'delivery_country_name',
  //   'delivery_country_id',
  //   'delivery_street',
  //   'delivery_locality',
  //   'delivery_postal_code',
  // )
  // .and(
  //   'billing_country_name',
  //   'billing_country_id',
  //   'billing_street',
  //   'billing_locality',
  //   'billing_postal_code',
  // )
  .and('payment_method_id', 'payment_method')
  .and('delivery_method_id', 'delivery_method')
  .and('delivery_departure_street', 'delivery_departure_postal_code', 'delivery_departure_locality')
  .and('vehicle_id', 'vehicle')
  .messages(messages);

const createFromHeaderBodyKeys: IShared.IRouteRequest<ICreateFromHeaderRequest> = {
  document_type_id: joi.number().required(),
  emission_date: joi.date().optional(),
  from_header: joi.array().items(joi.string().uuid().required()).required(),
  from_lines: joi.array().items(joi.string().uuid().optional()).required(),
  payments: joi.array().items(
    joi
      .object({
        document_header_id: joi.string().uuid().optional(),
        date: joi.date().required(),
        debit: joi.number().positive().allow(0).required(),
        credit: joi.number().allow(0).required(),
        payment_method_id: joi.string().uuid().required(),
        payment_method: joi.string().required(),
        notes: joi.string().allow('', null).required(),
      })
      .optional(),
  ),
  delivery_method_id: joi.string().optional(),
  delivery_adresses: joi
    .object({
      delivery_departure_street: joi.string().required(),
      delivery_departure_locality: joi.string().required(),
      delivery_departure_postal_code: joi.string().trim(true).lowercase().required(),
      delivery_street: joi.string().required(),
      delivery_locality: joi.string().required(),
      delivery_postal_code: joi.string().trim(true).lowercase().required(),
    })
    .allow(null)
    .optional(),
  email: joi
    .string()
    .email({ tlds: { allow: false } })
    .allow(null)
    .optional(),
  maturity_date: joi.object().keys(createFromHeaderMaturityDate).optional(),
  return_reason: joi.string().optional(),
  provider_ref: joi.string().optional(),
  global_discount: joi.number().min(0).max(100).optional(),
  current_account: joi.boolean().optional(),
};
export const createFromHeaderBody = joi.object().keys(createFromHeaderBodyKeys).messages(messages);

const findQueryKeys: IShared.IRouteRequest<IFindRequest> = {
  owner_id: joi.string().uuid().required(),
  document_type_id: joi.number().required(),
  closed: joi.boolean().optional(),
};
export const findQuery = joi.object().keys(findQueryKeys).messages(messages);

const basicSearchQueryKeys: IShared.IRouteRequest<IBasicSearchRequest> = {
  search_value: joi.string().allow('', null).optional(),
  per_page: joi.number().positive().integer().default(10).allow(null).optional(),
  inactive: joi.boolean().default(false).optional(),
  current_page: joi.number().positive().integer().allow(null).optional(),
};
export const basicSearchQuery = joi.object().keys(basicSearchQueryKeys).messages(messages);

const updateBodyKeys: IShared.IRouteRequestWithStamps<IUpdateRequest> = {
  external_data: joi.any().optional(),
  external_id: joi.string().allow(null).optional(),
  external_pdf_link: joi.string().allow(null).optional(),
  google_sheet_id: joi.string().allow(null).optional(),
  pdf_link: joi.string().allow(null).optional(),
  sent: joi.boolean().optional(),

  owner_id: joi.string().uuid().optional(),
  owner_number: joi.number().integer().positive().optional(),
  owner_name: joi.string().optional(),

  reason_for_exemption_id: joi.string().uuid().allow(null).optional(),
  reason_for_exemption: joi.string().allow(null).optional(),

  billing_persona_id: joi.string().uuid().allow(null).optional(),
  billing_persona_nif: joi.string().allow(null).optional(),
  billing_persona_name: joi.string().allow(null).optional(),
  billing_persona_email: joi
    .string()
    .email({ tlds: { allow: false } })
    .allow(null)
    .optional(),
  billing_persona_telephone: joi.string().allow(null).optional(),
  billing_persona_address_street: joi.string().allow(null).optional(),
  billing_persona_address_postal_code: joi.string().trim(true).lowercase().allow(null).optional(),
  billing_persona_address_locality: joi.string().allow(null).optional(),
  billing_persona_address_country_id: joi.string().allow(null).optional(),
  billing_persona_address_country_name: joi.string().allow(null).optional(),

  delivery_persona_id: joi.string().uuid().allow(null).optional(),
  delivery_persona_name: joi.string().allow(null).optional(),
  delivery_persona_email: joi
    .string()
    .email({ tlds: { allow: false } })
    .allow(null)
    .optional(),
  delivery_persona_telephone: joi.string().allow(null).optional(),
  delivery_persona_address_street: joi.string().allow(null).optional(),
  delivery_persona_address_postal_code: joi.string().trim(true).lowercase().allow(null).optional(),
  delivery_persona_address_locality: joi.string().allow(null).optional(),
  delivery_persona_address_country_id: joi.string().allow(null).optional(),
  delivery_persona_address_country_name: joi.string().allow(null).optional(),

  payment_method_id: joi.string().uuid().allow(null).optional(),
  payment_method: joi.string().allow(null).optional(),

  maturity_date_id: joi.string().uuid().allow(null).optional(),
  maturity_date: joi.string().allow(null).optional(),
  maturity_date_days: joi.number().integer().positive().allow(0).allow(null).optional(),

  delivery_method_id: joi.string().uuid().allow(null).optional(),
  delivery_method: joi.string().allow(null).optional(),

  delivery_departure_street: joi.string().allow(null).optional(),
  delivery_departure_postal_code: joi.string().trim(true).lowercase().allow(null).optional(),
  delivery_departure_locality: joi.string().allow(null).optional(),

  vehicle_id: joi.string().uuid().allow(null).optional(),
  vehicle: joi.string().allow(null).optional(),

  commissioner_id: joi.string().uuid().allow(null).optional(),
  commission_percentage: joi.number().positive().allow(0).optional(),
  commission_paid: joi.boolean().optional(),

  is_archived: joi.boolean().optional(),
  is_void: joi.boolean().optional(),
  who_handles: joi.string().allow(null).optional(),
  tags: joi.string().allow(null, '').optional(),
  warning: joi
    .string()
    .optional()
    .valid(...Object.values(IWarningType)),
  created_by: joi.string().uuid().allow(null).optional(),
};
export const updateBody = joi
  .object()
  .keys(updateBodyKeys)
  .and('reason_for_exemption_id', 'reason_for_exemption')
  .and('persona_id', 'contact_name', 'email', 'phone_number')
  .and(
    'delivery_country_name',
    'delivery_country_id',
    'delivery_street',
    'delivery_locality',
    'delivery_postal_code',
  )
  .and(
    'billing_country_name',
    'billing_country_id',
    'billing_street',
    'billing_locality',
    'billing_postal_code',
  )
  .and('payment_method_id', 'payment_method')
  .and('maturity_date_id', 'maturity_date', 'maturity_date_days')
  .and('delivery_method_id', 'delivery_method')
  .and('delivery_departure_street', 'delivery_departure_postal_code', 'delivery_departure_locality')
  .and('vehicle_id', 'vehicle')
  .messages(messages);

const transferClientOwnershipBodyKeys: IShared.IRouteRequest<ITransferOwnershipRequest> = {
  document_header_id: joi.string().uuid().required(),
  client_id: joi.string().uuid().required(),
};
export const transferClientOwnershipBody = joi
  .object()
  .keys(transferClientOwnershipBodyKeys)
  .messages(messages);

const updateManyWithPersonaBodyKeys: IShared.IRouteRequest<IUpdateManyWithPersona> = {
  id: joi.string().uuid().required(),
  name: joi.string().allow(null).optional(),
  telephone: joi.string().allow(null).optional(),
  email: joi
    .string()
    .email({ tlds: { allow: false } })
    .allow(null)
    .optional(),
  street: joi.string().allow(null).optional(),
  locality: joi.string().allow(null).optional(),
  postal_code: joi.string().trim(true).lowercase().allow(null).optional(),
  country_id: joi.string().uuid().allow(null).optional(),
  country_name: joi.string().allow(null).optional(),
};
export const updateManyWithPersonaBody = joi
  .object()
  .keys(updateManyWithPersonaBodyKeys)
  .messages(messages);

const sendClientNotificationBodyKeys: IShared.IRouteRequest<ISendClientNotificationRequest> = {
  name: joi.string().allow('', null).required(),
  email: joi
    .string()
    .email({ tlds: { allow: false } })
    .allow('', null)
    .required(),
  dontSendEmail: joi.boolean().default(false).optional(),
};
export const sendClientNotificationBody = joi
  .object()
  .keys(sendClientNotificationBodyKeys)
  .messages(messages);

const findByTypeQueryKeys: IShared.IRouteRequest<IFindByTypeRequest> = {
  documentTypeCodes: joi
    .array()
    .items(
      joi
        .string()
        .valid(...documentTypeCodeTypes)
        .required(),
    )
    .required(),
};
export const findByTypeQuery = joi.object().keys(findByTypeQueryKeys).messages(messages);

const documentPdfQueryKeys: IShared.IRouteRequest<IDocumentPdfRequest> = {
  token: joi.string().optional(),
};

export const documentPdfQuery = joi.object().keys(documentPdfQueryKeys).messages(messages);

export const documentLineQuery = joi
  .object<IShared.IRouteRequest<IDocumentListRequest>>()
  .keys({
    closed: joi.boolean().default(false).optional(),
  })
  .messages(messages);
