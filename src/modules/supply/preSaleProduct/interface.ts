import * as IShared from '../../../shared/interface';
import { PreSaleProduct } from '../../../shared/entities';

export type Entity = PreSaleProduct;
export const Route = 'pre_sale_product';
export const UpperName = 'PreSaleProduct';
export const LowerName = UpperName[0].toLowerCase() + UpperName.substring(1);

export type IFindRequest = Record<string, unknown>;

export type IFindResponse = Entity;

export interface IRepository {
  find(user_id: string): Promise<PreSaleProduct[]>;
  findById(selector: IShared.IFindByIdRequest): Promise<PreSaleProduct | null>;
}
