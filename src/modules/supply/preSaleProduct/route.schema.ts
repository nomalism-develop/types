import joi from 'joi';
import { messages } from '../../../shared/messages';
import * as IShared from '../../../shared/interface';
import { IFindRequest } from './interface';

const findQueryKeys: IShared.IRouteRequest<IFindRequest> = {};
export const findQuery = joi.object().keys(findQueryKeys).messages(messages);
