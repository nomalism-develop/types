import { ExternalDocumentType } from '../../../shared/entities';
import * as IShared from '../../../shared/interface';

export type Entity = ExternalDocumentType;
export const Route = 'external_document_type';
export const UpperName = 'ExternalDocumentType';
export const LowerName = UpperName[0].toLowerCase() + UpperName.substring(1);

export interface IRepository {
  find(): Promise<Entity[]>;
  findMinified(params?: IShared.IFindMinifiedRequest): Promise<IShared.IFindMinifiedResponse[]>;
}

export type IController = IShared.IEntityWithUserToken<IRepository>;
