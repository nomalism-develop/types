import * as IShared from '../../../shared/interface';
import { Counter } from '../../../shared/entities';

export type Entity = Counter;
export const Route = 'counter';
export const UpperName = 'Counter';
export const LowerName = UpperName[0].toLowerCase() + UpperName.substring(1);

export interface IFindByDocTypeRequest {
  document_type: number;
}

export interface ICreateRequest {
  name: string;
  document_type: number;
  year: number;
}

export interface IUpdateRequest {
  name: string;
  document_type: number;
  year: number;
}

export interface IRepository {
  findByDocType(data: IFindByDocTypeRequest): Promise<Entity | null>;
  findAll(): Promise<Entity[]>;
  create(data: ICreateRequest): Promise<Entity>;
  update(data: IUpdateRequest): Promise<Entity | null>;
}

export type IController = IShared.IEntityWithUserToken<IRepository>;

export type IApi = Omit<IRepository, 'findAll'>;
