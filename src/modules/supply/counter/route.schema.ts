import joi from 'joi';
import { messages } from '../../../shared/messages';
import * as IShared from '../../../shared/interface';
import { IFindByDocTypeRequest, ICreateRequest, IUpdateRequest } from './interfaces';

const createBodyKeys: IShared.IRouteRequest<ICreateRequest> = {
  document_type: joi.number().required(),
  year: joi.number().required(),
  name: joi.string().required(),
};
export const createBody = joi.object().keys(createBodyKeys).messages(messages);

const findByDocumentTypeParamsKeys: IShared.IRouteRequest<IFindByDocTypeRequest> = {
  document_type: joi.number().required(),
};
export const findByDocumentTypeParams = joi
  .object()
  .keys(findByDocumentTypeParamsKeys)
  .messages(messages);

const updateBodyKeys: IShared.IRouteRequest<IUpdateRequest> = {
  document_type: joi.number().required(),
  year: joi.number().required(),
  name: joi.string().required(),
};
export const updateBody = joi.object().keys(updateBodyKeys).messages(messages);
