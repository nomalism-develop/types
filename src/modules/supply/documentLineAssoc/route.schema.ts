import joi from 'joi';
import { messages } from '../../../shared/messages';
import * as IShared from '../../../shared/interface';
import { ICreateRequest, IUpdateRequest } from './interfaces';

const createBodyKeys: IShared.IRouteRequest<ICreateRequest> = {
  document_header_id: joi.string().uuid().required(),
  document_line_id: joi.string().uuid().required(),
};
export const createBody = joi.array().items(joi.object().keys(createBodyKeys)).messages(messages);

const updateBodyKeys: IShared.IRouteRequest<IUpdateRequest> = {
  external_id: joi.string().allow(null).optional(),
  origin_id: joi.string().allow(null).optional(),
};
export const updateBody = joi.object().keys(updateBodyKeys).messages(messages);
