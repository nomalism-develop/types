import * as IShared from '../../../shared/interface';
import { DocumentLineAssoc, DocumentLine } from '../../../shared/entities';
import * as IDocumentLine from '../documentLine/interfaces';
import { IDocumentLineVirtuals } from '../documentLine/interfaces';

export type Entity = DocumentLineAssoc;
export const Route = 'documentLineAssoc';
export const UpperName = 'DocumentLineAssoc';
export const LowerName = UpperName[0].toLowerCase() + UpperName.substring(1);

export interface IEntityWithDocumentLine extends Entity {
  document_line: DocumentLine;
}

export interface IEntityExtended extends Entity {
  document_line: IDocumentLine.IEntityExtended;
}

export interface ICreateRequest {
  document_header_id: string;
  document_line_id: string;
}

export interface IUpdateRequest {
  external_id?: string | null;
  origin_id?: string | null;
}

export interface IUpdateGroupRequest {
  document_line_id: string[];
}

export interface IRepository {
  findById(selector: IShared.IFindByIdRequest): Promise<Entity | null>;

  findByOwnerId(data: IShared.IFindByOwnerIdRequest): Promise<IEntityExtended[]>;

  findVirtualsByOwnerId(data: IShared.IFindByOwnerIdRequest): Promise<IDocumentLineVirtuals[]>;

  create(data: ICreateRequest[]): Promise<IEntityWithDocumentLine[]>;

  update(
    selector: IShared.IFindByIdRequest,
    data: IUpdateRequest,
  ): Promise<IEntityWithDocumentLine>;
}

export type IApi = Omit<IRepository, 'findById'>;

export type IController = IShared.IEntityWithUserToken<IRepository>;
