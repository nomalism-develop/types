import joi from 'joi';
import { messages } from '../../../shared/messages';
import * as IShared from '../../../shared/interface';
import {
  IFindRequest,
  IFindPaginatedRequest,
  ICreateRequest,
  IUpdateRequest,
  ISendEmailDocumentRequest,
  DocumentHeaderNoteUpdateRequest,
  DocumentHeaderNoteCreateRequest,
} from './interface';

const findQueryKeys: IShared.IRouteRequest<IFindRequest> = {};
export const findQuery = joi.object().keys(findQueryKeys).messages(messages);

// find with pagination query validation
const findWithPaginationQueryKeys: IShared.IRouteRequest<IFindPaginatedRequest> = {
  per_page: joi.number().integer().positive().default(10).optional(),
  current_page: joi.number().integer().positive().default(1).optional(),
};
export const findWithPaginationQuery = joi
  .object()
  .keys(findWithPaginationQueryKeys)
  .messages(messages);

// create body validation
const createBodyKeys: IShared.IRouteRequest<ICreateRequest> = {
  document_header_id: joi.string().uuid().required(),
  external_id: joi.string().required(),
  external_pdf_link: joi.string().required(),
  pdf_link: joi.string().required(),
};
export const createBody = joi.object().keys(createBodyKeys).messages(messages);

// update body validation
const updateBodyKeys: IShared.IRouteRequest<IUpdateRequest> = {
  external_id: joi.string(),
  external_pdf_link: joi.string(),
  pdf_link: joi.string(),
};
export const updateBody = joi.object().keys(updateBodyKeys).messages(messages);

const sendEmailDocumentBodyKeys: IShared.IRouteRequest<ISendEmailDocumentRequest> = {
  template: joi
    .string()
    .valid(...IShared.IUserSendEmailDocumentTemplateTypes)
    .required(),
  document_line_assoc_ids: joi.array().items(joi.string().uuid()),
  external_document_header_ids: joi.array().items(joi.string().uuid()),
  user: joi
    .object()
    .keys({
      name: joi.string().required(),
      email: joi.string().trim(true).lowercase().required(),
    })
    .optional(),
};

export const sendEmailDocumentBody = joi
  .object()
  .keys(sendEmailDocumentBodyKeys)
  .xor('document_line_assoc_ids', 'external_document_header_ids')
  .messages(messages);

// create Note body validation
const createNoteBodyKeys: IShared.IRouteRequest<DocumentHeaderNoteCreateRequest> = {
  document_header_id: joi.string().uuid().required(),
  note: joi.string().required(),
  type: joi.string().required().valid('Client', 'Provider'),
};

export const createNoteBody = joi.object().keys(createNoteBodyKeys).messages(messages);

// update Note body validation
const updateNoteBodyKeys: IShared.IRouteRequest<DocumentHeaderNoteUpdateRequest> = {
  note: joi.string().required().allow(''),
};
export const updateNoteBody = joi.object().keys(updateNoteBodyKeys).messages(messages);
