import * as IShared from '../../../shared/interface';
import { ExternalDocumentHeader } from '../../../shared/entities';
import { IUpdateDataRequest } from '../documentLine/interfaces';
export type Entity = ExternalDocumentHeader;
export declare const Route = 'external_document_header';
export declare const UpperName = 'ExternalDocumentHeader';
export declare const LowerName: string;
type IEntityExtended = Entity;
export type IFindByIdResponse = Omit<IEntityExtended, ''>;
export interface IFindByOwnerIdItem {
  id: string | null;
  document_header_id: string;
  is_void: boolean;
  pdf_link: string | null;
  external_id: string | null;
  label: string;
  visible: boolean;
  loading: boolean;
  emission_date: Date;
  retryPayload: IUpdateDataRequest | null;
}
export interface IFindByOwnerIdItemWithVersions extends IFindByOwnerIdItem {
  older_versions: IFindByOwnerIdItem[];
}

export type IFindRequest = Record<string, unknown>;
export type IFindResponse = Omit<IEntityExtended, ''>;
export interface IFindPaginatedRequest extends IFindRequest, IShared.IPaginationRequest {}
export type IFindWithPaginationResponse = IShared.IPaginationResponse<Omit<IEntityExtended, ''>>;
export interface ICreateRequest {
  document_header_id: string;
  external_id: string;
  external_pdf_link: string;
  pdf_link: string;
}
export interface IUpdateRequest {
  external_id?: string;
  external_pdf_link?: string;
  pdf_link?: string;
}
export interface ISendEmailDocument {
  template: IShared.IUserSendEmailDocumentTemplate;
  external_document_header_ids: string[];
  document_line_assoc_ids: string[];
  user: {
    name: string;
    email: string;
  };
}

export enum IDocumentHeaderNoteType {
  Provider = 'Provider',
  Client = 'Client',
}

export interface DocumentHeaderNote {
  id: string;
  note: string;
  type: IDocumentHeaderNoteType;
  created_at: Date;
  updated_at: Date;
  created_by: string;
  updated_by: string;
}

export type ISendEmailDocumentRequest = IShared.RequireOnlyOne<
  ISendEmailDocument,
  'document_line_assoc_ids' | 'external_document_header_ids'
>;
export interface IPublicFindByOwnerIdResponse {
  start_document_header_id: string;
  document_header_id: string;
  label: string;
  emission_date: Date;
  pdf_link: string;
  new_chat_messages: number;
  clients_proposals: string[];
  clients_proposals_tags: string[];
  document_header_notes: DocumentHeaderNote[];
}

export interface DocumentHeaderNoteCreateRequest {
  document_header_id: string;
  note: string;
  type: IDocumentHeaderNoteType;
}

export interface DocumentHeaderNoteUpdateRequest {
  note: string;
}

export interface IRepository {
  findById(selector: IShared.IFindByIdRequest): Promise<IFindByIdResponse | null>;

  publicFindByOwnerId(
    params: IShared.IFindByOwnerIdRequest,
  ): Promise<IPublicFindByOwnerIdResponse[]>;

  findByOwnerId(params: IShared.IFindByOwnerIdRequest): Promise<IFindByOwnerIdItemWithVersions[]>;

  sendEmailDocument: (data: ISendEmailDocumentRequest) => Promise<void>;

  find(selector: IFindRequest): Promise<IFindResponse[]>;

  findPaginated(selector: IFindPaginatedRequest): Promise<IFindWithPaginationResponse>;

  create(data: ICreateRequest): Promise<Entity>;

  update(selector: IShared.IFindByIdRequest, data: IUpdateRequest): Promise<Entity | null>;

  deleteOne(selector: IShared.IFindByIdRequest): Promise<Entity | null>;

  createDocumentHeaderNote: (
    data: DocumentHeaderNoteCreateRequest[],
  ) => Promise<DocumentHeaderNote[]>;

  updateDocumentHeaderNote: (
    selector: IShared.IFindByIdRequest,
    data: DocumentHeaderNoteUpdateRequest[],
  ) => Promise<DocumentHeaderNote[]>;
}
export type IController = IShared.IEntityWithUserToken<IRepository>;
export {};
