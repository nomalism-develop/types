import joi from 'joi';
import { messages } from '../../../shared/messages';
import * as IShared from '../../../shared/interface';
import {
  ICreateRequest,
  IUpdateRequest,
  IPreSaleData,
  IFindRequest,
  IRemovePreSaleProductRequest,
  IImportToClientProposalRequest,
  IUpdatePreSaleQuantityRequest,
  IPreSaleFindByUser,
} from './interface';

const createProduct = joi.object<IPreSaleData>().keys({
  id: joi.string().uuid().required(),
  quantity: joi.number().positive().required(),
});

// create body validation
const createBodyKeys: IShared.IRouteRequestWithStamps<ICreateRequest> = {
  product: createProduct,
  created_by: joi.string().uuid().optional(),
  updated_by: joi.string().uuid().optional(),
};

export const createBody = joi.object().keys(createBodyKeys).messages(messages);

// update body validation
const updateBodyKeys: IShared.IRouteRequestWithStamps<IUpdateRequest> = {
  barcode: joi.string().allow(null).optional(),
  product: createProduct.allow(null),
  updated_by: joi.string().uuid().optional(),
};
export const updateBody = joi.object().keys(updateBodyKeys).messages(messages);
// update body validation

const updatePreSaleQuantityBodyKeys: IShared.IRouteRequestWithStamps<IUpdatePreSaleQuantityRequest> =
  {
    quantity: joi.number().positive().required(),
    updated_by: joi.string().uuid().optional(),
  };

export const updatePreSaleQuantityBody = joi
  .object()
  .keys(updatePreSaleQuantityBodyKeys)
  .messages(messages);

// remove product body validation
const removeProductBodyKeys: IShared.IRouteRequestWithStamps<IRemovePreSaleProductRequest> = {
  pre_sale_product_id: joi.string().uuid(),
  updated_by: joi.string().uuid().optional(),
};

export const removeProductBody = joi.object().keys(removeProductBodyKeys).messages(messages);

// import into proposal body validation
const importInToClientProposalBodyKeys: IShared.IRouteRequestWithStamps<IImportToClientProposalRequest> =
  {
    pre_sale_id: joi.string().uuid().required(),
    document_header_id: joi.string().uuid().required(),
    updated_by: joi.string().uuid().optional(),
  };
export const importInToClientProposalBody = joi
  .object()
  .keys(importInToClientProposalBodyKeys)
  .messages(messages);

const findQueryKeys: IShared.IRouteRequest<IFindRequest> = {};
export const findQuery = joi.object().keys(findQueryKeys).messages(messages);

const findByUserParamsKeys: IShared.IRouteRequest<IPreSaleFindByUser> = {
  created_by: joi.string().uuid().required(),
};
export const findByUserParams = joi.object().keys(findByUserParamsKeys).messages(messages);
