import { IProductState } from './../../stock/productGoogleSheets/interface';
import * as IShared from '../../../shared/interface';
import { PreSale, PreSaleProduct } from '../../../shared/entities';
import * as DocumentLine from '../documentLine/interfaces';

export type Entity = PreSale;
export const Route = 'pre_sale';
export const UpperName = 'PreSale';
export const LowerName = UpperName[0].toLowerCase() + UpperName.substring(1);

export type IFindRequest = Record<string, unknown>;

export type IFindResponse = Entity;

export interface IFindResponseWithCount extends Entity {
  count_products: number;
}

export interface IPreSaleProduct {
  id: string;
  reference: string;
  designation: string;
  price_sale: number;
  thumbnail: string | null;
  measure: string;
  state: IProductState;
  location: string | null;
  promotions: DocumentLine.IPromotions[];
}

export interface IEntityExtended extends Entity {
  lines: {
    pre_sale_product: PreSaleProduct;
    product: IPreSaleProduct;
  }[];
}

export interface IPreSaleFindByUser {
  created_by: string;
}

export interface IPreSaleData {
  id: string;
  quantity: number;
}

export interface IUpdateRequest {
  barcode?: string;
  product?: IPreSaleData | null;
  updated_by?: string;
}

export interface IUpdatePreSaleQuantityRequest {
  quantity: number;
  updated_by?: string;
}

export interface IRemovePreSaleProductRequest {
  pre_sale_product_id: string;
  updated_by?: string;
}

export interface ICreateRequest {
  product: IPreSaleData;
  created_by?: string;
  updated_by?: string;
}

export interface IImportToClientProposalRequest {
  pre_sale_id: string;
  document_header_id: string;
  updated_by?: string;
}

export interface IRepository {
  find(selector: IPreSaleFindByUser): Promise<IFindResponseWithCount[]>;
  findById(selector: IShared.IFindByIdRequest): Promise<IEntityExtended | null>;
  findPending(): Promise<IEntityExtended[]>;
  create(data: ICreateRequest): Promise<IFindResponse>;
  update: (selector: IShared.IFindByIdRequest, data: IUpdateRequest) => Promise<Entity | null>;
  updatePreSaleQuantity: (
    selector: IShared.IFindByIdRequest,
    data: IUpdatePreSaleQuantityRequest,
  ) => Promise<Entity | null>;
  removeProduct: (
    selector: IShared.IFindByIdRequest,
    data: IRemovePreSaleProductRequest,
  ) => Promise<void>;
  importToClientProposal: (data: IImportToClientProposalRequest) => Promise<void>;
}

export type IController = IShared.IEntityWithUserToken<IRepository>;
