import * as IShared from '../../../shared/interface';
import { DocumentLineNote } from '../../../shared/entities';

// document lines Types
export const IDocumentLineNoteTypeEnum: {
  Prisao: 'Prisao';
  Quebra: 'Quebra';
  Entrada: 'Entrada';
  Compra: 'Compra';
  Devolucao: 'Devolucao';
  Transformado: 'Transformado';
  Inventario: 'Inventario';
  PC: 'PC';
  PF: 'PF';
} = {
  Prisao: 'Prisao',
  Quebra: 'Quebra',
  Entrada: 'Entrada',
  Compra: 'Compra',
  Devolucao: 'Devolucao',
  Transformado: 'Transformado',
  Inventario: 'Inventario',
  PC: 'PC',
  PF: 'PF',
};

export type IDocumentLineNoteType =
  (typeof IDocumentLineNoteTypeEnum)[keyof typeof IDocumentLineNoteTypeEnum];

export const documentLineNoteTypes = Object.keys(IDocumentLineNoteTypeEnum);

export type Entity = DocumentLineNote;
export const Route = 'documentLineNote';
export const UpperName = 'DocumentLineNote';
export const LowerName = UpperName[0].toLowerCase() + UpperName.substring(1);

export interface ICreateRequest {
  document_line_id: string;
  type: IDocumentLineNoteType;
  note: string;
}

export interface IUpdateRequest {
  note: string;
}

export interface IRepository {
  create(data: ICreateRequest): Promise<string>;
  update(selector: IShared.IFindByIdRequest, data: IUpdateRequest): Promise<void>;
  deleteOne(selector: IShared.IFindByIdRequest): Promise<void>;
}

export type IController = IShared.IEntityWithUserToken<IRepository>;
