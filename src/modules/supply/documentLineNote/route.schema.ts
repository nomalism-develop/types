import joi from 'joi';
import { messages } from '../../../shared/messages';
import * as IShared from '../../../shared/interface';
import { ICreateRequest, IUpdateRequest, documentLineNoteTypes } from './interfaces';

export const createBodyKeys: IShared.IRouteRequest<ICreateRequest> = {
  document_line_id: joi.string().uuid().required(),
  note: joi.string().required(),
  type: joi
    .string()
    .valid(...documentLineNoteTypes)
    .required(),
};
export const createBody = joi.object().keys(createBodyKeys).messages(messages);

const updateBodyKeys: IShared.IRouteRequest<IUpdateRequest> = {
  note: joi.string().required(),
};
export const updateBody = joi.object().keys(updateBodyKeys).messages(messages);
