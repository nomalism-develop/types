import { IProductType } from '../../stock/productGoogleSheets/interface';

export const Route = 'order_management';
export const UpperName = 'OrderManagement';
export const LowerName = UpperName[0].toLowerCase() + UpperName.substring(1);

export interface IFindProviderProductsRequest {
  provider_id: string;
}

export interface IFindProposals {
  id: string;
  name: string;
  start_document_header_id: string;
  pdf_link: string | null;
}

export interface IFindProviderProductsResponse {
  product_id: string;
  type: IProductType;
  obs: boolean;
  multimedia_id: string | null;
  ref: string;
  des: string;
  provider_ref: string | null;
  price_cost: number;
  price_sale: number;
  available: number;
  last_ef_date: Date | null;
  ordered_quantity: number;
  last_em_date: Date | null;
  notation: string;
  pfs: IFindProposals[];
  pcs: IFindProposals[];
  client_quantity: number;
  pre_ordered_quantity: number;
  last6m_consumption: number;
}

export interface IFindAllProviderProductsResponse extends IFindProviderProductsResponse {
  provider_id: string;
}

export interface IRepository {
  findProviderProducts(
    params: IFindProviderProductsRequest,
  ): Promise<IFindProviderProductsResponse[]>;
  findAllProviderProducts(): Promise<IFindAllProviderProductsResponse[]>;
}
