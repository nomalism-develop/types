import joi from 'joi';
import { messages } from '../../../shared/messages';
import * as IShared from '../../../shared/interface';
import {
  IFindRequest,
  IFindPaginatedRequest,
  IUnpaidPurchasesRequest,
  IProviderPaymentRequest,
  IExportPaymentsRequest,
  IExportCurrentAccountRequest,
  IUnpaidByDatePurchasesRequest,
  IProviderPaymentDocument,
  IExportCurrentAccountOutputOptions,
  IPaymentsNotExportedRequest,
  IFindCurrentAccountPaginatedRequest,
} from './interface';

const findQueryKeys: IShared.IRouteRequest<IFindRequest> = {};
export const findQuery = joi.object().keys(findQueryKeys).messages(messages);

// find with pagination query validation
const findWithPaginationQueryKeys: IShared.IRouteRequest<IFindPaginatedRequest> = {
  per_page: joi.number().integer().positive().default(10).optional(),
  current_page: joi.number().integer().positive().default(1).optional(),
};
export const findWithPaginationQuery = joi
  .object()
  .keys(findWithPaginationQueryKeys)
  .messages(messages);

// find settled material entrances
const findSettledMaterialEntranceQueryKeys: IShared.IRouteRequest<IUnpaidPurchasesRequest> = {
  provider_id: joi.string().uuid().required(),
  client_id: joi.string().uuid().allow(null).required(),
};
export const findSettledMaterialEntranceQuery = joi
  .object()
  .keys(findSettledMaterialEntranceQueryKeys)
  .messages(messages);

// create provider payment(s)
const providerPaymentDocumentKeys: IShared.IRouteRequest<IProviderPaymentDocument> = {
  document_header_id: joi.string().uuid().required(),
  value: joi.number().required(),
  discount_value: joi
    .number()
    .optional()
    .allow(0, '', null)
    .when('value', {
      is: joi.number().positive(),
      then: joi.number().max(joi.ref('value')),
      otherwise: joi.number().min(joi.ref('value')),
    }),
};
const providerPaymentDocument = joi.object().keys(providerPaymentDocumentKeys).required();
const providerPaymentBodyKeys: IShared.IRouteRequest<IProviderPaymentRequest> = {
  provider_id: joi.string().uuid().required(),
  client_id: joi.string().uuid().allow(null).optional(),
  documents: joi.array().items(providerPaymentDocument).required(),
  total: joi.number().positive().allow(0).required(),
};
export const providerPaymentBody = joi
  .array()
  .items(joi.object().keys(providerPaymentBodyKeys))
  .messages(messages);

// create export payments
const exportPaymentsBodyKeys: IShared.IRouteRequest<IExportPaymentsRequest> = {
  value: joi.number().positive().required(),
  provider: joi.string().required(),
  npf_ids: joi.array().items(joi.string().uuid().required()).optional(),
};
export const exportPaymentsBody = joi
  .array()
  .items(joi.object().keys(exportPaymentsBodyKeys))
  .messages(messages);

// create export payments
const exportCurrentAccountBetweenDatesQueryKeys: IShared.IRouteRequest<IExportCurrentAccountRequest> =
  {
    owner_id: joi.string().required(),
    output: joi
      .string()
      .valid(...IExportCurrentAccountOutputOptions)
      .required(),
    start_date: joi.string().optional(),
    end_date: joi.string().optional(),
    token: joi.string().required(),
  };
export const exportCurrentAccountBetweenDatesQuery = joi
  .object()
  .keys(exportCurrentAccountBetweenDatesQueryKeys)
  .messages(messages);

// find by date
const findSettledMaterialEntranceBuDateParamsKeys: IShared.IRouteRequest<IUnpaidByDatePurchasesRequest> =
  {
    due_date: joi.string().optional(),
  };
export const findSettledMaterialEntranceBuDateParams = joi
  .object()
  .keys(findSettledMaterialEntranceBuDateParamsKeys)
  .messages(messages);

export const findPaymentsNotExportedQuery = joi
  .object<IPaymentsNotExportedRequest>()
  .keys({
    whiteList: joi.array().items(joi.string().uuid().required()).optional(),
  })
  .messages(messages);

// find with pagination query validation
const findCurrentAccountWithPaginationQueryKeys: IShared.IRouteRequest<IFindCurrentAccountPaginatedRequest> =
  {
    owner_id: joi.string().uuid().required(),
    per_page: joi.number().integer().positive().default(10).optional(),
    current_page: joi.number().integer().positive().default(1).optional(),
  };
export const findCurrentAccountWithPaginationQuery = joi
  .object()
  .keys(findCurrentAccountWithPaginationQueryKeys)
  .messages(messages);
