import * as IShared from '../../../shared/interface';
import { Payment, DocumentHeader } from '../../../shared/entities';
import * as IUser from '../../user/users/interface';
import * as IObservation from '../../integration/observation/interfaces';

export import IFindProvidersWithClientIdResponse = IUser.IFindProvidersWithClientIdResponse;

export type Entity = Payment;
export const Route = 'payment';
export const UpperName = 'Payment';
export const LowerName = UpperName[0].toLowerCase() + UpperName.substring(1);

type IEntityExtended = Entity;

// omit any unnecessary fields
export type IFindByIdResponse = Omit<IEntityExtended, ''>;

// omit any unnecessary fields
export type IFindByOwnerIdResponse = Omit<IEntityExtended, ''>;

export type IFindRequest = Record<string, unknown>;

// omit any unnecessary fields
export type IFindResponse = Omit<IEntityExtended, ''>;

export interface IFindPaginatedRequest extends IFindRequest, IShared.IPaginationRequest {}

export type IFindWithPaginationResponse = IShared.IPaginationResponse<Omit<IEntityExtended, ''>>;

export interface ICreateForThisDocumentHeaderRequest {
  date: Date;
  debit: number;
  credit: number;
  payment_method_id?: string | null;
  payment_method?: string | null;
  notes: string | null;
}

export interface ICreateRequest extends ICreateForThisDocumentHeaderRequest {
  document_header_id: string;
}

export interface ICurrentAccountNotes {
  date: string;
  credit: number | null;
  debit: number | null;
  iban: string | null;
  payment_batch_id: string | null;
  payment_method: string | null;
  notes: string | null;
  created_at: string;
  created_by: string;
  updated_at: string;
  updated_by: string;
}

export interface IFindCurrentAccountByOwnerIdResponse {
  payment_date: string;
  emission_date: string;
  document_description: string;
  document_name: string;
  debit: number;
  credit: number;
  notes: ICurrentAccountNotes[];
  por_regularizar: number;
  balance: number;
  href?: string | null;
}

export interface IUnpaidPurchasesRequest {
  provider_id: string;
  client_id: string | null;
}

export interface IUnpaidByDatePurchasesRequest {
  due_date: Date;
}

export type IPaymentOrigin =
  | 'CC Fornecedor'
  | 'CC Cliente'
  | 'CC Fornecedor (Descontinuado)'
  | 'CC Cliente (Descontinuado)';

export interface IUnpaidPurchasesResponse extends Pick<Entity, 'date' | 'debit' | 'credit'> {
  document_header: Pick<DocumentHeader, 'emission_date' | 'pdf_link' | 'id'>;
  origin: IPaymentOrigin;
  document_type_code: string | null;
  document_name: string;
  user?: {
    user_name: string;
    provider_id: string;
    client_id?: string | null;
  };
  maturity_days: number;
  observations: IObservation.IFindByOwnerResponse[];
}

export interface IProviderPaymentDocument {
  document_header_id: string;
  value: number;
  discount_value?: number | null;
}

export interface IProviderPaymentRequest {
  provider_id: string;
  client_id: string | null;
  documents: IProviderPaymentDocument[];
  total: number;
}

export interface IPaymentsNotExportedRequest {
  whiteList?: string[];
}

export interface IPaymentsNotExportedResponse {
  iban: string;
  payment_ids: string[];
  npf_dh: {
    id: string;
    name: string;
    pdf_link: string | null;
    total: number;
  }[];
  provider: string;
  value: number;
}

export interface IExportPaymentsRequest {
  provider: string;
  value: number;
  npf_ids?: string[];
}

export type IBatchType = 'national' | 'international' | 'null';

export interface IPaymentsBatchNpfDh {
  id: string;
  name: string;
  sent: boolean;
  owner_id: string;
  owner_number: number;
  owner_name: string;
  external_document_header_id: string | null;
  pdf_link: string | null;
  total: number;
}

export interface IPaymentsBatchResponse {
  batch_id: string;
  batch_value: number;
  batch_type: IBatchType | null;
  multimedia_id: string | null;
  times_downloaded: number;
  npf_dh: IPaymentsBatchNpfDh[];
  updated_at: Date;
  updated_by: string;
  created_at: Date;
}

export const IExportCurrentAccountOutputEnum: {
  csv: 'csv';
  pdf: 'pdf';
} = {
  csv: 'csv',
  pdf: 'pdf',
};

export type IExportCurrentAccountOutput =
  (typeof IExportCurrentAccountOutputEnum)[keyof typeof IExportCurrentAccountOutputEnum];

export const IExportCurrentAccountOutputOptions = Object.keys(IExportCurrentAccountOutputEnum);

export interface IExportCurrentAccountRequest {
  token: string;
  owner_id: string;
  output: IExportCurrentAccountOutput;
  start_date: Date | null;
  end_date: Date | null;
}
export interface IFindCurrentAccountPaginatedRequest
  extends IShared.IFindByOwnerIdRequest,
    IShared.IPaginationRequest {}
export interface IRepository {
  findById(selector: IShared.IFindByIdRequest): Promise<IFindByIdResponse | null>;

  findByOwnerId(params: IShared.IFindByOwnerIdRequest): Promise<IFindByOwnerIdResponse[]>;

  find(selector: IFindRequest): Promise<IFindResponse[]>;

  findPaginated(selector: IFindPaginatedRequest): Promise<IFindWithPaginationResponse>;

  findPaymentsNotExported(
    params: IPaymentsNotExportedRequest,
  ): Promise<IPaymentsNotExportedResponse[]>;

  findPaymentBatches(): Promise<IPaymentsBatchResponse[]>;

  deleteBatch(data: IShared.IFindByIdRequest): Promise<void>;

  findCurrentAccountByOwnerId(
    params: IShared.IFindByOwnerIdRequest,
  ): Promise<IFindCurrentAccountByOwnerIdResponse[]>;

  findBalanceByOwnerId(params: IShared.IFindByOwnerIdRequest): Promise<number>;

  findSettledMaterialEntrance(params: IUnpaidPurchasesRequest): Promise<IUnpaidPurchasesResponse[]>;

  findSettledMaterialEntranceProviders(): Promise<IFindProvidersWithClientIdResponse[]>;

  findSettledMaterialEntranceByDate(
    params: IUnpaidByDatePurchasesRequest,
  ): Promise<IUnpaidPurchasesResponse[]>;

  createProvidersPayments(params: IProviderPaymentRequest[]): Promise<string[]>;

  exportPayments(params: IExportPaymentsRequest[]): Promise<void>;

  updateDownloadedBatchNumber(id: IShared.IFindByIdRequest): Promise<void>;

  exportCurrentAccount(
    params: IExportCurrentAccountRequest,
  ): Promise<IFindCurrentAccountByOwnerIdResponse[]>;

  findCurrentAccountPaginated(
    params: IFindCurrentAccountPaginatedRequest,
  ): Promise<IShared.IPaginationResponse<IFindCurrentAccountByOwnerIdResponse>>;
}

export type IController = IShared.IEntityWithUserToken<IRepository>;
