import joi from 'joi';
import { messages } from '../../../shared/messages';
import * as IShared from '../../../shared/interface';
import { ICreateRequest, IUpdateRequest } from './interface';

const createBodyKeys: IShared.IRouteRequest<ICreateRequest> = {
  name: joi.string().required(),
  document_set_id: joi.number().integer().allow(null).optional(),
  cash_vat_scheme_indicator: joi.number().integer().required(),
  active_by_default: joi.boolean().default(false).optional(),
  template_id: joi.number().integer().required(),
  img_gr_1: joi.string().required(),
};
export const createBody = joi.object().keys(createBodyKeys).messages(messages);

const updateBodyKeys: IShared.IRouteRequest<IUpdateRequest> = {
  name: joi.string(),
  document_set_id: joi.number().integer().allow(null).optional(),
  cash_vat_scheme_indicator: joi.number().integer().optional(),
  active_by_default: joi.boolean().optional(),
  template_id: joi.number().integer().optional(),
  img_gr_1: joi.string().optional(),
};
export const updateBody = joi.object().keys(updateBodyKeys).messages(messages);
