import joi from 'joi';
import { messages } from '../../../shared/messages';
import * as IShared from '../../../shared/interface';
import {
  documentTypeUserTypes,
  documentTypeCodeTypes,
  ICreateRequest,
  IUpdateRequest,
} from './interfaces';

const createBodyKeys: IShared.IRouteRequest<ICreateRequest> = {
  description: joi.string().required(),
  group: joi.string().required(),
  quantity_in: joi.boolean().default(false).optional(),
  quantity_out: joi.boolean().default(false).optional(),
  credit: joi.boolean().default(false).optional(),
  debit: joi.boolean().default(false).optional(),
  user_type: joi
    .string()
    .valid(...documentTypeUserTypes)
    .required(),
  code: joi
    .string()
    .valid(...documentTypeCodeTypes)
    .required(),
  external_document_type_id: joi.string().allow(null).optional(),
  ensure_same_provider: joi.boolean().default(false).optional(),
  final_consumer: joi.boolean().default(false).optional(),
  is_searchable: joi.boolean().default(false).optional(),
  invoice: joi.boolean().default(false).optional(),
  return: joi.boolean().default(false).optional(),
  pay: joi.boolean().default(false).optional(),
  allow_create_from_header_without_lines: joi.boolean().default(false).optional(),
  allow_commissioner: joi.boolean().default(false).optional(),
  duplicate_group: joi.string().allow(null).optional(),
  allow_duplicates: joi.boolean().default(false).optional(),
  requires_picking: joi.boolean().default(false).optional(),
  allow_document_line_assoc_plugins: joi.boolean().default(false).optional(),
};

export const createBody = joi.object().keys(createBodyKeys).messages(messages);

const updateBodyKeys: IShared.IRouteRequest<IUpdateRequest> = {
  description: joi.string(),
  group: joi.string(),
  quantity_in: joi.boolean(),
  quantity_out: joi.boolean(),
  credit: joi.boolean(),
  debit: joi.boolean(),
  user_type: joi
    .string()
    .valid(...documentTypeUserTypes)
    .optional(),
  code: joi
    .string()
    .valid(...documentTypeCodeTypes)
    .optional(),
  external_document_type_id: joi.string().allow(null, ''),
  ensure_same_provider: joi.boolean(),
  final_consumer: joi.boolean(),
  is_searchable: joi.boolean(),
  invoice: joi.boolean(),
  return: joi.boolean(),
  pay: joi.boolean(),
  allow_create_from_header_without_lines: joi.boolean(),
  allow_commissioner: joi.boolean(),
  duplicate_group: joi.string().allow(null).optional(),
  allow_duplicates: joi.boolean(),
  requires_picking: joi.boolean(),
  allow_document_line_assoc_plugins: joi.boolean(),
};

export const updateBody = joi.object().keys(updateBodyKeys).messages(messages);
