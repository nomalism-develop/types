import * as IShared from '../../../shared/interface';
import { DocumentType, ExternalDocumentType } from '../../../shared/entities';

export type Entity = DocumentType;
export const Route = 'documentType';
export const UpperName = 'DocumentType';
export const LowerName = UpperName[0].toLowerCase() + UpperName.substring(1);

export const DocumentTypeUserTypeEnum: {
  client: 'client';
  provider: 'provider';
  commissioner: 'commissioner';
  store_operator: 'store_operator';
  self: 'self';
} = {
  client: 'client',
  provider: 'provider',
  commissioner: 'commissioner',
  store_operator: 'store_operator',
  self: 'self',
};

export type IDocumentTypeUserType =
  (typeof DocumentTypeUserTypeEnum)[keyof typeof DocumentTypeUserTypeEnum];

export const documentTypeUserTypes = Object.keys(DocumentTypeUserTypeEnum);

export const DocumentTypeCodeTypeEnum: {
  PC: 'PC';
  FTPF: 'FTPF';
  PCTL: 'PCTL';
  AD: 'AD';
  NCAD: 'NCAD';
  FT: 'FT';
  FS: 'FS';
  FR: 'FR';
  NC: 'NC';
  GT: 'GT';
  RE: 'RE';
  LNC: 'LNC';
  NPC: 'NPC';
  QECI: 'QECI';
  OPI: 'OPI';
  OP: 'OP';
  PF: 'PF';
  EF: 'EF';
  EFSN: 'EFSN';
  EM: 'EM';
  GTF: 'GTF';
  DF: 'DF';
  CF: 'CF';
  NPF: 'NPF';
  ADF: 'ADF';
  NCF: 'NCF';
  NCRF: 'NCRF';
  FTF: 'FTF';
  PI: 'PI';
  IM: 'IM';
  IMN: 'IMN';
  QE: 'QE';
  TR: 'TR';
  TRS: 'TRS';
  GTI: 'GTI';
  ADSM: 'ADSM';
  FTSM: 'FTSM';
  FP: 'FP';
  RDP: 'RDP';
  NRCL: 'NRCL';
  FPM: 'FPM';
} = {
  PC: 'PC',
  FTPF: 'FTPF',
  PCTL: 'PCTL',
  AD: 'AD',
  NCAD: 'NCAD',
  FT: 'FT',
  FS: 'FS',
  FR: 'FR',
  NC: 'NC',
  GT: 'GT',
  RE: 'RE',
  LNC: 'LNC',
  NPC: 'NPC',
  QECI: 'QECI',
  OPI: 'OPI',
  OP: 'OP',
  PF: 'PF',
  EF: 'EF',
  EFSN: 'EFSN',
  EM: 'EM',
  GTF: 'GTF',
  DF: 'DF',
  CF: 'CF',
  NPF: 'NPF',
  ADF: 'ADF',
  NCF: 'NCF',
  NCRF: 'NCRF',
  FTF: 'FTF',
  PI: 'PI',
  IM: 'IM',
  IMN: 'IMN',
  QE: 'QE',
  TR: 'TR',
  TRS: 'TRS',
  GTI: 'GTI',
  ADSM: 'ADSM',
  FTSM: 'FTSM',
  FP: 'FP',
  RDP: 'RDP',
  NRCL: 'NRCL',
  FPM: 'FPM',
};

export type IDocumentTypeCodeType =
  (typeof DocumentTypeCodeTypeEnum)[keyof typeof DocumentTypeCodeTypeEnum];

export const documentTypeCodeTypes = Object.keys(DocumentTypeCodeTypeEnum);

export interface IEntityExtended extends DocumentType {
  external_document_type: ExternalDocumentType | null;
}

export interface ICreateRequest {
  external_document_type_id: string | null;

  code: IDocumentTypeCodeType;
  description: string;

  group: string;
  is_searchable: boolean;

  quantity_in?: boolean;
  quantity_out?: boolean;

  credit?: boolean;
  debit?: boolean;

  invoice: boolean;
  return: boolean;
  pay: boolean;
  allow_create_from_header_without_lines: boolean;
  allow_commissioner: boolean;
  duplicate_group?: string | null;
  allow_duplicates: boolean;
  requires_picking: boolean;
  allow_document_line_assoc_plugins: boolean;

  user_type: IDocumentTypeUserType;
  final_consumer: boolean;

  ensure_same_provider: boolean;
}

export interface IUpdateRequest {
  external_document_type_id?: string | null;

  code?: IDocumentTypeCodeType;
  description?: string;

  group?: string;
  is_searchable?: boolean;

  quantity_in?: boolean;
  quantity_out?: boolean;

  credit?: boolean;
  debit?: boolean;

  invoice: boolean;
  return: boolean;
  pay: boolean;
  allow_create_from_header_without_lines: boolean;
  allow_commissioner?: boolean;
  duplicate_group?: string | null;
  allow_duplicates?: boolean;
  requires_picking?: boolean;
  allow_document_line_assoc_plugins: boolean;
  user_type?: IDocumentTypeUserType;
  final_consumer?: boolean;

  ensure_same_provider?: boolean;
}

export interface IFindRequest {
  description?: string;
}

export interface IFindResponse {
  id: number;
  group: string;
  description: string;
}

export interface IRepository {
  findById(id: number): Promise<Entity | null>;
  findByQuery(data: IFindRequest): Promise<Entity[]>;

  findMinified(params?: IShared.IFindMinifiedRequest): Promise<IShared.IFindMinifiedResponse[]>;
  find(): Promise<IFindResponse[]>;
  findDetailed(): Promise<IEntityExtended[]>;

  create(data: ICreateRequest): Promise<IEntityExtended>;
  update(
    selector: IShared.IFindByIdNumberRequest,
    data: IUpdateRequest,
  ): Promise<IEntityExtended | null>;
  deleteOne(selector: IShared.IFindByIdNumberRequest): Promise<IEntityExtended | null>;
}

export type IController = IShared.IEntityWithUserToken<IRepository>;

export type IApi = Omit<IRepository, 'findById' | 'findByQuery'>;
