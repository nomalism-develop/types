import joi from 'joi';
import { messages } from '../../../shared/messages';
import * as IShared from '../../../shared/interface';
import {
  IConferirEncomendaClienteRequest,
  IFindStockToOrderRequest,
  conferirEncomendaClienteOptions,
  ISetProviderRequest,
  IUndoProviderOrderRequest,
  IUnsetCativado,
  IFindRequest,
} from './interfaces';

const undoProviderOrderBodyKeys: IShared.IRouteRequest<IUndoProviderOrderRequest> = {
  document_line_ids: joi.array().items(joi.string().uuid().required()).required(),
};

export const undoProviderOrderBody = joi
  .object()
  .keys(undoProviderOrderBodyKeys)
  .messages(messages);

export const conferirEncomendaClienteBody = joi
  .object<IConferirEncomendaClienteRequest>()
  .keys({
    option: joi
      .string()
      .valid(...conferirEncomendaClienteOptions)
      .required(),
    document_line_ids: joi.array().items(joi.string().uuid().required()).required(),
  })
  .messages(messages);

const findStockToOrderParamsKeys: IShared.IRouteRequest<IFindStockToOrderRequest> = {
  id_provider: joi.number().positive().required(),
};

export const findStockToOrderParamsBodyKeys = joi
  .object()
  .keys(findStockToOrderParamsKeys)
  .messages(messages);

const setProviderBodyKeys: IShared.IRouteRequest<ISetProviderRequest> = {
  document_line_assoc_ids: joi.array().items(joi.string().uuid().required()).required(),
  id_provider: joi.number().positive().required(),
};

export const setProviderBody = joi.object().keys(setProviderBodyKeys).messages(messages);

const unsetCativadoBodyKeys: IShared.IRouteRequest<IUnsetCativado> = {
  document_line_id: joi.string().uuid().required(),
};

export const unsetCativadoBody = joi.object().keys(unsetCativadoBodyKeys).messages(messages);

export const findQuery = joi
  .object<IFindRequest>()
  .keys({
    date_start: joi.date().required(),
    date_end: joi.date().required(),
  })
  .messages(messages);
