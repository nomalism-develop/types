import * as IShared from '../../../shared/interface';

export const Route = 'order';
export const UpperName = 'Order';
export const LowerName = UpperName[0].toLowerCase() + UpperName.substring(1);

export interface IUndoProviderOrderRequest {
  document_line_ids: string[];
}

export const ConferirEncomendaClienteOptionsEnum: {
  'Por Confirmar': 'Por Confirmar';
  'Espera Def. Det. Vendedor': 'Espera Def. Det. Vendedor';
  'Espera Info Cliente': 'Espera Info Cliente';
  'Espera Dev. Catálogo': 'Espera Dev. Catálogo';
  'Espera Envio (RMCA)': 'Espera Envio (RMCA)';
  'Espera Envio (RM)': 'Espera Envio (RM)';
  'Espera Marcação (RM)': 'Espera Marcação (RM)';
  'Espera Folha (RM)': 'Espera Folha (RM)';
  'Espera Vendedor Retificar (RM)': 'Espera Vendedor Retificar (RM)';
  'Espera Conferencia (RM)': 'Espera Conferencia (RM)';
  'Espera Ok Cliente (RM)': 'Espera Ok Cliente (RM)';
  'Espera Consulta Stock/Cotação': 'Espera Consulta Stock/Cotação';
  'Espera Receção (Consulta)': 'Espera Receção (Consulta)';
  'Espera Vendedor Retificar (Consulta)': 'Espera Vendedor Retificar (Consulta)';
  'Cliente Foi Pensar': 'Cliente Foi Pensar';
  'Espera Atelier': 'Espera Atelier';
  'Espera Produção Interna': 'Espera Produção Interna';
  'Espera Estofador': 'Espera Estofador';
  'Espera Prod (Mont)': 'Espera Prod (Mont)';
  'Espera Montagem': 'Espera Montagem';
  'Espera Corte Tecidos': 'Espera Corte Tecidos';
  'Espera Confecção (tecidos cortados)': 'Espera Confecção (tecidos cortados)';
  'Encomendar Serviço': 'Encomendar Serviço';
  'Encomendar Serviço Sem Notificação': 'Encomendar Serviço Sem Notificação';
  'Encomendar VA': 'Encomendar VA';
  'Encomendar VA Sem Notificação': 'Encomendar VA Sem Notificação';
  'Encomendar Stock': 'Encomendar Stock';
  'Cancelar Encomendar': 'Cancelar Encomendar';
  'Stock em Loja': 'Stock em Loja';
  Refaturar: 'Refaturar';
  'Desligar do Sheets': 'Desligar do Sheets';
  'Fechar Linha': 'Fechar Linha';
} = {
  'Por Confirmar': 'Por Confirmar',
  'Espera Def. Det. Vendedor': 'Espera Def. Det. Vendedor',
  'Espera Info Cliente': 'Espera Info Cliente',
  'Espera Dev. Catálogo': 'Espera Dev. Catálogo',
  'Espera Envio (RMCA)': 'Espera Envio (RMCA)',
  'Espera Envio (RM)': 'Espera Envio (RM)',
  'Espera Marcação (RM)': 'Espera Marcação (RM)',
  'Espera Folha (RM)': 'Espera Folha (RM)',
  'Espera Vendedor Retificar (RM)': 'Espera Vendedor Retificar (RM)',
  'Espera Conferencia (RM)': 'Espera Conferencia (RM)',
  'Espera Ok Cliente (RM)': 'Espera Ok Cliente (RM)',
  'Espera Consulta Stock/Cotação': 'Espera Consulta Stock/Cotação',
  'Espera Receção (Consulta)': 'Espera Receção (Consulta)',
  'Espera Vendedor Retificar (Consulta)': 'Espera Vendedor Retificar (Consulta)',
  'Cliente Foi Pensar': 'Cliente Foi Pensar',
  'Espera Atelier': 'Espera Atelier',
  'Espera Produção Interna': 'Espera Produção Interna',
  'Espera Estofador': 'Espera Estofador',
  'Espera Prod (Mont)': 'Espera Prod (Mont)',
  'Espera Montagem': 'Espera Montagem',
  'Espera Corte Tecidos': 'Espera Corte Tecidos',
  'Espera Confecção (tecidos cortados)': 'Espera Confecção (tecidos cortados)',
  'Encomendar Serviço': 'Encomendar Serviço',
  'Encomendar Serviço Sem Notificação': 'Encomendar Serviço Sem Notificação',
  'Encomendar VA': 'Encomendar VA',
  'Encomendar VA Sem Notificação': 'Encomendar VA Sem Notificação',
  'Encomendar Stock': 'Encomendar Stock',
  'Cancelar Encomendar': 'Cancelar Encomendar',
  'Stock em Loja': 'Stock em Loja',
  Refaturar: 'Refaturar',
  'Desligar do Sheets': 'Desligar do Sheets',
  'Fechar Linha': 'Fechar Linha',
};

export type IConferirEncomendaClienteOptions =
  (typeof ConferirEncomendaClienteOptionsEnum)[keyof typeof ConferirEncomendaClienteOptionsEnum];

export const conferirEncomendaClienteOptions = Object.keys(ConferirEncomendaClienteOptionsEnum);

export interface IConferirEncomendaClienteRequest {
  option: IConferirEncomendaClienteOptions;
  document_line_ids: string[];
}

export interface IConferirEncomendaClienteResponse {
  document_line_id: string;
}

export interface IFindStockToOrderRequest {
  id_provider: number;
}

export interface IFindStockToOrderResponse {
  document_line_assoc_id: string;
  document_line_id: string;
  document_number: string;
  owner_id: string;
  product: {
    product_id: string;
    reference?: string;
    designation?: string;
    thumbnail_multimedia_id?: string;
    quantity: number;
    unit_of_measure_notation: string;
  };
}

export interface ISetProviderRequest {
  document_line_assoc_ids: string[];
  id_provider: number;
}

export interface IUnsetCativado {
  document_line_id: string;
}

export interface IFindRequest {
  date_start: Date;
  date_end: Date;
}

export type IOrderType = 'VA' | 'Stock';

export interface IFindResponse {
  order_type: IOrderType;
  // pc document_header
  pc_id?: string;
  // pf document_header
  pf_id?: string;
  // document_line
  document_line_id: string;
  document_line_quantity: number;
  // product
  product_id: string;
  product_reference: string;
  product_designation: string;
  product_thumbnail: string | null;
  product_unit_of_measure_quantity_notation: string;
  // log
  done: boolean;
  error: unknown;
  created_at: Date | null;
}

export interface IRepository {
  findStockToOrder(data: IFindStockToOrderRequest): Promise<IFindStockToOrderResponse[]>;

  conferirEncomendaCliente(
    data: IConferirEncomendaClienteRequest,
  ): Promise<IConferirEncomendaClienteResponse[]>;

  undoEncomendaVA(selector: IShared.IFindByIdRequest): Promise<void>;

  setProvider(data: ISetProviderRequest): Promise<void>;

  unsetProvider(selector: IShared.IFindByIdRequest): Promise<void>;

  undoEncomendaStock(selector: IShared.IFindByIdRequest): Promise<void>;

  undoProviderOrder(data: IUndoProviderOrderRequest): Promise<void>;

  unsetCativado(data: IUnsetCativado): Promise<void>;

  find(params: IFindRequest): Promise<IFindResponse[]>;
}

export type IApi = IRepository;

export type IController = IShared.IEntityWithUserToken<IRepository>;
