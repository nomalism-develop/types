import * as IShared from '../../../shared/interface';

export interface IFindProviderReturnRequest {
  provider_id: string;
  search_value?: string | null;
}

export interface IFindProviderReturnOptionValue {
  document_line_id: string;
  product_id: string;
  product_thumbnail: string | null;
  product_reference: string;
  product_designation: string;
  quantity: number;
  unit_of_measure_quantity_notation: string;
  provider_total_without_tax: number;
  provider_total: number;
}

export interface IFindProviderReturnResponse {
  id: string; // document_header_id
  label: string; // document_name
  value: IFindProviderReturnOptionValue[];
}

export interface ICreateProviderCreditNoteFromReturnDocumentHeaders {
  id: string;
  document_line_ids: string[];
}

export interface ICreateProviderCreditNoteFromReturnRequest {
  emission_date: Date;
  external_document_name: string;
  observations: string;
  document_headers: ICreateProviderCreditNoteFromReturnDocumentHeaders[];
  acerto: number;
  total: number;
}

export interface IRepository {
  findProviderReturn(params: IFindProviderReturnRequest): Promise<IFindProviderReturnResponse[]>;

  createProviderCreditNoteFromReturn(
    params: ICreateProviderCreditNoteFromReturnRequest,
  ): Promise<void>;
}

export type IController = IShared.IEntityWithUserToken<IRepository>;
