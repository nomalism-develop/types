import joi from 'joi';
import { messages } from '../../../shared/messages';
import * as IShared from '../../../shared/interface';
import {
  IFindProviderReturnRequest,
  ICreateProviderCreditNoteFromReturnDocumentHeaders,
  ICreateProviderCreditNoteFromReturnRequest,
} from './interfaces';

const findProviderReturnQueryKeys: IShared.IRouteRequest<IFindProviderReturnRequest> = {
  search_value: joi.string().allow('', null).optional(),
  provider_id: joi.string().uuid().required(),
};
export const findProviderReturnQuery = joi
  .object()
  .keys(findProviderReturnQueryKeys)
  .messages(messages);

const createProviderCreditNoteFromReturnDocumentHeadersBodyKeys: IShared.IRouteRequest<ICreateProviderCreditNoteFromReturnDocumentHeaders> =
  {
    id: joi.string().uuid().required(),
    document_line_ids: joi.array().unique().items(joi.string().uuid().required()).required(),
  };
const createProviderCreditNoteFromReturnBodyKeys: IShared.IRouteRequest<ICreateProviderCreditNoteFromReturnRequest> =
  {
    emission_date: joi.date().required(),
    external_document_name: joi.string().required(),
    observations: joi.string().allow(null, '').required(),
    document_headers: joi
      .array()
      .items(
        joi.object().keys(createProviderCreditNoteFromReturnDocumentHeadersBodyKeys).required(),
      )
      .required(),
    acerto: joi.number().required(),
    total: joi.number().positive().required(),
  };
export const createProviderCreditNoteFromReturnBody = joi
  .object()
  .keys(createProviderCreditNoteFromReturnBodyKeys)
  .messages(messages);
