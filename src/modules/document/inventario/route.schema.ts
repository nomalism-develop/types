import joi from 'joi';
import { messages } from '../../../shared/messages';
import * as IShared from '../../../shared/interface';
import { ICreateInventoryRequest } from './interface';

const createInventoryRequestKeys: IShared.IRouteRequestWithStamps<ICreateInventoryRequest> = {
  product_id: joi.string().uuid().required(),
  registered_quantity: joi.number().allow(0).optional(),
  inventory_quantity: joi.number().positive().allow(0).required(),
  created_by: joi.string().uuid().optional(),
  updated_by: joi.string().uuid().optional(),
};
export const createInventoryRequest = joi
  .object()
  .keys(createInventoryRequestKeys)
  .messages(messages);
