import * as IShared from '../../../shared/interface';

export const Route = 'inventario';
export const UpperName = 'Inventario';
export const LowerName = UpperName[0].toLowerCase() + UpperName.substring(1);

export interface ICreateInventoryRequest {
  product_id: string;
  registered_quantity: number;
  inventory_quantity: number;
  created_by?: string;
  updated_by?: string;
}

export interface IRepository {
  createInventory(data: ICreateInventoryRequest): Promise<void>;
}

export type IController = IShared.IEntityWithUserToken<IRepository>;
