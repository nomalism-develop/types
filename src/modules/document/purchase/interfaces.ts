import * as IShared from '../../../shared/interface';
import { DocumentLineNote } from '../../../shared/entities';

export const Route = 'purchase';
export const UpperName = 'Purchase';
export const LowerName = UpperName[0].toLowerCase() + UpperName.substring(1);

export type IUnpaidMaterialEntranceLineType = 'Por comprar' | 'Comprado' | 'Devolvido' | 'Prisão';

export interface IUnpaidMaterialEntranceLineDetails {
  document_line_id: string;
  quantity: number;
  notes: DocumentLineNote[];
  pc_name: string | null;
  pc_id: string | null;
}

export interface IUnpaidMaterialEntranceLineResponse {
  group: string;
  document_line_ids: string[];
  product_id: string;
  reference: string;
  designation: string;
  multimedia_id: string | null;
  provider_ref: string | null;
  pf_id: string;
  ef_name: string;
  quantity: number;
  discount: number;
  provider_purchase_discount: number;
  provider_tax_value: number;
  client_tax_value: number;
  global_discount: number;
  preco_custo: number;
  preco_venda: number;
  margin: number | null;
  total_without_tax: number;
  total: number;
  types: IUnpaidMaterialEntranceLineType[];
  created_by: string;
  created_at: Date;
  details: IUnpaidMaterialEntranceLineDetails[];
  consignacao: boolean;
}

export interface IUnpaidMaterialEntranceItemResponse {
  name: string;
  lines: IUnpaidMaterialEntranceLineResponse[];
}

export interface IUnpaidMaterialEntranceResponse {
  entradaMaterial: IUnpaidMaterialEntranceItemResponse[];
}

export interface IPurchaseFromProviderShipping {
  product_id: string;
  price: number;
}

export interface IPurchaseFromProviderAdjustment {
  value: number; // (pode ser negativo)
}

export interface IPurchaseFromProviderTaxRetention {
  value: number;
}

export interface IPurchaseFromProviderRequest {
  invoice_number: string;
  invoice_emission_date: Date;
  invoice_due_date: Date;
  document_line_ids: string[];
  global_discount?: number | null;
  shipping?: IPurchaseFromProviderShipping;
  adjustment?: IPurchaseFromProviderAdjustment;
  tax_retention?: IPurchaseFromProviderTaxRetention;
  note?: string;
  vat_self_accounting?: boolean;
  purchase_without_invoice: boolean;
}

export interface ISyncRequest {
  provider_id: string;
  document_line_ids: string[];
}

export interface ISyncLines {
  document_line_id: string;
  preco_custo: number;
}

export interface ISyncResponse {
  provider_discount: number;
  provider_tax: number;
  lines: ISyncLines[];
}

export interface IRepository {
  findUnpaidMaterialEntrance(
    selector: IShared.IFindByIdRequest,
  ): Promise<IUnpaidMaterialEntranceResponse>;

  findUnpaidMaterialEntranceProviders(): Promise<IShared.IFindMinifiedResponse[]>;

  purchaseFromProvider(data: IPurchaseFromProviderRequest): Promise<void>;

  sync(data: ISyncRequest): Promise<ISyncResponse>;
}

export type IApi = IRepository;

export type IController = IShared.IEntityWithUserToken<IRepository>;
