import joi from 'joi';
import { messages } from '../../../shared/messages';
import * as IShared from '../../../shared/interface';

import {
  IPurchaseFromProviderRequest,
  ISyncRequest,
  IPurchaseFromProviderShipping,
  IPurchaseFromProviderAdjustment,
  IPurchaseFromProviderTaxRetention,
} from './interfaces';

const purchaseFromProviderBodyKeys: IShared.IRouteRequest<IPurchaseFromProviderRequest> = {
  invoice_number: joi.string().required(),
  invoice_emission_date: joi.date().required(),
  invoice_due_date: joi.date().required(),
  document_line_ids: joi.array().items(joi.string().required()).required(),
  global_discount: joi.number().min(0).max(100).allow(null).optional(),
  shipping: joi
    .object<IPurchaseFromProviderShipping>()
    .keys({
      product_id: joi.string().required(),
      price: joi.number().allow(0).positive().required(),
    })
    .optional(),
  adjustment: joi
    .object<IPurchaseFromProviderAdjustment>()
    .keys({
      value: joi.number().required(),
    })
    .optional(),
  tax_retention: joi
    .object<IPurchaseFromProviderTaxRetention>()
    .keys({
      value: joi.number().allow(0).positive().required(),
    })
    .optional(),
  note: joi.string().allow('', null).optional(),
  vat_self_accounting: joi.boolean().optional(),
  purchase_without_invoice: joi.boolean().required(),
};

export const purchaseFromProviderBody = joi
  .object()
  .keys(purchaseFromProviderBodyKeys)
  .messages(messages);

export const syncBody = joi.object<ISyncRequest>().keys({
  document_line_ids: joi.array().items(joi.string().uuid().required()).required(),
  provider_id: joi.string().uuid().required(),
});
