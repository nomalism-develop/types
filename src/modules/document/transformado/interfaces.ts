import * as IShared from '../../../shared/interface';

export const Route = 'transformado';
export const UpperName = 'Transformado';
export const LowerName = UpperName[0].toLowerCase() + UpperName.substring(1);

export interface IProductsCreateTransformado {
  id: string;
  quantity: number;
}

export interface ICreateTransformado {
  product_id: string;
  quantity: number;
  price_sale: number;
  price_cost: number;
  products: IProductsCreateTransformado[];
}

export interface IRepository {
  create(data: ICreateTransformado): Promise<string>;
}

export type IController = IShared.IEntityWithUserToken<IRepository>;
