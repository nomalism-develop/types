import joi from 'joi';

import * as IShared from '../../../shared/interface';
import { messages } from '../../../shared/messages';
import { ICreateTransformado, IProductsCreateTransformado } from './interfaces';

const productsCreateTransformadosKeys: IShared.IRouteRequestWithStamps<IProductsCreateTransformado> =
  {
    id: joi.string().uuid().required().invalid(joi.ref('/product_id')),
    quantity: joi.number().positive().required(),
  };

const createTransformadosKeys: IShared.IRouteRequestWithStamps<ICreateTransformado> = {
  product_id: joi.string().uuid().required(),
  quantity: joi.number().positive().required(),
  price_cost: joi.number().positive().allow(0).required(),
  price_sale: joi.number().positive().allow(0).required(),
  products: joi.array().items(joi.object().keys(productsCreateTransformadosKeys)).min(1),
};
export const createTransformadosBody = joi
  .object()
  .keys(createTransformadosKeys)
  .messages(messages);
