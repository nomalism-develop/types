import * as IShared from '../../../shared/interface';

export interface IFindProviderInvoiceRequest {
  provider_id: string;
  search_value?: string | null;
}

export interface IFindProviderInvoiceResponse {
  id: string; // invoice document_header_id
  label: string; // invoice external_document_name
  value: number; // invoice grand total
}

export interface IValueByVatTax {
  taxPercentage: number;
  total: number;
}

export interface ICreateProviderFinancialCreditNoteRequest {
  external_document_name: string;
  provider_id: string;
  parent_document_header_id: string | null;
  emission_date: Date;
  observations: string;
  values: IValueByVatTax[];
}

export interface IRepository {
  findProviderInvoice(params: IFindProviderInvoiceRequest): Promise<IFindProviderInvoiceResponse[]>;

  createProviderFinancialCreditNote(
    params: ICreateProviderFinancialCreditNoteRequest,
  ): Promise<void>;
}

export type IController = IShared.IEntityWithUserToken<IRepository>;
