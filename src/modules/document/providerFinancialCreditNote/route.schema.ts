import joi from 'joi';
import { messages } from '../../../shared/messages';
import * as IShared from '../../../shared/interface';
import {
  IFindProviderInvoiceRequest,
  IValueByVatTax,
  ICreateProviderFinancialCreditNoteRequest,
} from './interfaces';

const findProviderInvoiceQueryKeys: IShared.IRouteRequest<IFindProviderInvoiceRequest> = {
  search_value: joi.string().allow('', null).optional(),
  provider_id: joi.string().uuid().required(),
};
export const findProviderInvoiceQuery = joi
  .object()
  .keys(findProviderInvoiceQueryKeys)
  .messages(messages);

const valueByVatTaxKeys: IShared.IRouteRequest<IValueByVatTax> = {
  taxPercentage: joi.number().positive().allow(0).required(),
  total: joi.number().positive().required(),
};

const createProviderFinancialCreditNoteBodyKeys: IShared.IRouteRequest<ICreateProviderFinancialCreditNoteRequest> =
  {
    emission_date: joi.date().required(),
    external_document_name: joi.string().required(),
    provider_id: joi.string().uuid().required(),
    parent_document_header_id: joi.string().uuid().optional().allow(null, ''),
    observations: joi.string().allow(null, '').required(),
    values: joi
      .array()
      .unique('taxPercentage')
      .items(joi.object().keys(valueByVatTaxKeys).required())
      .required(),
  };
export const createProviderFinancialCreditNoteBody = joi
  .object()
  .keys(createProviderFinancialCreditNoteBodyKeys)
  .messages(messages);
