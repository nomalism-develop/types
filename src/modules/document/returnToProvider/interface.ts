import { DocumentLineNote } from '../../../shared/entities';
import * as IShared from '../../../shared/interface';

export const Route = 'return_to_provider';
export const UpperName = 'ReturnToProvider';
export const LowerName = UpperName[0].toLowerCase() + UpperName.substring(1);

export interface IFindLinesToReturnResponse {
  document_line_assoc_id: string;
  df_name: string;
  pdf_link: string | null;
  document_line_id: string;
  provider_id: string;
  edh_id: string;
  reference: string;
  designation: string;
  thumbnail: string | null;
  product_id: string;
  quantity: number;
  emails_sent: number;
  already_picked_up: boolean;
  return_to_provider_without_credit_note: boolean;
  notes: DocumentLineNote[];
  updated_by: string | null;
  updated_at: Date;
}

export const checkLinesToReturnOptions = [
  'Levantar',
  'Cancelar Levantamento',
  'Enviar',
  'Cancelar Envio',
  'Quebrar',
  'Cancelar Devolução',
] as const;

export type ICheckLinesToReturnOptions = (typeof checkLinesToReturnOptions)[number];

export interface ICheckLinesToReturnLinesRequest {
  document_line_assoc_id: string;
  document_line_id: string;
}

export interface ICheckLinesToReturnRequest {
  option: ICheckLinesToReturnOptions;
  lines: ICheckLinesToReturnLinesRequest[];
}

export interface IRepository {
  checkLinesToReturn(params: ICheckLinesToReturnRequest): Promise<void>;

  findLinesToReturn(
    params: IShared.IFindOptionalByOwnerIdRequest,
  ): Promise<IFindLinesToReturnResponse[]>;

  findLinesToReturnProviders(): Promise<IShared.IFindMinifiedResponse[]>;
}

export type IController = IShared.IEntityWithUserToken<IRepository>;
