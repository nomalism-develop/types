import joi from 'joi';
import { messages } from '../../../shared/messages';
import * as IShared from '../../../shared/interface';
import {
  ICheckLinesToReturnRequest,
  ICheckLinesToReturnLinesRequest,
  checkLinesToReturnOptions,
} from './interface';

// check lines to return
const checkLinesToReturnLinesKeys: IShared.IRouteRequest<ICheckLinesToReturnLinesRequest> = {
  document_line_assoc_id: joi.string().uuid().required(),
  document_line_id: joi.string().uuid().required(),
};

const checkLinesToReturnBodyKeys: IShared.IRouteRequest<ICheckLinesToReturnRequest> = {
  option: joi
    .string()
    .valid(...checkLinesToReturnOptions)
    .required(),
  lines: joi.array().items(joi.object().keys(checkLinesToReturnLinesKeys).required()).required(),
};

export const checkLinesToReturnBody = joi
  .object()
  .keys(checkLinesToReturnBodyKeys)
  .messages(messages);
