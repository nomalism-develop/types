import * as IShared from '../../../shared/interface';

export interface ICreateRequest {
  document_header_id?: string;
  from_document_header_id: string;
  document_type_id: number;
  product_designation: string;
  price_sale: number;
}

export interface IRepository {
  create(params: ICreateRequest): Promise<IShared.ITaskCluster[]>;
}

export type IController = IShared.IEntityWithUserToken<IRepository>;
