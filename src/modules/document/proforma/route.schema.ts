import joi from 'joi';
import { messages } from '../../../shared/messages';
import { type ICreateRequest } from './interfaces';

export const createBody = joi
  .object<ICreateRequest>()
  .keys({
    document_header_id: joi.string().uuid().optional(),
    document_type_id: joi.number().integer().positive().required(),
    from_document_header_id: joi.string().uuid().required(),
    price_sale: joi.number().positive().required(),
    product_designation: joi.string().required(),
  })
  .messages(messages);
