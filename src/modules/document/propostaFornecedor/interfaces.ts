import * as IShared from '../../../shared/interface';

export interface IProviderProposalData {
  product_id: string;
  quantity: number;
}

export interface IProviderProposalRequest {
  provider_id: string;
  data: IProviderProposalData[];
}

export interface IRepository {
  createProviderProposal(data: IProviderProposalRequest): Promise<string>;
}

export type IController = IShared.IEntityWithUserToken<IRepository>;
