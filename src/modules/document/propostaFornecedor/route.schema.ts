import joi from 'joi';
import { messages } from '../../../shared/messages';
import * as IShared from '../../../shared/interface';
import { IProviderProposalRequest, IProviderProposalData } from './interfaces';

const providerProposalData: IShared.IRouteRequest<IProviderProposalData> = {
  product_id: joi.string().uuid().required(),
  quantity: joi.number().required(),
};

const providerPRoposalBodyKeys: IShared.IRouteRequest<IProviderProposalRequest> = {
  provider_id: joi.string().uuid().required(),
  data: joi
    .array()
    .items({
      ...providerProposalData,
    })
    .required(),
};

export const providerProposalBody = joi.object().keys(providerPRoposalBodyKeys).messages(messages);
