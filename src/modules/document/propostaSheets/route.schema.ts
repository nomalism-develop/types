import joi from 'joi';

import * as IShared from '../../../shared/interface';
import { messages } from '../../../shared/messages';
import { ICloneSheetToNewDocumentRequest, IImportFromSheetRequest } from './interface';

export const importFromSheetKeys = joi
  .object<IImportFromSheetRequest>()
  .keys({
    id: joi.string().required(),
    original_document_header_id: joi.string().uuid().optional(),
  })
  .messages(messages);

// find by google sheet id validation
const findByGoogleSheetIdKeys: IShared.IRouteRequestWithStamps<IShared.IFindByIdRequest> = {
  id: joi.string().required(),
};
export const findByGoogleSheetId = joi.object().keys(findByGoogleSheetIdKeys).messages(messages);

export const cloneSheetToNewDocumentQuery = joi.object<ICloneSheetToNewDocumentRequest>().keys({
  id: joi.string().uuid().required(),
});
