import * as IShared from '../../../shared/interface';
import { DocumentLinePendingConflict, DocumentLine } from '../../../shared/entities';

export type Entity = DocumentLinePendingConflict;

export const Route = 'documentLine_pending_conflict';
export const UpperName = 'DocumentLinePendingConflict';
export const LowerName = UpperName[0].toLowerCase() + UpperName.substring(1);

export const IConflictEnum: {
  update: 'update';
  delete: 'delete';
  price: 'price';
} = {
  update: 'update',
  delete: 'delete',
  price: 'price',
};
export type IConflict = (typeof IConflictEnum)[keyof typeof IConflictEnum];
export interface IFindByGooglesheetIdResponse {
  document_header_id: string;
}

export type IConflictCreateRequest = Omit<
  Entity,
  'id' | 'created_at' | 'created_by' | 'updated_at' | 'updated_by'
>;

export interface IConflictFindByOwnerIdResponse extends Entity {
  document_line: Pick<DocumentLine, 'preco_venda' | 'client_discount' | 'quantity'> & {
    designation: string;
    reference: string;
  };
}

export interface ICreateSheetFromDocumentResponse {
  google_sheet_id: string;
}

export interface ICloneSheetToNewDocumentResponse {
  id: string;
  document_number: number;
}

export interface IIsProcessingResponse {
  is_processing: boolean;
  last_error: string | null;
}

export interface ICloneSheetToNewDocumentRequest {
  id: string;
}

export interface IImportFromSheetRequest {
  id: string;
  original_document_header_id?: string;
}

export interface IRepository {
  importFromSheet(params: IImportFromSheetRequest): Promise<IFindByGooglesheetIdResponse>;

  isProcessing(params: IShared.IFindByIdRequest): Promise<IIsProcessingResponse>;

  conflictAccept(params: IShared.IFindByIdRequest): Promise<void>;

  conflictDelete(params: IShared.IFindByIdRequest): Promise<void>;

  conflictFindByOwnerId(
    params: IShared.IFindByOwnerIdRequest,
  ): Promise<IConflictFindByOwnerIdResponse[]>;
}

export interface IApi extends IRepository {
  createSheetFromDocument(
    params: IShared.IFindByIdRequest,
  ): Promise<ICreateSheetFromDocumentResponse>;

  cloneSheetToNewDocument(
    params: ICloneSheetToNewDocumentRequest,
  ): Promise<ICloneSheetToNewDocumentResponse>;
}

export type IController = IShared.IEntityWithUserToken<IRepository>;
