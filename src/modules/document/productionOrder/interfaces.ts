import * as IShared from '../../../shared/interface';

export interface ICreateProductionOrderRequest {
  provider_id: string;
  group_id: string;
}

export interface IRepository {
  createProductionOrder(data: ICreateProductionOrderRequest): Promise<void>;
}

export type IController = IShared.IEntityWithUserToken<IRepository>;
