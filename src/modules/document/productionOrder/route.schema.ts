import joi from 'joi';
import { messages } from '../../../shared/messages';
import * as IShared from '../../../shared/interface';
import { ICreateProductionOrderRequest } from './interfaces';

const createProductionOrderBodyKeys: IShared.IRouteRequest<ICreateProductionOrderRequest> = {
  provider_id: joi.string().uuid().required(),
  group_id: joi.string().uuid().required(),
};
export const createProductionOrderBody = joi
  .object()
  .keys(createProductionOrderBodyKeys)
  .messages(messages);
