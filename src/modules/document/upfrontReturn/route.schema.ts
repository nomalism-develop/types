import joi from 'joi';

import * as IShared from '../../../shared/interface';
import { messages } from '../../../shared/messages';
import { ICreateRequest } from './interfaces';

const createBodyKeys: IShared.IRouteRequestWithStamps<ICreateRequest> = {
  current_account: joi.boolean().required(),
};

export const createBody = joi.object().keys(createBodyKeys).messages(messages);
