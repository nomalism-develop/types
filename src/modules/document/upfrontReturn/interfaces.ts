import * as IShared from '../../../shared/interface';

export const Route = 'upfrontReturn';
export const UpperName = 'UpfrontReturn';
export const LowerName = UpperName[0].toLowerCase() + UpperName.substring(1);

export interface ICreateRequest {
  current_account: boolean;
}

export interface IRepository {
  create(id: IShared.IFindByIdRequest, body: ICreateRequest): Promise<void>;
}

export type IController = IShared.IEntityWithUserToken<IRepository>;

export type IApi = IRepository;
