import joi from 'joi';
import { messages } from '../../../shared/messages';
import * as IShared from '../../../shared/interface';
import { ICreateRequest, IDeliveryAddressRequest } from './interfaces';

const deliveryAddress: IShared.IRouteRequest<IDeliveryAddressRequest> = {
  delivery_departure_street: joi.string().required(),
  delivery_departure_locality: joi.string().required(),
  delivery_departure_postal_code: joi.string().required(),
  delivery_street: joi.string().required(),
  delivery_locality: joi.string().required(),
  delivery_postal_code: joi.string().required(),
};

const createBillOfLadingBodyKeys: IShared.IRouteRequest<ICreateRequest> = {
  persona_id: joi.string().uuid().required(),
  persona_name: joi.string().required(),
  persona_nif: joi.string().required(),
  document_type_id: joi.number().required(),
  delivery_addresses: joi
    .array()
    .items({
      ...deliveryAddress,
    })
    .required(),
  document_line_ids: joi.array().unique().items(joi.string().uuid().required()).required(),
  from_document_header_id: joi.string().required(),
};
export const createBillOfLadingBody = joi
  .object()
  .keys(createBillOfLadingBodyKeys)
  .messages(messages);
