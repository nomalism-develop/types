import * as IShared from '../../../shared/interface';

export interface IDeliveryAddressRequest {
  delivery_street: string;
  delivery_locality: string;
  delivery_postal_code: string;
  delivery_departure_locality: string;
  delivery_departure_postal_code: string;
  delivery_departure_street: string;
}

export interface ICreateRequest {
  persona_id: string;
  persona_name: string;
  persona_nif: string;
  delivery_addresses: IDeliveryAddressRequest;
  document_type_id: number;
  from_document_header_id: string;
  document_line_ids: string[];
}

export interface ICreateResponse {
  brokerMessages: IShared.IBrokerMessage<IShared.IBrokerTopic>[];
  tasks: IShared.ITaskCluster[];
}

export interface IRepository {
  create(data: ICreateRequest): Promise<IShared.ITaskCluster[]>;
}

export type IController = IShared.IEntityWithUserToken<IRepository>;
