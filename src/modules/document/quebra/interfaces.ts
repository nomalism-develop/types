import { DocumentLineNote } from '../../../shared/entities';
import * as IShared from '../../../shared/interface';

export const Route = 'quebra';
export const UpperName = 'Quebra';
export const LowerName = UpperName[0].toLowerCase() + UpperName.substring(1);

export interface IInventoryLossResponse {
  document_line_id: string;
  id_provider: number;
  product_id: string;
  product_thumbnail: string;
  product_reference: string;
  product_description: string;
  quantity: number;
  unit_of_measure_quantity_notation: string;
  notes: DocumentLineNote[];
  updated_by: string | null;
  updated_at: Date;
}

export interface ICreateManyProducts {
  id: string;
  quantity: number;
  note?: string;
}

export interface ICreateManyRequest {
  products: ICreateManyProducts[];
}

export interface IDeleteInventoryLossRequest {
  document_line_ids: string[];
}

export interface IRepository {
  findInventoryLosses(): Promise<IInventoryLossResponse[]>;

  findInventoryLossesProviders(): Promise<IShared.IFindMinifiedResponse[]>;

  createMany(data: ICreateManyRequest): Promise<string>;

  deleteInventoryLosses(data: IDeleteInventoryLossRequest): Promise<void>;
}

export type IController = IShared.IEntityWithUserToken<IRepository>;
