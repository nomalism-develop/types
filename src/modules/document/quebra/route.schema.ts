import joi from 'joi';

import * as IShared from '../../../shared/interface';
import { messages } from '../../../shared/messages';
import { ICreateManyProducts, ICreateManyRequest, IDeleteInventoryLossRequest } from './interfaces';

const productsCreateQuebraKeys: IShared.IRouteRequestWithStamps<ICreateManyProducts> = {
  id: joi.string().uuid().required(),
  quantity: joi.number().positive().required(),
  note: joi.string().optional(),
};

const createQuebraKeys: IShared.IRouteRequestWithStamps<ICreateManyRequest> = {
  products: joi.array().items(joi.object().keys(productsCreateQuebraKeys)).min(1).required(),
};
export const createQuebraBody = joi.object().keys(createQuebraKeys).messages(messages);
// inventoryLossRequest
const deleteInventoryLossBodyKeys: IShared.IRouteRequest<IDeleteInventoryLossRequest> = {
  document_line_ids: joi.array().items(joi.string().uuid().required()).required(),
};

export const deleteInventoryLossBody = joi
  .object()
  .keys(deleteInventoryLossBodyKeys)
  .messages(messages);
