import joi from 'joi';
import { messages } from '../../../shared/messages';
import * as IShared from '../../../shared/interface';
import {
  IFinalizarEntradaDeMaterialRequest,
  IUndoEntradaDeMaterialRequest,
  IPrintLabelParamsRequest,
  IPrintLabelQueryWithTokenRequest,
  IPrintBulkLabelRequest,
} from './interfaces';

const finalizarEntradaDeMaterialBodyKeys: IShared.IRouteRequest<IFinalizarEntradaDeMaterialRequest> =
  {
    provider_id: joi.string().uuid().required(),
    saved_em_picking_ids: joi.array().items(joi.string().uuid().required()).optional(),
  };

export const finalizarEntradaDeMaterialBody = joi
  .object()
  .keys(finalizarEntradaDeMaterialBodyKeys)
  .messages(messages);

const undoEntradaDeMaterialBodyKeys: IShared.IRouteRequest<IUndoEntradaDeMaterialRequest> = {
  document_line_ids: joi.array().items(joi.string().uuid().required()).required(),
};

export const undoEntradaDeMaterialBody = joi
  .object()
  .keys(undoEntradaDeMaterialBodyKeys)
  .messages(messages);

const printLabelParamsKeys: IShared.IRouteRequest<IPrintLabelParamsRequest> = {
  product_id: joi.string().uuid().required(),
};

export const printLabelParams = joi.object().keys(printLabelParamsKeys).messages(messages);

const printLabelQueryKeys: IShared.IRouteRequest<IPrintLabelQueryWithTokenRequest> = {
  ef_name: joi.string().required(),
  pc_document_number: joi.number().integer().positive().allow(null).optional(),
  prison: joi.boolean().required(),
  quantity: joi.number().positive().required(),
  note: joi.string().allow(null, '').optional(),
  token: joi.string().required(),
};

export const printLabelQuery = joi.object().keys(printLabelQueryKeys).messages(messages);

export const printBulkLabelQuery = joi.object<IPrintBulkLabelRequest>().keys({
  groupLabel: joi.boolean().required(),
  saved_em_picking_ids: joi.string().required(),
  token: joi.string().required(),
});
