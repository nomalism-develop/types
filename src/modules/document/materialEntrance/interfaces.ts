import { DocumentHeader } from '../../../shared/entities';
import * as IShared from '../../../shared/interface';
import { type IProductType } from '../../stock/productGoogleSheets/interface';

export const Route = 'material_entrance';
export const UpperName = 'MaterialEntrance';
export const LowerName = UpperName[0].toLowerCase() + UpperName.substring(1);

// finalizar material entrance request
export const ICreateMaterialEntranceClientOrderOptionEnum: {
  'Vai desdobrar linha na PC (estado Espera Chegada)': 'Vai desdobrar linha na PC (estado Espera Chegada)';
  'Vai desdobrar linha na PC (estado Espera Envio Encomenda)': 'Vai desdobrar linha na PC (estado Espera Envio Encomenda)';
  'Vai atualizar quantidade na PC': 'Vai atualizar quantidade na PC';
  'Vai desligar a PC da PF': 'Vai desligar a PC da PF';
} = {
  'Vai desdobrar linha na PC (estado Espera Chegada)':
    'Vai desdobrar linha na PC (estado Espera Chegada)',
  'Vai desdobrar linha na PC (estado Espera Envio Encomenda)':
    'Vai desdobrar linha na PC (estado Espera Envio Encomenda)',
  'Vai atualizar quantidade na PC': 'Vai atualizar quantidade na PC',
  'Vai desligar a PC da PF': 'Vai desligar a PC da PF',
};

export type ICreateMaterialEntranceClientOrderOption =
  (typeof ICreateMaterialEntranceClientOrderOptionEnum)[keyof typeof ICreateMaterialEntranceClientOrderOptionEnum];

export const createMaterialEntranceClientOrderOptions = Object.keys(
  ICreateMaterialEntranceClientOrderOptionEnum,
);

export interface IGetMaterialEntranceInfoClientOrder {
  ef_dla_id: string;
  ordered_quantity: number;

  pc_dh_id: string;
  pc_dh_number: number;
  pc_virtual_name: string;
  pc_dla_id: string;
  pc_sheet: string | null;
}

export interface IEntradaDeMaterialClientOrder extends IGetMaterialEntranceInfoClientOrder {
  document_line_id: string;
  quantity: number;
  option: ICreateMaterialEntranceClientOrderOption | null;
  note: string | null;
  printed: string | null;
}

export const ICreateMaterialEntranceStockWithoutOrderOptionEnum: {
  'Vai criar nova linha na PF': 'Vai criar nova linha na PF';
} = {
  'Vai criar nova linha na PF': 'Vai criar nova linha na PF',
};

export type ICreateMaterialEntranceStockWithoutOrderOption =
  (typeof ICreateMaterialEntranceStockWithoutOrderOptionEnum)[keyof typeof ICreateMaterialEntranceStockWithoutOrderOptionEnum];

export const createMaterialEntranceStockWithoutOrderOptions = Object.keys(
  ICreateMaterialEntranceStockWithoutOrderOptionEnum,
);

export const ICreateMaterialEntranceStockOptionEnum: {
  'Vai desdobrar linha na PF (estado Espera Chegada)': 'Vai desdobrar linha na PF (estado Espera Chegada)';
  'Vai desdobrar linha na PF (estado Espera Envio Encomenda)': 'Vai desdobrar linha na PF (estado Espera Envio Encomenda)';
  'Vai atualizar quantidade na PF': 'Vai atualizar quantidade na PF';
} = {
  'Vai desdobrar linha na PF (estado Espera Chegada)':
    'Vai desdobrar linha na PF (estado Espera Chegada)',
  'Vai desdobrar linha na PF (estado Espera Envio Encomenda)':
    'Vai desdobrar linha na PF (estado Espera Envio Encomenda)',
  'Vai atualizar quantidade na PF': 'Vai atualizar quantidade na PF',
};

export type ICreateMaterialEntranceStockOption =
  (typeof ICreateMaterialEntranceStockOptionEnum)[keyof typeof ICreateMaterialEntranceStockOptionEnum];

export const createMaterialEntranceStockOptions = Object.keys(
  ICreateMaterialEntranceStockOptionEnum,
);

// get material entrance info request
export interface IGetMaterialEntranceInfoRequest {
  ef_dla_ids: string[];
}

// get material entrance info response
export interface IGetMaterialEntranceInfoStock {
  ef_dla_id: string | null;
  ordered_quantity: number;
}

export interface IEntradaDeMaterialStock extends IGetMaterialEntranceInfoStock {
  document_line_id: string;
  quantity: number;
  option:
    | ICreateMaterialEntranceStockOption
    | ICreateMaterialEntranceStockWithoutOrderOption
    | null;
  note: string | null;
  printed: string | null;
}

export interface IEntradaDeMaterialNC {
  ef_dla_id: null;
  ordered_quantity: 0;

  document_line_id: string;
  quantity: number;
  option: null;
  note: string | null;
  printed: string | null;
}

export interface IGetMaterialEntranceInfoResponse {
  clientOrders: IEntradaDeMaterialClientOrder[];
  stock: IEntradaDeMaterialStock;
  not_ok: IEntradaDeMaterialNC;
  saved_id: string | null;
}

// unfinished picking response
export interface IUnfinishedPickingLineGroup {
  groupId: string;
  product_id: string;
  product_reference: string | null;
  product_designation: string | null;
  product_thumbnail: string | null;
  product_provider_ref: string | null;
  product_measure: string | null;
  product_stock: number;
  product_type: IProductType;
  ordered_quantity: number;
  provider_estimated_delivery_date: string;
  ef_dla_ids: string[];
}

export type IUnfinishedPickingLineGroupExtended = IUnfinishedPickingLineGroup &
  IGetMaterialEntranceInfoResponse;

export interface IUnfinishedPickingsResponse extends Pick<DocumentHeader, 'id' | 'owner_id'> {
  identifier: string;
  start_document_header_id: string | null; // EFs do PHC não têm PF
  order_date: Date;
  lineGroups: IUnfinishedPickingLineGroupExtended[];
}

// undo material entrance request
export interface IUndoEntradaDeMaterialRequest {
  document_line_ids: string[];
}

export interface IFinalizarEntradaDeMaterialRequest {
  provider_id: string;
  saved_em_picking_ids?: string[];
}

export interface IFinalizarEntradaDeMaterialResponse {
  broker_messages: IShared.IBrokerMessage<IShared.IBrokerTopic>[];
  document_number: string;
}

export interface IPrintLabelParamsRequest {
  product_id: string;
}

export interface IPrintLabelQueryRequest {
  ef_name: string;
  pc_document_number?: number | null;
  prison: boolean;
  quantity: number;
  note?: string;
}

export interface IPrintBulkLabelRequest {
  groupLabel: boolean;
  saved_em_picking_ids: string;
  token: string;
}

export interface IPrintLabelQueryWithTokenRequest extends IPrintLabelQueryRequest {
  token: string;
}

interface IRepository {
  finalizarEntradaMaterial(
    data: IFinalizarEntradaDeMaterialRequest,
  ): Promise<IFinalizarEntradaDeMaterialResponse>;

  undoEntradaMaterial(data: IUndoEntradaDeMaterialRequest): Promise<void>;

  findUnfinishedPickings(
    selector: IShared.IFindByIdRequest,
  ): Promise<IUnfinishedPickingsResponse[]>;

  findMaterialEntranceProviders(): Promise<IShared.IFindMinifiedResponse[]>;
}

export type IApi = Omit<IRepository, 'finalizarEntradaMaterial'> & {
  finalizarEntradaMaterial(
    data: IFinalizarEntradaDeMaterialRequest,
  ): Promise<IFinalizarEntradaDeMaterialResponse>;
};

export type IController = IShared.IEntityWithUserToken<IRepository>;
