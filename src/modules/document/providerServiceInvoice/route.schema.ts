import joi from 'joi';
import { messages } from '../../../shared/messages';
import * as IShared from '../../../shared/interface';
import { type ICreateProviderServiceInvoiceRequest, type IValueByVatTax } from './interfaces';

const valueByVatTaxKeys: IShared.IRouteRequest<IValueByVatTax> = {
  taxPercentage: joi.number().positive().allow(0).required(),
  total: joi.number().positive().required(),
};

const createProviderServiceInvoiceBodyKeys: IShared.IRouteRequest<ICreateProviderServiceInvoiceRequest> =
  {
    provider_id: joi.string().uuid().required(),
    emission_date: joi.date().required(),
    external_document_name: joi.string().required(),
    observations: joi.string().allow(null, '').required(),
    values: joi
      .array()
      .unique('taxPercentage')
      .items(joi.object().keys(valueByVatTaxKeys).required())
      .required(),
  };
export const createProviderServiceInvoiceBody = joi
  .object()
  .keys(createProviderServiceInvoiceBodyKeys)
  .messages(messages);
