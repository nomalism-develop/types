import * as IShared from '../../../shared/interface';

export interface IValueByVatTax {
  taxPercentage: number;
  total: number;
}

export interface ICreateProviderServiceInvoiceRequest {
  provider_id: string;
  external_document_name: string;
  emission_date: Date;
  observations: string;
  values: IValueByVatTax[];
}

export interface IRepository {
  createProviderServiceInvoice(params: ICreateProviderServiceInvoiceRequest): Promise<void>;
}

export type IController = IShared.IEntityWithUserToken<IRepository>;
