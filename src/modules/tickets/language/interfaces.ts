import { TicketsLanguage } from '../../../shared/entities';
import * as IShared from '../../../shared/interface';

export type Entity = TicketsLanguage;
export const Route = 'tickets_language';
export const UpperName = 'TicketsLanguage';
export const LowerName = UpperName[0].toLowerCase() + UpperName.substring(1);

export type ICreateRequest = Omit<Entity, 'id'>;

export type IUpdateRequest = ICreateRequest;
export interface IFindMinifiedRequest {
  value?: string;
}

export interface IRepository {
  create(data: ICreateRequest): Promise<Entity>;
  find(): Promise<Entity[]>;
  findById(id: IShared.IFindByIdNumberRequest): Promise<Entity | null>;
  findMinified(params?: IFindMinifiedRequest): Promise<Entity[]>;
  update(id: IShared.IFindByIdNumberRequest, data: IUpdateRequest): Promise<void>;
  deleteOne(id: IShared.IFindByIdNumberRequest): Promise<void>;
}

export type IController = IShared.IEntityWithUserToken<IRepository>;
