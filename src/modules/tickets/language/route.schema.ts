import joi from 'joi';
import { messages } from '../../../shared/messages';
import * as IShared from '../../../shared/interface';
import { ICreateRequest, IUpdateRequest } from './interfaces';

// create body validation
const createBodyKeys: IShared.IRouteRequest<ICreateRequest> = {
  order: joi.number().required(),
  name: joi.string().required(),
  alpha2: joi.string().required(),
  lang: joi.string().required(),
  voice_name: joi.string().required(),
  ssml: joi.string().required(),
  call_text: joi.string().required(),
};
export const createBody = joi.object().keys(createBodyKeys).messages(messages);

// update body validation
const updateBodyKeys: IShared.IRouteRequest<IUpdateRequest> = {
  name: joi.string().optional(),
  alpha2: joi.string().optional(),
  lang: joi.string().optional(),
  voice_name: joi.string().optional(),
  ssml: joi.string().optional(),
  call_text: joi.string().optional(),
  order: joi.number().optional(),
};
export const updateBody = joi.object().keys(updateBodyKeys).messages(messages);
