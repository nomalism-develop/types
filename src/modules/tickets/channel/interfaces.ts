import { Channel } from '../../../shared/entities';
import * as IShared from '../../../shared/interface';

export type Entity = Channel;
export const Route = 'channel';
export const UpperName = 'Channel';
export const LowerName = UpperName[0].toLowerCase() + UpperName.substring(1);

export interface IChannel {
  id: number;
  name: Record<string, string>;
  text: Record<string, string>;
  color: string;
  text_color: string;
  disabled_color: string;
  last_called: number;
}
export interface IFindMinifiedRequest {
  value?: string;
}

export type ICreateRequest = Omit<Entity, 'id'>;

export type IUpdateRequest = ICreateRequest;

export interface IRepository {
  create(data: ICreateRequest): Promise<Entity>;
  find(): Promise<Entity[]>;
  findById(id: IShared.IFindByIdNumberRequest): Promise<Entity | null>;
  findMinified(params?: IFindMinifiedRequest): Promise<Entity[]>;
  update(id: IShared.IFindByIdNumberRequest, data: IUpdateRequest): Promise<void>;
  deleteOne(id: IShared.IFindByIdNumberRequest): Promise<void>;
}

export type IController = IShared.IEntityWithUserToken<IRepository>;
