import joi from 'joi';
import { messages } from '../../../shared/messages';
import * as IShared from '../../../shared/interface';
import { ICreateRequest, IUpdateRequest } from './interfaces';

// create body validation
const createBodyKeys: IShared.IRouteRequest<ICreateRequest> = {
  order: joi.number().required(),
  color: joi.string().required(),
  disabled_color: joi.string().required(),
  key: joi.string().required(),
  text_color: joi.string().required(),
};
export const createBody = joi.object().keys(createBodyKeys).messages(messages);

// update body validation
const updateBodyKeys: IShared.IRouteRequest<IUpdateRequest> = {
  order: joi.number().optional(),
  color: joi.string().optional(),
  disabled_color: joi.string().optional(),
  key: joi.string().optional(),
  text_color: joi.string().optional(),
};
export const updateBody = joi.object().keys(updateBodyKeys).messages(messages);
