import { Tickets, TicketsLanguage } from '../../../shared/entities';
import { IChannel } from '../channel/interfaces';
import * as IShared from '../../../shared/interface';

export type Entity = Tickets;
export const Route = 'tickets';
export const UpperName = 'Tickets';
export const LowerName = UpperName[0].toLowerCase() + UpperName.substring(1);

export interface ILastTicketCalledToday {
  language: TicketsLanguage;
  text_to_read: string;
}

export interface ILastTicketCalledTodayByChannel {
  channel_id: number;
  number: number;
}

export interface IRemainingTodayTickets {
  id: string;
  channel_id: number;
  number: number;
  language_id: number;
  text_to_read: string;
  created_at: Date;
}

export interface ITicketsFindTodayResponse {
  channels: IChannel[];
  languages: TicketsLanguage[];
  remaining_today_tickets_to_call: IRemainingTodayTickets[];
  last_ticket_called_today: ILastTicketCalledToday | null;
  last_tickets_called_today_by_channel?: ILastTicketCalledTodayByChannel[];
}

export interface ITicketCreateRequest {
  channel_id: number;
  language_id: number;
  created_by: string;
}

export interface ITicketCreateResponse {
  channel_id: number;
  number: number;
  channel_name: string;
  last_ticket_called_today: ILastTicketCalledToday | null;
  remaining_today_tickets_to_call: IRemainingTodayTickets[];
  last_tickets_called_today_by_channel?: ILastTicketCalledTodayByChannel[];
}

export interface ITicketUpdateRequest {
  id: string;
  who_attended: string;
}

export interface ITicketUpdateResponse {
  number: number;
  channel_id: number;
  language: TicketsLanguage;
  text_to_read: string;
  last_ticket_called_today: ILastTicketCalledToday | null;
  remaining_today_tickets_to_call: IRemainingTodayTickets[];
  last_tickets_called_today_by_channel?: ILastTicketCalledTodayByChannel[];
}

export interface ITicketUndoResponse {
  last_ticket_called_today: ILastTicketCalledToday | null;
  remaining_today_tickets_to_call: IRemainingTodayTickets[];
  last_tickets_called_today_by_channel?: ILastTicketCalledTodayByChannel[];
}

export interface IRepository {
  create(data: ITicketCreateRequest): Promise<ITicketCreateResponse>;
  findToday(): Promise<ITicketsFindTodayResponse>;
  update(data: ITicketUpdateRequest): Promise<ITicketUpdateResponse>;
  undoLastCall(): Promise<ITicketUndoResponse>;
}

export type IController = IShared.IEntityWithUserToken<IRepository>;
