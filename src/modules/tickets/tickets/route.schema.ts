import joi from 'joi';
import { messages } from '../../../shared/messages';
import * as IShared from '../../../shared/interface';
import { ITicketCreateRequest, ITicketUpdateRequest } from './interfaces';

// create body validation
const createBodyKeys: IShared.IRouteRequestWithStamps<ITicketCreateRequest> = {
  channel_id: joi.number().required(),
  language_id: joi.number().required(),
  created_by: joi.string().uuid().required(),
};
export const createBody = joi.object().keys(createBodyKeys).messages(messages);

// update body validation
const updateBodyKeys: IShared.IRouteRequest<ITicketUpdateRequest> = {
  id: joi.string().uuid().required(),
  who_attended: joi.string().uuid().required(),
};
export const updateBody = joi.object().keys(updateBodyKeys).messages(messages);
