import joi from 'joi';
import { messages } from '../../../shared/messages';
import * as IShared from '../../../shared/interface';
import { ICreateRequest, IUpdateRequest } from './interfaces';

// create body validation
const createBodyKeys: IShared.IRouteRequest<ICreateRequest> = {
  name: joi.string().required(),
  text: joi.string().required(),
  channel_id: joi.number().required(),
  language_id: joi.number().required(),
  translation_id: joi.string().required(),
};
export const createBody = joi.object().keys(createBodyKeys).messages(messages);

// update body validation
const updateBodyKeys: IShared.IRouteRequest<IUpdateRequest> = {
  name: joi.string().optional(),
  text: joi.string().optional(),
  channel_id: joi.number().optional(),
  language_id: joi.number().optional(),
  translation_id: joi.string().optional(),
};
export const updateBody = joi.object().keys(updateBodyKeys).messages(messages);
