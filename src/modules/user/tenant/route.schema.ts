import joi from 'joi';
import { messages } from '../../../shared/messages';
import * as IShared from '../../../shared/interface';
import { ICreateRequest, IUpdateRequest } from './interface';

// create body validation
const createBodyKeys: IShared.IRouteRequest<ICreateRequest> = {
  customer_id: joi.string().required(),
  name: joi.string().required(),
  taxpayer_number: joi.string().required(),
  commercial_registration_number: joi.number().greater(0),
  share_capital: joi.number().greater(0),
  conservatory: joi.string(),
  logo: joi.string(),
  email: joi.string().trim(true).lowercase().required(),
  address: joi.string().required(),
  postal_code: joi.string().trim(true).lowercase().required(),
  locality: joi.string().required(),
  country: joi.string().required(),
  Timezone: joi.string(),
  telephone: joi.string(),
  fax: joi.string(),
  website: joi.string(),
};
export const createBody = joi.object().keys(createBodyKeys).messages(messages);

// update body validation
const updateBodyKeys: IShared.IRouteRequest<IUpdateRequest> = {
  customer_id: joi.string(),
  name: joi.string(),
  taxpayer_number: joi.string(),
  commercial_registration_number: joi.number().greater(0),
  share_capital: joi.number().greater(0),
  conservatory: joi.string(),
  logo: joi.string(),
  email: joi.string().trim(true).lowercase(),
  address: joi.string(),
  postal_code: joi.string().trim(true).lowercase(),
  locality: joi.string(),
  country: joi.string(),
  Timezone: joi.string(),
  telephone: joi.string(),
  fax: joi.string(),
  website: joi.string(),
};
export const updateBody = joi.object().keys(updateBodyKeys).messages(messages);
