import * as IShared from '../../../shared/interface';
import { Tenant } from '../../../shared/entities';

export type Entity = Tenant;
export const Route = 'tenant';
export const UpperName = 'Tenant';
export const LowerName = UpperName[0].toLowerCase() + UpperName.substring(1);

export interface ICreateRequest {
  // create fields
  name: string;
  customer_id: string;
  taxpayer_number: string;
  commercial_registration_number?: number | null;
  share_capital?: number | null;
  conservatory?: string | null;
  logo?: string | null;
  email: string;
  address: string;
  postal_code: string;
  locality: string;
  country: string;
  Timezone?: string | null;
  telephone?: string | null;
  fax?: string | null;
  website?: string | null;
}

export interface IUpdateRequest {
  // updatable fields
  name?: string;
  customer_id?: string;
  taxpayer_number?: string;
  commercial_registration_number?: number | null;
  share_capital?: number | null;
  conservatory?: string | null;
  logo?: string | null;
  email?: string;
  address?: string;
  postal_code?: string;
  locality?: string;
  country?: string;
  Timezone?: string | null;
  telephone?: string | null;
  fax?: string | null;
  website?: string | null;
}

export interface IRepository {
  find(): Promise<Entity[]>;

  findById(selector: IShared.IFindByIdRequest): Promise<Entity | null>;

  create(data: ICreateRequest): Promise<Entity>;

  update(selector: IShared.IFindByIdRequest, data: IUpdateRequest): Promise<Entity | null>;
}

export type IController = IShared.IEntityWithUserToken<IRepository>;
