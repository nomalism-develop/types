import joi from 'joi';
import { messages } from '../../../shared/messages';
import * as IShared from '../../../shared/interface';
import {
  IFindPaginatedRequest,
  ICreateRequest,
  IUpdateRequest,
  IFindByQueryRequest,
  ICreateMainPersona,
} from './interface';
import { chatTypes } from '../../stock/chat/interfaces';

const createMainPersonaKeys: IShared.IRouteRequest<ICreateMainPersona> = {
  name: joi.string().allow(null, '').empty('').optional(),
  nif: joi.string().allow(null, '').empty('').optional(),
  email: joi
    .string()
    .email({ tlds: { allow: false } })
    .allow(null, '')
    .empty('')
    .optional(),
  telephone: joi.string().allow(null, '').empty('').optional(),
};

// create body validation
const createBodyKeys: IShared.IRouteRequest<ICreateRequest> = {
  user_id: joi.string().uuid().optional(),

  main_persona: joi.object().keys(createMainPersonaKeys).optional(),

  deferred_payment: joi.boolean().default(false).optional(),
  default_discount: joi.number().positive().allow(0).default(0).max(100).optional(),
  exemption_reason_id: joi.string().uuid().allow(null).optional(),

  inactive: joi.boolean().default(false).optional(),
  newsletter: joi.boolean().default(false).optional(),

  country_id: joi.string().uuid().allow(null).optional(),
  language_id: joi.string().uuid().allow(null).optional(),
  client_type_id: joi.string().uuid().allow(null).optional(),
  segments_area_id: joi.string().uuid().allow(null).optional(),
  default_payment_method_id: joi.string().uuid().allow(null).optional(),
  default_maturity_date_id: joi.string().uuid().allow(null).optional(),
  default_delivery_method_id: joi.string().uuid().allow(null).optional(),
  default_vehicle_id: joi.string().uuid().allow(null).optional(),
};
export const createBody = joi.object().keys(createBodyKeys).messages(messages);

// update body validation
const updateBodyKeys: IShared.IRouteRequest<IUpdateRequest> = {
  chat_type: joi
    .string()
    .valid(...chatTypes)
    .optional(),
  main_persona_id: joi.string().uuid().optional(),
  contact_persona_id: joi.string().uuid().optional(),
  nif: joi.string().allow(null).optional(),
  country_id: joi.string().uuid().allow(null).optional(),
  language_id: joi.string().uuid().allow(null).optional(),
  client_type_id: joi.string().uuid().allow(null).optional(),
  newsletter: joi.boolean().optional(),
  inactive: joi.boolean().optional(),
  segments_area_id: joi.string().uuid().allow(null).optional(),
  default_discount: joi.number().positive().allow(0).max(100).optional(),
  default_payment_method_id: joi.string().uuid().allow(null).optional(),
  default_maturity_date_id: joi.string().uuid().allow(null).optional(),
  default_delivery_method_id: joi.string().uuid().allow(null).optional(),
  default_vehicle_id: joi.string().uuid().allow(null).optional(),
  deferred_payment: joi.boolean().optional(),
};
export const updateBody = joi.object().keys(updateBodyKeys).messages(messages);
const findQueryKeys: IShared.IRouteRequest<IFindByQueryRequest> = {
  number: joi.array().items(joi.number().positive().required()).optional(),
  id: joi.array().items(joi.string().uuid().required()).optional(),
  nif: joi.string().optional(),
};
export const findQuery = joi.object().keys(findQueryKeys).messages(messages);

// find with pagination query validation
const findWithPaginationQueryKeys: IShared.IRouteRequest<IFindPaginatedRequest> = {
  per_page: joi.number().integer().positive().default(10).optional(),
  current_page: joi.number().integer().positive().default(1).optional(),
};
export const findWithPaginationQuery = joi
  .object()
  .keys(findWithPaginationQueryKeys)
  .messages(messages);
