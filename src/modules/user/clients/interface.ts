import * as IShared from '../../../shared/interface';
import {
  Clients,
  Users,
  Country,
  ReasonForExemption,
  ClientType,
  SegmentsArea,
  Language,
  Vehicles,
  DeliveryMethods,
  MaturityDates,
  PaymentMethods,
  Persona,
} from '../../../shared/entities';
import type { IChatType } from '../../stock/chat/interfaces';

export type Entity = Clients;
export const Route = 'client';
export const UpperName = 'Clients';
export const LowerName = UpperName[0].toLowerCase() + UpperName.substring(1);

interface IMainPersona extends Persona {
  country: Country | null;
  reason_for_exemption: ReasonForExemption | null;
}

interface IFindDetailedResponse extends Entity {
  user: Users;
  main_persona: IMainPersona;
  client_type: ClientType | null;
  segments_area: SegmentsArea | null;
  country: Country | null;
  language: Language | null;
  default_payment_method: PaymentMethods | null;
  default_maturity_date: MaturityDates | null;
  default_delivery_method: DeliveryMethods | null;
  default_vehicle: Vehicles | null;
}

// omit any unnecessary fields
export type IFindByIdResponse = Omit<IFindDetailedResponse, ''>;

// omit any unnecessary fields
export type IFindByOwnerIdResponse = Omit<
  IFindDetailedResponse,
  | 'user'
  | 'main_persona'
  | 'client_type'
  | 'segments_area'
  | 'country'
  | 'language'
  | 'default_payment_method'
  | 'default_maturity_date'
  | 'default_delivery_method'
  | 'default_vehicle'
>;

export interface IFindByQueryRequest {
  number?: number[];
  id?: string[];
  nif?: string | null;
}

export interface IFindRequest {}

// omit any unnecessary fields
export type IFindResponse = Omit<IFindDetailedResponse, ''>;

export interface IFindPaginatedRequest extends IFindRequest, IShared.IPaginationRequest {}

export type IFindWithPaginationResponse = IShared.IPaginationResponse<
  Omit<IFindDetailedResponse, ''>
>;

export type ICreateMainPersona = Pick<Persona, 'nif' | 'name' | 'email' | 'telephone'>;

export interface ICreateRequest {
  user_id?: string;
  main_persona: ICreateMainPersona;
  deferred_payment?: boolean;
  default_discount?: number;
  exemption_reason_id?: string | null;
  newsletter?: boolean;
  inactive?: boolean;
  country_id?: string | null;
  language_id?: string | null;
  client_type_id?: string | null;
  segments_area_id?: string | null;
  default_payment_method_id?: string | null;
  default_maturity_date_id?: string | null;
  default_delivery_method_id?: string | null;
  default_vehicle_id?: string | null;
}

export interface IUpdateRequest {
  // updatable fields
  chat_type?: IChatType;
  nif?: string | null;
  country_id?: string | null;
  language_id?: string | null;
  client_type_id?: string | null;
  newsletter?: boolean;
  inactive?: boolean;
  segments_area_id?: string | null;
  main_persona_id?: string;
  contact_persona_id?: string;
  default_discount?: number;
  default_payment_method_id?: string | null;
  default_maturity_date_id?: string | null;
  default_delivery_method_id?: string | null;
  default_vehicle_id?: string | null;
  deferred_payment?: boolean;
}

export interface IRepository {
  findById(selector: IShared.IFindByIdRequest): Promise<IFindByIdResponse | null>;

  findByOwnerId(params: IShared.IFindByOwnerIdRequest): Promise<IFindByOwnerIdResponse[]>;

  findMinified(params?: IShared.IFindMinifiedRequest): Promise<IShared.IFindMinifiedResponse[]>;

  findByQuery(params: IFindByQueryRequest): Promise<IFindResponse[]>;

  find(selector: IFindRequest): Promise<IFindResponse[]>;

  findPaginated(selector: IFindPaginatedRequest): Promise<IFindWithPaginationResponse>;

  create(data: ICreateRequest): Promise<IFindResponse>;

  createOrUpdate(data: ICreateRequest): Promise<IFindResponse>;

  update(selector: IShared.IFindByIdRequest, data: IUpdateRequest): Promise<IFindResponse | null>;

  deleteOne(selector: IShared.IFindByIdRequest): Promise<IFindResponse | null>;
}

export type IController = IShared.IEntityWithUserToken<IRepository>;
