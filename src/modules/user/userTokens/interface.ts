import * as IShared from '../../../shared/interface';
import { UserTokens } from '../../../shared/entities';

export type Entity = UserTokens;
export const Route = 'user_token';
export const UpperName = 'UserTokens';
export const LowerName = UpperName[0].toLowerCase() + UpperName.substring(1);

export type IFindRequest = Record<string, unknown>;

export interface IFindPaginatedRequest extends IFindRequest, IShared.IPaginationRequest {}

export type IFindWithPaginationResponse = IShared.IPaginationResponse<UserTokens>;

export interface ICreateRequest {
  // create fields
  user_id: string;
  expires_date: Date;
  refresh_token: string;
}

export interface IUpdateRequest {
  consumed?: boolean;
}

export interface IRepository {
  findById(selector: IShared.IFindByIdRequest): Promise<Entity | null>;
  findByToken(id: string): Promise<Entity | null>;

  findByOwnerId(params: IShared.IFindByOwnerIdRequest): Promise<Entity[]>;
  find(selector: IFindRequest): Promise<Entity[]>;
  findPaginated(selector: IFindPaginatedRequest): Promise<IFindWithPaginationResponse>;

  generate(user_id: string): Promise<Entity>;
  update(selector: IShared.IFindByIdRequest, data: IUpdateRequest): Promise<Entity | null>;
  deleteOne(selector: IShared.IFindByIdRequest): Promise<Entity | null>;
}

export type IApi = Omit<IRepository, 'generate' | 'findByToken' | 'update'>;

export type IController = IShared.IEntityWithUserToken<IRepository>;
