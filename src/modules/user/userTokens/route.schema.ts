import joi from 'joi';
import { messages } from '../../../shared/messages';
import * as IShared from '../../../shared/interface';
import { IFindRequest, IFindPaginatedRequest } from './interface';

const findQueryKeys: IShared.IRouteRequest<IFindRequest> = {};
export const findQuery = joi.object().keys(findQueryKeys).messages(messages);

// find with pagination query validation
const findWithPaginationQueryKeys: IShared.IRouteRequest<IFindPaginatedRequest> = {
  per_page: joi.number().integer().positive().default(10).optional(),
  current_page: joi.number().integer().positive().default(1).optional(),
};
export const findWithPaginationQuery = joi
  .object()
  .keys(findWithPaginationQueryKeys)
  .messages(messages);
