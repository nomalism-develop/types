import * as IShared from '../../../shared/interface';
import { GroupPermissions } from '../../../shared/entities';

export type Entity = GroupPermissions;
export const Route = 'group_permissions';
export const UpperName = 'GroupPermissions';
export const LowerName = UpperName[0].toLowerCase() + UpperName.substring(1);

export interface ICreateRequest {
  code: string;
  description: string;
}

export interface IUpdateRequest {
  code: string;
  description: string;
}

export interface IRepository {
  findByCode(code: string): Promise<Entity | null>;
  findById(selector: IShared.IFindByIdNumberRequest): Promise<Entity | null>;
  changeEnable(selector: IShared.IFindByIdNumberRequest): Promise<boolean>;

  find(): Promise<Entity[]>;
  findMinified(params?: IShared.IFindMinifiedRequest): Promise<IShared.IFindMinifiedResponse[]>;
  create(data: ICreateRequest): Promise<Entity>;
  deleteOne(selector: IShared.IFindByIdNumberRequest): Promise<Entity | null>;
}

export type IController = IShared.IEntityWithUserToken<IRepository>;

export type IApi = Omit<IRepository, 'findByCode' | 'findById' | 'changeEnable'>;
