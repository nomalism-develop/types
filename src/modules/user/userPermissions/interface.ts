import * as IShared from '../../../shared/interface';
import { UserPermissions, GroupPermissions, Permissions } from '../../../shared/entities';

export type Entity = UserPermissions;
export const Route = 'user_permissions';
export const UpperName = 'UserPermissions';
export const LowerName = UpperName[0].toLowerCase() + UpperName.substring(1);

interface IFindDetailedResponse extends Entity {
  group_permission: GroupPermissions;
  permission: Permissions;
}

export type IFindByOwnerIdResponse = Omit<IFindDetailedResponse, ''>;

export interface ICreateRequest {
  user_id: string;
  permission_id: number;
  group_id: number;
}

export interface IRepository {
  findByOwnerId(params: IShared.IFindByOwnerIdRequest): Promise<IFindByOwnerIdResponse[]>;
  create(data: ICreateRequest): Promise<IFindByOwnerIdResponse>;
  deleteOne(selector: IShared.IFindByIdNumberRequest): Promise<IFindByOwnerIdResponse | null>;
}

export type IController = IShared.IEntityWithUserToken<IRepository>;
