import joi from 'joi';
import { messages } from '../../../shared/messages';
import * as IShared from '../../../shared/interface';
import { ICreateRequest } from './interface';

// create body validation
const createBodyKeys: IShared.IRouteRequest<ICreateRequest> = {
  group_id: joi.number().positive().required(),
  permission_id: joi.number().positive().required(),
  user_id: joi.string().uuid().required(),
};
export const createBody = joi.object().keys(createBodyKeys).messages(messages);
