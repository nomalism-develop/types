import joi from 'joi';
import { messages } from '../../../shared/messages';
import * as IShared from '../../../shared/interface';
import {
  IFindRequest,
  IFindPaginatedRequest,
  ICreateRequest,
  IUpdateRequest,
  ICreateMainPersona,
} from './interface';
import { chatTypes } from '../../stock/chat/interfaces';

const createMainPersonaKeys: IShared.IRouteRequest<ICreateMainPersona> = {
  name: joi.string().optional(),
  nif: joi.string().allow(null).optional(),
  email: joi
    .string()
    .email({ tlds: { allow: false } })
    .allow(null)
    .optional(),
  telephone: joi.string().allow(null).optional(),
};

// create body validation
const createBodyKeys: IShared.IRouteRequest<ICreateRequest> = {
  user_id: joi.string().uuid().optional(),
  manufacturer: joi.boolean().default(false).optional(),
  inactive: joi.boolean().default(false).optional(),

  default_discount: joi.number().positive().allow(0).default(0).max(100).optional(),
  exemption_reason_id: joi.string().uuid().allow(null).optional(),

  main_persona: joi.object().keys(createMainPersonaKeys).required(),
  provider_type_id: joi.string().uuid().allow(null).optional(),
  country_id: joi.string().uuid().allow(null).optional(),
  language_id: joi.string().uuid().allow(null).optional(),
  support_language_id: joi.string().uuid().allow(null).optional(),
  default_payment_method_id: joi.string().uuid().allow(null).optional(),
  default_maturity_date_id: joi.string().uuid().allow(null).optional(),
  default_delivery_method_id: joi.string().uuid().allow(null).optional(),
  default_vehicle_id: joi.string().uuid().allow(null).optional(),
  personas: joi.array().items(joi.object().keys(createMainPersonaKeys)).optional(),
};
export const createBody = joi.object().keys(createBodyKeys).messages(messages);

// update body validation
const updateBodyKeys: IShared.IRouteRequest<IUpdateRequest> = {
  chat_type: joi
    .string()
    .valid(...chatTypes)
    .optional(),
  manufacturer: joi.boolean().optional(),
  inactive: joi.boolean().optional(),

  default_discount: joi.number().positive().allow(0).max(100).optional(),

  provider_type_id: joi.string().uuid().allow(null).optional(),
  country_id: joi.string().uuid().allow(null).optional(),
  language_id: joi.string().uuid().allow(null).optional(),
  support_language_id: joi.string().uuid().allow(null).optional(),
  main_persona_id: joi.string().uuid().optional(),
  contact_persona_id: joi.string().uuid().optional(),
  default_payment_method_id: joi.string().uuid().allow(null).optional(),
  default_maturity_date_id: joi.string().uuid().allow(null).optional(),
  default_delivery_method_id: joi.string().uuid().allow(null).optional(),
  default_vehicle_id: joi.string().uuid().allow(null).optional(),
};
export const updateBody = joi.object().keys(updateBodyKeys).messages(messages);

// find query validation
const findQueryKeys: IShared.IRouteRequest<IFindRequest> = {
  number: joi.array().items(joi.number().positive().allow(0).required()).optional(),
  id: joi.array().items(joi.string().uuid().required()).optional(),
};
export const findQuery = joi.object().keys(findQueryKeys).messages(messages);

// find with pagination query validation
const findWithPaginationQueryKeys: IShared.IRouteRequest<IFindPaginatedRequest> = {
  per_page: joi.number().integer().positive().default(10).optional(),
  current_page: joi.number().integer().positive().default(1).optional(),
};
export const findWithPaginationQuery = joi
  .object()
  .keys(findWithPaginationQueryKeys)
  .messages(messages);
