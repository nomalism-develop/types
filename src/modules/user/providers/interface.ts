import * as IShared from '../../../shared/interface';
import {
  Providers,
  Users,
  Country,
  ReasonForExemption,
  Language,
  ProviderType,
  PaymentMethods,
  MaturityDates,
  DeliveryMethods,
  Vehicles,
  Persona,
} from '../../../shared/entities';
import type { IChatType } from '../../stock/chat/interfaces';

export type Entity = Providers;
export const Route = 'provider';
export const UpperName = 'Providers';
export const LowerName = UpperName[0].toLowerCase() + UpperName.substring(1);

interface IMainPersona extends Persona {
  country: Country | null;
  reason_for_exemption: ReasonForExemption | null;
}

interface IFindDetailedResponse extends Entity {
  user: Users;
  main_persona: IMainPersona;
  country: Country | null;
  language: Language | null;
  support_language: Language | null;
  provider_type: ProviderType | null;
  default_payment_method: PaymentMethods | null;
  default_maturity_date: MaturityDates | null;
  default_delivery_method: DeliveryMethods | null;
  default_vehicle: Vehicles | null;
}

// omit any unnecessary fields
export type IFindByIdResponse = Omit<IFindDetailedResponse, ''>;

// omit any unnecessary fields
export type IFindByOwnerIdResponse = Omit<
  IFindDetailedResponse,
  | 'user'
  | 'main_persona'
  | 'country'
  | 'language'
  | 'support_language'
  | 'provider_type'
  | 'default_payment_method'
  | 'default_maturity_date'
  | 'default_delivery_method'
  | 'default_vehicle'
>;

export interface IFindByQueryRequest {
  number?: number[];
  id?: string[];
}

export interface IFindRequest {}

// omit any unnecessary fields
export type IFindResponse = Omit<IFindDetailedResponse, ''>;

export type IFindPaginatedRequest = IShared.IPaginationRequest;

export type IFindWithPaginationResponse = IShared.IPaginationResponse<
  Omit<IFindDetailedResponse, ''>
>;

export type ICreateMainPersona = Pick<Persona, 'nif' | 'name' | 'email' | 'telephone'>;

export interface ICreateRequest {
  user_id?: string;
  manufacturer?: boolean;
  inactive?: boolean;

  default_discount?: number;
  exemption_reason_id?: string | null;

  main_persona: ICreateMainPersona;
  provider_type_id?: string | null;
  country_id?: string | null;
  language_id?: string | null;
  support_language_id?: string | null;
  default_payment_method_id?: string | null;
  default_maturity_date_id?: string | null;
  default_delivery_method_id?: string | null;
  default_vehicle_id?: string | null;
  personas?: ICreateMainPersona[];
}

export interface IUpdateRequest {
  chat_type?: IChatType;
  manufacturer?: boolean;
  inactive?: boolean;
  default_discount?: number;
  main_persona_id?: string;
  contact_persona_id?: string;
  country_id?: string;
  language_id?: string;
  support_language_id?: string | null;
  provider_type_id?: string;
  default_payment_method_id?: string;
  default_maturity_date_id?: string;
  default_delivery_method_id?: string;
  default_vehicle_id?: string | null;
}

export interface IRepository {
  findById(selector: IShared.IFindByIdRequest): Promise<IFindByIdResponse | null>;

  findByOwnerId(params: IShared.IFindByOwnerIdRequest): Promise<IFindByOwnerIdResponse[]>;
  findMinified(params?: IShared.IFindMinifiedRequest): Promise<IShared.IFindMinifiedResponse[]>;
  findMinifiedManufacturer(
    params?: IShared.IFindMinifiedRequest,
  ): Promise<IShared.IFindMinifiedResponse[]>;
  findByQuery(params: IFindByQueryRequest): Promise<IFindResponse[]>;
  find(selector: IFindRequest): Promise<IFindResponse[]>;
  findPaginated(selector: IFindPaginatedRequest): Promise<IFindWithPaginationResponse>;

  create(data: ICreateRequest): Promise<IFindResponse>;
  update(selector: IShared.IFindByIdRequest, data: IUpdateRequest): Promise<IFindResponse | null>;
  deleteOne(selector: IShared.IFindByIdRequest): Promise<IFindResponse | null>;
}

export type IController = IShared.IEntityWithUserToken<IRepository>;
