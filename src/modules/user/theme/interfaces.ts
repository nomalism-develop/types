import * as IShared from '../../../shared/interface';
import { Theme } from '../../../shared/entities';

export type Entity = Theme;
export const Route = 'theme';
export const UpperName = 'Theme';
export const LowerName = UpperName[0].toLowerCase() + UpperName.substring(1);

export interface ISessionStorageColor {
  name: string;
  hex: string;
}
export interface ICreateOrUpdateRequest {
  colors: ISessionStorageColor[];
  store_operator_id: string;
}

export interface IRepository {
  createOrUpdate(data: ICreateOrUpdateRequest): Promise<void>;
}
export type IController = IShared.IEntityWithUserToken<IRepository>;
