import joi from 'joi';
import { messages } from '../../../shared/messages';
import * as IShared from '../../../shared/interface';
import { ICreateOrUpdateRequest } from './interfaces';

const createOrUpdateBodyKeys: IShared.IRouteRequest<ICreateOrUpdateRequest> = {
  colors: joi
    .array()
    .items(
      joi.object({
        name: joi.string().required(),
        hex: joi.string().required(),
      }),
    )
    .required(),
  store_operator_id: joi.string().uuid().required(),
};

export const createOrUpdateBody = joi.object().keys(createOrUpdateBodyKeys).messages(messages);
