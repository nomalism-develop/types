import * as IShared from '../../../shared/interface';

export const Route = 'password';

export interface IForgotPassword {
  account: string;
}

export interface IResetPassword {
  token: string;
  password: string;
  password_confirmation: string;
}

export interface IRepository {
  resetPassword(params: IResetPassword): Promise<void>;

  forgotPassword(params: IForgotPassword): Promise<void>;
}

export type IController = IShared.IEntityWithUserToken<IRepository>;
