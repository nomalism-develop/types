import joi from 'joi';
import { messages } from '../../../shared/messages';
import * as IShared from '../../../shared/interface';
import { IForgotPassword, IResetPassword } from './interfaces';
// create body validation
const forgotBodyKeys: IShared.IRouteRequest<IForgotPassword> = {
  account: joi
    .string()
    .email({ tlds: { allow: false } })
    .required(),
};

export const forgotBody = joi.object().keys(forgotBodyKeys).messages(messages);

// update body validation
const resetBodyKeys: IShared.IRouteRequest<IResetPassword> = {
  token: joi.string().uuid().required(),
  password: joi.string().required(),
  password_confirmation: joi.string().valid(joi.ref('password')),
};

export const resetBody = joi.object().keys(resetBodyKeys).messages(messages);
