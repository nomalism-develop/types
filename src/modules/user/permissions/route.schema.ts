import joi from 'joi';
import { messages } from '../../../shared/messages';
import * as IShared from '../../../shared/interface';
import { ICreateRequest } from './interface';

// create body validation
const createBodyKeys: IShared.IRouteRequest<ICreateRequest> = {
  code: joi.string().required(),
  description: joi.string().required(),
};
export const createBody = joi.object().keys(createBodyKeys).messages(messages);
