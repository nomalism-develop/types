import * as IShared from '../../../shared/interface';
import { Permissions } from '../../../shared/entities';

export type Entity = Permissions;
export const Route = 'permission';
export const UpperName = 'Permissions';
export const LowerName = UpperName[0].toLowerCase() + UpperName.substring(1);

export interface ICreateRequest {
  // create fields
  code: string;
  description: string;
}

export interface IUpdateRequest {
  // create fields
  code: string;
  description: string;
}

export interface IRepository {
  findById(selector: IShared.IFindByIdNumberRequest): Promise<Entity | null>;
  findByCode(code: string): Promise<Entity | null>;
  changeEnable(selector: IShared.IFindByIdNumberRequest): Promise<boolean>;

  find(): Promise<Entity[]>;
  findMinified(params?: IShared.IFindMinifiedRequest): Promise<IShared.IFindMinifiedResponse[]>;
  create(data: ICreateRequest): Promise<Entity>;
  deleteOne(selector: IShared.IFindByIdNumberRequest): Promise<Entity | null>;
}

export type IController = IShared.IEntityWithUserToken<IRepository>;

export type IApi = Omit<IRepository, 'findById' | 'findByCode' | 'changeEnable'>;
