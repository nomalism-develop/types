import * as IShared from '../../../shared/interface';
import {
  StoreOperator,
  Users,
  ReasonForExemption,
  UserPositions,
  Language,
  type Persona,
  type Country,
} from '../../../shared/entities';
import type { IChatType } from '../../stock/chat/interfaces';

export type Entity = StoreOperator;
export const Route = 'store_operator';
export const UpperName = 'StoreOperator';
export const LowerName = UpperName[0].toLowerCase() + UpperName.substring(1);

interface IMainPersona extends Persona {
  country: Country | null;
  reason_for_exemption: ReasonForExemption | null;
}

interface IFindDetailedResponse extends Entity {
  user: Users;
  support_language: Language | null;
  user_position: UserPositions | null;
  main_persona: IMainPersona;
}

// omit any unnecessary fields
export type IFindByIdResponse = Omit<IFindDetailedResponse, ''>;

// omit any unnecessary fields
export type IFindByOwnerIdResponse = Omit<
  IFindDetailedResponse,
  'user' | 'support_language' | 'user_position' | 'main_persona'
>;

export type IFindRequest = Record<string, unknown>;

// omit any unnecessary fields
export type IFindResponse = Omit<IFindDetailedResponse, ''>;

export interface IFindPaginatedRequest extends IFindRequest, IShared.IPaginationRequest {}

export type IFindWithPaginationResponse = IShared.IPaginationResponse<
  Omit<IFindDetailedResponse, ''>
>;

export type ICreateMainPersona = Pick<Persona, 'nif' | 'name' | 'email' | 'telephone'>;

export interface ICreateRequest {
  // create fields
  user_id: string;
  user_position_id?: string | null;
  location_id: string;
  internal_email?: string | null;
  support_language_id?: string | null;
  main_persona: ICreateMainPersona;
}

export interface IUpdateRequest {
  // updatable fields
  chat_type?: IChatType;
  inactive?: boolean;
  pin?: string;
  old_pin?: string;
  user_position_id?: string;
  location_id?: string;
  internal_email?: string | null;
  support_language_id?: string | null;
  main_persona_id?: string;
  contact_persona_id?: string;
}

export interface ICheckPinRequest {
  conference_pin: string;
}

export interface IFindUnique {
  user_id: string;
}

export interface IStoreOperatorUpdateRequest {
  name: string | null;
  account: string | null;
  email: string | null;
  internal_email: string | null;
  inactive: boolean;
}
export interface IRepository {
  findById(selector: IShared.IFindByIdRequest): Promise<IFindByIdResponse | null>;
  checkPin(data: ICheckPinRequest): Promise<IFindResponse | null>;
  findUnique(data: IFindUnique): Promise<IFindResponse | null>;

  findByOwnerId(params: IShared.IFindByOwnerIdRequest): Promise<IFindByOwnerIdResponse[]>;
  findMinified(params?: IShared.IFindMinifiedRequest): Promise<IShared.IFindMinifiedResponse[]>;
  find(selector: IFindRequest): Promise<IFindResponse[]>;
  findPaginated(selector: IFindPaginatedRequest): Promise<IFindWithPaginationResponse>;

  create(data: ICreateRequest): Promise<IFindResponse>;
  update(selector: IShared.IFindByIdRequest, data: IUpdateRequest): Promise<IFindResponse | null>;
  deleteOne(selector: IShared.IFindByIdRequest): Promise<IFindResponse | null>;

  updateStoreOperator(
    selector: IShared.IFindByIdRequest,
    data: IStoreOperatorUpdateRequest,
  ): Promise<void>;
}

export type IController = IShared.IEntityWithUserToken<IRepository>;

export type IApi = Omit<IRepository, 'findUnique'>;
