import joi from 'joi';
import { messages } from '../../../shared/messages';
import * as IShared from '../../../shared/interface';
import {
  IFindRequest,
  IFindPaginatedRequest,
  ICreateRequest,
  IUpdateRequest,
  ICheckPinRequest,
  type ICreateMainPersona,
  IStoreOperatorUpdateRequest,
} from './interface';
import { chatTypes } from '../../stock/chat/interfaces';

const createMainPersonaKeys: IShared.IRouteRequest<ICreateMainPersona> = {
  name: joi.string().optional(),
  nif: joi.string().allow(null).optional(),
  email: joi
    .string()
    .email({ tlds: { allow: false } })
    .allow(null)
    .optional(),
  telephone: joi.string().allow(null).optional(),
};

// create body validation
const createBodyKeys: IShared.IRouteRequest<ICreateRequest> = {
  user_id: joi.string().uuid().required(),
  user_position_id: joi.string().uuid().allow(null).optional(),
  location_id: joi.string().uuid().required(),
  internal_email: joi
    .string()
    .email({ tlds: { allow: false } })
    .optional(),
  support_language_id: joi.string().uuid().allow(null).optional(),
  main_persona: joi.object().keys(createMainPersonaKeys).required(),
};
export const createBody = joi.object().keys(createBodyKeys).messages(messages);

// update body validation
const updateBodyKeys: IShared.IRouteRequest<IUpdateRequest> = {
  chat_type: joi
    .string()
    .valid(...chatTypes)
    .optional(),
  old_pin: joi.string().optional(),
  pin: joi
    .when('old_pin', {
      is: joi.exist(),
      then: joi.required(),
    })
    .optional(),
  inactive: joi.boolean().optional(),
  internal_email: joi
    .string()
    .email({ tlds: { allow: false } })
    .optional(),
  location_id: joi.string().uuid().optional(),
  user_position_id: joi.string().uuid().allow(null).optional(),
  support_language_id: joi.string().uuid().allow(null).optional(),
  main_persona_id: joi.string().uuid().optional(),
  contact_persona_id: joi.string().uuid().optional(),
};
export const updateBody = joi.object().keys(updateBodyKeys).messages(messages);

const checkPinBodyKeys: IShared.IRouteRequest<Omit<ICheckPinRequest, 'logged_user_id'>> = {
  conference_pin: joi.string().required(),
};

export const checkPinBody = joi.object().keys(checkPinBodyKeys).messages(messages);

const findQueryKeys: IShared.IRouteRequest<IFindRequest> = {};
export const findQuery = joi.object().keys(findQueryKeys).messages(messages);

// find with pagination query validation
const findWithPaginationQueryKeys: IShared.IRouteRequest<IFindPaginatedRequest> = {
  per_page: joi.number().integer().positive().default(10).optional(),
  current_page: joi.number().integer().positive().default(1).optional(),
};
export const findWithPaginationQuery = joi
  .object()
  .keys(findWithPaginationQueryKeys)
  .messages(messages);

// update store operator query validation
const updateStoreOperatorQueryKeys: IShared.IRouteRequest<IStoreOperatorUpdateRequest> = {
  name: joi.string().allow(null, '').optional(),
  account: joi.string().allow(null, '').optional(),
  email: joi
    .string()
    .email({ tlds: { allow: false } })
    .allow(null, '')
    .optional(),
  internal_email: joi
    .string()
    .email({ tlds: { allow: false } })
    .allow(null, '')
    .optional(),
  inactive: joi.boolean().optional(),
};
export const updateStoreOperatorQuery = joi
  .object()
  .keys(updateStoreOperatorQueryKeys)
  .messages(messages);
