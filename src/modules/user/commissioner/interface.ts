import * as IShared from '../../../shared/interface';
import {
  type Commissioner,
  type Users,
  type Country,
  type Language,
  type PaymentMethods,
  type DeliveryMethods,
  type MaturityDates,
  type Vehicles,
  type Persona,
  type ReasonForExemption,
} from '../../../shared/entities';
import type { IChatType } from '../../stock/chat/interfaces';

export type Entity = Commissioner;
export const Route = 'commissioner';
export const UpperName = 'Commissioner';
export const LowerName = UpperName[0].toLowerCase() + UpperName.substring(1);

interface IMainPersona extends Persona {
  country: Country | null;
  reason_for_exemption: ReasonForExemption | null;
}

interface IFindDetailedResponse extends Entity {
  user: Users;
  main_persona: IMainPersona;
  country: Country | null;
  language: Language | null;
  default_payment_method: PaymentMethods | null;
  default_maturity_date: MaturityDates | null;
  default_delivery_method: DeliveryMethods | null;
  default_vehicle: Vehicles | null;
}

// omit any unnecessary fields
export type IFindByIdResponse = Omit<IFindDetailedResponse, ''>;

// omit any unnecessary fields
export type IFindByOwnerIdResponse = Omit<
  IFindDetailedResponse,
  | 'user'
  | 'main_persona'
  | 'country'
  | 'language'
  | 'default_payment_method'
  | 'default_maturity_date'
  | 'default_delivery_method'
  | 'default_vehicle'
>;

export type IFindRequest = Record<string, unknown>;

// omit any unnecessary fields
export type IFindResponse = Omit<IFindDetailedResponse, ''>;

export interface IFindPaginatedRequest extends IFindRequest, IShared.IPaginationRequest {}

export type IFindWithPaginationResponse = IShared.IPaginationResponse<
  Omit<IFindDetailedResponse, ''>
>;

export type ICreateMainPersona = Pick<Persona, 'nif' | 'name' | 'email' | 'telephone'>;

export interface ICreateRequest {
  // create fields
  user_id: string;

  default_percentage?: number;
  // exemption_reason_id?: string | null; // todo

  main_persona: ICreateMainPersona;
  country_id?: string | null;
  language_id?: string | null;
  default_payment_method_id?: string | null;
  default_maturity_date_id?: string | null;
  default_delivery_method_id?: string | null;
  default_vehicle_id?: string | null;
}

export interface IUpdateRequest {
  // updatable fields
  chat_type?: IChatType;
  default_percentage?: number;
  // exemption_reason_id?: string | null; // todo
  main_persona_id?: string;
  contact_persona_id?: string;
  country_id?: string | null;
  language_id?: string | null;
  default_payment_method_id?: string | null;
  default_maturity_date_id?: string | null;
  default_delivery_method_id?: string | null;
  default_vehicle_id?: string | null;
}

export interface IRepository {
  findById(selector: IShared.IFindByIdRequest): Promise<IFindByIdResponse | null>;

  findByOwnerId(params: IShared.IFindByOwnerIdRequest): Promise<IFindByOwnerIdResponse[]>;
  findMinified(params?: IShared.IFindMinifiedRequest): Promise<IShared.IFindMinifiedResponse[]>;
  find(selector: IFindRequest): Promise<IFindResponse[]>;
  findPaginated(selector: IFindPaginatedRequest): Promise<IFindWithPaginationResponse>;

  create(data: ICreateRequest): Promise<IFindResponse>;
  update(selector: IShared.IFindByIdRequest, data: IUpdateRequest): Promise<IFindResponse | null>;
  deleteOne(selector: IShared.IFindByIdRequest): Promise<IFindResponse | null>;
}

export type IController = IShared.IEntityWithUserToken<IRepository>;
