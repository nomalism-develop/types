import * as IShared from '../../../shared/interface';
import { Swift } from '../../../shared/entities';

export type Entity = Swift;
export const Route = 'swift';
export const UpperName = 'Swift';
export const LowerName = UpperName[0].toLowerCase() + UpperName.substring(1);

type IFindDetailedResponse = Entity;

// omit any unnecessary fields
export type IFindByIdResponse = Omit<IFindDetailedResponse, ''>;

export type IFindRequest = Record<string, unknown>;

// omit any unnecessary fields
export type IFindResponse = Omit<IFindDetailedResponse, ''>;

export interface IFindPaginatedRequest extends IFindRequest, IShared.IPaginationRequest {}

export type IFindWithPaginationResponse = IShared.IPaginationResponse<
  Omit<IFindDetailedResponse, ''>
>;

export interface ICreateRequest {
  // create fields
  bank: string;
  city: string;
  branch?: string;
  code: string;
  country_id: string;
}

export interface IUpdateRequest {
  // updatable fields
  bank?: string;
  city?: string;
  branch?: string | null;
  code?: string;
  country_id?: string;
}

export interface IRepository {
  findById(selector: IShared.IFindByIdRequest): Promise<IFindByIdResponse | null>;

  findMinified(params?: IShared.IFindMinifiedRequest): Promise<IShared.IFindMinifiedResponse[]>;
  find(selector: IFindRequest): Promise<IFindResponse[]>;
  findPaginated(selector: IFindPaginatedRequest): Promise<IFindWithPaginationResponse>;

  create(data: ICreateRequest): Promise<Entity>;
  update(selector: IShared.IFindByIdRequest, data: IUpdateRequest): Promise<Entity | null>;
  deleteOne(selector: IShared.IFindByIdRequest): Promise<Entity | null>;
}

export type IController = IShared.IEntityWithUserToken<IRepository>;
