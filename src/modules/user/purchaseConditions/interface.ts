import * as IShared from '../../../shared/interface';
import { PurchaseConditions } from '../../../shared/entities';

export type Entity = PurchaseConditions;
export const Route = 'purchase_conditions';
export const UpperName = 'PurchaseConditions';
export const LowerName = UpperName[0].toLowerCase() + UpperName.substring(1);

interface IFindDetailedResponse extends Entity {}

// omit any unnecessary fields
export type IFindByIdResponse = Omit<IFindDetailedResponse, 'product_category'>;

// omit any unnecessary fields
export type IFindByOwnerIdResponse = Omit<IFindDetailedResponse, ''>;

export type IFindRequest = Record<string, unknown>;

// omit any unnecessary fields
export type IFindResponse = Omit<IFindDetailedResponse, ''>;

export interface IFindPaginatedRequest extends IFindRequest, IShared.IPaginationRequest {}

export type IFindWithPaginationResponse = IShared.IPaginationResponse<
  Omit<IFindDetailedResponse, ''>
>;

export interface ICreateRequest {
  // create fields
  provider_id: string;
  discount: string;
  deadline: string;
  minimum_quantity: string;
}

export interface IUpdateRequest {
  // updatable fields
  discount?: string;
  deadline?: string;
  minimum_quantity?: string;
}

export interface IRepository {
  findById(selector: IShared.IFindByIdRequest): Promise<IFindByIdResponse | null>;

  findByOwnerId(params: IShared.IFindByOwnerIdRequest): Promise<IFindByOwnerIdResponse[]>;
  find(selector: IFindRequest): Promise<IFindResponse[]>;
  findPaginated(selector: IFindPaginatedRequest): Promise<IFindWithPaginationResponse>;

  create(data: ICreateRequest): Promise<Entity>;
  update(
    selector: IShared.IFindByIdRequest,
    data: IUpdateRequest,
  ): Promise<IFindByOwnerIdResponse | null>;
  deleteOne(selector: IShared.IFindByIdRequest): Promise<IFindByOwnerIdResponse | null>;
}

export type IController = IShared.IEntityWithUserToken<IRepository>;
