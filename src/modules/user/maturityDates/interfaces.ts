import * as IShared from '../../../shared/interface';
import { MaturityDates } from '../../../shared/entities';

export type Entity = MaturityDates;
export const Route = 'maturity_dates';
export const UpperName = 'MaturityDates';
export const LowerName = UpperName[0].toLowerCase() + UpperName.substring(1);

export interface ICreateRequest {
  maturity_date_id: number;
  name: string;
  days: number;
}

export interface IUpdateRequest {
  maturity_date_id?: number;
  name?: string;
  days?: number;
}

export interface IRepository {
  findById(selector: IShared.IFindByIdRequest): Promise<Entity | null>;

  find(): Promise<Entity[]>;
  findMinified(params?: IShared.IFindMinifiedRequest): Promise<IShared.IFindMinifiedResponse[]>;
  create(data: ICreateRequest): Promise<Entity>;
  update(selector: IShared.IFindByIdRequest, data: IUpdateRequest): Promise<Entity | null>;
  deleteOne(selector: IShared.IFindByIdRequest): Promise<Entity | null>;
}

export type IController = IShared.IEntityWithUserToken<IRepository>;
