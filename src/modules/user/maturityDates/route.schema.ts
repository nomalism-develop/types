import joi from 'joi';
import { messages } from '../../../shared/messages';
import * as IShared from '../../../shared/interface';
import { ICreateRequest, IUpdateRequest } from './interfaces';

const createBodyKeys: IShared.IRouteRequest<ICreateRequest> = {
  name: joi.string().required(),
  maturity_date_id: joi.number().integer().required(),
  days: joi.number().integer().required(),
};

export const createBody = joi.object().keys(createBodyKeys).messages(messages);
const updateBodyKeys: IShared.IRouteRequest<IUpdateRequest> = {
  name: joi.string(),
  maturity_date_id: joi.number().integer(),
  days: joi.number().integer(),
};
export const updateBody = joi.object().keys(updateBodyKeys).messages(messages);
