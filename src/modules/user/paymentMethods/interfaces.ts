import * as IShared from '../../../shared/interface';
import { PaymentMethods } from '../../../shared/entities';

export type Entity = PaymentMethods;
export const Route = 'payment_methods';
export const UpperName = 'PaymentMethods';
export const LowerName = UpperName[0].toLowerCase() + UpperName.substring(1);

export interface ICreateRequest {
  payment_method_id?: number | null;
  name: string;
  is_numerary: boolean;
  is_mb: boolean;
  is_credit: boolean;
  hidden: boolean | null;
}

export interface IUpdateRequest {
  payment_method_id?: number | null;
  name?: string;
  is_numerary?: boolean;
  is_mb?: boolean;
  is_credit?: boolean;
  hidden?: boolean | null;
}

export interface IRepository {
  findById(selector: IShared.IFindByIdRequest): Promise<Entity | null>;

  find(): Promise<Entity[]>;
  findMinified(params?: IShared.IFindMinifiedRequest): Promise<IShared.IFindMinifiedResponse[]>;
  create(data: ICreateRequest): Promise<Entity>;
  update(selector: IShared.IFindByIdRequest, data: IUpdateRequest): Promise<Entity | null>;
  deleteOne(selector: IShared.IFindByIdRequest): Promise<Entity | null>;
}

export type IController = IShared.IEntityWithUserToken<IRepository>;
