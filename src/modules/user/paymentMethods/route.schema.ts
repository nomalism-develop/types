import joi from 'joi';
import { messages } from '../../../shared/messages';
import * as IShared from '../../../shared/interface';
import { ICreateRequest, IUpdateRequest } from './interfaces';

const createBodyKeys: IShared.IRouteRequest<ICreateRequest> = {
  name: joi.string().required(),
  payment_method_id: joi.number().integer().allow(null).optional(),
  is_numerary: joi.boolean().default(false).optional(),
  is_mb: joi.boolean().default(false).optional(),
  is_credit: joi.boolean().default(false).optional(),
  hidden: joi.boolean().default(false).allow(null).optional(),
};
export const createBody = joi.object().keys(createBodyKeys).messages(messages);
const updateBodyKeys: IShared.IRouteRequest<IUpdateRequest> = {
  name: joi.string().optional(),
  payment_method_id: joi.number().integer().allow(null).optional(),
  is_numerary: joi.boolean().optional(),
  is_mb: joi.boolean().optional(),
  is_credit: joi.boolean().optional(),
  hidden: joi.boolean().allow(null).optional(),
};
export const updateBody = joi.object().keys(updateBodyKeys).messages(messages);
