import * as Users from '../users/interface';

export interface ICreateRequest {
  // create fields
  account: string;
  password: string;
}

export interface ICreateResponse {
  access_token: string;
  user: Users.IFindByAccountResponse;
  refresh_token: string;
  roles: string[];
  expiresInToken: number;
  expiresInRefreshToken: number;
}

export interface IRepository {
  create(data: ICreateRequest): Promise<ICreateResponse>;
}

export interface IController {
  create(data: ICreateRequest, ip: string): Promise<ICreateResponse>;
}
