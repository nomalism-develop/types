import joi from 'joi';
import { messages } from '../../../shared/messages';
import * as IShared from '../../../shared/interface';
import { ICreateRequest } from './interface';

const sessionBodyKeys: IShared.IRouteRequest<ICreateRequest> = {
  account: joi.string().required(),
  password: joi.string().required(),
};
export const sessionBody = joi.object().keys(sessionBodyKeys).messages(messages);
