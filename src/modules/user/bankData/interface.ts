import * as IShared from '../../../shared/interface';
import { BankData, Country, Swift } from '../../../shared/entities';

export type Entity = BankData;
export const Route = 'bank_data';
export const UpperName = 'BankData';
export const LowerName = UpperName[0].toLowerCase() + UpperName.substring(1);

interface IFindDetailedResponse extends Entity {
  swift: Swift | null;
  country: Country;
}

// omit any unnecessary fields
export type IFindByIdResponse = Omit<IFindDetailedResponse, 'swift' | 'country'>;

// omit any unnecessary fields
export type IFindByOwnerIdResponse = Omit<IFindDetailedResponse, ''>;

export type IFindRequest = Record<string, unknown>;

// omit any unnecessary fields
export type IFindResponse = Omit<IFindDetailedResponse, 'swift' | 'country'>;

export interface IFindPaginatedRequest extends IFindRequest, IShared.IPaginationRequest {}

export type IFindWithPaginationResponse = IShared.IPaginationResponse<
  Omit<IFindDetailedResponse, 'swift' | 'country'>
>;

export interface ICreateRequest {
  // create fields
  provider_id?: string | null;
  client_id?: string | null;
  store_operator_id?: string | null;
  commissioner_id?: string | null;
  iban: string;
  swift_id: string | null;
  country_id: string;
}

export interface IUpdateRequest {
  // updatable fields
  iban?: string;
  swift_id?: string | null;
  country_id?: string;
}

export interface IRepository {
  findById(selector: IShared.IFindByIdRequest): Promise<IFindByIdResponse | null>;

  findByOwnerId(params: IShared.IFindByOwnerIdRequest): Promise<IFindByOwnerIdResponse[]>;
  find(selector: IFindRequest): Promise<IFindResponse[]>;
  findPaginated(selector: IFindPaginatedRequest): Promise<IFindWithPaginationResponse>;

  create(data: ICreateRequest): Promise<IFindByOwnerIdResponse>;
  update(
    selector: IShared.IFindByIdRequest,
    data: IUpdateRequest,
  ): Promise<IFindByOwnerIdResponse | null>;
  deleteOne(selector: IShared.IFindByIdRequest): Promise<IFindByOwnerIdResponse | null>;
}

export type IController = IShared.IEntityWithUserToken<IRepository>;
