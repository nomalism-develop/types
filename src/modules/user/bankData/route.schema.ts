import joi from 'joi';
import { messages } from '../../../shared/messages';
import * as IShared from '../../../shared/interface';
import { IFindRequest, IFindPaginatedRequest, ICreateRequest, IUpdateRequest } from './interface';

// create body validation
const createBodyKeys: IShared.IRouteRequest<ICreateRequest> = {
  provider_id: joi.string(),
  client_id: joi.string(),
  store_operator_id: joi.string(),
  commissioner_id: joi.string(),
  iban: joi.string().required(),
  swift_id: joi.string().allow(null).optional(),
  country_id: joi.string().required(),
};
export const createBody = joi
  .object()
  .keys(createBodyKeys)
  .xor('provider_id', 'client_id', 'store_operator_id', 'commissioner_id')
  .messages(messages);

// update body validation
const updateBodyKeys: IShared.IRouteRequest<IUpdateRequest> = {
  iban: joi.string().optional(),
  swift_id: joi.string().allow(null).optional(),
  country_id: joi.string().optional(),
};
export const updateBody = joi.object().keys(updateBodyKeys).messages(messages);
const findQueryKeys: IShared.IRouteRequest<IFindRequest> = {};
export const findQuery = joi.object().keys(findQueryKeys).messages(messages);

// find with pagination query validation
const findWithPaginationQueryKeys: IShared.IRouteRequest<IFindPaginatedRequest> = {
  per_page: joi.number().integer().positive().default(10).optional(),
  current_page: joi.number().integer().positive().default(1).optional(),
};
export const findWithPaginationQuery = joi
  .object()
  .keys(findWithPaginationQueryKeys)
  .messages(messages);
