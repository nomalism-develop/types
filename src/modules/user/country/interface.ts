import * as IShared from '../../../shared/interface';
import { Country } from '../../../shared/entities';

export type Entity = Country;
export const Route = 'country';
export const UpperName = 'Country';
export const LowerName = UpperName[0].toLowerCase() + UpperName.substring(1);

type IFindDetailedResponse = Entity;

// omit any unnecessary fields
export type IFindByIdResponse = Omit<IFindDetailedResponse, ''>;

export type IFindRequest = {
  name?: string;
};

// omit any unnecessary fields
export type IFindResponse = Omit<IFindDetailedResponse, ''>;

export interface IFindPaginatedRequest extends IFindRequest, IShared.IPaginationRequest {}

export type IFindWithPaginationResponse = IShared.IPaginationResponse<
  Omit<IFindDetailedResponse, ''>
>;

export interface ICreateRequest {
  // create fields
  name: string;
  alpha2: string;
  external_id: string;
}

export interface IUpdateRequest {
  // updatable fields
  name?: string;
  alpha2?: string;
  external_id?: string;
}

export interface IRepository {
  findById(selector: IShared.IFindByIdRequest): Promise<IFindByIdResponse | null>;

  findMinified(params?: IShared.IFindMinifiedRequest): Promise<IShared.IFindMinifiedResponse[]>;
  findByAlpha2(params?: IShared.IFindMinifiedRequest): Promise<IShared.IFindMinifiedResponse[]>;
  find(selector: IFindRequest): Promise<IFindResponse[]>;
  findPaginated(selector: IFindPaginatedRequest): Promise<IFindWithPaginationResponse>;

  create(data: ICreateRequest): Promise<Entity>;
  update(selector: IShared.IFindByIdRequest, data: IUpdateRequest): Promise<Entity | null>;
  deleteOne(selector: IShared.IFindByIdRequest): Promise<Entity | null>;
}

export type IController = IShared.IEntityWithUserToken<IRepository>;
