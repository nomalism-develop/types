import joi from 'joi';
import { messages } from '../../../shared/messages';
import * as IShared from '../../../shared/interface';
import {
  IFindRequest,
  IFindPaginatedRequest,
  ICreateRequest,
  IUpdateRequest,
  Entity,
  IFindResponse,
  IFindByIdResponse,
  IFindWithPaginationResponse,
} from './interface';

// create body validation
const createBodyKeys: IShared.IRouteRequest<ICreateRequest> = {
  name: joi.string().required(),
  alpha2: joi.string().required(),
  external_id: joi.string().required(),
};
export const createBody = joi.object().keys(createBodyKeys).messages(messages);

// update body validation
const updateBodyKeys: IShared.IRouteRequest<IUpdateRequest> = {
  name: joi.string().optional(),
  alpha2: joi.string().optional(),
  external_id: joi.string().optional(),
};
export const updateBody = joi.object().keys(updateBodyKeys).messages(messages);
const findQueryKeys: IShared.IRouteRequest<IFindRequest> = {
  name: joi.string().optional(),
};
export const findQuery = joi.object().keys(findQueryKeys).messages(messages);

// find with pagination query validation
const findWithPaginationQueryKeys: IShared.IRouteRequest<IFindPaginatedRequest> = {
  ...findQueryKeys,
  per_page: joi.number().integer().positive().default(10).optional(),
  current_page: joi.number().integer().positive().default(1).optional(),
};

export const findWithPaginationQuery = joi
  .object()
  .keys(findWithPaginationQueryKeys)
  .messages(messages);

// Create Body Validate Response
const createUpdateDeleteBodyResponseKeys: IShared.IRouteResponseWithoutStamps<Entity> = {
  id: joi.string().uuid().required(),
  name: joi.string().required(),
  alpha2: joi.string().required(),
  external_id: joi.string(),
  origin_id: joi.string().allow(null).optional(),
};
export const createUpdateDeleteBodyResponse = joi.object().keys({
  ...createUpdateDeleteBodyResponseKeys,
  ...IShared.joiValidateStamps,
});

const findBodyResponseKeys: IShared.IRouteResponseWithoutStamps<IFindResponse> = {
  id: joi.string().uuid().required(),
  name: joi.string().required(),
  alpha2: joi.string().required(),
  external_id: joi.string(),
  origin_id: joi.string().allow(null).optional(),
};

export const findBodyResponse = joi
  .array()
  .items({ ...findBodyResponseKeys, ...IShared.joiValidateStamps })
  .required();

export const findByIdBodyResponseKeys: IShared.IRouteResponseWithoutStamps<IFindByIdResponse> = {
  id: joi.string().uuid().required(),
  name: joi.string().required(),
  alpha2: joi.string().required(),
  external_id: joi.string(),
  origin_id: joi.string().allow(null).optional(),
};

export const findByIdBodyResponse = joi
  .object()
  .keys({ ...findByIdBodyResponseKeys, ...IShared.joiValidateStamps })
  .allow(null);

const findPaginatedBodyResponseKeys: IShared.IRouteResponsePagination<IFindWithPaginationResponse> =
  {
    per_page: joi.number().required(),
    current_page: joi.number().required(),
    total: joi.number().required(),
    data: joi.array().items(findBodyResponseKeys).required(),
  };

export const findPaginationBodyResponse = joi
  .object()
  .keys(findPaginatedBodyResponseKeys)
  .messages(messages);
