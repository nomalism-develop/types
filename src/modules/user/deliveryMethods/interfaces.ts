import * as IShared from '../../../shared/interface';
import { DeliveryMethods } from '../../../shared/entities';

export type Entity = DeliveryMethods;
export const Route = 'delivery_methods';
export const UpperName = 'DeliveryMethods';
export const LowerName = UpperName[0].toLowerCase() + UpperName.substring(1);

export interface ICreateRequest {
  delivery_method_id: number;
  name: string;
}

export interface IUpdateRequest {
  delivery_method_id?: number;
  name?: string;
}

export interface IRepository {
  create(data: ICreateRequest): Promise<Entity>;
  find(): Promise<Entity[]>;
  findMinified(params?: IShared.IFindMinifiedRequest): Promise<IShared.IFindMinifiedResponse[]>;
  findById(selector: IShared.IFindByIdRequest): Promise<Entity | null>;
  update(selector: IShared.IFindByIdRequest, body: IUpdateRequest): Promise<Entity | null>;
  deleteOne(selector: IShared.IFindByIdRequest): Promise<Entity | null>;
}

export type IController = IShared.IEntityWithUserToken<IRepository>;
