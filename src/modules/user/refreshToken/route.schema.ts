import joi from 'joi';
import { messages } from '../../../shared/messages';
import * as IShared from '../../../shared/interface';
import { ICreateRequest } from './interface';

const refreshTokenBodyKeys: IShared.IRouteRequest<ICreateRequest> = {
  token: joi.string(),
  ip: joi.string(),
};
export const refreshTokenBody = joi.object().keys(refreshTokenBodyKeys).messages(messages);
