import * as IShared from '../../../shared/interface';

export interface ICreateRequest {
  token: string;
  ip?: string;
}

export interface ICreateResponse {
  access_token: string;
  refresh_token: string;
  expiresInToken: number;
  expiresInRefreshToken: number;
}

export interface IRepository {
  create(data: ICreateRequest): Promise<ICreateResponse>;
}

export type IController = IShared.IEntityWithUserToken<IRepository>;
