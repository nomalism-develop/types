import * as IShared from '../../../shared/interface';
import { Vehicles } from '../../../shared/entities';

export type Entity = Vehicles;
export const Route = 'vehicles';
export const UpperName = 'Vehicles';
export const LowerName = UpperName[0].toLowerCase() + UpperName.substring(1);

export interface ICreateRequest {
  description: string;
  vehicle_id: number;
  number_plate: string;
}

export interface IUpdateRequest {
  description?: string;
  vehicle_id?: number;
  number_plate?: string;
}

export interface IRepository {
  find(): Promise<Entity[]>;
  findById(id: string): Promise<Entity | null>;
  create(data: ICreateRequest): Promise<Entity>;
  update(selector: IShared.IFindByIdRequest, data: IUpdateRequest): Promise<Entity | null>;
  deleteOne(selector: IShared.IFindByIdRequest): Promise<Entity | null>;
}

export type IController = IShared.IEntityWithUserToken<IRepository>;

export type IApi = Omit<IRepository, 'findById'>;
