import joi from 'joi';
import { messages } from '../../../shared/messages';
import * as IShared from '../../../shared/interface';
import { ICreateRequest, IUpdateRequest } from './interfaces';

const createBodyKeys: IShared.IRouteRequest<ICreateRequest> = {
  description: joi.string().required(),
  vehicle_id: joi.number().integer().required(),
  number_plate: joi.string().required(),
};
export const createBody = joi.object().keys(createBodyKeys).messages(messages);
const updateBodyKeys: IShared.IRouteRequest<IUpdateRequest> = {
  description: joi.string(),
  vehicle_id: joi.number().integer(),
  number_plate: joi.string(),
};
export const updateBody = joi.object().keys(updateBodyKeys).messages(messages);
