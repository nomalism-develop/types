import joi from 'joi';
import { messages } from '../../../shared/messages';
import * as IShared from '../../../shared/interface';
import {
  IFindRequest,
  IFindPaginatedRequest,
  ICreateRequest,
  IUpdateRequest,
  IFindProvidersWithClientIdRequest,
  IFindFromClientOrProviderRequest,
} from './interface';

// create body validation
const createBodyKeys: IShared.IRouteRequest<ICreateRequest> = {
  account: joi.string().allow(null).required(),
  password: joi.string().required(),
};
export const createBody = joi.object().keys(createBodyKeys).messages(messages);

// update body validation
const updateBodyKeys: IShared.IRouteRequest<IUpdateRequest> = {
  account: joi.string().optional(),
  old_password: joi.string().optional(),
  password: joi.string().optional(),
};
export const updateBody = joi.object().keys(updateBodyKeys).messages(messages);
const findQueryKeys: IShared.IRouteRequest<IFindRequest> = {};
export const findQuery = joi.object().keys(findQueryKeys).messages(messages);

const findProvidersWithClientIdQueryKeys: IShared.IRouteRequest<IFindProvidersWithClientIdRequest> =
  {
    provider_ids: joi.array().items(joi.string().uuid().required()).optional(),
  };

export const findProvidersWithClientIdQuery = joi
  .object()
  .keys(findProvidersWithClientIdQueryKeys)
  .messages(messages);

const findFromClientOrProviderIdBodyKeys: IShared.IRouteRequest<IFindFromClientOrProviderRequest> =
  {
    ids: joi.array().items(joi.string().uuid().required()).required(),
  };

export const findFromClientOrProviderIdBody = joi
  .object()
  .keys(findFromClientOrProviderIdBodyKeys)
  .messages(messages);

// find with pagination query validation
const findWithPaginationQueryKeys: IShared.IRouteRequest<IFindPaginatedRequest> = {
  per_page: joi.number().integer().positive().default(10).optional(),
  current_page: joi.number().integer().positive().default(1).optional(),
};
export const findWithPaginationQuery = joi
  .object()
  .keys(findWithPaginationQueryKeys)
  .messages(messages);
