import * as IShared from '../../../shared/interface';
import {
  Providers,
  Country,
  Language,
  ProviderType,
  ReasonForExemption,
  Persona,
  Clients,
  SegmentsArea,
  ClientType,
  StoreOperator,
  UserPositions,
  Commissioner,
  Users,
  PaymentMethods,
  MaturityDates,
  DeliveryMethods,
  Vehicles,
  Theme,
} from '../../../shared/entities';

export type Entity = Users;
export type EntityWithoutPassword = Omit<Users, 'password'>;
export const Route = 'users';
export const UpperName = 'Users';
export const LowerName = UpperName[0].toLowerCase() + UpperName.substring(1);

interface IMainPersona extends Persona {
  country: Country | null;
  reason_for_exemption: ReasonForExemption | null;
}

interface IStoreOperator extends Pick<StoreOperator, 'id' | 'location_id'> {
  main_persona: Pick<Persona, 'name' | 'email'>;
  theme: Pick<Theme, 'colors'>;
}

export interface IFindByAccountResponse extends Pick<Entity, 'id' | 'account'> {
  store_operator: IStoreOperator;
}

// omit any unnecessary fields
export interface IFindByIdResponse extends EntityWithoutPassword {
  provider:
    | (Providers & {
        country: Country | null;
        language: Language | null;
        support_language: Language | null;
        provider_type: ProviderType | null;
        default_payment_method: PaymentMethods | null;
        default_maturity_date: MaturityDates | null;
        default_delivery_method: DeliveryMethods | null;
        default_vehicle: Vehicles | null;
        main_persona: IMainPersona;
      })
    | null;
  client:
    | (Clients & {
        country: Country | null;
        language: Language | null;
        segments_area: SegmentsArea | null;
        client_type: ClientType | null;
        default_payment_method: PaymentMethods | null;
        default_maturity_date: MaturityDates | null;
        default_delivery_method: DeliveryMethods | null;
        default_vehicle: Vehicles | null;
        main_persona: IMainPersona;
      })
    | null;
  store_operator:
    | (StoreOperator & {
        user_position: UserPositions | null;
        support_language: Language | null;
        main_persona: IMainPersona;
      })
    | null;
  commissioner:
    | (Commissioner & {
        country: Country | null;
        language: Language | null;
        default_payment_method: PaymentMethods | null;
        default_maturity_date: MaturityDates | null;
        default_delivery_method: DeliveryMethods | null;
        default_vehicle: Vehicles | null;
        main_persona: IMainPersona;
      })
    | null;
}

export type IFindRequest = Record<string, unknown>;

// omit any unnecessary fields
export type IFindResponse = EntityWithoutPassword;

export interface IFindPaginatedRequest extends IFindRequest, IShared.IPaginationRequest {}

export type IFindWithPaginationResponse = IShared.IPaginationResponse<EntityWithoutPassword>;

export interface IFindProvidersWithClientIdResponse {
  name: string;
  provider_id: string;
  client_id: string | null;
}

export interface IFindProvidersWithClientIdRequest {
  provider_ids: string[];
}

export interface ICreateRequest {
  // create fields
  account: string | null;
  password: string;
}

export interface IUpdateRequest {
  // updatable fields
  account?: string | null;
  old_password?: string;
  password?: string;
}

export interface IUpdatePassword {
  password: string;
}

export interface ISendForgotPassword {
  token: string;
  user: {
    name: string;
    email: string;
  };
}

export interface IFindFromClientOrProviderRequest {
  ids: string[];
}

export interface IFindFromClientOrProviderResponse {
  name: string;
  provider: string | null;
  provider_id: string | null;
  client_id: string | null;
  iban: string | null;
}

export interface IRepository {
  findById(selector: IShared.IFindByIdRequest): Promise<IFindByIdResponse | null>;

  findMinified(params?: IShared.IFindMinifiedRequest): Promise<IShared.IFindMinifiedResponse[]>;

  find(selector: IFindRequest): Promise<IFindResponse[]>;

  findPaginated(selector: IFindPaginatedRequest): Promise<IFindWithPaginationResponse>;

  resetPassword(id: string, data: IUpdatePassword): Promise<Users>;

  removeAccess(user_id: string): Promise<boolean>;

  create(data: ICreateRequest): Promise<IFindResponse>;

  update(selector: IShared.IFindByIdRequest, data: IUpdateRequest): Promise<IFindResponse | null>;

  deleteOne(selector: IShared.IFindByIdRequest): Promise<IFindResponse | null>;

  findProvidersWithClientId(
    data: IFindProvidersWithClientIdRequest,
  ): Promise<IFindProvidersWithClientIdResponse[]>;

  findFromClientOrProviderId(
    data: IFindFromClientOrProviderRequest,
  ): Promise<IFindFromClientOrProviderResponse[]>;
}

export type IController = IShared.IEntityWithUserToken<IRepository>;

export type IApi = Omit<IRepository, 'resetPassword' | 'removeAccess'>;
