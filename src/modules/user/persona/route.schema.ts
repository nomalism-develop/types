import joi from 'joi';
import { messages } from '../../../shared/messages';
import * as IShared from '../../../shared/interface';
import {
  IBasicSearchRequest,
  ICreateRequest,
  IDeletePersonaRequest,
  IFindByEmailRequest,
  IFindRequest,
  IUpdateRequest,
} from './interface';

// create body validation
const createBodyKeys: IShared.IRouteRequest<ICreateRequest> = {
  name: joi.string().allow(null, '').optional(),
  nif: joi.string().allow(null, '').optional(),
  street: joi.string().allow(null, '').optional(),
  locality: joi.string().allow(null, '').optional(),
  postal_code: joi.string().trim(true).lowercase().allow(null, '').optional(),
  country_id: joi.string().uuid().allow(null, '').optional(),
  telephone: joi.string().allow(null, '').optional(),
  email: joi
    .string()
    .email({ tlds: { allow: false } })
    .allow(null, '')
    .optional(),
  observations: joi.string().allow(null, '').optional(),
  provider_id: joi.string().uuid().allow(null, ''),
  client_id: joi.string().uuid().allow(null, ''),
  commissioner_id: joi.string().uuid().allow(null, ''),
  store_operator_id: joi.string().uuid().allow(null, ''),
  reason_for_exemption_id: joi.string().uuid().allow(null, '').optional(),
  main: joi.boolean().default(false).optional(),
  contact: joi.boolean().default(false).optional(),
};

export const createBody = joi
  .object()
  .keys(createBodyKeys)
  .xor('provider_id', 'client_id', 'commissioner_id')
  .messages(messages);

// update body validation
const updateBodyKeys: IShared.IRouteRequest<IUpdateRequest> = {
  name: joi.string().allow(null, '').optional(),
  nif: joi.string().allow(null, '').optional(),
  street: joi.string().allow(null, '').optional(),
  locality: joi.string().allow(null, '').optional(),
  postal_code: joi.string().trim(true).lowercase().allow(null, '').optional(),
  country_id: joi.string().uuid().allow(null, '').optional(),
  telephone: joi.string().allow(null, '').optional(),
  email: joi
    .string()
    .email({ tlds: { allow: false } })
    .allow(null, '')
    .optional(),
  observations: joi.string().allow(null, '').optional(),
  reason_for_exemption_id: joi.string().uuid().allow(null, '').optional(),
  main: joi.boolean().optional(),
  contact: joi.boolean().optional(),
};
export const updateBody = joi.object().keys(updateBodyKeys).messages(messages);

const findQueryKeys: IShared.IRouteRequest<IFindRequest> = {
  search_value: joi.string().allow('', null).optional(),
};
export const findQuery = joi.object().keys(findQueryKeys).messages(messages);

const getBasicSearchQueryKeys: IShared.IRouteRequest<IBasicSearchRequest> = {
  per_page: joi.number().integer().positive().default(10).optional(),
  current_page: joi.number().integer().positive().default(1).optional(),
  inactive: joi.boolean().default(false).optional(),
  search_value: joi.string().allow('', null).optional(),
  types: joi.string().allow('', null).optional(),
};
export const getBasicSearchParamsValidate = joi
  .object()
  .keys(getBasicSearchQueryKeys)
  .messages(messages);

const deleteQueryKeys: IShared.IRouteRequest<IDeletePersonaRequest> = {
  id: joi.string().uuid().required(),
  new_main_persona_id: joi.string().uuid().optional(),
};
export const deleteQueryValidate = joi.object().keys(deleteQueryKeys).messages(messages);

const findByEmailQueryKeys: IShared.IRouteRequest<IFindByEmailRequest> = {
  email: joi
    .string()
    .trim(true)
    .lowercase()
    .email({ tlds: { allow: false } }),
};
export const FindByEmailQueryValidate = joi.object().keys(findByEmailQueryKeys).messages(messages);
