import type * as IShared from '../../../shared/interface';
import type { Persona, Country, ReasonForExemption } from '../../../shared/entities';
import type { IChatType } from '../../stock/chat/interfaces';

export type Entity = Persona;
export const Route = 'persona';
export const UpperName = 'Persona';
export const LowerName = UpperName[0].toLowerCase() + UpperName.substring(1);

export interface IEntityExtended extends Entity {
  formatted_address: string;
  country: Country | null;
  reason_for_exemption: ReasonForExemption | null;
}

export interface IFindByOwnerIdResponse extends IEntityExtended {
  main: boolean;
  contact: boolean;
}

export interface ICreateRequest {
  name: string | null;
  nif: string | null;
  street: string | null;
  postal_code: string | null;
  locality: string | null;
  country_id: string | null;
  telephone: string | null;
  email: string | null;
  observations: string | null;
  provider_id: string | null;
  client_id: string | null;
  commissioner_id: string | null;
  store_operator_id: string | null;
  reason_for_exemption_id: string | null;
  main?: boolean;
  contact?: boolean;
}

export interface IUpdateRequest {
  name?: string | null;
  nif?: string | null;
  street?: string | null;
  postal_code?: string | null;
  locality?: string | null;
  country_id?: string | null;
  telephone?: string | null;
  email?: string | null;
  observations?: string | null;
  reason_for_exemption_id?: string | null;
  main?: boolean;
  contact?: boolean;
}

export interface IBasicSearchRequest extends IShared.IPaginationRequest {
  search_value: string | null;
  inactive?: boolean;
  types?: string;
}

export interface IBasicSearchResponse {
  id: string;
  name: string;
  number: number;
  updated_at: Date;
}

export interface IFindRequest {
  search_value: string;
}
export interface IFindResponse {
  persona_id: string;
  persona_name: string;
  persona_nif: string | null;
  persona_street: string;
  persona_postal_code: string;
  persona_locality: string;
  persona_country_id: string;
}

export interface IDeletePersonaRequest {
  id: string;
  new_main_persona_id?: string;
}

export interface IFindContactPersonaByOwnerId
  extends Pick<Persona, 'id' | 'name' | 'email' | 'telephone'> {
  chat_type: IChatType;
}

export interface IFindByEmailRequest {
  email: string;
}

export interface IFindByEmailResponse {
  persona_id: string;
  persona_name: string;
  persona_nif: string | null;
  client?: {
    id: string;
    name: string;
    chat_type: IChatType;
  } | null;
  provider?: {
    id: string;
    name: string;
    chat_type: IChatType;
  } | null;
  commissioner?: {
    id: string;
    name: string;
    chat_type: IChatType;
  } | null;
}

export interface IRepository {
  findById(selector: IShared.IFindByIdRequest): Promise<IEntityExtended | null>;

  findByOwnerId(params: IShared.IFindByOwnerIdRequest): Promise<IFindByOwnerIdResponse[]>;

  findContactPersonaByOwnerId(
    params: IShared.IFindByOwnerIdRequest,
  ): Promise<IFindContactPersonaByOwnerId>;

  create(data: ICreateRequest): Promise<IEntityExtended>;
  update(selector: IShared.IFindByIdRequest, data: IUpdateRequest): Promise<IEntityExtended | null>;
  deleteOne(params: IDeletePersonaRequest): Promise<IEntityExtended | null>;

  findByBasic(
    data: IBasicSearchRequest,
  ): Promise<IShared.IPaginationResponse<IBasicSearchResponse>>;

  find(data: IFindRequest): Promise<IFindResponse[]>;

  findByEmail(data: IFindByEmailRequest): Promise<IFindByEmailResponse[]>;
}

export type IController = IShared.IEntityWithUserToken<IRepository>;
