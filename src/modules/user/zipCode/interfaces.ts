import { ZipCode } from '../../../shared/entities';

export type Entity = ZipCode;
export const Route = 'zip_code';
export const UpperName = 'ZipCode';
export const LowerName = UpperName[0].toLowerCase() + UpperName.substring(1);

export interface ICreateRequest {
  postal_code: string;
  address: string;
  locality: string;
  parish: string;
  county: string;
  county_code: number;
  district: string;
  latitude: string;
  longitude: string;
}

export interface IFindByPostalCode {
  postal_code: string;
}

export interface IRepository {
  findByPostalCode(params: IFindByPostalCode): Promise<Entity[]>;
  create(data: ICreateRequest[]): Promise<void>;
}
