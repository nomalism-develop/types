import joi from 'joi';
import { messages } from '../../../shared/messages';
import * as IShared from '../../../shared/interface';

import { IFindByPostalCode, ICreateRequest } from './interfaces';

const createBodyKeys: IShared.IRouteRequest<ICreateRequest> = {
  address: joi.string().required(),
  county: joi.string().required(),
  county_code: joi.number().required(),
  postal_code: joi.string().trim(true).lowercase().required(),
  locality: joi.string().required(),
  parish: joi.string().required(),
  district: joi.string().required(),
  latitude: joi.string().required(),
  longitude: joi.string().required(),
};

export const createBody = joi.array().items(joi.object().keys(createBodyKeys)).messages(messages);

// findByPostalCode body validation
const findByPostalCodeBodyKeys: IShared.IRouteRequest<IFindByPostalCode> = {
  postal_code: joi.string().trim(true).lowercase().required(),
};

export const findByPostalCodeBody = joi.object().keys(findByPostalCodeBodyKeys).messages(messages);
