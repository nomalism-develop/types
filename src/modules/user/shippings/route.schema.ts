import joi from 'joi';
import { messages } from '../../../shared/messages';
import * as IShared from '../../../shared/interface';
import { IFindRequest, IFindPaginatedRequest, ICreateRequest, IUpdateRequest } from './interface';

// create body validation
const createBodyKeys: IShared.IRouteRequest<ICreateRequest> = {
  provider_id: joi.string().uuid().required(),
  quantity: joi.string().required(),
  type: joi.string().required().valid('Até', 'Entre', 'A Partir de'),
  value: joi.string().required(),
  unit: joi.string().required(),
};
export const createBody = joi.object().keys(createBodyKeys).messages(messages);

// update body validation
const updateBodyKeys: IShared.IRouteRequest<IUpdateRequest> = {
  quantity: joi.string().optional(),
  type: joi.string().valid('Até', 'Entre', 'A Partir de').optional(),
  value: joi.string().optional(),
  unit: joi.string().optional(),
};
export const updateBody = joi.object().keys(updateBodyKeys).messages(messages);
const findQueryKeys: IShared.IRouteRequest<IFindRequest> = {};
export const findQuery = joi.object().keys(findQueryKeys).messages(messages);

// find with pagination query validation
const findWithPaginationQueryKeys: IShared.IRouteRequest<IFindPaginatedRequest> = {
  per_page: joi.number().integer().positive().default(10).optional(),
  current_page: joi.number().integer().positive().default(1).optional(),
};
export const findWithPaginationQuery = joi
  .object()
  .keys(findWithPaginationQueryKeys)
  .messages(messages);
