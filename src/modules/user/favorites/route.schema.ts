import joi from 'joi';
import { messages } from '../../../shared/messages';
import * as IShared from '../../../shared/interface';
import { IFindOneRequest, ICreateRequest, IDeleteRequest } from './interfaces';

export const findByUrlQueryKeys: IShared.IRouteRequest<IFindOneRequest> = {
  url: joi.string().required(),
};
export const findByUrlQuery = joi.object().keys(findByUrlQueryKeys).messages(messages);

export const createBodyKeys: IShared.IRouteRequest<ICreateRequest> = {
  url: joi.string().required(),
};
export const createBody = joi.object().keys(createBodyKeys).messages(messages);

const deleteByUrlQueryKeys: IShared.IRouteRequest<IDeleteRequest> = {
  url: joi.string().required(),
};
export const deleteByUrlQuery = joi.object().keys(deleteByUrlQueryKeys).messages(messages);
