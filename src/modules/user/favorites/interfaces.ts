import * as IShared from '../../../shared/interface';
import { Favorites } from '../../../shared/entities';

export type Entity = Favorites;
export const Route = 'favorites';
export const UpperName = 'Favorites';
export const LowerName = UpperName[0].toLowerCase() + UpperName.substring(1);

export interface ICreateRequest {
  url: string;
}

export interface IDeleteRequest {
  url: string;
}

export interface IFindOneRequest {
  url: string;
}

export interface IRepository {
  list(user_id: string): Promise<Entity[]>;
  findByUrl(params: IFindOneRequest): Promise<boolean>;
  create(data: ICreateRequest): Promise<Entity>;
  deleteOne(params: IDeleteRequest): Promise<Entity | null>;
}

export type IController = IShared.IEntityWithUserToken<IRepository>;
