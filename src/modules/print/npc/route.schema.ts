import joi from 'joi';
import { messages } from '../../../shared/messages';
import * as IShared from '../../../shared/interface';
import { IUpdateRequest } from './interfaces';

// update body validation
const updateBodyKeys: IShared.IRouteRequest<IUpdateRequest> = {
  name: joi.string().optional().allow(null),
  secret: joi.string().optional().allow(null),
};
export const updateBody = joi.object().keys(updateBodyKeys).messages(messages);
