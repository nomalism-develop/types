import * as IShared from '../../../shared/interface';
import { Npc } from '../../../shared/entities';

export type Entity = Npc;
export const Route = 'npc';
export const UpperName = 'Npc';
export const LowerName = UpperName[0].toLowerCase() + UpperName.substring(1);

export interface IUpdateRequest {
  name?: string;
  secret?: string;
}

export interface IRepository {
  find: () => Promise<Entity[]>;
  update: (params: IShared.IFindByIdRequest, data: IUpdateRequest) => Promise<Entity | null>;
}

export type IController = IShared.IEntityWithUserToken<IRepository>;
