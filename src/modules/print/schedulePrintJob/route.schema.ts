import joi from 'joi';
import { messages } from '../../../shared/messages';
import * as IShared from '../../../shared/interface';
import {
  ICreateManySchedulePrintJobRequest,
  ICreateSchedulePrintJobRequest,
  IDispatchSchedulePrintJobRequest,
} from './interfaces';

const dispatchSchedulePrintJobBodyKeys: IShared.IRouteRequest<IDispatchSchedulePrintJobRequest> = {
  last_print_job_id: joi.string().uuid().optional(),
  mac_address: joi.string(),
  printers_a4: joi.array().items(joi.string()),
  printers_pos: joi.array().items(joi.string()),
};

export const dispatchSchedulePrintJobBody = joi
  .object()
  .keys(dispatchSchedulePrintJobBodyKeys)
  .messages(messages);

const createSchedulePrintJobBodyKeys: IShared.IRouteRequest<ICreateSchedulePrintJobRequest> = {
  external_document_id: joi.string().optional(),
  printer_id: joi.string().uuid().required(),
  external_url: joi.string().optional(),
  copies: joi.number().integer().positive().required(),
};

export const createSchedulePrintJobBody = joi
  .object()
  .keys(createSchedulePrintJobBodyKeys)
  .xor('external_document_id', 'multimedia_id', 'external_url')
  .messages(messages);

export const createManySchedulePrintJobBody = joi
  .object<ICreateManySchedulePrintJobRequest>()
  .keys({
    jobs: joi.array().items(createSchedulePrintJobBody).required(),
  })
  .messages(messages);
