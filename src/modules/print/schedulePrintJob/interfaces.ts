import * as IShared from '../../../shared/interface';
import { ScheduledPrintJob } from '../../../shared/entities';

export type Entity = ScheduledPrintJob;
export const Route = 'schedule_print_job';
export const UpperName = 'SchedulePrintJob';
export const LowerName = UpperName[0].toLowerCase() + UpperName.substring(1);

export interface IDispatchSchedulePrintJobRequest {
  last_print_job_id?: string | undefined;
  mac_address: string;
  printers_a4: string[];
  printers_pos: string[];
}

export interface ICreateSchedulePrintJobRequest {
  external_document_id?: string | undefined;
  external_url?: string | undefined;
  printer_id: string;
  copies: number;
}

export interface ICreateManySchedulePrintJobRequest {
  jobs: ICreateSchedulePrintJobRequest[];
}

export interface IDispatchSchedulePrintJobResponse {
  npc_id: string;
  secret?: string | null;
  print_job?: {
    id: string;
    moloni_json?: unknown;
    external_url?: string | null;
    printer_id: string;
    orientation: 'landscape' | 'portrait';
  };
}

export interface IRepository {
  dispatchSchedulePrintJob: (
    data: IDispatchSchedulePrintJobRequest,
  ) => Promise<IDispatchSchedulePrintJobResponse>;
  findByOwner: (data: IShared.IFindByOwnerIdRequest) => Promise<Entity[]>;
  create: (data: ICreateSchedulePrintJobRequest) => Promise<void>;
  createMany: (data: ICreateManySchedulePrintJobRequest) => Promise<void>;
  deleteOne: (params: IShared.IFindByIdRequest) => Promise<void>;
}

export type IController = IShared.IEntityWithUserToken<IRepository>;

export type IApi = IRepository;
