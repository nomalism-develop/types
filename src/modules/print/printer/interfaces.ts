import * as IShared from '../../../shared/interface';
import { Printer } from '../../../shared/entities';

export type Entity = Printer;
export const Route = 'printer';
export const UpperName = 'Printer';
export const LowerName = UpperName[0].toLowerCase() + UpperName.substring(1);

export type IFindDetailedResponse = Entity & {
  npc: Pick<Entity, 'name'>;
};

export interface IUpdateRequest {
  name: string;
  enable: boolean;
  paper: boolean;
  receipt: boolean;
  sticker: boolean;
  normalWidth: number;
  condensedWidth: number;
  dotWidth: number;
  hasDrawer: number;
  hasCutter: number;
  lowDensity: number;
  imagePrintMode: number;
}

export interface IRepository {
  find: () => Promise<IFindDetailedResponse[]>;
  findByOwnerId: (selector: IShared.IFindByOwnerIdRequest) => Promise<Entity[]>;
  update: (params: IShared.IFindByIdRequest, data: IUpdateRequest) => Promise<Entity | null>;
}

export type IController = IShared.IEntityWithUserToken<IRepository>;
