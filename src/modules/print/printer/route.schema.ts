import joi from 'joi';
import { messages } from '../../../shared/messages';
import * as IShared from '../../../shared/interface';
import { IUpdateRequest } from './interfaces';

// update body validation
const updateBodyKeys: IShared.IRouteRequest<IUpdateRequest> = {
  name: joi.string().optional(),
  enable: joi.boolean().optional(),
  paper: joi.boolean().optional(),
  receipt: joi.boolean().optional(),
  sticker: joi.boolean().optional(),
  normalWidth: joi.number().integer().positive().optional(),
  condensedWidth: joi.number().integer().positive().optional(),
  dotWidth: joi.number().integer().positive().optional(),
  hasDrawer: joi.number().integer().valid(0, 1).optional(),
  hasCutter: joi.number().integer().valid(0, 1).optional(),
  lowDensity: joi.number().integer().valid(0, 1).optional(),
  imagePrintMode: joi.number().integer().valid(0, 1).optional(),
};

export const updateBody = joi.object().keys(updateBodyKeys).messages(messages);
