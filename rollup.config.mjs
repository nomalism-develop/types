import typescript from '@rollup/plugin-typescript';
import terser from '@rollup/plugin-terser';

export default {
  input: 'src/index.ts', 
  output: [
    {
      name: '@nomalism-com/types',
      file: 'dist/index.min.js',
      format: 'umd',
      plugins: [terser()],
      globals: {
        joi: 'joi',
      },
    },
  ],
  plugins: [
    typescript(),
  ],
  external: ['joi'],
};
